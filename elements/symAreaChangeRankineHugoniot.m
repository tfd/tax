classdef symAreaChangeRankineHugoniot < AcBlock & sss
    % symRankineHugoniot class describing a temperature jump across a flat
    % flame considering heat release fluctuations. It can deal with known
    % upstream or downstream boundaries in steady state.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = rankineHugoniot(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.nRef: number of flame reference inputs/ heat release
    %          contributions
    %          * pars.Tratio:   temperature ratio (T_d/T_u)
    %          * pars.kratio:   ratio of isentropic exponants (kappa_d/kappa_u)
    %          * pars.Hu:       lower heating value of the fuel
    %          * pars.Version:  level of simplification in the model
    %          * pars.a_CaHb:   number of C atoms per fuel molecule
    %          * pars.b_CaHb:   number of H atoms per fuel molecule
    %          * pars.Aratio:   ratio of cross sectional areas (A_d/A_u)
    % Output:  * sys: rankineHugoniot object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  24 November 2021
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties (SetAccess = protected) % long range, since all can affect the connections
        % ratios of temperature T2/T1, isentropic exponent kap2/kap1, lower
        % heating value, type of equation, coefficients of the chemical
        % formula of the fuel a and b in C_a H_b, area ratio, name of fuel
        Tratio, kratio, Hu, Version, a_CaHb, b_CaHb, Aratio, fuel
    end
    properties (Dependent = true) % cannot be changed actively, but only by
        % changing the influencing parameters:
        % ratio of molar masses, fuel mass fraction alpha, equivalence
        % ratio, molar mass of fuel, mean flow properties (index 1: inlet,
        % index 2: outlet, also for Mach<0
        Mratio, alpha, phi, M_F, meanflow
    end
    properties (Constant) % cannot be changed whatsoever:
        % universal gas constant
        Rmol = 8.3144598*1e3; % J/(kmol K);
        % number of moles of air per mole of oxygen
        n_A = 1 + 79/21; % -
        % molar mass of air
        M_A = (31.998 + 79/21*28.014)/(1 + 79/21); % consistent with fluid data tables
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
        H2, CH4, C2H4, C2H6, C3H8, C4H10, isoC4H10, CO2, H2O, H2Og, O2, N2 % fluid data tables
    end
    
    methods
        function sys = symAreaChangeRankineHugoniot(pars)
            % Determine number of flames (no sys.property, since change by
            % changeParam is not intended)
            if isfield(pars,'nRef') % flame
                if (iscell(pars.nRef))
                    nFlames = eval(cell2mat(pars.nRef));
                else
                    nFlames = pars.nRef;
                end
            else % tempJump
                nFlames = 0;
            end
            FlamePorts = cell(nFlames,1);
            
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,FlamePorts{:},AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)+nFlames));
             
            % load fluid data (immutable, required for sys.pars2sys, therefore here)
            fluidFiles = {'H2', 'CH4', 'C2H4', 'C2H6', 'C3H8', 'C4H10', 'isoC4H10', 'CO2', 'H2O', 'H2Og', 'O2', 'N2'};
            elementPath = mfilename('fullpath');
            elementPath = elementPath(1:find(elementPath=='/',1,'last'));
            for i = 1:numel(fluidFiles)
                fluid = load([elementPath,'Gases/',fluidFiles{i},'.mat']);
                sys.(fluidFiles{i}) = fluid.(fluidFiles{i});
            end
           
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            %% Df
            conac = con([1,end]);
            confl = con(2:end-1);

            %% Solve for mean
            [conac,sys] = solveMean(sys,conac); % including sys, because it might get updated: sys.kratio, sys.Hu, sys.Aratio
            con = {conac{1},confl{1:end},conac{2}}; 
            
            checkFlame = true;
            for i = 1: numel(confl)
                if not(isfield(sys.Connection{1+i},'FlameName'))
                    checkFlame = false;
                end
            end
            
            sys.Connection = con;
            if Block.checkPort(conac,AcBlock.Port)&&(checkFlame)
                sys = update(sys);
            end
        end

        %% Set functions
        function sys = set.Tratio(sys, Tratio)
            if not(isequal(sys.Tratio, Tratio))
                sys.Tratio = Tratio;
                sys.uptodate = false;
            end
        end
        function sys = set.Aratio(sys, Aratio)
            if not(isequal(sys.Aratio, Aratio))
                sys.Aratio = Aratio;
                sys.uptodate = false;
            end
        end
        function sys = set.a_CaHb(sys, a_CaHb)
            if not(isequal(sys.a_CaHb, a_CaHb))
                sys.a_CaHb = a_CaHb;
                sys.uptodate = false;
            end
        end
        function sys = set.b_CaHb(sys, b_CaHb)
            if not(isequal(sys.b_CaHb, b_CaHb))
                sys.b_CaHb = b_CaHb;
                sys.uptodate = false;
            end
        end
        function sys = set.kratio(sys, kratio)
            if not(isequal(sys.kratio, kratio))
                sys.kratio = kratio;
                sys.uptodate = false;
            end
        end
        function sys = set.Hu(sys, Hu)
            if not(isequal(sys.Hu, Hu))
                sys.Hu = Hu;
                sys.uptodate = false;
            end
        end
        function sys = set.Version(sys, Version)
            if not(isequal(sys.Version, Version))
                sys.Version = Version;
                if strcmp(sys.Version,'constant cp, 1st order in Mach, fixed heat source')
                    warning('wID:Mach_order','Because there are terms of the order Ma^-1, this version''s precision is approximately of order Mach^0.9. The problematic terms are the coefficients for f and g inputs in the entropy equation, i.e. this issue becomes small with moving flames.');warning('off','wID:Mach_order');
                end
                sys.uptodate = false;
            end
        end

        %% Get functions
        function Mratio = get.Mratio(sys)
            if isempty(sys.alpha)
                Mratio = [];
            else
                M_1 = (sys.M_F + 1/sys.phi*(sys.a_CaHb + sys.b_CaHb/4)*sys.M_A*sys.n_A)/(1 + 1/sys.phi*(sys.a_CaHb + sys.b_CaHb/4)*sys.n_A);
                M_2 = (sys.a_CaHb*44.009 + sys.b_CaHb/2*18.015 - (sys.a_CaHb + sys.b_CaHb/4)*31.998 + 1/sys.phi*(sys.a_CaHb + sys.b_CaHb/4)*sys.M_A*sys.n_A) ...
                    /(sys.a_CaHb + sys.b_CaHb/2 - (sys.a_CaHb + sys.b_CaHb/4) + 1/sys.phi*(sys.a_CaHb + sys.b_CaHb/4)*sys.n_A);
                con = sys.Connection;
                if sys.Connection{1}.dir*sys.Connection{1}.Mach>=0
                    if Block.checkField(con{1},'Mmol') && not(Block.isequalAbs(con{1}.Mmol,M_1))
                        warning('wID:M1_vs_BC1',['Inlet molar mass is inconsistent with fuel mass fraction and fuel composition. Difference is ',num2str(con{1}.Mmol-M_1),' g/mol. Using value from BC.']);warning('off','wID:M1_vs_BC1');
                        M_1 = con{1}.Mmol;
                    end
                    if Block.checkField(con{2},'Mmol') && not(Block.isequalAbs(con{2}.Mmol,M_2))
                        warning('wID:M2_vs_BC2',['Outlet molar mass is inconsistent with fuel mass fraction and fuel composition. Difference is ',num2str(con{2}.Mmol-M_2),' g/mol. Using value from BC.']);warning('off','wID:M2_vs_BC2');
                        M_2 = con{2}.Mmol;
                    end
                    Mratio = M_2/M_1;
                else
                    if Block.checkField(con{2},'Mmol') && not(Block.isequalAbs(con{2}.Mmol,M_1))
                        warning('wID:M1_vs_BC2',['Inlet molar mass is inconsistent with fuel mass fraction and fuel composition. Difference is ',num2str(con{2}.Mmol-M_1),' g/mol. Using value from BC.']);warning('off','wID:M1_vs_BC2');
                        M_1 = con{2}.Mmol;
                    end
                    if Block.checkField(con{1},'Mmol') && not(Block.isequalAbs(con{1}.Mmol,M_2))
                        warning('wID:M2_vs_BC1',['Outlet molar mass is inconsistent with fuel mass fraction and fuel composition. Difference is ',num2str(con{1}.Mmol-M_2),' g/mol. Using value from BC.']);warning('off','wID:M2_vs_BC1');
                        M_2 = con{1}.Mmol;
                    end
                    Mratio = M_1/M_2;
                end
                if strncmp(sys.Version,'constant cp',11) && ~isequal(Mratio, 1)
                    warning('wID:Reset_MR_to_1',['Resetting sys.Mratio from ', num2str(Mratio),' to 1, since other values would be inconsistent with assumption of constant cp.']);warning('off','wID:Reset_MR_to_1');
                    Mratio = 1;
                end
            end
        end
        function phi = get.phi(sys)
            alpha_stoichiometric = sys.M_F/(sys.M_F + (sys.a_CaHb + sys.b_CaHb/4)*sys.M_A*sys.n_A);
            phi = sys.alpha/alpha_stoichiometric;
        end
        function M_F = get.M_F(sys)
            M_F = 12.011*sys.a_CaHb + 1.008*sys.b_CaHb;
        end
        function alpha = get.alpha(sys)
            if sys.Connection{1}.dir*sys.Connection{1}.Mach>=0
                alpha = sys.Connection{1}.alpha; % inlet
                c = sys.Connection{1}.c; % inlet
                k = sys.Connection{1}.kappa; % inlet
                R = sys.Rmol/sys.Connection{1}.Mmol; % upstream!
                TR = sys.Tratio;
            else
                alpha = sys.Connection{end}.alpha; % inlet
                c = sys.Connection{end}.c; % inlet
                k = sys.Connection{end}.kappa; % inlet
                R = sys.Rmol/sys.Connection{end}.Mmol; % upstream!
                TR = 1/sys.Tratio;
            end
            if ~any(isempty([alpha,c,k,R,TR]))
                % provide minimal guess for alpha, in case there is none given
                T = c^2/(k*R);
                cp_est = R*k/(k-1);
                alpha_min = T*(TR-1)/sys.Hu*cp_est;
                if alpha < alpha_min && ~strncmp(sys.Version,'constant cp',11) % for constant cp this is obsolete
                    warning('wID:estimate_alpha',['Fuel mass fraction contradicts energy balance. Using guess based on linear interpolation of cp instead: alpha = ',num2str(alpha_min),' instead of ',num2str(alpha)]);warning('off','wID:estimate_alpha')
                    alpha = alpha_min;
                end
            end
        end
        function mf = get.meanflow(sys)
            % this only works after the boundaries are fully set up...
            % distinction between regular flow and inverted flow (Mach<0)
            if sys.Connection{1}.dir*sys.Connection{1}.Mach>=0
                mf.Ma_1 = sys.Connection{1}.dir*sys.Connection{1}.Mach; % inlet, will be used in outsourced code
                mf.c_1 = sys.Connection{1}.c; % inlet
                mf.k_1 = sys.Connection{1}.kappa; % inlet
                mf.rho_1 = sys.Connection{1}.rho; % inlet
                mf.A_1 = sys.Connection{1}.A; % inlet
                mf.R_1 = sys.Rmol/sys.Connection{1}.Mmol; % inlet
                mf.Ma_2 = sys.Connection{end}.dir*sys.Connection{end}.Mach; % outlet
                mf.c_2 = sys.Connection{end}.c; % outlet
                mf.k_2 = sys.Connection{end}.kappa; % outlet
                mf.rho_2 = sys.Connection{end}.rho; % outlet
                mf.A_2 = sys.Connection{end}.A; % outlet
                mf.R_2 = sys.Rmol/sys.Connection{end}.Mmol; % outlet
            else
                mf.Ma_1 = - sys.Connection{end}.dir*sys.Connection{end}.Mach; % inlet (minus, because by definition M>=0 at inlet), will be used in outsourced code
                mf.c_1 = sys.Connection{end}.c; % inlet
                mf.k_1 = sys.Connection{end}.kappa; % inlet
                mf.rho_1 = sys.Connection{end}.rho; % inlet
                mf.A_1 = sys.Connection{end}.A; % inlet
                mf.R_1 = sys.Rmol/sys.Connection{end}.Mmol; % inlet
                mf.Ma_2 = - sys.Connection{1}.dir*sys.Connection{1}.Mach; % outlet
                mf.c_2 = sys.Connection{1}.c; % outlet
                mf.k_2 = sys.Connection{1}.kappa; % outlet
                mf.rho_2 = sys.Connection{1}.rho; % outlet
                mf.A_2 = sys.Connection{1}.A; % outlet
                mf.R_2 = sys.Rmol/sys.Connection{1}.Mmol; % outlet
            end

%             % time constants and dynamic properties
%             mf.t_1 = sys.V1/(mf.A_1*mf.c_1*mf.Ma_1); % s, time constant for unburnt volume
%             mf.t_2 = sys.V2/(mf.A_2*mf.c_2*mf.Ma_2); % s, time constant for burnt volume
%             mf.C_s = ; % -, relation between burnt and unburnt non-dimentional entropy fluctuation
%             mf.C_M = ; % -, relation between burnt and unburnt non-dimentional molar mass fluctuation

            % some more abbreviations, chemistry and data
            a = sys.a_CaHb;
            b = sys.b_CaHb;
            mf.C_1 = 1/(1+21*sys.phi/(25*(4*a+b))) - 1/(1 + 21/25 * sys.phi/(4*a+b) * sys.M_F/sys.M_A); % chemistry for educts of CaHb
            mf.C_2 = 1/(1+21*sys.phi/100*b/(4*a+b)) - 1/(1 + 21/25 * sys.phi/(4*a+b) * sys.M_F/sys.M_A); % chemistry for products of CaHb
            mf.T_1 = mf.c_1^2/(mf.k_1*mf.R_1);
            mf.T_2 = mf.c_2^2/(mf.k_2*mf.R_2);
            mf.p_1 = mf.rho_1*mf.c_1^2/mf.k_1;
            mf.p_2 = mf.rho_2*mf.c_2^2/mf.k_2;
            if strncmp(sys.Version,'constant cp',11) % must be adiabatic lacking further knowledge
                mf.h_1 = mf.k_1/(mf.k_1-1)*mf.R_1*mf.T_1;
                mf.h_2 = mf.k_1/(mf.k_1-1)*mf.R_1*mf.T_2;
                mf.q = mf.h_2 - mf.h_1 + .5.*(mf.c_2*mf.Ma_2)^2 - .5.*(mf.c_1*mf.Ma_1)^2;
                mf.q_net = mf.q;
            else
                mf.q = sys.alpha*sys.Hu;
                % mass fractions of educts
                mf.x_1F = sys.alpha;
                mf.x_1O2 = (1-mf.x_1F) * .21 * sys.O2.M.value/sys.M_A;
                mf.x_1N2 = 1 - mf.x_1F - mf.x_1O2;
                % partial pressures of educts
                mf.p_1F = mf.p_1 * 1 / (1 + 1/sys.phi*sys.n_A);
                mf.p_1O2 = mf.p_1 * 1/sys.phi / (1 + 1/sys.phi*sys.n_A);
                mf.p_1N2 = mf.p_1 - mf.p_1F - mf.p_1O2;
                % sensible enthalpies of educts
                mf.h_1O2 = interp2(sys.O2.T.value,sys.O2.p.value,sys.O2.hs.value.',mf.T_1,mf.p_1O2);
                mf.h_1N2 = interp2(sys.N2.T.value,sys.N2.p.value,sys.N2.hs.value.',mf.T_1,mf.p_1N2);
                if strcmp(sys.fuel,'other')
                    % estimate cp of fuel, does not make much difference
                    % anyway, because alpha << 1
                    cp_1 = mf.k_1/(mf.k_1-1)*mf.R_1;
                    auxh_1O2 = interp2(sys.O2.T.value,sys.O2.p.value,sys.O2.hs.value.',mf.T_1+1,mf.p_1O2);
                    cp_O2 = auxh_1O2-mf.h_1O2; % works because we chose exactly 1K as dT
                    auxh_1N2 = interp2(sys.N2.T.value,sys.N2.p.value,sys.N2.hs.value.',mf.T_1+1,mf.p_1N2);
                    cp_N2 = auxh_1N2-mf.h_1N2; % works because we chose exactly 1K as dT
                    cp_F = (cp_1 - mf.x_1O2*cp_O2 - mf.x_1N2*cp_N2)/mf.x_1F; % mass average, error comes from T_1 ~= T0
                    mf.h_1F = cp_F*(mf.T_1 - sys.CH4.T0.value);
                else
                    mf.h_1F = interp2(sys.(sys.fuel).T.value,sys.(sys.fuel).p.value,sys.(sys.fuel).hs.value.',mf.T_1,mf.p_1F);
                end
                % partial pressures of products
                n_2 = b/4 + 1/sys.phi*(a+b/4)*sys.n_A;
                mf.p_2CO2 = mf.p_2 * a/n_2;
                mf.p_2H2O = mf.p_2 * b/2/n_2;
                mf.p_2O2 = mf.p_2 * (1/sys.phi - 1)*(a+b/4)/n_2;
                mf.p_2N2 = mf.p_2 - mf.p_2CO2 - mf.p_2H2O - mf.p_2O2;
                % mass fractions of products
                M_2 = (a*sys.CO2.M.value + b/2*sys.H2Og.M.value - (a+b/4)*sys.O2.M.value + (a+b/4)/sys.phi*sys.n_A*sys.M_A)/n_2; % check with the respective sys.Connection{}.Mmol!
                mf.x_2CO2 = mf.p_2CO2/mf.p_2 * sys.CO2.M.value/M_2;
                mf.x_2H2O = mf.p_2H2O/mf.p_2 * sys.H2O.M.value/M_2;
                mf.x_2O2 = mf.p_2O2/mf.p_2 * sys.O2.M.value/M_2;
                mf.x_2N2 = 1 - mf.x_2CO2 - mf.x_2H2O - mf.x_2O2; % check with x_1N2
                % sensible enthalpies of products
                mf.h_2CO2 = interp2(sys.CO2.T.value,sys.CO2.p.value,sys.CO2.hs.value.',mf.T_2,mf.p_2CO2);
                mf.h_2H2O = interp2(sys.H2Og.T.value,sys.H2Og.p.value,sys.H2Og.hs.value.',mf.T_2,mf.p_2H2O);
                mf.h_2O2 = interp2(sys.O2.T.value,sys.O2.p.value,sys.O2.hs.value.',mf.T_2,mf.p_2O2);
                mf.h_2N2 = interp2(sys.N2.T.value,sys.N2.p.value,sys.N2.hs.value.',mf.T_2,mf.p_2N2);
                % net heat release (after losses to the walls) is the
                % difference of kinetic energies and sensible enthalpies
                mf.h_2 = mf.x_2CO2*mf.h_2CO2 + mf.x_2H2O*mf.h_2H2O + mf.x_2O2*mf.h_2O2 + mf.x_2N2*mf.h_2N2; % products
                mf.h_1 = mf.x_1F*mf.h_1F + mf.x_1O2*mf.h_1O2 + mf.x_1N2*mf.h_1N2; % educts
                mf.q_net = mf.h_2 - mf.h_1 + .5.*(mf.c_2*mf.Ma_2)^2 - .5.*(mf.c_1*mf.Ma_1)^2;
            end
        end

        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end
            sys = sys.clear; % cleanup for updates...

            % get all the meanflow quantities
            mf = sys.meanflow;

            % explore the flame inputs
            confl = sys.Connection(2:end-1);
            nFlames = size(confl,2);
            if nFlames ~= 0
                FlameNames = cellfun(@(x) x.FlameName, confl, 'UniformOutput', false);
                potentiallyKnownETF = cellfun(@(x) isfield(x,'knownETF'),confl);
                knownETF = cellfun(@(x) isfield(x,'knownETF') && x.knownETF,confl);
                potentiallyRequiredETF = cellfun(@(x) isfield(x,'FTFphi'),confl);
                requiredETF = cellfun(@(x) isfield(x,'FTFphi') && x.FTFphi,confl);
                if ~all(potentiallyKnownETF==potentiallyRequiredETF)
                    error('The variables potentiallyKnownETF and potentiallyRequiredETF are not the same, are you maybe using an outdated reference block?')
                end
            end
            
            if strcmp(sys.Version,'constant cp, classic Rankine-Hugoniot, f and g only')
                % Often called 1st order in Mach, actually 0th order
                xi = mf.rho_1*mf.c_1/(mf.rho_2*mf.c_2);
                Theta = mf.T_2/mf.T_1;
                T = [ ...
                    xi,                         0,  0;...
                    -mf.Ma_1*mf.k_1*(Theta-1),  1,  mf.Ma_1*mf.c_1*(Theta-1);...
                    ]; % I'm sure of this one...
                D = [ ...
                    -(xi + mf.Ma_1*mf.k_1*(Theta-1) - 1)/(xi + mf.Ma_1*mf.k_1*(Theta-1) + 1),   2/(xi + mf.Ma_1*mf.k_1*(Theta-1) + 1),                                      mf.Ma_1*mf.c_1*(Theta-1)/(xi + mf.Ma_1*mf.k_1*(Theta-1) + 1);...
                    2*xi/(xi + mf.Ma_1*mf.k_1*(Theta-1) + 1),                                   (xi - mf.Ma_1*mf.k_1*(Theta-1) - 1)/(xi + mf.Ma_1*mf.k_1*(Theta-1) + 1),    mf.Ma_1*mf.c_1*(Theta-1)*xi/(xi + mf.Ma_1*mf.k_1*(Theta-1) + 1);...
                    ]; % test below
                % check: set pu,uu,Q, get pd,ud, compute fu,gu,fd,gd, check consistency of D
                pu = rand(1,1);uu = rand(1,1);Q = rand(1,1);
                out = T*[pu;uu;Q];
                pd = out(1);ud = out(2);
                fu = .5*(pu+uu);gu = .5*(pu-uu);fd = .5*(pd+ud);gd = .5*(pd-ud);
                out = D*[fu;gd;Q];
                if abs(out(1)-gu)>1e4*eps || abs(out(2)-fd)>1e4*eps
                    error('Check of Rankine-Hugoniot failed.');
                end
                % fill up matrix to 6-by-7
                D = [ ...
                    D(:,1:2), zeros(2,2), D(:,3), D(:,3), zeros(2,1);...
                    zeros(4,7);...
                    ];
                % scale/non-dimensionalize for comparability with automatic D-matrices
                D(1,:) = D(1,:)/mf.c_1; % g_1 = g_1_plus * c_1
                D(2,:) = D(2,:)/mf.c_2; % f_2 = f_2_plus * c_2
                D(:,1) = D(:,1)*mf.c_1; % x = coeff*f_1_plus = (coeff/c_1)*f_1
                D(:,2) = D(:,2)*mf.c_2; % x = coeff*g_2_plus = (coeff/c_2)*f_2
            elseif strcmp(sys.Version,'constant cp, 1st order in Mach, fixed heat source')
                % Caution! Strobio-Chen et al. 2016 neglects some 1st order
                % terms by normalizing u' with u_mean, instead of c. This
                % formula does not and is therefore not in perfect
                % accordance with Strobio-Chen et al. 2016.
                D = taXAcFixed_MachOrder1(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2); % autocode, no exception for missing ETF.
                D = blkdiag(D,zeros(2,1)); % fill up matrix to 6-by-7
            elseif strcmp(sys.Version,'constant cp, 1st order in Mach, moving flame')
                % Flame movement is an output! Computed such that there is
                % no 1st order entropy wave generation when there is no
                % heat release perturbation from equivalence ratio
                % fluctiations.
                % Inconsistencies between ETF and phi-FTF are compensated
                % by assuming flame motion caused equivalence ratio
                % fluctuations.
                if any(requiredETF & ~knownETF) && any(requiredETF & knownETF) % at least one phi-ref with known ETF and at least one with unknown ETF
                    error('Dynamic mixing of multiple fuels is not implemented yet, and I don''t see, why else more than one phi-reference would make sense.');
                elseif any(requiredETF & ~knownETF)
                    D = taXAcNoETF_MachOrder1(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                    D = blkdiag(D,zeros(1,1)); % fill up matrix to 6-by-7
                else
                    D = taXAcMoving_MachOrder1(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                end
            elseif strcmp(sys.Version,'realistic fluid, 1st order in Mach, moving flame')
                % Constant fluid properties over the temperature jump are
                % no longer assumed. Instead the lower heating value is
                % used to cover the additional degree fo freedom.
                % Since fluid properties are taken care of by solveMean,
                % the code is the same as const cp (previous)
                if any(requiredETF & ~knownETF) && any(requiredETF & knownETF) % at least one phi-ref with known ETF and at least one with unknown ETF
                    error('Dynamic mixing of multiple fuels is not implemented yet, and I don''t see, why else more than one phi-reference would make sense.');
                elseif any(requiredETF & ~knownETF)
                    D = taXAcNoETF_MachOrder1(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                    D = blkdiag(D,zeros(1,1)); % fill up matrix to 6-by-7
                else
                    D = taXAcMoving_MachOrder1(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                end
            elseif strcmp(sys.Version,'realistic fluid, 3rd order in Mach, moving flame')
                % 3rd order in Mach number, but still there will be an
                % error, compare to full Mach order.
                if any(requiredETF & ~knownETF) && any(requiredETF & knownETF) % at least one phi-ref with known ETF and at least one with unknown ETF
                    error('Dynamic mixing of multiple fuels is not implemented yet, and I don''t see, why else more than one phi-reference would make sense.');
                elseif any(requiredETF & ~knownETF)
                    D = taXAcNoETF_MachOrder3(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                    D = blkdiag(D,zeros(1,1)); % fill up matrix to 6-by-7
                else
                    D = taXAcMoving_MachOrder3(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                end
            elseif strcmp(sys.Version,'realistic fluid, full order in Mach, moving flame')
                % The best we've got...
                if any(requiredETF & ~knownETF) && any(requiredETF & knownETF) % at least one phi-ref with known ETF and at least one with unknown ETF
                    error('Dynamic mixing of multiple fuels is not implemented yet, and I don''t see, why else more than one phi-reference would make sense.');
                elseif any(requiredETF & ~knownETF)
                    D = taXAcNoETF_MachOrderNaN(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                    D = blkdiag(D,zeros(1,1)); % fill up matrix to 6-by-7
%                 elseif sys.V1~=0 || sys.V2~=0
%                     s = tf('s');
%                     D = taXAccumMoving_MachOrderNaN(mf.A_2/mf.A_1, mf.C_2, C_s_plus, C_w_plus, mf.Ma_1, mf.Ma_2, mf.c_1, mf.c_2/mf.c_1, mf.k_1, mf.k_2, mf.q, mf.q_net, mf.rho_2/mf.rho_1, s, mf.t_1, mf.t_2, mf.Ma_2*mf.c_2/(mf.Ma_1*mf.c_1));
                else
                    D = taXAcMoving_MachOrderNaN(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.p_1,mf.p_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2);
                end
            elseif strcmp(sys.Version,'realistic fluid, full order in Mach, fixed heat source')
                % For comparison only.
                D = taXAcFixed_MachOrderNaN(mf.A_1,mf.A_2,mf.C_2,mf.Ma_1,mf.Ma_2,mf.c_1,mf.c_2,mf.k_1,mf.k_2,mf.q,mf.q_net,mf.rho_1,mf.rho_2); % autocode
                D = blkdiag(D,zeros(2,1)); % fill up matrix to 6-by-7
            end

            % scale inputs and outputs (transform them from
            % non-dimensional form to what is needed in taX)
            D(1,:) = D(1,:)*mf.c_1; % g_1 = g_1_plus * c_1
            D(2,:) = D(2,:)*mf.c_2; % f_2 = f_2_plus * c_2
                % entropy stays non-dimensional
            D(4,:) = D(4,:)*sys.alpha*(1-sys.alpha)/mf.C_2; % make an alpha out of the molar mass. Unnecessary, since we delete alpha waves anyway (complete combustion!).
            D(5:6,:) = D(5:6,:)*mf.c_1*mf.A_1; % Vdot_s = Vdot_s_plus * c_1 * A_1
            D(:,1) = D(:,1)/mf.c_1; % x = coeff*f_1_plus = (coeff/c_1)*f_1
            D(:,2) = D(:,2)/mf.c_2; % x = coeff*g_2_plus = (coeff/c_2)*f_2
                % entropy again remains unchanged
            D(:,4) = D(:,4)*mf.C_1/(sys.alpha*(1-sys.alpha)); % convert incoming alpha to molar mass
                % heat release and ETF remain unchanged

            % write as a property
            if nFlames == 0
                % delete last 3 columns, as they represent inputs from active flames
                sys.D = D(:,1:end-3);
            else
                sys.D = D;
            end

            % reformat D to accomodate all inputs and outputs
            waves_conv_D = {'s','alpha'}; % switch s and alpha?
            [bool,pos] = ismember(sys.waves,waves_conv_D);
            sys.D = sys.D([1:2,pos(bool).'+2,5:end],[1:2,pos(bool).'+2,5:end]);
            % add omega (and potentially other) waves
            for i = 3:numel(sys.waves)
                if ~ismember(sys.waves{i},waves_conv_D) % omega waves and others are not transmitted
                    sys.D = [sys.D(1:i-1,:);zeros(1,size(sys.D,2));sys.D(i:end,:)]; % add an empty line
                    sys.D = [sys.D(:,1:i-1),zeros(size(sys.D,1),1),sys.D(:,i:end)]; % add an empty column
                end
            end
            % adapt matrix to inverted flow (if that's the case)
            if sys.Connection{1}.dir*sys.Connection{1}.Mach<0 % inverted! Mach<0
                sys.D(1:2,:) = sys.D([2,1],:); % switch first two rows (output relations)
                sys.D(:,1:2) = sys.D(:,[2,1]); % switch first two columns
                % luckily, all the convective stuff does not change!
                sys.D(5:6,:) = -sys.D(5:6,:); % u_s has now inverted direction
            end
            % repeat inputs as outputs
            sys.D = [sys.D(1:numel(sys.waves),:);eye(numel(sys.waves)),zeros(numel(sys.waves),size(sys.D,2)-numel(sys.waves));sys.D(numel(sys.waves)+1:end,:)]; % add inputs to outputs
            % set up acoustic ports
            sys = twoport(sys,[1,2+nFlames]);
            sys.y{end-1} = [sys.Name,'_flameMotion_u']; % u'_s / u_mean from Q_u
            sys.y{end} = [sys.Name,'_flameMotion_phi']; % u'_s / u_mean from Q_phi (only occurs if there is a contradiction between ETF and FTF)

            if nFlames ~= 0
                % set up an internal sss Qp for the flame(s) with its own D-matrix DQ
                DQ = zeros(3,nFlames+sum(knownETF & requiredETF)); % 3 outputs, to cover the last 3 inputs of sys.D
                % all that don't need an ETF use Q_u column of sys.D
                DQ(1,~requiredETF) = 1; % works despite ~requiredETF being shorter than size(DQ,2).
                % all that need an ETF use Q_phi column of sys.D
                DQ(2,requiredETF) = 1;
                % all that need an ETF, but where it is unknown, use the phi-FTF with a factor from sys.D such that there is no flame motion due to phi fluctuation
                ETF_factor = -sys.D(end,end-1)/sys.D(end,end); % (ETF input)/(phi-FTF input)
                DQ(3,requiredETF & ~knownETF) = ETF_factor;
                % all that need an ETF and where one is known use the known ETF
                DQ(3,nFlames + (1:sum(knownETF & requiredETF))) = 1;
                % make a system of DQ
                Qp = sss(DQ);
                % configure inputs and outputs of Qp
                Qp.u(1:nFlames) = FlameNames(:);
                Qp.u(potentiallyKnownETF) = cellfun(@(x) [x,'_Q'], Qp.u(potentiallyKnownETF), 'UniformOutput', false); % add _Q for all that also have the feature _T
                Qp.u(nFlames+1:end) = cellfun(@(x) [x,'_T'], FlameNames(knownETF & requiredETF), 'UniformOutput', false);
                Qp.y = {sys.Name;[sys.Name,'_Qphi'];[sys.Name,'_T']};
                Qp.InputGroup.FlameQ = 1:nFlames; % heat release fluctuations, to be transferred to sys during connecting
                Qp.InputGroup.FlameT = 1+nFlames:nFlames+sum(knownETF & requiredETF); % ETF, to be transferred to sys during connecting
                % name sys inputs such that they are recognized when connecting with Qp
                sys.u{numel(sys.waves)+1} = sys.Name;
                sys.u{numel(sys.waves)+2} = [sys.Name,'_Qphi'];
                sys.u{numel(sys.waves)+3} = [sys.Name,'_T'];
                % connect!
                sys = connect(sys,Qp,[sys.u(1:numel(sys.waves));Qp.u],sys.y);
                % re-sort OutputGroup for easier visual check of output order
                for wavec = sys.waves.'
                    wave = char(wavec);
                    auxSort = sortrows([sys.y(sys.OutputGroup.(wave)),num2cell(sys.OutputGroup.(wave).')]);
                    sys.OutputGroup.(wave) = cell2mat(auxSort(:,2)).';
                end
            end

            sys.uptodate = true;
        end
        
        %% mean solve algorithm (check for identity)
        function [con,sys] = solveMean(sys,con)
            %% Warnings and reset in case somebody has changed Hu, Aratio and kratio with changeParam despite constant cp
            if strncmp(sys.Version,'constant cp',11) && ~isequal(sys.kratio, 1)
                warning('wID:Reset_kR_to_1','Resetting sys.kratio to 1, since other values would be inconsistent with assumption of constant cp.');warning('off','wID:Reset_kR_to_1');
                sys.kratio = 1;
            end
            if strncmp(sys.Version,'constant cp',11) && ~isnan(sys.Hu)
                warning('wID:Reset_Hu_to_NaN','Resetting sys.Hu to NaN, since other values would be inconsistent with assumption of constant cp.');warning('off','wID:Reset_Hu_to_NaN');
                sys.Hu = NaN; % just to make sure there are no accidents in the implementation of solveMean...
            end
            if strcmp(sys.Version,'constant cp, classic Rankine-Hugoniot, f and g only') && ~isequal(sys.Aratio, 1)
                warning('wID:Reset_AR_to_1','Resetting sys.Aratio to 1, since other values would be inconsistent with assumption of classic Rankine-Hugoniot.');warning('off','wID:Reset_AR_to_1');
                sys.Aratio = 1;
            end
            if Block.checkField(con{1},'Mach') || Block.checkField(con{2},'Mach') % this condition is repeated later, so no trouble with undefined functions ...
                Mach = con{1}.dir*con{1}.Mach; % sign was not yet swapped!
                if isempty(Mach)
                    Mach = con{2}.dir*con{2}.Mach;
                end
                if (Mach>0 && sys.Aratio<1) || (Mach<0 && sys.Aratio>1)
                    sys.Aratio = 1;
                    warning('wID:Reset_AR_to_1','Resetting sys.Aratio to 1. The pressure drop is based on the jet condition and requires a backward facing step, or asymptotically no step at all. If Ma>0, it should be Aratio>=1; and if Ma<0, Aratio<=1.');warning('off','wID:Reset_AR_to_1');
                end
            end
            
            %% Swap sign of upstream velocity
            if Block.checkField(con{1},'Mach')
                con{1}.Mach = -con{1}.Mach;
            end

            %% Simple propagations
            % Propagate area
            if Block.checkField(con{1},'A') && Block.checkField(con{2},'A')
                if not(Block.isequalAbs(con{1}.A*sys.Aratio,con{2}.A))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'A')
                con{2}.A = con{1}.A*sys.Aratio;
            elseif Block.checkField(con{2},'A')
                con{1}.A = con{2}.A/sys.Aratio;
            end
            % Propagate position
            if Block.checkField(con{1},'pos') && Block.checkField(con{2},'pos')
                if not(Block.isequalAbs(con{1}.pos,con{2}.pos))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'pos')
                con{2}.pos = con{1}.pos;
            elseif Block.checkField(con{2},'pos')
                con{1}.pos = con{2}.pos;
            end
            % Propagate isentropic exponent
            if Block.checkField(con{1},'kappa') && Block.checkField(con{2},'kappa')
                if not(Block.isequalAbs(con{1}.kappa*sys.kratio,con{2}.kappa))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'kappa')
                con{2}.kappa = con{1}.kappa*sys.kratio;
            elseif Block.checkField(con{2},'kappa')
                con{1}.kappa = con{2}.kappa/sys.kratio;
            end

            %% Propagate (i.e. burn) fuel mass fraction, but only if at least on Mach number is known
            if Block.checkField(con{1},'Mach') || Block.checkField(con{2},'Mach')
                Mach = con{1}.Mach; % sign was already swapped!
                if isempty(Mach)
                    Mach = con{2}.Mach;
                end                    
                if Block.checkField(con{2},'alpha') && Mach>0
                    if con{2}.alpha~=0
                        error('Mean values are inconsistent.')
                    end
                elseif Block.checkField(con{1},'alpha') && Mach<0
                    if con{1}.alpha~=0
                        error('Mean values are inconsistent.')
                    end
                elseif Mach==0
                    error('Mean values are inconsistent.')
                elseif Mach>0
                    con{2}.alpha = 0;
                elseif Mach<0
                    con{1}.alpha = 0;
                end
                if (Mach>0 && Block.checkField(sys.Connection{1},'alpha')) ...
                        || Mach<0 && Block.checkField(sys.Connection{end},'alpha') % use sys.Connection here so get.Mratio works
                    MR = sys.Mratio;
                end
            end

            %% Propagate sonic velocity and molar mass
            % ..., since they require knowledge of alpha (such that Mratio
            % is not empty, and there is a higher chance that is the case
            % after the propagation of alpha.
            if exist('MR','var')
                % Propagate sonic velocity
                if Block.checkField(con{1},'c') && Block.checkField(con{2},'c') && ~isempty(MR)
                    if not(Block.isequalAbs(con{1}.c*sqrt(sys.Tratio*sys.kratio/MR),con{2}.c))
                        error('Mean values are inconsistent.')
                    end
                elseif Block.checkField(con{1},'c') && ~isempty(MR)
                    con{2}.c = con{1}.c*sqrt(sys.Tratio*sys.kratio/MR);
                elseif Block.checkField(con{2},'c') && ~isempty(MR)
                    con{1}.c = con{2}.c/sqrt(sys.Tratio*sys.kratio/MR);
                end
                % Propagate molar mass
                if Block.checkField(con{1},'Mmol') && Block.checkField(con{2},'Mmol') && ~isempty(MR)
                    if not(Block.isequalAbs(con{1}.Mmol*MR,con{2}.Mmol))
                        error('Mean values are inconsistent.')
                    end
                elseif Block.checkField(con{1},'Mmol') && ~isempty(MR)
                    con{2}.Mmol = con{1}.Mmol*MR;
                elseif Block.checkField(con{2},'Mmol') && ~isempty(MR)
                    con{1}.Mmol = con{2}.Mmol/MR;
                end
            end

            %% Propagate density and Mach number
            % Solve numerically, should be quick enough. This only runs for
            % long-range parameter change plus once initially.
            if Block.checkField(con{1},'c') && Block.checkField(con{2},'c') ...
                    && Block.checkField(con{1},'kappa') && Block.checkField(con{2},'kappa') % nothing works without these two...
                % define equations: mass and momentum
                mass = @(rho1,rho2,Ma1,Ma2) rho1*con{1}.c*con{1}.A*Ma1 - rho2*con{2}.c*con{2}.A*Ma2; % mass is only first order, therefore the version does not matter
                if Block.checkField(con{1},'Mach') || Block.checkField(con{2},'Mach') % this condition is repeated later, so no trouble with undefined functions ...
                    Mach = con{1}.Mach; % sign was already swapped!
                    if isempty(Mach)
                        Mach = con{2}.Mach;
                    end
                    if Mach>=0
                        momentum = @(rho1,rho2,Ma1,Ma2) rho1*con{1}.A*con{1}.c^2*Ma1^2 + rho1*con{2}.A*con{1}.c^2/con{1}.kappa - rho2*con{2}.A*con{2}.c^2*Ma2^2 - rho2*con{2}.A*con{2}.c^2/con{2}.kappa; % including pressure drop due to acceleration, no matter the Mach-order
                        % Caution: the momentum balance in human-readable form
                        % is: rho1*A1*u1^2 + p1*A2 = rho2*A2*u2^2 + p2*A2. Yes,
                        % p1*A2, this is due to the jet condition: at the face
                        % plate, there is p1, but its area is A2-A1, from the
                        % inlet you have an additional p1*A1. Google Borda-
                        % Carnot equation for details and consequences.
                    else
                        momentum = @(rho1,rho2,Ma1,Ma2) rho1*con{1}.A*con{1}.c^2*Ma1^2 + rho1*con{1}.A*con{1}.c^2/con{1}.kappa - rho2*con{2}.A*con{2}.c^2*Ma2^2 - rho2*con{1}.A*con{2}.c^2/con{2}.kappa; % including pressure drop due to acceleration, no matter the Mach-order
                    end
                end
                % solve
                opts = optimset('fsolve');
                opts = optimset(opts,'Display','off','TolFun',1e4*eps,'TolX',1e4*eps,'MaxFunEvals',5e3,'MaxIter',1e4);
                if Block.checkField(con{1},'rho') && Block.checkField(con{1},'Mach')
                    rho_1 = con{1}.rho;
                    Ma_1 = con{1}.Mach;
                    % a good initial value is key here, because there is more than one solution!
                    [result,err] = fsolve(@(x) [mass(rho_1,x(1),Ma_1,x(2));momentum(rho_1,x(1),Ma_1,x(2))],[rho_1/sys.Tratio;Ma_1*sqrt(sys.Tratio)],opts); % approximate rho(2) and Mach(2) with Tratio
                    rho_2 = result(1);
                    Ma_2 = result(2);
                elseif Block.checkField(con{1},'rho') && Block.checkField(con{2},'Mach')
                    rho_1 = con{1}.rho;
                    Ma_2 = con{2}.Mach;
                    % a good initial value is key here, because there is more than one solution!
                    [result,err] = fsolve(@(x) [mass(rho_1,x(1),x(2),Ma_2);momentum(rho_1,x(1),x(2),Ma_2)],[rho_1/sys.Tratio;Ma_2/sqrt(sys.Tratio)],opts); % approximate rho(2) and Mach(1) with Tratio
                    rho_2 = result(1);
                    Ma_1 = result(2);
                elseif Block.checkField(con{2},'rho') && Block.checkField(con{1},'Mach')
                    rho_2 = con{2}.rho;
                    Ma_1 = con{1}.Mach;
                    % a good initial value is key here, because there is more than one solution!
                    [result,err] = fsolve(@(x) [mass(x(1),rho_2,Ma_1,x(2));momentum(x(1),rho_2,Ma_1,x(2))],[rho_2*sys.Tratio;Ma_1*sqrt(sys.Tratio)],opts); % approximate rho(1) and Mach(2) with Tratio
                    rho_1 = result(1);
                    Ma_2 = result(2);
                elseif Block.checkField(con{2},'rho') && Block.checkField(con{2},'Mach')
                    rho_2 = con{2}.rho;
                    Ma_2 = con{2}.Mach;
                    % a good initial value is key here, because there is more than one solution!
                    [result,err] = fsolve(@(x) [mass(x(1),rho_2,x(2),Ma_2);momentum(x(1),rho_2,x(2),Ma_2)],[rho_2*sys.Tratio;Ma_2/sqrt(sys.Tratio)],opts); % approximate rho(1) and Mach(1) with Tratio
                    rho_1 = result(1);
                    Ma_1 = result(2);
                end
                if any(err>1e4*eps)
                    warning('wID:mean_error',['When solving mean quantities Ma and rho, the error was greater than the tolerance of ',num2str(1e4*eps),'.']);warning('off','wID:mean_error');
                end
                % check for inconsistencies between heat release and flow direction
                if strncmp(sys.Version,'realistic fluid',15) && sign(Ma_1*sys.Hu*(sys.Tratio-1))<0 % negative Ma and positive Hu contradict Tratio>1 and so on...
                    error('Mean values are inconsistent: Flow direction and lower heating value contradict temperature ratio.')
                end
                reduceTol = 1e2;
                if (Block.checkField(con{1},'rho') && ~Block.isequalAbs(con{1}.rho/reduceTol,rho_1/reduceTol)) ...
                        || (Block.checkField(con{2},'rho') && ~Block.isequalAbs(con{2}.rho/reduceTol,rho_2/reduceTol)) ...
                        || (Block.checkField(con{1},'Mach') && ~Block.isequalAbs(con{1}.Mach/reduceTol,Ma_1/reduceTol)) ...
                        || (Block.checkField(con{2},'Mach') && ~Block.isequalAbs(con{2}.Mach/reduceTol,Ma_2/reduceTol))
                    error('Mean values are inconsistent.')
                elseif abs(Ma_1) > 1 || abs(Ma_2) > 1 || Ma_1*Ma_2 < 0 % check for unrealistic Mach numbers, due to bad initial value
                    error('Unrealistic Mach numbers. Please check initial values in symAreaChangeRankineHugoniot --> solveMean.');
                else % renew all, to keep the code short
                    con{1}.rho = rho_1;
                    con{2}.rho = rho_2;
                    con{1}.Mach = Ma_1;
                    con{2}.Mach = Ma_2;
                end
            end
            
            %% (Re)swap sign of upstream velocity
            if Block.checkField(con{1},'Mach')
                con{1}.Mach = -con{1}.Mach;
            end
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            if ~isfield(pars,'Version') % this is what happens to the tempJump
                sys.Version = 'constant cp, 1st order in Mach, fixed heat source';
                references.Version = [];
            elseif iscell(pars.Version)
                sys.Version = char(pars.Version);
            else
                sys.Version = pars.Version;
            end
            if iscell(pars.Tratio)
                sys.Tratio = eval(cell2mat(pars.Tratio));
            else
                sys.Tratio = pars.Tratio;
            end
            if strcmp(sys.Version,'constant cp, classic Rankine-Hugoniot, f and g only') ...
                    || ~isfield(pars,'Version') % tempJump!
                sys.Aratio = 1;
                references.Aratio = [];
            else
                if iscell(pars.Aratio)
                    sys.Aratio = eval(cell2mat(pars.Aratio));
                else
                    sys.Aratio = pars.Aratio;
                end
            end
            if ~isfield(pars,'Version') % tempJump!
                sys.fuel = 'other';
            elseif iscell(pars.fuel)
                sys.fuel = char(pars.fuel);
            else
                sys.fuel = pars.fuel;
            end
            if strncmp(sys.Version,'constant cp',11)
                sys.a_CaHb = 1; % methane
                sys.b_CaHb = 4; % methane
                sys.kratio = 1;
                sys.Hu = NaN; % so there is an error, if it is used anyway
                references.a_CaHb = [];
                references.b_CaHb = [];
                references.kratio = [];
                references.Hu = [];
                references.fuel = [];
            else
                if strcmp(sys.fuel,'other')
                    % realistic fluid
                    if iscell(pars.a_CaHb)
                        sys.a_CaHb = eval(cell2mat(pars.a_CaHb));
                    else
                        sys.a_CaHb = pars.a_CaHb;
                    end
                    if iscell(pars.b_CaHb)
                        sys.b_CaHb = eval(cell2mat(pars.b_CaHb));
                    else
                        sys.b_CaHb = pars.b_CaHb;
                    end
                    if ~isfield(pars,'Hu')
                        sys.Hu = 50.0e6; % J/kg
                        references.Hu = [];
                    elseif iscell(pars.Hu)
                        sys.Hu = eval(cell2mat(pars.Hu));
                    else
                        sys.Hu = pars.Hu;
                    end
                else
                    % known fuel
                    switch sys.fuel
                        case 'H2'
                            sys.a_CaHb = 0;
                            sys.b_CaHb = 2;
                        case 'CH4'
                            sys.a_CaHb = 1;
                            sys.b_CaHb = 4;
                        case 'C2H4'
                            sys.a_CaHb = 2;
                            sys.b_CaHb = 4;
                        case 'C2H6'
                            sys.a_CaHb = 2;
                            sys.b_CaHb = 6;
                        case 'C3H8'
                            sys.a_CaHb = 3;
                            sys.b_CaHb = 8;
                        case 'C4H10'
                            sys.a_CaHb = 4;
                            sys.b_CaHb = 10;
                        case 'isoC4H10'
                            sys.a_CaHb = 4;
                            sys.b_CaHb = 10;
                    end
                    % when pars2sys is executed within changeParams to
                    % create a dummy element, the fluid files are not
                    % loaded!
                    if isstruct(sys) && ~isfield(sys,sys.fuel)
                        elementPath = mfilename('fullpath');
                        elementPath = elementPath(1:find(elementPath=='/',1,'last'));
                        fluid = load([elementPath,'Gases/',sys.fuel,'.mat']);
                        sys.(sys.fuel) = fluid.(sys.fuel);
                        fluid = load([elementPath,'Gases/CO2.mat']);
                        sys.CO2 = fluid.CO2;
                        fluid = load([elementPath,'Gases/H2Og.mat']);
                        sys.H2Og = fluid.H2Og;
                    end
                    % finally compute Hu
                    sys.Hu = 1e6*(sys.(sys.fuel).hf0.value - sys.a_CaHb*sys.CO2.hf0.value - sys.b_CaHb/2*sys.H2Og.hf0.value)/sys.(sys.fuel).M.value;
                end
                if iscell(pars.kratio)
                    sys.kratio = eval(cell2mat(pars.kratio));
                else
                    sys.kratio = pars.kratio;
                end
            end
            if ~exist('references','var')
                references = [];
            end
        end
    end
end