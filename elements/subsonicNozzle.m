classdef subsonicNozzle < AcBlock & sss
    % SUBSONICNOZZLE compact nozzle or diffusor using the relations by
    % Marble and Candel (1977)
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = lZeta(pars);
    % Input:   * pars.Name:         string of name of the subsonicNozzle
    %          * pars.MachRatio:    ratio of Mach numbers Ma_downstream / Ma_upstream
    % Output:  * sys: subsonicNozzle object
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  24 September 2021
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties (SetAccess = protected) % long range!
        Machratio % ratio of Mach numbers, downstream / upstream, i.e. for Mach>0 and Machratio>1 --> nozzle and so on.
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end
    
    methods
        function sys = subsonicNozzle(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = solveMean(sys,con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end

        %% Get & Set functions
        function sys = set.Machratio(sys, Machratio)
            if not(isequal(sys.Machratio, Machratio))
                sys.Machratio = Machratio;
                sys.uptodate = false;
            end
        end

        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end

            % some abbreviations
            M1 = sys.Connection{1}.dir * sys.Connection{1}.Mach;
            M2 = sys.Connection{2}.dir * sys.Connection{2}.Mach;
            k = sys.Connection{1}.kappa;
            c1 = sys.Connection{1}.c;
            c2 = sys.Connection{2}.c;

            % from Marble and Candel 1977
            sys.D = [...
                (M2-M1)/(1-M1) * (1+M1)/(M2+M1) * (1-(k-1)/2*M1*M2)/(1+(k-1)/2*M1*M2),...
                (2*M1)/(M1+M2) * (1-M2)/(1-M1) * (1+(k-1)/2*M1^2)/(1+(k-1)/2*M1*M2) * c1/c2,... % might be M2^2 instead of M1^2, index is missing in the paper! -- No, it's M1, I checked in taXValid!
                -(M2-M1)/(1-M1) * M1/2/(1+(k-1)/2*M1*M2) * c1;... % added *M2 to last M1 (for symmetry, checked with taXValid)
                
                (2*M2)/(1+M2) * (1+M1)/(M2+M1) * (1+(k-1)/2*M2^2)/(1+(k-1)/2*M1*M2) * c2/c1,...
                -(M2-M1)/(M2+M1) * (1-M2)/(1+M2) * (1-(k-1)/2*M1*M2)/(1+(k-1)/2*M1*M2),...
                (M2-M1)/(1+M2) * M2/2/(1+(k-1)/2*M1*M2) * c2;...
                
                0,... % f and g don't produce entropy
                0,... % f and g don't produce entropy
                1;... % entropy waves are passed through (incl. normalization by cp)
                ];

%             sys.D = [...
%                 (M2-M1)/(1-M1) * (1+M1)/(M2+M1) * (1-(k-1)/2*M1*M2)/(1+(k-1)/2*M1*M2),...
%                 (2*M2)/(1+M2) * (1+M1)/(M2+M1) * (1+(k-1)/2*M2^2)/(1+(k-1)/2*M1*M2) * c2/c1,...
%                 -(M2-M1)/(1-M1) * M1/2/(1+(k-1)/2*M1) * c1;...
%                 
%                 (2*M1)/(M1+M2) * (1-M2)/(1-M1) * (1+(k-1)/2*M1^2)/(1+(k-1)/2*M1*M2) * c1/c2,... % might be M2^2 instead of M1^2, index is missing in the paper!
%                 -(M2-M1)/(M2+M1) * (1-M2)/(1+M2) * (1-(k-1)/2*M1*M2)/(1+(k-1)/2*M1*M2),...
%                 (M2-M1)/(1+M2) * M2/2/(1+(k-1)/2*M1*M2) * c2;...
%                 
%                 0,... % f and g don't produce entropy
%                 0,... % f and g don't produce entropy
%                 1;... % entropy waves are passed through (incl. normalization by cp)
%                 ];

            % reformat D to accomodate all inputs and outputs
            if ~any(strcmp(sys.waves,'s'))
                sys.D = sys.D(1:2,1:2); % omit entropy row and column
            end
            for i = 3:numel(sys.waves)
                if strcmp(sys.waves{i},'alpha') % phi waves are just passed through
                    sys.D = [sys.D(1:i-1,:);zeros(1,size(sys.D,2));sys.D(i:end,:)]; % add an empty line
                    sys.D = [sys.D(:,1:i-1),zeros(size(sys.D,1),1),sys.D(:,i:end)]; % add an empty column
                    sys.D(i,i) = 1; % pass through...
                elseif strncmp(sys.waves{i},'omega',5) % omega waves are not transmitted (for now, actually there will be interaction)
                    sys.D = [sys.D(1:i-1,:);zeros(1,size(sys.D,2));sys.D(i:end,:)]; % add an empty line
                    sys.D = [sys.D(:,1:i-1),zeros(size(sys.D,1),1),sys.D(:,i:end)]; % add an empty column
                end
            end

            % repeat inputs as outputs
            sys.D = [sys.D(1:numel(sys.waves),:);eye(numel(sys.waves)),zeros(numel(sys.waves),size(sys.D,2)-numel(sys.waves));sys.D(numel(sys.waves)+1:end,:)]; % add inputs to outputs

            % set up acoustic ports
            sys = twoport(sys);
            
            sys.uptodate = true;
        end
        
        %% mean solve algorithm
        function con = solveMean(sys,con)
            %% Swap sign of upstream velocity
            if Block.checkField(con{1},'Mach')
                con{1}.Mach = -con{1}.Mach;
            end

            %% Simple propagations
            % Propagate position
            if Block.checkField(con{1},'pos') && Block.checkField(con{2},'pos')
                if not(Block.isequalAbs(con{1}.pos,con{2}.pos))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'pos')
                con{2}.pos = con{1}.pos;
            elseif Block.checkField(con{2},'pos')
                con{1}.pos = con{2}.pos;
            end
            % Propagate isentropic exponent
            if Block.checkField(con{1},'kappa') && Block.checkField(con{2},'kappa')
                if not(Block.isequalAbs(con{1}.kappa,con{2}.kappa))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'kappa')
                con{2}.kappa = con{1}.kappa;
            elseif Block.checkField(con{2},'kappa')
                con{1}.kappa = con{2}.kappa;
            end
            % Propagate molar mass
            if Block.checkField(con{1},'Mmol') && Block.checkField(con{2},'Mmol')
                if not(Block.isequalAbs(con{1}.Mmol,con{2}.Mmol))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'Mmol')
                con{2}.Mmol = con{1}.Mmol;
            elseif Block.checkField(con{2},'Mmol')
                con{1}.Mmol = con{2}.Mmol;
            end
            % Propagate fuel mass fraction
            if Block.checkField(con{1},'alpha') && Block.checkField(con{2},'alpha')
                if not(Block.isequalAbs(con{1}.alpha,con{2}.alpha))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'alpha')
                con{2}.alpha = con{1}.alpha;
            elseif Block.checkField(con{2},'alpha')
                con{1}.alpha = con{2}.alpha;
            end
            
            %% Propagation of quantities affected by the Mach number
            % Propagate Mach number
            if Block.checkField(con{1},'Mach') && Block.checkField(con{2},'Mach')
                if not(Block.isequalAbs(con{1}.Mach*sys.Machratio,con{2}.Mach))
                    error('Mean values are inconsistent.')
                end
            elseif Block.checkField(con{1},'Mach')
                con{2}.Mach = con{1}.Mach*sys.Machratio;
            elseif Block.checkField(con{2},'Mach')
                con{1}.Mach = con{2}.Mach/sys.Machratio;
            end
            if Block.checkField(con{1},'Mach') && Block.checkField(con{2},'Mach') ...
                    && (max(abs([con{1}.Mach,con{2}.Mach]))>1 || con{1}.Mach==0 || con{2}.Mach==0) % supersonic flow and zero mean flow are forbidden in general!
                error('Mean values are inconsistent.')
            end
            % general computations (can only be done when both Mach numbers and kappas are known)
            if Block.checkField(con{1},'Mach') && Block.checkField(con{2},'Mach') ...
                    && Block.checkField(con{1},'kappa') && Block.checkField(con{2},'kappa')
                k = con{1}.kappa;
                Ma1 = con{1}.Mach; % no dir, since sign was already swapped
                Ma2 = con{2}.Mach;
                Tratio = (1+(k-1)/2*Ma1^2)/(1+(k-1)/2*Ma2^2);

                % Propagate sonic velocity
                if Block.checkField(con{1},'c') && Block.checkField(con{2},'c')
                    if not(Block.isequalAbs(con{1}.c*sqrt(Tratio),con{2}.c))
                        error('Mean values are inconsistent.')
                    end
                elseif Block.checkField(con{1},'c')
                    con{2}.c = con{1}.c*sqrt(Tratio);
                elseif Block.checkField(con{2},'c')
                    con{1}.c = con{2}.c/sqrt(Tratio);
                end
                % Propagate density
                if Block.checkField(con{1},'rho') && Block.checkField(con{2},'rho')
                    if not(Block.isequalAbs(con{1}.rho*Tratio^(1/(k-1)),con{2}.rho))
                        error('Mean values are inconsistent.')
                    end
                elseif Block.checkField(con{1},'rho')
                    con{2}.rho = con{1}.rho*Tratio^(1/(k-1));
                elseif Block.checkField(con{2},'rho')
                    con{1}.rho = con{2}.rho/Tratio^(1/(k-1));
                end
                % Propagate area: Machzahlquerschnittsgleichung
                Aratio = subsonicNozzle.Machzahlquerschnittsgleichung(Ma2,k)/subsonicNozzle.Machzahlquerschnittsgleichung(Ma1,k);
                if Block.checkField(con{1},'A') && Block.checkField(con{2},'A')
                    if not(Block.isequalAbs(con{1}.A*Aratio,con{2}.A))
                        error('Mean values are inconsistent.')
                    end
                elseif Block.checkField(con{1},'A')
                    con{2}.A = con{1}.A*Aratio;
                elseif Block.checkField(con{2},'A')
                    con{1}.A = con{2}.A/Aratio;
                end
            end

            %% (Re)swap sign of upstream velocity
            if Block.checkField(con{1},'Mach')
                con{1}.Mach = -con{1}.Mach;
            end
        end
    end
    
    methods(Static)
        %% compute area normalized by critical cross-sectional area
        function A_by_AStar = Machzahlquerschnittsgleichung(Mach,kappa)
            A_by_AStar = ((kappa+1)/2)^(-(kappa+1)/2/(kappa-1)) * (1+(kappa-1)/2*Mach^2)^((kappa+1)/2/(kappa-1))/Mach;
        end

        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if iscell(pars.Machratio)
                sys.Machratio = eval(cell2mat(pars.Machratio));
            else
                sys.Machratio = pars.Machratio;
            end
            references = [];
        end
    end
end