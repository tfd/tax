classdef scatteringDummy < AcBlock & sss
    % DUCT dummy element for scattering matrix computations.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = scatteringDummy(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.loc: 'Upstream' or 'Downstream', location of block
    %          with respect to the internal part of the scattering system 
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: scatteringDummy object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  24 May 2021
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        % 'Upstream' or 'Downstream', location of block with respect to the
        %  internal part of the scattering system, option to deactivate the
        %  scatteringDummy
        loc, active
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end
    
    methods
        function sys = scatteringDummy(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)));
            
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            con = Block.readPort(pars,sys.Connection);
            sys = set_Connection(sys, con);
        end

        %% Set functions
        function sys = set.loc(sys, loc)
            if not(isequal(sys.loc, loc))
                sys.loc = loc;
                sys.uptodate = false;
            end
        end
        function sys = set.active(sys, active)
            if not(isequal(sys.active, active))
                sys.active = active;
                sys.uptodate = false;
            end
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = Block.solveMean(con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end
            
            D = eye(numel(sys.waves));
            sys.D = [D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)
            sys = twoport(sys);
            
            if sys.active
                switch char(sys.loc)
                    case 'Upstream' % is the location of the dummy with respect to the scattering system
                        if sys.Connection{1}.Mach*sys.Connection{1}.dir>=0 % Flow is directed into the scatter system
                            sys.OutputGroup.Scatter = 1;
                            sys.InputGroup.Scatter = [1,3:numel(sys.waves)];
                        else
                            sys.OutputGroup.Scatter = [1,3:numel(sys.waves)];
                            sys.InputGroup.Scatter = 1;
                        end
                    case 'Downstream' % is the location of the dummy with respect to the scattering system
                        if sys.Connection{1}.Mach*sys.Connection{1}.dir>=0 % Flow is directed out of the scatter system
                            sys.OutputGroup.Scatter = [2,3:numel(sys.waves)];
                            sys.InputGroup.Scatter = 2;
                        else
                            sys.OutputGroup.Scatter = 2;
                            sys.InputGroup.Scatter = [2,3:numel(sys.waves)];
                        end
                end
            else
                sys.OutputGroup.Scatter = [];
                sys.InputGroup.Scatter = [];
            end
            
            sys.uptodate = true;
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if iscell(pars.loc)
                sys.loc = char(cell2mat(pars.loc));
            else
                sys.loc = pars.loc;
            end
            sys.active = strcmp(pars.active, 'on');
            references = [];
        end
    end
end