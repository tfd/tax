classdef openEnd < AcBlock & sss
    % OPENEND open end class.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = openEnd(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: openEnd object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  30 Sep 2021
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties (SetAccess = protected)
        Area % long range effects! 'protected' is a marker so changeParam cannot change it without updating the entire system
        Mach
        rho
        c_sound
        kappa
        Mmol
        alpha
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end

    methods
        function sys = openEnd(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(1,1));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            con = Block.readPort(pars,sys.Connection);
            % sign convention for Downstream BCs
            if strcmp(char(pars.loc),'Downstream')
                con{1}.Mach = -con{1}.Mach;
            end
            sys = set_Connection(sys, con);
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = con(1);
            if Block.checkPort(con(1),AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end
            Ma = sys.Connection{1}.Mach;
            
            % Mach Number has a sign, by which it covers up and downstream ends
            outFlow = sys.Connection{1}.Mach<0 || (sys.Connection{1}.Mach==0 && sys.Connection{1}.dir<0); % is meanflow leaving the system here? No dir!
            nin = 1 + outFlow*(numel(sys.waves)-2);
            nout = numel(sys.waves);
            sys.D = [-(1-Ma)/(1+Ma),zeros(1,nin-1);zeros(nout-1-nin,nin);eye(nin)]; % reflection coefficient corresponds to const mass flow rate
            
            sys= oneport(sys);
            
            sys.uptodate = true;
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            % save BC for reference in changeParam. Caution, using
            % different variable names than in Connection to avoid
            % conflicts with sss matrices A (or a) and C (or c):
            %   A --> Area
            %   c --> c_sound
            for field = {'Area','Mach','rho','c_sound','kappa','Mmol','alpha'}
                fc = char(field);
                if iscell(pars.(fc))
                    sys.(fc) = eval(cell2mat(pars.(fc)));
                else
                    sys.(fc) = pars.(fc);
                end
            end
            references = [];
        end
    end
end