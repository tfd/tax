classdef junction < AcBlock & sss
    % Junction class with volume.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = junction(pars);
    % Input:   * pars.Name: string of name of the triJunction
    % optional * pars.V: volume
    %          * pars.mix: string if 'on' mix gases (differences in set_Connection)
    %          * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: junction object
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de), Thomas Emmert (emmert@tfd.mw.tum.de)
    % Last Change:  17 May 2017
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        Volume,mix
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end
    
    methods
        function sys = junction(pars)
            nPorts = str2double(char(pars.nPorts));
            ports = repmat({AcBlock.Port},[1,nPorts]);
            % Call constructor with correct number of ports and port type
            sys@AcBlock(ports{:});
            % Call constructor with not necessarily correct in and output dimension
            sys@sss(zeros(nPorts));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;

            % allow to provide BCs via pars
            sys.Connection = Block.readPort(pars,sys.Connection);
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            %% analyse set of connections
            n = length(con);
            chk = zeros(1,n);
            chkIn = zeros(1,n);
            for i = 1:n
                chk(i) = Block.checkField(con{i},'Mach');
                if chk(i)
                    chkIn(i) = con{i}.Mach<0;
                end
            end
            % Mach vector
            Mach=zeros(1,n);
            vchk=find(chk);
            for i=vchk
                Mach(i)=con{i}.Mach;
            end
            %% mix or propagate
            if sys.mix % mix
                con = junction.solveMeanMix(con);
            else
                % Propagate everything but Mach number
                con = junction.solveMean(con);
                % solve mass balance or check consictency, if there is enough information
                if all(chk) % all Mach numbers known --> check consistency
                    if not(sum(Mach)==0)
                        error('Mean values are inconsistent.')
                    end
                elseif sum(chk) == n-1 % only one Mach number is missing --> solve mass balance
                    % mass balance (sign convention: Mach pointing out of the element)
                    Mach(chk==0)=-sum(Mach);

                    % append
                    for i=1:n
                        con{i}.Mach = Mach(i);
                    end
                end
            end
            %% write
            sys.Connection = con;
            
            if Block.checkPort(con,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Set functions
        function sys = set.Volume(sys, Volume)
            if not(isequal(sys.Volume, Volume))
                sys.Volume = Volume;
                sys.uptodate = false;
            end
        end
        function sys = set.mix(sys, mix)
            if not(isequal(sys.mix, mix))
                sys.mix = mix;
                sys.uptodate = false;
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            elseif ~Block.checkPort(sys.Connection,AcBlock.Port)
                error('Mean values are incomplete.');
            end
            Ma = cellfun(@(x) x.Mach, sys.Connection);
            rho = cellfun(@(x) x.rho, sys.Connection);
            c = cellfun(@(x) x.c, sys.Connection);
            kappa = cellfun(@(x) x.kappa, sys.Connection);
            dir = cellfun(@(x) x.dir, sys.Connection);

            A = sys.Connection{1}.A;
            rhoVol = sys.Connection{find(Ma>=0,1,'first')}.rho;
            cVol = sys.Connection{find(Ma>=0,1,'first')}.c;
            kappaVol = sys.Connection{find(Ma>=0,1,'first')}.kappa;
            V = sys.Volume;

            %% coefficients for general equation:
            % all symbolic...
            syms s
            ncon = numel(sys.Connection);
            nwav = numel(sys.waves);
            for i = 1:ncon
                % f and g
                fg.fport(1,i) = (c(i)^2*dir(i)*rho(i))/(kappa(i)-1) + (Ma(i)*c(i)^2*kappa(i)*rho(i))/(kappa(i)-1);
                fg.gport(1,i) = (Ma(i)*c(i)^2*kappa(i)*rho(i))/(kappa(i)-1) - (c(i)^2*dir(i)*rho(i))/(kappa(i)-1);
                fg.fvol(1,i) = (s*V*c(i)*rho(i))/(A*(kappaVol-1));
                fg.gvol(1,i) = (s*V*c(i)*rho(i))/(A*(kappaVol-1));
                % entropy s/cp
                scp.fport(1,i) = Ma(i)*rho(i) + dir(i)*rho(i);
                scp.gport(1,i) = Ma(i)*rho(i) - dir(i)*rho(i);
                scp.sport(1,i) = -Ma(i)*c(i)*rho(i);
                scp.fvol(1,i) = (s*V*c(i)*rho(i))/(A*cVol^2);
                scp.gvol(1,i) = (s*V*c(i)*rho(i))/(A*cVol^2);
                scp.svol(1,i) = -(s*V*rhoVol)/A;
            end
            % transport property zeta
            for ii = 3:nwav
                if ~strcmp(sys.waves{ii},'s') && ~strncmp(sys.waves{ii},'omega',5)
                    if isfield(sys.Connection{find(Ma>=0,1,'first')},sys.waves{ii})
                        zetaValue = cellfun(@(x) x.(sys.waves{ii}),sys.Connection);
                        zetaVol = sys.Connection{find(Ma>=0,1,'first')}.(sys.waves{ii});
                    else
                        zetaValue = zeros(1,ncon);
                        zetaVol = 0;
                    end                        
                    for i = 1:ncon
                        zeta.(sys.waves{ii}).fport(1,i) = Ma(i)*rho(i)*zetaValue(i) + dir(i)*rho(i)*zetaValue(i);
                        zeta.(sys.waves{ii}).gport(1,i) = Ma(i)*rho(i)*zetaValue(i) - dir(i)*rho(i)*zetaValue(i);
                        zeta.(sys.waves{ii}).sport(1,i) = -Ma(i)*c(i)*rho(i)*zetaValue(i);
                        zeta.(sys.waves{ii}).zetaport(1,i) = Ma(i)*c(i)*rho(i);
                        zeta.(sys.waves{ii}).fvol(1,i) = (s*V*c(i)*rho(i)*zetaVol)/(A*cVol^2);
                        zeta.(sys.waves{ii}).gvol(1,i) = (s*V*c(i)*rho(i)*zetaVol)/(A*cVol^2);
                        zeta.(sys.waves{ii}).svol(1,i) = -(s*V*rhoVol*zetaVol)/A;
                        zeta.(sys.waves{ii}).zetavol(1,i) = (s*V*rhoVol)/A;
                    end
                end
            end

            %% build up combined in-out matrix
            % coefficient matrix
            fgMat = repmat(...
                [fg.fport,fg.gport;zeros(ncon-1,2*ncon)]...
                +...
                [   fg.fvol(1),                     zeros(1,ncon-1),                fg.gvol(1),                     zeros(1,ncon-1);...
                    -rho(1)*c(1)*ones(ncon-1,1),    diag(rho(2:end)'.*c(2:end)'),   -rho(1)*c(1)*ones(ncon-1,1),    diag(rho(2:end)'.*c(2:end)')    ],...
                [2,1]);
            iout1 = find(Ma>=0,1,'first');
            scpMat = [zeros(iout1-1,3*ncon);scp.fport,scp.gport,scp.sport;zeros(ncon-iout1,3*ncon)]...
                +...
                [	zeros(iout1-1,2*ncon),                                                                                          diag(ones(1,iout1-1)),      -ones(iout1-1,1),       zeros(iout1-1,ncon-iout1);...
                 	zeros(1,iout1-1),           scp.fvol(iout1),    zeros(1,ncon-1),    scp.gvol(iout1),    zeros(1,ncon-iout1),    zeros(1,iout1-1),           scp.svol(iout1),        zeros(1,ncon-iout1);...
                 	zeros(ncon-iout1,2*ncon),                                                                                       zeros(ncon-iout1,iout1-1),  -ones(ncon-iout1,1),    diag(ones(1,ncon-iout1))    ];
            for ii = 3:nwav
                if ~strcmp(sys.waves{ii},'s') && ~strncmp(sys.waves{ii},'omega',5)
                    zetaMat{ii} = [zeros(iout1-1,4*ncon);zeta.(sys.waves{ii}).fport,zeta.(sys.waves{ii}).gport,zeta.(sys.waves{ii}).sport,zeta.(sys.waves{ii}).zetaport;zeros(ncon-iout1,4*ncon)]...
                        +...
                        [	zeros(iout1-1,3*ncon),                                                                                                                                                                                  diag(ones(1,iout1-1)),      -ones(iout1-1,1),                       zeros(iout1-1,ncon-iout1);...
                            zeros(1,iout1-1),           zeta.(sys.waves{ii}).fvol(iout1),   zeros(1,ncon-1),    zeta.(sys.waves{ii}).gvol(iout1),   zeros(1,ncon-1),    zeta.(sys.waves{ii}).svol(iout1),   zeros(1,ncon-iout1),    zeros(1,iout1-1),           zeta.(sys.waves{ii}).zetavol(iout1),    zeros(1,ncon-iout1);...
                            zeros(ncon-iout1,3*ncon),                                                                                                                                                                               zeros(ncon-iout1,iout1-1),  -ones(ncon-iout1,1),                    diag(ones(1,ncon-iout1))    ]; %#ok<AGROW>
                end
            end
            CoeffMat(1:2*ncon,1:2*ncon) = fgMat;
            iscp = find(strcmp(sys.waves,'s'),1,'first');
            for ii = 3:nwav
                if ~strcmp(sys.waves{ii},'s') && ~strncmp(sys.waves{ii},'omega',5)
                    CoeffMat((ii-1)*ncon+(1:ncon),[1:2*ncon,(iscp-1)*ncon+(1:ncon),(ii-1)*ncon+(1:ncon)]) = zetaMat{ii};
                elseif strcmp(sys.waves{ii},'s')
                    CoeffMat((ii-1)*ncon+(1:ncon),[1:2*ncon,(ii-1)*ncon+(1:ncon)]) = scpMat;
                else % omega: identity --> all out are zero
                    CoeffMat((ii-1)*ncon+(1:ncon),(ii-1)*ncon+(1:ncon)) = diag(ones(1,ncon));
                end
            end
            % temporary system (for size and running multiport)
            nin = ncon+(nwav-2)*sum(Ma<0);
            nout = ncon+(nwav-2)*sum(Ma>=0) + nin;
            sys.A = zeros(0,0);sys.B = zeros(0,nin);sys.C = zeros(nout,0);sys.D = zeros(nout,nin);sys.E = zeros(0,0);
            sys = multiport(sys);
            for ii = 1:nwav
                for i = 1:ncon
                    allNames{(ii-1)*ncon+i,1} = [num2str(sys.Connection{i}.idx,'%02d'),sys.waves{ii}]; %#ok<AGROW>
                end
            end
            iin = cellfun(@(x) find(strcmp(allNames,x)),sys.u);
                auxy = sys.y;
                uyIndex = cellfun(@(x) strcmp(x(end-2:end),'_uy'), sys.y);
                auxy(uyIndex) = cellfun(@(x) x(1:end-3),auxy(uyIndex),'UniformOutput',false);
            iout = cellfun(@(x) find(strcmp(allNames,x)),auxy(~uyIndex));
            % output matrix
            OutMat = CoeffMat(iout,iout);
            % input matrix
            InMat = CoeffMat(iout,iin);
            % invert output matrix and multiply with negative input matrix
            sym_sys = linsolve(OutMat,-InMat(:,1));
            for i = 2:nin
                sym_sys = [sym_sys,linsolve(OutMat,-InMat(:,i))]; %#ok<AGROW>
            end
            sym_sys = [sym_sys;eye(numel(iin))];
            % convert to tf system
            [num,den]=numden(sym_sys);
            for i = 1:size(sym_sys,1)
                for ii = 1:size(sym_sys,2)
                    auxnum = sym2poly(num(i,ii));
                    auxden = sym2poly(den(i,ii));
                    tf_sys(i,ii) = tf(auxnum,auxden); %#ok<AGROW>
                end
            end

            %% set up system
            ss_sys = ss(tf_sys);

            sys.A = ss_sys.A;sys.B = ss_sys.B; sys.C = ss_sys.C; sys.D = ss_sys.D; sys.E = ss_sys.E;

            sys = multiport(sys);

            sys.uptodate = true;
        end
    end

    methods(Static)
        %% Overload solveMean
        % arbitrary number of connections, no propagation of Mach number
        function con = solveMean(con,fields)
            %% Get number and fields of connections
            n=length(con);
            if nargin<2 % if fields is given, these fields are explicitely propagated
                fields=[];
                for i=1:n
                    if not(isempty(con{i}))
                        fieldsn = fieldnames(con{i})';
                        fields = unique([fields,fieldsn]);
                    end
                end
                % Delete Mach, idx and dir from fields list
                fields(strcmp(fields,'Mach'))=[];
                fields(strcmp(fields,'idx'))=[];
                fields(strcmp(fields,'dir'))=[];
            end
            %% Propagate quantities
            for fieldc = fields
                field = char(fieldc);
                % generate vector from checkField
                chk=zeros(1,n);
                for i=1:n
                    chk(i)=Block.checkField(con{i},field);
                end
                vchk=find(chk);
                % check consistency
                if sum(chk)>1 % more than one entry
                    for i=vchk(2:end)
                        if not(Block.isequalAbs(con{vchk(1)}.(field),con{i}.(field)))
                            error('Mean values are inconsistent.');
                        end
                    end
                end
                % match
                if sum(chk)>0 % at least one entry
                    for i=1:n
                        con{i}.(field) = con{vchk(1)}.(field);
                    end
                end
            end
        end
        %% Add solveMeanMix
        % arbitrary number of connections; mixing of properties assuming
        % equal areas, ideal gas and const heat capacity; automatic OutPort
        % recognition
        function con = solveMeanMix(con)
            %% initialize
            % constants
            Rmol = 8314.4598; % J/(kmol K)
            % propagate areas
            con = junction.solveMean(con,{'A','pos'});
            % get number and fields of connections
            n = length(con);
            fields = [];
            for i=1:n
                if not(isempty(con{i}))
                    fieldsn = fieldnames(con{i})';
                    fields = unique([fields,fieldsn]);
                end
            end
            % delete idx and dir from fields list
            fields(strcmp(fields,'idx'))=[];
            fields(strcmp(fields,'dir'))=[];
            % delete alpha from fieldswoa list
            fieldswoa = fields;
            fieldswoa(strcmp(fieldswoa,'alpha'))=[]; % out because it can be determined afterwards
            % find fully defined, Mach-defined and In-connections
            chkMach = cellfun(@(x) Block.checkField(x,'Mach'), con);
            chkIn = false(size(chkMach));
            chkIn(chkMach) = cellfun(@(x) x.Mach<0, con(chkMach));
            chkOut = false(size(chkMach));
            chkOut(chkMach) = cellfun(@(x) x.Mach>=0, con(chkMach)); % explicit
            %% safe run
            % always propagate explicit Out, save and update all chk
            con(chkOut) = junction.solveMean(con(chkOut));
            consave = con;
            chkFull = cellfun(@(x) Block.checkPort({x},fields), con);
            chkFullwoa = cellfun(@(x) Block.checkPort({x},fieldswoa), con);
            chkMach = cellfun(@(x) Block.checkField(x,'Mach'), con);
            chkIn(chkMach) = cellfun(@(x) x.Mach<0, con(chkMach));
            chkOut(chkMach) = cellfun(@(x) x.Mach>=0, con(chkMach));
            % further determination impossible if:
            if sum(~chkMach)>1 ||... % more than one Mach number is missing or
                    sum(~chkFull & chkIn)>1 ||... % more than one InPort is still undefined or
                    (any(~chkFull & chkIn) && all(~chkFull(chkOut))) % all OutPorts and min one InPort are still undefined
                return
            end
            % compute known mass and mole flow rates, heat capacities, temperatures and pressure
            ip = find(chkFullwoa,1,'first');
            p = con{ip}.rho*con{ip}.c^2/con{ip}.kappa;
            mdot = nan(1,n);
            mdot(chkFullwoa) = cellfun(@(x) x.rho*x.Mach*x.c*x.A, con(chkFullwoa));
            ndot = nan(1,n);
            ndot(chkFullwoa) = cellfun(@(x) x.rho*x.Mach*x.c*x.A/x.Mmol, con(chkFullwoa));
            cp = nan(1,n);
            cp(chkFullwoa) = cellfun(@(x) x.kappa/(x.kappa-1)*Rmol/x.Mmol, con(chkFullwoa));
            T = nan(1,n);
            T(chkFullwoa) = cellfun(@(x) x.Mmol*x.c^2/(x.kappa*Rmol), con(chkFullwoa));
            if max(T(chkFullwoa))-min(T(chkFullwoa))>100
                warning('Mixing with high temperature difference contradicts assumption of constant heat capacities. Results may be inexact.');
            end
            alpha = nan(1,n);
            alpha(chkFull) = cellfun(@(x) x.alpha, con(chkFull));
            % check if a complete solution is consistent
            if all(chkFull) && (...
                    abs(sum(mdot))/max(abs(mdot))/numel(mdot)>1e2*eps ||... % mass conservation
                    abs(sum(ndot))/max(abs(ndot))/numel(ndot)>1e2*eps ||... % mole conservation (no chemical reactions!)
                    abs(sum(mdot.*cp.*T))/max(abs(mdot.*cp.*T))/numel(mdot)>1e2*eps ||... % energy conservation
                    abs(sum(alpha.*mdot))/max(abs(alpha.*mdot))/numel(mdot)>1e2*eps ... % mass conservation for fuel
                    )
                error('Mean values are inconsistent.');
            elseif all(chkFull)
                return
            end
            %% assume undefined (except alpha) Ports are either all Out or all In
            % compute missing mass flow rate
            mdotmiss = -sum(mdot(chkFullwoa));
            if mdotmiss == 0 && ~any(chkOut&chkFullwoa) % incomplete definition
                con = consave; % reset
            elseif mdotmiss == 0 % miss is out by definition
                iOutFullwoa = find(chkFullwoa&chkOut,1,'first'); % first fully defined OutPort
                Mmolmiss = con{iOutFullwoa}.Mmol;
                cpmiss = con{iOutFullwoa}.kappa/(con{iOutFullwoa}.kappa-1)*Rmol/con{iOutFullwoa}.Mmol;
                Tmiss = con{iOutFullwoa}.c^2/con{iOutFullwoa}.kappa/Rmol*con{iOutFullwoa}.Mmol;
%                 alphamiss = con{iOutFull}.alpha;
            else
                % compute missing mole flow rate, heat capacity and temperature
                ndotmiss = -sum(ndot(chkFullwoa));
                Mmolmiss = mdotmiss/ndotmiss;
                cpmiss = -sum(mdot(chkFullwoa).*cp(chkFullwoa))/mdotmiss;
                Tmiss = -sum(mdot(chkFullwoa).*cp(chkFullwoa).*T(chkFullwoa))/(mdotmiss*cpmiss);
%                 alphamiss = -sum(mdot(chkFull).*alpha(chkFull))/mdotmiss;
            end
            % compute connection properties from intermediate results
            iNotFullwoa = find(~chkFullwoa,1,'first'); % first not fully defined connection
            con{iNotFullwoa}.Mmol = Mmolmiss;
            con{iNotFullwoa}.kappa = cpmiss/(cpmiss-Rmol/con{iNotFullwoa}.Mmol);
            con{iNotFullwoa}.rho = p*con{iNotFullwoa}.Mmol/(Rmol*Tmiss);
            con{iNotFullwoa}.c = sqrt(con{iNotFullwoa}.kappa*Rmol*Tmiss/con{iNotFullwoa}.Mmol);
%             con{iNotFull}.alpha = alphamiss;
            % propagate all except alpha and Mach to all not fully defined connections
            con(~chkFullwoa) = junction.solveMean(con(~chkFullwoa),fieldswoa(~strcmp(fieldswoa,'Mach')));
            % solve continuity
            Machmiss = mdotmiss/(con{iNotFullwoa}.rho*con{iNotFullwoa}.c*con{iNotFullwoa}.A); % sum of all Mach numbers of formerly undefined connections
            con{~chkMach}.Mach = Machmiss - sum(cellfun(@(x) x.Mach, con(~chkFullwoa & chkMach)));
            %% check assumption
            % update chkIn and chkOut
            chkIn = cellfun(@(x) x.Mach<0, con);
            chkOut = cellfun(@(x) x.Mach>=0, con);
            if any(chkIn&~chkFullwoa) && any(chkOut&~chkFullwoa) % assumption was not met
                con = consave; % reset
            end
            % propagate explicit Out again to check consistency
            con(chkOut) = junction.solveMean(con(chkOut));
            %% compute alpha afterwards if possible
            chkFullNew = cellfun(@(x) Block.checkPort({x},fields), con);
            if all(chkFullNew) && abs(sum(alpha.*mdot))/max(abs(alpha.*mdot))/numel(mdot)>eps % mass conservation for fuel
                error('Mean values are inconsistent.');
            elseif ~all(chkFullNew) && (~any(chkIn(~chkFullNew)) || sum(~chkFullNew)==1) % still incomplete, but completion possible: all missing alphas are Out or there is only one (possibly In)
                mdot = cellfun(@(x) x.rho*x.Mach*x.c*x.A, con);
                alpha(chkFullNew) = cellfun(@(x) x.alpha, con(chkFullNew));
                alphamiss = -sum(mdot(chkFullNew).*alpha(chkFullNew))/sum(mdot(~chkFullNew));
                iNotFullNew = find(~chkFullNew,1,'first'); % first not fully defined connection
                con{iNotFullNew}.alpha = alphamiss;
                con(~chkFullNew) = junction.solveMean(con(~chkFullNew),{'alpha'}); % propagate alpha to all not fully defined connections
            end
        end

        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            if isfield(pars,'Volume')
                if iscell(pars.Volume)
                    sys.Volume = eval(cell2mat(pars.Volume));
                else
                    sys.Volume = pars.Volume;
                end
            else
                sys.Volume = 0;
            end
            if isfield(pars,'mix')
                if iscell(pars.mix)
                    sys.mix = strcmp('on',char(pars.mix));
                else
                    sys.mix = strcmp('on',pars.mix);
                end
            else
                sys.mix = false;
            end
            references = [];
        end
    end
end