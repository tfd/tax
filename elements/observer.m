classdef observer < AcBlock & sss
    % OBSERVER observer class to have nice plots of frequency responses.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = observer(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.property: f_g, p_u, impedance_admittance, intensity
    %                           property to be observered
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: observer object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de)
    % Last Change:  27 Mar 2015
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        property
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end
    
    methods
        function sys = observer(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(3*numel(pars.waves),numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            con = Block.readPort(pars,sys.Connection);
            sys = set_Connection(sys, con);
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = Block.solveMean(con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end

            D = eye(numel(sys.waves));
            D = [D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)
            switch sys.property
                case 'p_u'
                    auxD = [1*(sys.state.f.rho(1)*sys.state.f.c(1)), 1*(sys.state.g.rho(1)*sys.state.g.c(1)); 1, -1];
                    sys.y{2*numel(sys.waves)+1} = [sys.Name,'_p'];
                    sys.y{2*numel(sys.waves)+2} = [sys.Name,'_u'];
                case 'f_g'
                    auxD = [1, 0; 0, 1];
                    sys.y{2*numel(sys.waves)+1} = [sys.Name,'_f'];
                    sys.y{2*numel(sys.waves)+2} = [sys.Name,'_g'];
            end
            for i = 3:numel(sys.waves)
                sys.y{2*numel(sys.waves)+i} = [sys.Name,'_',sys.waves{i}];
            end
            sys.D = [...
                D;... % throughput
                auxD,zeros(2,numel(sys.waves)-2);... % named output of fg or pu
                zeros(numel(sys.waves)-2,2),eye(numel(sys.waves)-2)... % named output of waves
                ];
            
            sys = twoport(sys);
            sys.OutputGroup.Observer = 2*numel(sys.waves)+(1:numel(sys.waves));
            
            sys.uptodate = true;
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if iscell(pars.property)
                %% Create Block from Simulink getmodel()
                %% Load model
                sys.property = char(pars.property);
            else
                sys.property = pars.property;
            end
            references = [];
        end
    end
end