% Linearize syms
function [lin,varargout] = linSym(eqn,varargin)
if numel(varargin{1})>1
    varargin = varargin{1};
end
% build derivatives
for i = 1:nargin-1
    % make fluctuation variables
    varargout{i} = sym([char(varargin{i}),'_prime']); %#ok<AGROW>
    fluct(i,1) = varargout{i}; %#ok<AGROW>
    % differentiate (check for cell)
    if iscell(varargin)
        dvec(:,i) = diff(eqn,varargin{i}); %#ok<AGROW>
    else
        dvec(:,i) = diff(eqn,varargin(i)); %#ok<AGROW>
    end
end
lin = dvec*fluct;
