% expandExcept
function expr = expandExcept(expr,exc)
for i = 1:numel(exc)
    auxVar(i) = sym(['aux_' num2str(i)]); %#ok<AGROW>
end
expr = subs(expr,exc,auxVar);
expr = expand(expr);
expr = subs(expr,auxVar,exc);
end