function file = setupFluidFile(base,p,T)
    file = base;
    file.p.value = reshape(p,1,numel(p));
    file.p.unit = 'Pa';
    file.T.value = reshape(T,numel(T),1);
    file.T.unit = 'K';
    if iscell(file.name)
        try
            hs0 = CoolProp.PropsSI('H','T',file.T0.value,'P',file.p0.value,file.name{1}); % T in K, p in Pa!
            name = file.name{1};
        catch
            try
                hs0 = CoolProp.PropsSI('H','T',file.T0.value,'P',file.p0.value,file.name{2}); % T in K, p in Pa!
                name = file.name{2};
            catch
                hs0 = CoolProp.PropsSI('H','T',file.T0.value,'P',file.p0.value,file.name{3}); % T in K, p in Pa!
                name = file.name{3};
            end
        end
    else
        hs0 = CoolProp.PropsSI('H','T',file.T0.value,'P',file.p0.value,file.name); % T in K, p in Pa!
        name = file.name;
    end
    % compute sensible enthalpies
    file.hs.value = nan(numel(T),numel(p));
    for i = 1:numel(T)
        for ii = 1:numel(p)
            try %#ok<TRYNC>
                file.hs.value(i,ii) = CoolProp.PropsSI('H','T',T(i),'P',p(ii),name) - hs0;
            end
        end
    end
    file.hs.unit = 'J/kg';
end