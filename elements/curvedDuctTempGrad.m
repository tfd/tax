classdef curvedDuctTempGrad < AcBlock & sss
    % curvedDuctTempGrad Acoustic model of a Duct with varying cross-
    % section and mean temperature gradient. Details on the derivation of
    % the model equation and a description of the implementation can be
    % found in SchaePolif2019
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = curvedDuctTempGrad(pars);
    % Input:   * pars.Name: string of name of the chokedExit
    %          * pars.l:    length of duct
    %          * pars.fMax: maximum frequency to be resolved by duct
    %          * pars.order: order of upwind discretization (1-3)
    %          * pars.minres: minimal resolution of longest wavelength
    %          * pars.Mach: Mach number
    %          * pars.c: speed of sound
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: curvedDuctTempGrad object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felicitas
    % Schaefer (schaefer@tfd.mw.tum.de)
    % Last Change:  01 Aug 2019
    % ------------------------------------------------------------------
    % See also: Duct, symLZeta, AcBlock.Port, AcBlock, Block, simpleDuct
    
    properties
        % length of duct
        l
        % order of upwind discretization (1-3)
        order
        % minimal resolution of longest wavelength
        minres
        % maximum frequency to be resolved by duct
        fMax
        % positions of sensors inside the duct
        sensorPositions
        % overwrite propagation velocities
        cOver
        % Cross section of the curvedDuctTempGrad as function of x
        A_ratio
        % Mean temperature along the curvedDuctTempGrad as function of x
        T_ratio
        % allow propagation of waves
        activeProp = struct('f',true,'g',true,'alpha',false,'s',false)
    end
    properties (SetAccess = protected) % long range effects! 'protected' is a marker so changeParam cannot change it without updating the entire system
        
        % checks if T or A are defined via a data file or an explicit function
        FunDef = struct('T',true,'A',true)
        % Struct of velocity, cross section, density and speed of sound MeanField
        MeanField
    end
    properties(Dependent)
        % spacial discretization
        dX
        % number of spatial discretization points
        N
        % propagation velocity for all waves
        cProp
    end
    
    methods
        function sys = curvedDuctTempGrad(pars)
            % Call empty constructors with correct in and output dimension
            % and port number
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            sys@sss(zeros(2,2));
            
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            % to allow BCs via pars:
            con = Block.readPort(pars,sys.Connection);
            sys = set_Connection(sys, con);
        end

        %% Set functions
        function sys = set.l(sys, l)
            if not(isequal(sys.l, l))
                sys.l = l;
                sys.uptodate = false;
            end
        end
        function sys = set.sensorPositions(sys, sensorPositions)
            if not(isequal(numel(sys.sensorPositions), numel(sensorPositions))) ||...
                    not(all(isequal(sys.sensorPositions, sensorPositions)))
                sys.sensorPositions = sensorPositions;
                sys.uptodate = false;
            end
        end
        function sys = set.order(sys, order)
            if not(isequal(sys.order, order))
                if ((order<=3) && (0<=order))||(order==Inf)
                    sys.order = order;
                    sys.uptodate = false;
                else
                    warning(['Order out of range [1-3, Inf]:',num2str(order)])
                    sys.order = order;
                    sys.uptodate = false;
                end
            end
        end
        function sys = set.minres(sys, minres)
            if not(isequal(sys.minres, minres))
                sys.minres = minres;
                sys.uptodate = false;
            end
        end
        function sys = set.fMax(sys, fMax)
            if not(isequal(sys.fMax, fMax))
                sys.fMax = fMax;
                sys.uptodate = false;
            end
        end
        function sys = set.cOver(sys, cOver)
            if not(isequal(sys.cOver, cOver))
                sys.cOver = cOver;
                sys.uptodate = false;
            end
        end
        function sys = set.activeProp(sys, activeProp)
            fnames = fieldnames(activeProp);
            for i = 1:numel(fnames)
                field = fnames{i};
                if not(isequal(sys.activeProp.(field), activeProp.(field)))
                    sys.activeProp = activeProp;
                    sys.uptodate = false;
                end
            end
        end
        
        %% Get functions
        function N = get.N(sys)
            MeanField = getMeanField(sys,1e2);
            [umin, i_umin] = min(MeanField.u);
            [cmin, i_cmin] = min(MeanField.c);
            try
                u_min = 2*umin-MeanField.u(i_umin+1);
            catch
                u_min = 2*umin-MeanField.u(i_umin-1);
            end
            try
                c_min = 2*cmin-MeanField.c(i_cmin+1);
            catch
                c_min = 2*cmin-MeanField.c(i_cmin-1);
            end
            cProp_min.f = u_min+c_min;
            cProp_min.g = u_min-c_min;
            cProp_min.alpha = u_min;
            cProp_min.s = u_min;
            for field = sys.waves'
                lambdaMin = 1/sys.fMax.(char(field))*abs(cProp_min.(char(field)));
                if lambdaMin==0
                    N.(char(field)) = 0;
                else
                    dXmax = lambdaMin/sys.minres;
                    N.(char(field)) = ceil(sys.l/dXmax);
                end
            end
            % enforce same discretization for f and g
            N.f = max(N.f,N.g);
            N.g = N.f;
        end
        
        function MeanField = get.MeanField(sys)
            MeanField = getMeanField(sys,sys.N.f+1);
        end
        
        function cProp = get.cProp(sys)
            MeanField = sys.MeanField;
            for wavec = sys.waves'
                wave = char(wavec);
                if strcmp(wave,'f'); cProp.f = MeanField.u+MeanField.c; %#ok<ALIGN>
                elseif strcmp(wave,'g'); cProp.g = MeanField.u-MeanField.c;
                else; cProp.(wave) = MeanField.u; end % default
            end
            % overwrite option
            Overwrite = fieldnames(sys.cOver);
            for i = 1:numel(Overwrite)
                if isfield(cProp,Overwrite{i}) && ~isempty(sys.cOver.(Overwrite{i}))
                    if sign(sys.cOver.(Overwrite{i}))==sign(Mach)
                        cProp.(Overwrite{i}) = sys.cOver.(Overwrite{i});
                    else
                        warning(['Upstream propagation of any other wave than g is not implemented yet. Omitting to overwrite sys.cProp.',Overwrite{i},'!']);
                    end
                end
            end
        end
        function dX = get.dX(sys)
            N = sys.N;
            l = sys.l;
            for field = sys.waves'
                if N.(char(field))>0
                    dX.(char(field)) = l/N.(char(field));
                else
                    dX.(char(field)) = 0;
                end
            end
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            % if by the time this code runs one position is still missing,
            % makeGeo was not run --> we may invent stuff: x-direction only
            if isempty(con{1}.pos) && ~isempty(con{2}.pos)
                con{1}.pos = con{2}.pos - [sys.l,0,0];
            elseif isempty(con{2}.pos) && ~isempty(con{1}.pos)
                con{2}.pos = con{1}.pos + [sys.l,0,0];
            end
            % exception for position
            savePos = con{2}.pos;
            con{2}.pos = con{1}.pos;
            % solve
            newcon = solveMean(sys,con);
            newcon{2}.pos = savePos; % insert exception
            sys.Connection = newcon; % execute exception after newcon is already as correct as possible, so update is not called unnecessarily often
            % update
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Get mean field properties
        function MeanField = getMeanField(sys,N)
            c = sys.Connection{1}.c;
            u = sys.Connection{1}.dir*sys.Connection{1}.Mach*c;
            rho = sys.Connection{1}.rho;
            A = sys.Connection{1}.A;
            kappa = sys.Connection{1}.kappa;
            R = 8.314459/sys.Connection{1}.Mmol*1000;
            T0 = c^2/(sys.Connection{1}.kappa*R);
            
            T_vec=ones(1,N)*T0;
            switch sys.FunDef.T
                case 'Enter Function'
                    xx = linspace(0,sys.l,N);
                    for kk=1:N
                        %Evaluate the area function given in terms of x
                        x = xx(kk); %#ok<NASGU>
                        T_vec(kk) = eval(sys.T_ratio)*T0;
                    end
                case 'Load Data'
                    xx = linspace(0,sys.l,N);
                    x_Dat = sys.T_ratio(:,1);
                    T_Dat = sys.T_ratio(:,2);
                    T_vec = interp1(x_Dat,T_Dat,xx);
            end
            const = sys.Connection{1}.c/sqrt(T_vec(1));
            c_vec = const*arrayfun(@(T) sqrt(T),T_vec);
            A_vec=ones(1,N).*sys.Connection{1}.A;
            switch sys.FunDef.A
                case 'Enter Function'
                    xx = linspace(0,sys.l,N);
                    for kk=1:N
                        %Evaluate the area function given in terms of x
                        x = xx(kk); %#ok<NASGU>
                        A_vec(kk) = eval(sys.A_ratio)*A;
                    end
                case 'Load Data'
                    xx = linspace(0,sys.l,N);
                    x_Dat = sys.A_ratio(:,1);
                    A_Dat = sys.A_ratio(:,2);
                    A_vec = interp1(x_Dat,A_Dat,xx);
            end
            
            if any(T_vec(:)<0)
                error('Found temperatures values below zero. Please check temperature function!');
            elseif any(A_vec(:)<0)
                error('Found invalid values for cross-section area. Values must be positive. Please check area function!');
            end
            
            ufun =@(u_vec) u*u_vec.^2-(u^2+c^2/kappa)*u_vec+c^2/kappa*u*T_vec./T0;
            options = optimset('Display','off');
            u_vec = fsolve(ufun,u*ones(size(T_vec)),options);                   % Calculation of u_vec from momentum and mass equation (see Li&Morgans(2017), Appendix A)
            if u>0
                rho_vec = rho*u*A_vec(1)./(u_vec.*A_vec);                       % rho_vec from mass equation
                p_vec= rho_vec.*R.*T_vec;                                       % p_vec from ideal gas law
            else
                p_vec = ones(size(A_vec))*rho*R*T_vec(1);                       % if no mean flow: dp/dx = 0
                rho_vec = p_vec./(R.*T_vec);                                    % rho_vec from ideal gas law
            end
            
            % Save mean field in structure
            MeanField.p = p_vec;
            MeanField.A = A_vec;
            MeanField.c = c_vec;
            MeanField.u = u_vec;
            MeanField.rho = rho_vec;
            MeanField.T = T_vec;
        end
        
        function sys = update(sys)
            %% Check if system is uptodate
            if sys.uptodate
                return
            end
            sys= sys.clear();
            
            % curvedDuctTempGrad resolution
            N = sys.N; %#ok<*PROP>
            dX = sys.dX;
            MeanField = sys.MeanField;
            A_vec = MeanField.A;
            u_vec = MeanField.u;
            c_vec = MeanField.c;
            rho_vec = MeanField.rho;
            p_vec = MeanField.p;
            diff=2*sys.l/(N.f+1);
            
            
            %% Run through all wave types
            % Limit the sensor positions
            if any(sys.sensorPositions>sys.l)||any(sys.sensorPositions<0)
                error([sys.Name,': Sensor positions out of curvedDuctTempGrad.'])
            end
            sensN = numel(sys.sensorPositions);
            % Initialization
            fields = {}; % fields are all waves that are active in the duct
            for wavec = sys.waves'
                wave = char(wavec);
                if isfield(sys.activeProp,wave) && sys.activeProp.(wave)
                    fields = [fields;{wave}]; %#ok<AGROW>
                end
            end
            cProp = sys.cProp; % wave propagation velocity
            allN = cellfun(@(x) N.(x),fields);
            n = sum(max(allN,1)+1)+numel(fields)*sensN; % for output only, including _uy terms
            Disc = sparse(0,0); % initialize A
            B = sparse(0,0); % initialize B
            C = sparse(n,0); % initialize C
            D = sparse(n,numel(fields)); % initialize D
            % Loop
            for i = 1:numel(fields)
                if cProp.(fields{i}) == 0
                    %% No convection (propagation velocity is zero, therefore also D)
                    Disc_new = sparse(0,0);
                    Bnew = sparse(0,1);
                    Cnew = sparse(n,0);
                    Dnew = sparse(n,1); % just for existence, does nothing
                elseif allN(i) == 0
                    %% curvedDuctTempGrad is compact with respect to wavelength
                    Disc_new = sparse(0,0);
                    Bnew = sparse(0,1);
                    Cnew = sparse(n,0);
                    Dnew = sparse(n,1); % just for existence, does nothing
                    if strcmp(fields{i},'f')
                        D([i+1,i+numel(fields)],i) = 1; %#ok<SPRIX>
                    elseif strcmp(fields{i},'g')
                        D([i-1,i+numel(fields)],i) = 1; %#ok<SPRIX>
                    else
                        D([i,i+numel(fields)],i) = 1; %#ok<SPRIX>
                    end
                    if ~isempty(sys.sensorPositions) % position can only be zero
                        D(sum(allN+1)+i,i) = 1; %#ok<SPRIX>
                    end
                else
                    %% Input is on the boundary and acting on the time derivative of the next stencil by spatial derivative
                    if (sys.order==1)||(allN(i)==1)
                        [Disc_new,Bnew] = curvedDuctTempGrad.getD(cProp.(fields{i}),dX.(fields{i}),allN(i),1);
                    elseif (sys.order==2)||(allN(i)==2)||(allN(i)==3)
                        [Disc_new,Bnew] = curvedDuctTempGrad.getD(cProp.(fields{i}),dX.(fields{i}),allN(i),2);
                    elseif (sys.order==3)
                        [Disc_new,Bnew] = curvedDuctTempGrad.getD(cProp.(fields{i}),dX.(fields{i}),allN(i),3);
                    else
                        [Disc_new,Bnew] = curvedDuctTempGrad.getD(cProp.(fields{i}),dX.(fields{i}),allN(i),Inf);
                    end
                    %% Output is directly the value on the respective stencils
                    % Keep strange order for f and g (legacy for compatibility)
                    forward = (cProp.(fields{i})(1)>0);
                    if strcmp(fields{i},'f')
                        Cnew = sparse(i+1,allN(i),1,n,allN(i)); % connection
                        internal = sparse( 2*numel(fields)+(1:2:2*allN(i)-2), 1:allN(i)-1, 1, n, allN(i) ); % internal
                    elseif strcmp(fields{i},'g')
                        Cnew = sparse(i-1,1,1,n,allN(i)); % connection
                        internal = sparse( 2*numel(fields)+(2:2:2*allN(i)-2), 2:allN(i),   1, n, allN(i) ); % internal
                    else
                        Cnew = sparse(i,1+forward*(allN(i)-1),1,n,allN(i)); % connection
                        internal = sparse( 2*numel(fields)+sum(allN(1:i-1)-1)+(1:allN(i)-1), (2:allN(i))-forward, 1, n, allN(i) ); % internal
                    end
                    Cnew = Cnew + internal;
                    Dnew = sparse(i+numel(fields),1,1,n,1); % initialize with input connection, works independently from wave type!
                    %% Compute sensor outputs
                    % Determine sensor indices from position and adjacent stencils
                    idXSense = sys.sensorPositions/dX.(fields{i});
                    idXLow = floor(idXSense);
                    idXHigh = ceil(idXSense);
                    % Interpolation factor
                    alpha = idXSense-idXLow;
                    % Limit sensor interpolation indices to stencils that are entirely inside the duct
                    idXLow(idXLow<0) = 0; % against numerical errors
                    idXHigh(idXHigh>N.(fields{i})) = N.(fields{i}); % against numerical errors
                    % Compute and append outputs
                    if sensN > 0
                        index1 = idXLow+~forward>=1 & idXHigh+~forward<=N.(fields{i});
                        Csense =    sparse( sum(allN+1)+sensN*(i-1)+find(index1), idXHigh(index1)+~forward,   alpha(index1),      n,  allN(i)) +...
                            sparse( sum(allN+1)+sensN*(i-1)+find(index1), idXLow(index1)+~forward,    (1-alpha(index1)),  n,  allN(i));
                        Dsense =    sparse(                                                                                       n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index2 = ~index1 & idXLow==idXHigh & idXLow+~forward==0;
                        Csense =    sparse(                                                                                       n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index2), 1,                          1,                  n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index3 = ~index1 & ~index2 & idXLow+~forward==0;
                        Csense =    sparse( sum(allN+1)+sensN*(i-1)+find(index3), idXHigh(index3)+~forward,   alpha(index3),      n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index3), 1,                          (1-alpha(index3)),  n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index4 = ~index1 & ~index2 & ~index3 & idXLow==idXHigh & idXHigh+~forward==N.(fields{i})+1;
                        Csense =    sparse(                                                                                       n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index4), 1,                          1,                  n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index5 = ~index1 & ~index2 & ~index3 & ~index4 & idXHigh+~forward==N.(fields{i})+1;
                        Csense =    sparse( sum(allN+1)+sensN*(i-1)+find(index5), idXLow(index5)+~forward,    (1-alpha(index5)),  n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index5), 1,                          alpha(index5),      n,  1);
                        if ~all(1 == index1 + index2 + index3 + index4 + index5)
                            error('Sensor assignment problem!');
                        end
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                    end
                end
                Disc = blkdiag(Disc,Disc_new);
                B = blkdiag(B,Bnew);
                C = [C,Cnew]; %#ok<AGROW>
                D(:,i) = D(:,i) + Dnew; %#ok<SPRIX>
            end
            
            S = curvedDuctTempGrad.getS(rho_vec, c_vec, u_vec, A_vec, p_vec, sys.Connection{1}.kappa, diff,N.f);
            A = Disc + S; % System matrix = discretization matrix + summation matrix
            %% Populate continuous time system matrices
            sys = sys.updatesss(sss(A,B,C,D,[],0));
            
            id = num2str(sys.Connection{1}.idx,'%02d');
            for i = 1:numel(fields)
                forward = (cProp.(fields{i})(1)>0);
                sys.StateGroup.(char([fields{i},'_',id])) = sum(allN(1:i-1))+(2:allN(i))-forward;
            end
            
            %% Give names to sensor outputs
            sys.OutputGroup.Sensor = [];
            for i = 1:numel(fields)
                for ii = 1:sensN
                    sys.y{sum(max(allN,1)+1)+sensN*(i-1)+ii} = [fields{i},'_',sys.Name,'@',num2str(sys.sensorPositions(ii)','%G')];
                    sys.OutputGroup.Sensor = [sys.OutputGroup.Sensor, sum(max(allN,1)+1)+sensN*(i-1)+ii];
                end
            end
            
            % Give names to the ports and create in- and output groups
            sys = twoport(sys);
            
            %% Give names to internal state outputs
            format =['%0' ,num2str(ceil(log10(2*max(allN)-2))),'d' ];
            for i = 1:numel(fields)
                %                 forward = (cProp.(fields{i})>0);
                if strcmp(fields{i},'f') % odd
                    for ii = 2*numel(fields)+(1:2:N.f+N.g-2)
                        sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),'f','_',sys.Name,'_',num2str(ceil((ii-2*numel(fields))/2+1),format)];
                        sys.OutputGroup.f = [sys.OutputGroup.f(1:end-1),ii,sys.OutputGroup.f(end)];
                    end
                elseif strcmp(fields{i},'g') % even
                    for ii = 2*numel(fields)+(2:2:N.f+N.g-2)
                        sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),'g','_',sys.Name,'_',num2str(ceil((ii-2*numel(fields))/2+1),format)];
                        sys.OutputGroup.g = [sys.OutputGroup.g(1:end-1),ii,sys.OutputGroup.g(end)];
                    end
                else % regular
                    for ii = 2*numel(fields)+sum(allN(1:i-1)-1)+(1:N.(fields{i})-1)
                        %                         sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),fields{i},'_',sys.Name,'_',num2str(ii-2*numel(fields)-sum(allN(1:i-1)-1)+~forward,format)];
                        sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),fields{i},'_',sys.Name,'_',num2str(ii-2*numel(fields)-sum(allN(1:i-1)-1)+1,format)];
                        sys.OutputGroup.(fields{i}) = [sys.OutputGroup.(fields{i})(1:end-1),ii,sys.OutputGroup.(fields{i})(end)];
                    end
                end
            end
            
            %% Populate plotting quantities
            for i = 1:numel(fields)
                if ~isinf(dX.(fields{i})) && dX.(fields{i})>0
                    sys.state.(fields{i}).x = 0:dX.(fields{i}):sys.l;
                else % no discretization
                    sys.state.(fields{i}).x = [0,0];
                end
                len = length(sys.state.(fields{i}).x);
                sys.state.(fields{i}).idx   = linspace(0,1,len);
                sys.state.(fields{i}).rho	= rho_vec;
                sys.state.(fields{i}).c     = c_vec;
                sys.state.(fields{i}).Mach	= u_vec./c_vec;
                sys.state.(fields{i}).A     = A_vec;
                sys.state.(fields{i}).kappa = ones(1,len)*sys.Connection{1}.kappa;
                sys.state.(fields{i}).Mmol	= ones(1,len)*sys.Connection{1}.Mmol;
                sys.state.(fields{i}).alpha	= ones(1,len)*sys.Connection{1}.alpha;
                sys.state.(fields{i}).pos	= sys.Connection{1}.pos.' + (sys.Connection{2}.pos-sys.Connection{1}.pos).'*sys.state.(fields{i}).idx;
            end
            
            sys.uptodate = true;
        end
        
        %% Determine time step according to CFL number for time simulation
        function Ts = CFLtoTs(sys, cfl)
            c = sys.Connection{1}.c;
            Mach = -sys.Connection{1}.Mach;
            u= Mach*c;
            
            Ts = cfl*sys.dX/(u+c);
        end
        
        %% mean solve algorithm (check for identity)
        function con = solveMean(sys,con)
            fields1=[];fields2=[];
            if not(isempty(con{1}))
                fields1 = fieldnames(con{1})';
            end
            if not(isempty(con{2}))
                fields2 = fieldnames(con{2})';
            end
            fields = unique([fields1,fields2]);
            
            %% Swap sign of downstream velocity
            % Downstream Block of connection has a velocity pointing
            % inwards (negative)
            if Block.checkField(con{2},'Mach')
                con{2}.Mach = -con{2}.Mach;
            end
            if strcmp(sys.FunDef.T,'Load Data')
                T_Dat = sys.T_ratio(:,2);
                T_rat = T_Dat(end)/T_Dat(1);
            else
                x=sys.l; %#ok<NASGU>
                T_rat = eval(sys.T_ratio);
            end
            if strcmp(sys.FunDef.A,'Load Data')
                A_Dat = sys.A_ratio(:,2);
                A_rat = A_Dat(end)/A_Dat(1);
            else
                x=sys.l; %#ok<NASGU>
                A_rat = eval(sys.A_ratio);
            end
            
            %% Propagate quantities
            for fieldc = fields
                field = char(fieldc);
                if strcmp(field,'A') % special treatment for area
                    %% Propagate area
                    if Block.checkField(con{1},'A')&&Block.checkField(con{2},'A')
                        if abs(A_rat*con{1}.A-con{2}.A)>1e-6
                            error('Mean values are inconsistent.')
                        end
                    elseif Block.checkField(con{1},'A')
                        con{2}.A = A_rat*con{1}.A;
                    elseif Block.checkField(con{2},'A')
                        con{1}.A = con{2}.A/A_rat;
                    end
                elseif strcmp(field,'c') % special treatment for speed of sound
                    %% Propagate speed of sound
                    if Block.checkField(con{1},'c')&&Block.checkField(con{2},'c')
                        if not(Block.isequalAbs(con{1}.c*sqrt(T_rat),con{2}.c,1e-6))
                            error('Mean values are inconsistent.')
                        end
                    elseif Block.checkField(con{1},'c')
                        con{2}.c = con{1}.c*sqrt(T_rat);
                    elseif Block.checkField(con{2},'c')
                        con{1}.c = con{2}.c/sqrt(T_rat);
                    end
                elseif strcmp(field,'rho') % special treatment for density
                    %% Propagate density
                    if isequal(con{1}.Mach, 0)||isequal(con{2}.Mach, 0)
                        if Block.checkField(con{1},'rho')&&Block.checkField(con{2},'rho')
                            if not(Block.isequalAbs(con{1}.rho/T_rat,con{2}.rho,1e-6))
                                error('Mean values are inconsistent.')
                            end
                        elseif Block.checkField(con{1},'rho')
                            con{2}.rho = con{1}.rho/T_rat;
                        elseif Block.checkField(con{2},'rho')
                            con{1}.rho = con{2}.rho*T_rat;
                        end
                    else
                        if Block.checkField(con{1},'Mach')
                            c1 = con{1}.c;
                            kappa = con{1}.kappa;
                            u1 = abs(con{1}.Mach)*c1;
                            u2 = 1/2*((u1+c1^2/(u1*kappa))-sqrt((u1+c1^2/(u1*kappa))^2-4*c1^2/kappa*T_rat));
                            u_rat = u2/u1;
                        elseif Block.checkField(con{2},'Mach')
                            c2 = con{2}.c;
                            kappa = con{2}.kappa;
                            u2 = abs(con{2}.Mach)*c2;
                            u1 = 1/2*((u2+c2^2/(u2*kappa))-sqrt((u2+c2^2/(u2*kappa))^2-4*c2^2/kappa/T_rat));
                            u_rat = u2/u1;
                        end
                        if Block.checkField(con{1},'rho')&&Block.checkField(con{2},'rho')
                            if not(Block.isequalAbs(con{1}.rho/A_rat/u_rat,con{2}.rho,1e-6))
                                error('Mean values are inconsistent.')
                            end
                        elseif Block.checkField(con{1},'rho')
                            con{2}.rho = con{1}.rho/A_rat/u_rat;
                        elseif Block.checkField(con{2},'rho')
                            con{1}.rho = con{2}.rho*A_rat*u_rat;
                        end
                    end
                elseif strcmp(field,'Mach') % special treatment for Mach number
                    %% Propagate Mach number
                    if not(isequal(con{1}.Mach,0) || isequal(con{2}.Mach,0))
                        if Block.checkField(con{1},'Mach')&&Block.checkField(con{2},'Mach')
                            c1 = con{1}.c;
                            kappa = con{1}.kappa;
                            u1 = abs(con{1}.Mach)*c1;
                            u2 = 1/2*((u1+c1^2/(u1*kappa))-sqrt((u1+c1^2/(u1*kappa))^2-4*c1^2/kappa*T_rat));
                            if not(Block.isequalAbs(u2/u1/sqrt(T_rat)*con{1}.Mach,con{2}.Mach,1e-6))
                                error('Mean values are inconsistent.')
                            end
                        elseif Block.checkField(con{1},'Mach')
                            c1 = con{1}.c;
                            kappa = con{1}.kappa;
                            u1 = abs(con{1}.Mach)*c1;
                            u2 = 1/2*((u1+c1^2/(u1*kappa))-sqrt((u1+c1^2/(u1*kappa))^2-4*c1^2/kappa*T_rat));
                            con{2}.Mach = u2/u1/sqrt(T_rat)*con{1}.Mach;
                        elseif Block.checkField(con{2},'Mach')
                            c1 = con{2}.c;
                            kappa = con{2}.kappa;
                            u1 = abs(con{2}.Mach)*c1;
                            u2 = 1/2*((u1+c1^2/(u1*kappa))-sqrt((u1+c1^2/(u1*kappa))^2-4*c1^2/kappa/T_rat));
                            con{1}.Mach = u2/u1*sqrt(T_rat)*con{2}.Mach;
                        end
                    else
                        if Block.checkField(con{1},'Mach')&&Block.checkField(con{2},'Mach')
                            if not(Block.isequalAbs(con{1}.Mach,con{2}.Mach,1e-6))
                                error('Mean values are inconsistent.')
                            end
                        elseif Block.checkField(con{1},'Mach')
                            con{2}.Mach = con{1}.Mach;
                        elseif Block.checkField(con{2},'Mach')
                            con{1}.Mach = con{2}.Mach;
                        end
                    end
                else
                    %% Check for consistency or match quantities
                    if Block.checkField(con{1},field)&&Block.checkField(con{2},field)
                        if (not(Block.isequalAbs(con{1}.(field),con{2}.(field),1e-6))) && (not(strcmp(field,'idx')||strcmp(field,'dir')))
                            error('Mean values are inconsistent.')
                        end
                    elseif Block.checkField(con{1},field)
                        con{2}.(field) = con{1}.(field);
                    elseif Block.checkField(con{2},field)
                        con{1}.(field) = con{2}.(field);
                    end
                end
            end
            
            %% (Re)swap sign of downstream velocity
            if Block.checkField(con{2},'Mach')
                con{2}.Mach = -con{2}.Mach;
            end
        end
        
    end
    
    methods(Static)
        
        %% Discretization matrices / System Matrix
        % State vector does not contain f0 and gn, because they are
        % inputs to the system and therefore not part of state!
        % Opposed to that g0 and fn are outputs and therefore these
        % states exist.
        %       f1
        %       f2
        %  x =  ...
        %       fn=fd
        %       g0=gu
        %       g1
        %       ...
        %       gn-1
        function [A,B] = getD(c_vec,dX,N,order)
            %% Input is on the boundary and acting on the time derivative of the next
            % stencil by spatial derivative
            if ~isinf(order)
                %% internal
                if c_vec(1)>0 % positive wave propagation
                    scheme = -ceil((order+1)/2):ceil((order-2)/2);
                else % negative wave propagation
                    scheme = -ceil((order-2)/2):ceil((order+1)/2);
                end
                % calc and scale coefficients
                coeff = Duct.getCoeff(dX,scheme);
                if c_vec(1)>0 % positive wave propagation
                    % sort coefficients to diagonal (0) and minor diagonals
                    % matrix f
                    auxA = arrayfun(@(cf)...
                        sparse(max(1,1-cf):min(N,N-cf), max(1,1+cf):min(N,N+cf), -c_vec(max(2,2-cf):min(N+1,N+1-cf)).*coeff(scheme==cf), N, N),...
                        scheme, 'UniformOutput', false);
                else
                    % sort coefficients to diagonal (0) and minor diagonals
                    % matrix g
                    auxA = arrayfun(@(cf)...
                        sparse(max(1,1-cf):min(N,N-cf), max(1,1+cf):min(N,N+cf), -c_vec(max(1,1-cf):min(N,N-cf)).*coeff(scheme==cf), N, N),...
                        scheme, 'UniformOutput', false);
                end
                A = auxA{1};
                for i = 2:order+1
                    A = A + auxA{i};
                end
                B = sparse(N,1);
                if c_vec(1)>0 % positive wave propagation
                    B(1,1) = -c_vec(2)*coeff(scheme==min(scheme)); % fu first input
                else % negative wave propagation
                    B(N,1) = -c_vec(N)*coeff(scheme==max(scheme)); % gd second input
                end
                %% boundary
                if c_vec(1)>0 % positive wave propagation
                    inscheme = scheme; % input
                    line = -min(scheme)-1; % last line requiring manipulation because of input
                    while min(inscheme)<-1
                        inscheme = inscheme+1;
                        coeff = Duct.getCoeff(dX,inscheme);
                        A(line,:) = sparse(ones(1,order),1:order,-c_vec(line+1)*coeff(2:end),1,N); % coeff(2:end) requires the scheme to be sorted in increasing order!
                        B(line+1,1) = B(line,1); %#ok<SPRIX> % shift!
                        B(line,1) = -c_vec(line)*coeff(1); %#ok<SPRIX>
                        line = line-1;
                    end
                    outscheme = scheme; % output
                    line = N-max(scheme)+1; % first line requiring manipulation because of output
                    while max(outscheme)>0
                        outscheme = outscheme-1;
                        coeff = Duct.getCoeff(dX,outscheme);
                        A(line,:) = sparse(ones(1,order+1),N-order:N,-c_vec(line)*coeff,1,N); % coeff requires the scheme to be sorted in increasing order!
                        line = line+1;
                    end
                else % negative wave propagation
                    outscheme = scheme; % output
                    line = -min(scheme); % last line requiring manipulation because of output
                    while min(outscheme)<0
                        outscheme = outscheme+1;
                        coeff = Duct.getCoeff(dX,outscheme);
                        A(line,:) = sparse(ones(1,order+1),1:order+1,-c_vec(line)*coeff,1,N); % coeff requires the scheme to be sorted in increasing order!
                        line = line-1;
                    end
                    inscheme = scheme; % input
                    line = N-max(scheme)+2; % first line requiring manipulation because of input
                    while max(inscheme)>1
                        inscheme = inscheme-1;
                        coeff = Duct.getCoeff(dX,inscheme);
                        A(line,:) = sparse(ones(1,order),N-order+1:N,-c_vec(line)*coeff(1:end-1),1,N); % coeff(1:end-1) requires the scheme to be sorted in increasing order!
                        B(line-1,1) = B(line,1); %#ok<SPRIX> % shift!
                        B(line,1) = -c_vec(line)*coeff(end); %#ok<SPRIX>
                        line = line+1;
                    end
                end
            else
                % Maximum order
                A = zeros(N,N);
                B = zeros(N,1);
                for i = 1: N
                    scheme = -i+~(c>0):N-i+~(c>0);
                    coeff = Duct.getCoeff(dX,scheme);
                    A(i,:) = -c*coeff(1+(c>0):end-~(c>0));
                    if c>0 % positive wave propagation
                        B(i,1) = -c*coeff(1);
                    else % negative wave propagation
                        B(i,1) = -c*coeff(end);
                    end
                end
                A = sparse(A);
                B = sparse(B);
            end
        end
        
        function S = getS(rho_vec, c_vec, u_vec, A_vec, p_vec, kappa, diff, nf)
            % getS receives arguments contained in MeanField and assembles
            % the second matrix containing psi1-4 (see SchaefPolif2019)
            
            rho_c=c_vec.*rho_vec;
            
            % Calculate derivatives using central difference scheme
            drhocdx=zeros(size(c_vec));
            dudx=zeros(size(u_vec));
            dAdx=zeros(size(u_vec));
            dpdx = zeros(size(rho_c));
            for i=2:length(c_vec)-1
                dpdx(i) = (p_vec(i+1)-p_vec(i-1))/(diff);
                drhocdx(i) = (rho_c(i+1)-rho_c(i-1))/diff;
                dudx(i) = (u_vec(i+1)-u_vec(i-1))/diff;
                dAdx(i) = (A_vec(i+1)-A_vec(i-1))/diff;
            end
            % Set derivatives at boundary
            drhocdx(end)=drhocdx(end-1);
            dudx(end) = dudx(end-1);
            dAdx(end)=dAdx(end-1);
            dpdx(end)=dpdx(end-1);
            % Assemble matrix
            % Each term is implemented as a matrix
            % Term containing dAdx
            % A_Surf is assembled from 4 Matrices [A_Surf_ff, A_Surf_fg;
            % A_Surf_fg, A_Surf_gg] (see SchaefPolif2019)
            A_Surf1 = sparse(1:nf,1:nf,-1./(2*A_vec(2:end)).*dAdx(2:end),nf,nf); % A_Surf_1 contains prefactors and is of size A_Surf_ff
            A_Surf = [(c_vec(2:end)+kappa*u_vec(2:end)).*A_Surf1,(-c_vec(2:end)-kappa*u_vec(2:end)).*A_Surf1;(c_vec(2:end)-kappa*u_vec(2:end)).*A_Surf1,(-c_vec(2:end)+kappa*u_vec(2:end)).*A_Surf1];
            
            % Term containing 1/rho*d(rho*c)/dx
            A_rhoc_1 = sparse(1:nf,1:nf,-1/2./rho_vec(2:end).*drhocdx(2:end),nf,nf);
            A_rhoc= [(1+u_vec(2:end)./c_vec(2:end)).*A_rhoc_1,(1+u_vec(2:end)./c_vec(2:end)).*A_rhoc_1;(-1+u_vec(2:end)./c_vec(2:end)).*A_rhoc_1,(-1+u_vec(2:end)./c_vec(2:end)).*A_rhoc_1];
            
            % Term containing du/dx
            A_dudx_1 = sparse(1:nf,1:nf,-1/2.*dudx(2:end),nf,nf);
            A_dudx = [(1+u_vec(2:end)./c_vec(2:end)+kappa).*A_dudx_1,(-1+u_vec(2:end)./c_vec(2:end)+kappa).*A_dudx_1;(-1-u_vec(2:end)./c_vec(2:end)+kappa).*A_dudx_1,(1-u_vec(2:end)./c_vec(2:end)+kappa).*A_dudx_1];
            
            % Term containing dp/dx
            A_dpdx_1 = sparse(1:nf,1:nf,-1/2.*dpdx(2:end)./rho_c(2:end),nf,nf);
            A_dpdx = [A_dpdx_1,-A_dpdx_1;A_dpdx_1,-A_dpdx_1];
            
            % Assembling of final matrix S
            S = A_rhoc+A_dudx+A_Surf+A_dpdx;
        end
        
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if isa(pars.fMax,'double') && numel(pars.fMax) == 1
                sys.fMax.f = pars.fMax;
                sys.fMax.g = pars.fMax;
            elseif isstruct(pars.fMax) && all(ismember(sys.waves,fieldnames(pars.fMax)))
                for wavec = sys.waves'
                    wave = char(wavec);
                    sys.fMax.(wave) = pars.fMax.(wave);
                end
            else
                error('incorrect specification of fMax or waves');
            end
            sys.cOver = struct(); % initialize with empty struct as long as there is no field for adjustment
            references.cOver = [];
            % activate propagation
            sys.activeProp.f = true;
            sys.activeProp.g = true;
            sys.activeProp.alpha = strcmp(pars.phi_active,'on');
            sys.activeProp.s = strcmp(pars.T_active,'on');
            % set references for activeProp
            references.activeProp.f = [];
            references.activeProp.g = [];
            references.activeProp.alpha = 'phi_active';
            references.activeProp.s = 'T_active';
            %% Profile definition for cross-sectional area and temperature
            sys.FunDef.T = cell2mat(pars.PopUp_T);
            sys.FunDef.A = cell2mat(pars.PopUp_A);
            references.FunDef.T = 'PopUp_T';
            references.FunDef.A = 'PopUp_A';
            if iscell(pars.l)
                sys.l = eval(cell2mat(pars.l));
                switch sys.FunDef.T
                    case 'Enter Function'
                        sys.T_ratio = cell2mat(pars.T_fun);
                        references.T_ratio = 'T_fun';
                    case 'Load Data'
                        Data = load(cell2mat(pars.path_T));
                        fields = fieldnames(Data);
                        Data = Data.(fields{1});
                        if size(Data,1)<size(Data(2))
                            Data = Data';
                        end
                        sys.T_ratio = Data;
                        references.T_ratio = 'path_T';
                    case 'Constant Temperature'
                        sys.T_ratio = '1';
                        references.T_ratio = [];
                end
                switch sys.FunDef.A
                    case 'Enter Function'
                        sys.A_ratio = cell2mat(pars.A_fun);
                        references.A_ratio = 'A_fun';
                    case 'Load Data'
                        Data = load(cell2mat(pars.path_A));
                        fields = fieldnames(Data);
                        Data = Data.(fields{1});
                        if size(Data,1)<size(Data(2))
                            Data = Data';
                        end
                        sys.A_ratio = Data;
                        references.A_ratio = 'path_A';
                    case 'Constant Area'
                        sys.A_ratio = '1';
                        references.A_ratio = [];
                end
                sys.order = eval(cell2mat(pars.order));
                sys.minres = eval(cell2mat(pars.minres));
                sys.sensorPositions = eval(cell2mat(pars.sensorPositions));
                
            else
                sys.l = pars.l;
                sys.order = pars.order;
                sys.minres = pars.minres;
                sys.sensorPositions = pars.sensorPositions;
%                 sys.Connection{2}.Mach = abs(pars.Mach);
%                 pars = rmfield(pars,{'Mach','c'});
            end
        end
    end
end
