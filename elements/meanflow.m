classdef meanflow < AcBlock & sss
    % MEANFLOW meanflow class to specify the Mach number through a Duct
    % (needed for meanflow after e.g. a trijunction)
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = observer(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.property: f_g, p_u, impedance_admittance, intensity
    %                           property to be observered
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: observer object
    % ------------------------------------------------------------------
    % Authors:      Matthias Häringer, Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  30 Sep 2021
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties (SetAccess = protected)
        Area % long range effects! 'protected' is a marker so changeParam cannot change it without updating the entire system
        Mach
        rho
        c_sound
        kappa
        Mmol
        alpha
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end

    methods
        function sys = meanflow(pars)            
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)));

            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;

            con = Block.readPort(pars,sys.Connection);
            % sign convention for Mach numbers
            con{1}.Mach = -con{1}.Mach;
            sys = set_Connection(sys, con);
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = Block.solveMean(con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end

        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end

            D = eye(numel(sys.waves));
            sys.D = [D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)

            sys = twoport(sys);

            sys.uptodate = true;
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            % save BC for reference in changeParam. Caution, using
            % different variable names than in Connection to avoid
            % conflicts with sss matrices A (or a) and C (or c):
            %   A --> Area
            %   c --> c_sound
            for field = {'Area','Mach','rho','c_sound','kappa','Mmol','alpha'}
                fc = char(field);
                if iscell(pars.(fc))
                    sys.(fc) = eval(cell2mat(pars.(fc)));
                else
                    sys.(fc) = pars.(fc);
                end
            end
            references = [];
        end
    end
end