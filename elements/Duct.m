classdef Duct < AcBlock & sss
    % DUCT Acoustic modell of a duct.% simpleDuct Acoustic modell of a duct.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = Duct(pars);
    % Input:   * pars.Name: string of name of the chokedExit
    %          * pars.l:    length of duct
    %          * pars.fMax: maximum frequency to be resolved by duct
    %          * pars.order: order of upwind discretization (1-3)
    %          * pars.minres: minimal resolution of longest wavelength
    %          * pars.Mach: Mach number
    %          * pars.c: speed of sound
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: Duct object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  21 Apr 2017
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block, simpleDuct
    
    properties
        % length of duct
        l
        % order of upwind discretization (1-3)
        order
        % minimal resolution of longest wavelength
        minres
        % maximum frequency to be resolved by duct
        fMax
        % positions of sensors inside the duct
        sensorPositions
        % overwrite propagation velocities
        cOver
        % allow propagation of waves
        activeProp = struct('f',true,'g',true,'alpha',false,'s',false,'omega1',false,'omega2',false,'omegafree',false)
    end
    properties (Dependent = true)
        % spacial discretization
        dX
        % number of spatial discretization points
        N
        % propagation velocity for all waves
        cProp
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end
    
    methods
        function sys = Duct(pars)
            % Call empty constructors with correct in and output dimension
            % and port number
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            sys@sss(zeros(2,2));
            
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            % this gives you the option to write BCs in the pars when
            % manually initializing the block:
            con = Block.readPort(pars,sys.Connection);
            sys = set_Connection(sys, con);
        end
        
        %% Set functions
        function sys = set.l(sys, l)
            if not(isequal(sys.l, l))
                sys.l = l;
                sys.uptodate = false;
            end
        end
        function sys = set.sensorPositions(sys, sensorPositions)
            if not(isequal(numel(sys.sensorPositions), numel(sensorPositions))) ||...
                    not(all(isequal(sys.sensorPositions, sensorPositions)))
                sys.sensorPositions = sensorPositions;
                sys.uptodate = false;
            end
        end
        function sys = set.order(sys, order)
            if not(isequal(sys.order, order))
                if ((order<=3) && (0<=order))||(order==Inf)
                    sys.order = order;
                    sys.uptodate = false;
                else
                    warning(['Order out of range [1-3, Inf]:',num2str(order)])
                    sys.order = order;
                    sys.uptodate = false;
                end
            end
        end
        function sys = set.minres(sys, minres)
            if not(isequal(sys.minres, minres))
                sys.minres = minres;
                sys.uptodate = false;
            end
        end
        function sys = set.fMax(sys, fMax)
            if not(isequal(sys.fMax, fMax))
                sys.fMax = fMax;
                sys.uptodate = false;
            end
        end
        function sys = set.cOver(sys, cOver)
            if not(isequal(sys.cOver, cOver))
                sys.cOver = cOver;
                sys.uptodate = false;
            end
        end
        function sys = set.activeProp(sys, activeProp)
            fnames = fieldnames(activeProp);
            for i = 1:numel(fnames)
                field = fnames{i};
                if not(isequal(sys.activeProp.(field), activeProp.(field)))
                    sys.activeProp = activeProp;
                    sys.uptodate = false;
                end
            end
        end
        
        %% Get functions
        function N = get.N(sys)
            cProp = sys.cProp;
            for field = sys.waves'
                lambdaMin = 1/sys.fMax.(char(field))*abs(cProp.(char(field)));
                if lambdaMin==0
                    N.(char(field)) = 0;
                else
                    dXmax = lambdaMin/sys.minres;
                    N.(char(field)) = ceil(sys.l/dXmax);
                end
            end
            % enforce same discretization for f and g
            N.f = max(N.f,N.g);
            N.g = N.f;
        end
        function cProp = get.cProp(sys)
            c = sys.Connection{1}.c;
            Mach = sys.Connection{1}.dir*sys.Connection{1}.Mach;
            u = c*Mach;
            for wavec = sys.waves'
                wave = char(wavec);
                if strcmp(wave,'f'); cProp.f = u+c; %#ok<ALIGN>
                elseif strcmp(wave,'g'); cProp.g = u-c;
                elseif strcmp(wave,'omega1'); cProp.omega1 = u; % adapt with Alp!
                elseif strcmp(wave,'omega2'); cProp.omega2 = u; % adapt with Alp!
%                 elseif strcmp(wave,'omegafree'); cProp.omegafree = u;
                else; cProp.(wave) = u; end % default
            end
            % overwrite option
            Overwrite = fieldnames(sys.cOver);
            for i = 1:numel(Overwrite)
                if isfield(cProp,Overwrite{i}) && ~isempty(sys.cOver.(Overwrite{i}))
                    if sign(sys.cOver.(Overwrite{i}))==sign(Mach)
                        cProp.(Overwrite{i}) = sys.cOver.(Overwrite{i});
                    else
                        warning(['Upstream propagation of any other wave than g is not implemented yet. Omitting to overwrite sys.cProp.',Overwrite{i},'!']);
                    end
                end
            end
        end
        function dX = get.dX(sys)
            for field = sys.waves'
                if sys.N.(char(field))>0
                    dX.(char(field)) = sys.l/sys.N.(char(field));
                else
                    dX.(char(field)) = 0;
                end
            end
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            % if by the time this code runs one position is still missing,
            % makeGeo was not run --> we may invent stuff: x-direction only
            if isempty(con{1}.pos) && ~isempty(con{2}.pos)
                con{1}.pos = con{2}.pos - [sys.l,0,0];
            elseif isempty(con{2}.pos) && ~isempty(con{1}.pos)
                con{2}.pos = con{1}.pos + [sys.l,0,0];
            end
            % exception for position
            savePos = con{2}.pos;
            con{2}.pos = con{1}.pos;
            % solve
            newcon = Block.solveMean(con);
            newcon{2}.pos = savePos; % insert exception
            sys.Connection = newcon; % execute exception after newcon is already as correct as possible, so update is not called unnecessarily often
            % update
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Declaration of Abstract functions
        function sys = update(sys)
            %% Check if system is uptodate
            if sys.uptodate
                return
            end
            sys= sys.clear();
            
            Mach = sys.Connection{1}.dir*sys.Connection{1}.Mach;
            c = sys.Connection{1}.c;
            rho = sys.Connection{1}.rho;
            
            % Duct resolution
            N = sys.N; %#ok<*PROP>
            dX = sys.dX;

            %% Run through all wave types
            % Limit the sensor positions
            if any(sys.sensorPositions>sys.l)||any(sys.sensorPositions<0)
                error([sys.Name,': Sensor positions out of duct.'])
            end
            sensN = numel(sys.sensorPositions);
            % Initialization
            fields = {}; % fields are all waves that are active in the duct
            for wavec = sys.waves'
                wave = char(wavec);
                if isfield(sys.activeProp,wave) && sys.activeProp.(wave)
                    fields = [fields;{wave}]; %#ok<AGROW>
                end
            end
            cProp = sys.cProp; % wave propagation velocity
            allN = cellfun(@(x) sys.N.(x),fields);
            n = sum(max(allN,1)+1)+numel(fields)*sensN; % for output only, including _uy terms
            A = sparse(0,0); % initialize A
            B = sparse(0,0); % initialize B
            C = sparse(n,0); % initialize C
            D = sparse(n,numel(fields)); % initialize D
            % Loop
            for i = 1:numel(fields)
                if cProp.(fields{i}) == 0
                    %% No convection (propagation velocity is zero, therefore also D)
                    Anew = sparse(0,0);
                    Bnew = sparse(0,1);
                    Cnew = sparse(n,0);
                    Dnew = sparse(n,1); % just for existence, does nothing
                elseif allN(i) == 0
                    %% Duct is compact with respect to wavelength
                    Anew = sparse(0,0);
                    Bnew = sparse(0,1);
                    Cnew = sparse(n,0);
                    Dnew = sparse(n,1); % just for existence, does nothing
                    if strcmp(fields{i},'f')
                        D([i+1,i+numel(fields)],i) = 1; %#ok<SPRIX>
                    elseif strcmp(fields{i},'g')
                        D([i-1,i+numel(fields)],i) = 1; %#ok<SPRIX>
                    else
                        D([i,i+numel(fields)],i) = 1; %#ok<SPRIX>
                    end
                    if ~isempty(sys.sensorPositions) % position can only be zero
                        D(sum(allN+1)+i,i) = 1; %#ok<SPRIX>
                    end
                else
                    %% Input is on the boundary and acting on the time derivative of the next stencil by spatial derivative
                    if (sys.order==1)||(allN(i)==1)
                        [Anew,Bnew] = Duct.getMatr(cProp.(fields{i}),dX.(fields{i}),allN(i),1);
                    elseif (sys.order==2)||(allN(i)==2)||(allN(i)==3)
                        [Anew,Bnew] = Duct.getMatr(cProp.(fields{i}),dX.(fields{i}),allN(i),2);
                    elseif (sys.order==3)
                        [Anew,Bnew] = Duct.getMatr(cProp.(fields{i}),dX.(fields{i}),allN(i),3);
                    else
                        [Anew,Bnew] = Duct.getMatr(cProp.(fields{i}),dX.(fields{i}),allN(i),Inf);
                    end
                    %% Output is directly the value on the respective stencils
                    % Keep strange order for f and g (legacy for compatibility)
                    forward = (cProp.(fields{i})>0);
                    if strcmp(fields{i},'f')
                        Cnew = sparse(i+1,allN(i),1,n,allN(i)); % connection
                        internal = sparse( 2*numel(fields)+(1:2:2*allN(i)-2), 1:allN(i)-1, 1, n, allN(i) ); % internal
                    elseif strcmp(fields{i},'g')
                        Cnew = sparse(i-1,1,1,n,allN(i)); % connection
                        internal = sparse( 2*numel(fields)+(2:2:2*allN(i)-2), 2:allN(i),   1, n, allN(i) ); % internal
                    else
                        Cnew = sparse(i,1+forward*(allN(i)-1),1,n,allN(i)); % connection
                        internal = sparse( 2*numel(fields)+sum(allN(1:i-1)-1)+(1:allN(i)-1), (2:allN(i))-forward, 1, n, allN(i) ); % internal
                    end
                    Cnew = Cnew + internal;
                    Dnew = sparse(i+numel(fields),1,1,n,1); % initialize with input connection, works independently from wave type!
                    %% Compute sensor outputs
                    % Determine sensor indices from position and adjacent stencils
                    idXSense = sys.sensorPositions/sys.dX.(fields{i});
                    idXLow = floor(idXSense);
                    idXHigh = ceil(idXSense);
                    % Interpolation factor
                    alpha = idXSense-idXLow;
                    % Limit sensor interpolation indices to stencils that are entirely inside the duct
                    idXLow(idXLow<0) = 0; % against numerical errors
                    idXHigh(idXHigh>N.(fields{i})) = N.(fields{i}); % against numerical errors
                    % Compute and append outputs
                    if sensN > 0
                        index1 = idXLow+~forward>=1 & idXHigh+~forward<=N.(fields{i});
                        Csense =    sparse( sum(allN+1)+sensN*(i-1)+find(index1), idXHigh(index1)+~forward,   alpha(index1),      n,  allN(i)) +...
                                    sparse( sum(allN+1)+sensN*(i-1)+find(index1), idXLow(index1)+~forward,    (1-alpha(index1)),  n,  allN(i));
                        Dsense =    sparse(                                                                                       n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index2 = ~index1 & idXLow==idXHigh & idXLow+~forward==0;
                        Csense =    sparse(                                                                                       n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index2), 1,                          1,                  n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index3 = ~index1 & ~index2 & idXLow+~forward==0;
                        Csense =    sparse( sum(allN+1)+sensN*(i-1)+find(index3), idXHigh(index3)+~forward,   alpha(index3),      n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index3), 1,                          (1-alpha(index3)),  n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index4 = ~index1 & ~index2 & ~index3 & idXLow==idXHigh & idXHigh+~forward==N.(fields{i})+1;
                        Csense =    sparse(                                                                                       n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index4), 1,                          1,                  n,  1);
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                        index5 = ~index1 & ~index2 & ~index3 & ~index4 & idXHigh+~forward==N.(fields{i})+1;
                        Csense =    sparse( sum(allN+1)+sensN*(i-1)+find(index5), idXLow(index5)+~forward,    (1-alpha(index5)),  n,  allN(i));
                        Dsense =    sparse( sum(allN+1)+sensN*(i-1)+find(index5), 1,                          alpha(index5),      n,  1);
                        if ~all(1 == index1 + index2 + index3 + index4 + index5)
                            error('Sensor assignment problem!');
                        end
                        Cnew = Cnew + Csense;
                        Dnew = Dnew + Dsense;
                    end
                end
                A = blkdiag(A,Anew);
                B = blkdiag(B,Bnew);
                C = [C,Cnew]; %#ok<AGROW>
                D(:,i) = D(:,i) + Dnew; %#ok<SPRIX>
            end
            
            %% Populate continuous time system matrices
            sys = sys.updatesss(sss(A,B,C,D,[],0));
            
            id = num2str(sys.Connection{1}.idx,'%02d');
            for i = 1:numel(fields)
                forward = (cProp.(fields{i})>0);
                sys.StateGroup.(char([fields{i},'_',id])) = sum(allN(1:i-1))+(2:allN(i))-forward;
            end
            
            %% Give names to sensor outputs
            sys.OutputGroup.Sensor = [];
            for i = 1:numel(fields)
                for ii = 1:sensN
                    sys.y{sum(max(allN,1)+1)+sensN*(i-1)+ii} = [fields{i},'_',sys.Name,'@',num2str(sys.sensorPositions(ii)','%G')];
                    sys.OutputGroup.Sensor = [sys.OutputGroup.Sensor, sum(max(allN,1)+1)+sensN*(i-1)+ii];
                end
            end
            
            % Give names to the ports and create in- and output groups
            sys = twoport(sys);
            
            %% Give names to internal state outputs
            format =['%0' ,num2str(ceil(log10(2*max(allN)-2))),'d' ];
            for i = 1:numel(fields)
%                 forward = (cProp.(fields{i})>0);
                if strcmp(fields{i},'f') % odd
                    for ii = 2*numel(fields)+(1:2:N.f+N.g-2)
                        sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),'f','_',sys.Name,'_',num2str(ceil((ii-2*numel(fields))/2+1),format)];
                        sys.OutputGroup.f = [sys.OutputGroup.f(1:end-1),ii,sys.OutputGroup.f(end)];
                    end
                elseif strcmp(fields{i},'g') % even
                    for ii = 2*numel(fields)+(2:2:N.f+N.g-2)
                        sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),'g','_',sys.Name,'_',num2str(ceil((ii-2*numel(fields))/2+1),format)];
                        sys.OutputGroup.g = [sys.OutputGroup.g(1:end-1),ii,sys.OutputGroup.g(end)];
                    end
                else % regular
                    for ii = 2*numel(fields)+sum(allN(1:i-1)-1)+(1:N.(fields{i})-1)
%                         sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),fields{i},'_',sys.Name,'_',num2str(ii-2*numel(fields)-sum(allN(1:i-1)-1)+~forward,format)];
                        sys.y{ii} = [num2str(sys.Connection{1}.idx,'%02d'),fields{i},'_',sys.Name,'_',num2str(ii-2*numel(fields)-sum(allN(1:i-1)-1)+1,format)];
                        sys.OutputGroup.(fields{i}) = [sys.OutputGroup.(fields{i})(1:end-1),ii,sys.OutputGroup.(fields{i})(end)];
                    end
                end
            end
            
            %% Populate plotting quantities
            for i = 1:numel(fields)
                if ~isinf(sys.dX.(fields{i})) && sys.dX.(fields{i})>0
                    sys.state.(fields{i}).x = 0:dX.(fields{i}):sys.l;
                else % no discretization
                    sys.state.(fields{i}).x = [0,0];
                end
                len = length(sys.state.(fields{i}).x);
                sys.state.(fields{i}).idx   = linspace(0,1,len);
                sys.state.(fields{i}).rho	= ones(1,len)*rho;
                sys.state.(fields{i}).c     = ones(1,len)*c;
                sys.state.(fields{i}).Mach	= ones(1,len)*Mach;
                sys.state.(fields{i}).A     = ones(1,len)*sys.Connection{1}.A;
                sys.state.(fields{i}).kappa = ones(1,len)*sys.Connection{1}.kappa;
                sys.state.(fields{i}).Mmol	= ones(1,len)*sys.Connection{1}.Mmol;
                sys.state.(fields{i}).alpha	= ones(1,len)*sys.Connection{1}.alpha;
                sys.state.(fields{i}).pos	= sys.Connection{1}.pos.' + (sys.Connection{2}.pos-sys.Connection{1}.pos).'*sys.state.(fields{i}).idx;
            end
            
            sys.uptodate = true;
        end
        
        %% Determine time step according to CFL number for time simulation
        function Ts = CFLtoTs(sys, cfl)
            c = sys.Connection{1}.c;
            Mach = -sys.Connection{1}.Mach;
            u= Mach*c;
            
            Ts = cfl*sys.dX/(u+c);
        end
    end
    
    methods(Static)
        %% Compute Coefficients of upwind scheme, given a stencil n
        % e.g. third order stencil: n = [1,0,-1,-2]
        % https://en.wikipedia.org/wiki/Finite_difference_coefficient
        % Documentation in Duct_symbolic.m
        function an = getCoeff(dX, n)
            An = (zeros(length(n)))*dX; % Cast for symbolic dX
            % Expand to maximum order i of stencil length(n)-1
            for i=0:length(n)-1
                % Coefficient matrix of Taylor series expansion
                An(i+1,:) = (n).^i;
            end
            sol = zeros(1,length(n))';
            sol(2,1) = factorial(1)/dX;
            an = An\sol;
        end

        %% Finite difference coeffcients of upwind scheme
        % http://en.wikipedia.org/wiki/Upwind_scheme
        % https://en.wikipedia.org/wiki/Finite_difference_coefficient
        % all coefficients have negative sign compared to the reference, as they
        % are moved to the rhs of the equation.

        %% Discretization matrices / System Matrix
        % State vector does not contain f0 and gn, because they are
        % inputs to the system and therefore not part of state!
        % Opposed to that g0 and fn are outputs and therefore these
        % states exist.
        %       f1
        %       f2
        %  x =  ...
        %       fn=fd
        %       g0=gu
        %       g1
        %       ...
        %       gn-1
        function [A,B] = getMatr(c,dX,N,order)
            %% Input is on the boundary and acting on the time derivative of the next
            % stencil by spatial derivative
            if ~isinf(order)
                %% internal
                if c>0 % positive wave propagation
                    scheme = -ceil((order+1)/2):ceil((order-2)/2);
                else % negative wave propagation
                    scheme = -ceil((order-2)/2):ceil((order+1)/2);
                end
                % calc and scale coefficients
                coeff = -c*Duct.getCoeff(dX,scheme);
                % sort coefficients to diagonal (0) and minor diagonals
                auxA = arrayfun(@(cf)...
                    sparse(max(1,1-cf):min(N,N-cf), max(1,1+cf):min(N,N+cf), coeff(scheme==cf), N, N),...
                    scheme, 'UniformOutput', false);
                A = auxA{1};
                for i = 2:order+1
                    A = A + auxA{i};
                end
                B = sparse(N,1);
                if c>0 % positive wave propagation
                    B(1,1) = coeff(scheme==min(scheme)); % fu first input
                else % negative wave propagation
                    B(N,1) = coeff(scheme==max(scheme)); % gd second input
                end
                %% boundary
                if c>0 % positive wave propagation
                    inscheme = scheme; % input
                    line = -min(scheme)-1; % last line requiring manipulation because of input
                    while min(inscheme)<-1
                        inscheme = inscheme+1;
                        coeff = -c*Duct.getCoeff(dX,inscheme);
                        A(line,:) = sparse(ones(1,order),1:order,coeff(2:end),1,N); % coeff(2:end) requires the scheme to be sorted in increasing order!
                        B(line+1,1) = B(line,1); %#ok<SPRIX> % shift!
                        B(line,1) = coeff(1); %#ok<SPRIX>
                        line = line-1;
                    end
                    outscheme = scheme; % output
                    line = N-max(scheme)+1; % first line requiring manipulation because of output
                    while max(outscheme)>0
                        outscheme = outscheme-1;
                        coeff = -c*Duct.getCoeff(dX,outscheme);
                        A(line,:) = sparse(ones(1,order+1),N-order:N,coeff,1,N); % coeff requires the scheme to be sorted in increasing order!
                        line = line+1;
                    end
                else % negative wave propagation
                    outscheme = scheme; % output
                    line = -min(scheme); % last line requiring manipulation because of output
                    while min(outscheme)<0
                        outscheme = outscheme+1;
                        coeff = -c*Duct.getCoeff(dX,outscheme);
                        A(line,:) = sparse(ones(1,order+1),1:order+1,coeff,1,N); % coeff requires the scheme to be sorted in increasing order!
                        line = line-1;
                    end
                    inscheme = scheme; % input
                    line = N-max(scheme)+2; % first line requiring manipulation because of input
                    while max(inscheme)>1
                        inscheme = inscheme-1;
                        coeff = -c*Duct.getCoeff(dX,inscheme);
                        A(line,:) = sparse(ones(1,order),N-order+1:N,coeff(1:end-1),1,N); % coeff(1:end-1) requires the scheme to be sorted in increasing order!
                        B(line-1,1) = B(line,1); %#ok<SPRIX> % shift!
                        B(line,1) = coeff(end); %#ok<SPRIX>
                        line = line+1;
                    end
                end
            else
                % Maximum order
                A = zeros(N,N);
                B = zeros(N,1);
                for i = 1: N
                    scheme = -i+~(c>0):N-i+~(c>0);
                    coeff = Duct.getCoeff(dX,scheme);
                    A(i,:) = -c*coeff(1+(c>0):end-~(c>0));
                    if c>0 % positive wave propagation
                        B(i,1) = -c*coeff(1);
                    else % negative wave propagation
                        B(i,1) = -c*coeff(end);
                    end
                end
                A = sparse(A);
                B = sparse(B);
            end
        end
        
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if isa(pars.fMax,'double') && numel(pars.fMax) == 1
                sys.fMax.f = pars.fMax;
                sys.fMax.g = pars.fMax;
            elseif isstruct(pars.fMax) && all(ismember(sys.waves,fieldnames(pars.fMax)))
                for wavec = sys.waves'
                    wave = char(wavec);
                    sys.fMax.(wave) = pars.fMax.(wave);
                end
            else
                error('incorrect specification of fMax or waves');
            end
            sys.cOver = struct(); % initialize with empty struct as long as there is no field for adjustment
            references.cOver = [];
            % activate propagation
            sys.activeProp.f = true;
            sys.activeProp.g = true;
            sys.activeProp.alpha = strcmp(pars.phi_active,'on');
            sys.activeProp.s = strcmp(pars.T_active,'on');
            sys.activeProp.omega1 = strcmp(pars.omega_active,'on');
            sys.activeProp.omega2 = strcmp(pars.omega_active,'on');
            sys.activeProp.omegafree = strcmp(pars.omegafree_active,'on');
            % set references for activeProp
            references.activeProp.f = [];
            references.activeProp.g = [];
            references.activeProp.alpha = 'phi_active';
            references.activeProp.s = 'T_active';
            references.activeProp.omega1 = 'omega_active';
            references.activeProp.omega2 = 'omega_active';
            references.activeProp.omegafree = 'omegafree_active';
            
            %% Create Block from Simulink using getmodel()
            if iscell(pars.l)
                sys.l = eval(cell2mat(pars.l));
                sys.order = eval(cell2mat(pars.order));
                sys.minres = eval(cell2mat(pars.minres));
                sys.sensorPositions = eval(cell2mat(pars.sensorPositions));
            else
                sys.l = pars.l;
                sys.order = pars.order;
                sys.minres = pars.minres;
                sys.sensorPositions = pars.sensorPositions;
%                 sys.Connection{2}.Mach = abs(pars.Mach); % why???
%                 sys.Connection{2}.c = abs(pars.c); % why???
%                 sys.Connection{2}.rho = 1; % why???
%                 sys.Connection{2}.A = 1; % why???
%                 pars = rmfield(pars,{'Mach','c'}); % why???
            end
        end
    end
end