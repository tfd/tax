classdef AcousticScatteringMatrix < AcBlock & sss
    % ScatteringMatrix block class. This can be used to load custom
    % scattering matrices into tax. It is deprecated by the Element class.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = ScatteringMatrix(pars);
    % Input:   * pars.Name:      string of name of the chokedExit
    %          * pars.fileName:  filename of model
    %          * pars.modelName: name of the model
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: Element object
    % ------------------------------------------------------------------
    % Authors:      Matthias Häringer, Guillaume Fournier (fournier@tfd.mw.tum.de)
    % Last Change:  24 May 2022
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block, sssobserver < AcBlock & sss
    
    properties
        fileName
    end
    
    methods
        function sys = AcousticScatteringMatrix(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(3*numel(pars.waves),numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if iscell(pars.filename)
                sys.fileName = char(pars.filename);
            else
                sys.fileName = pars.filename;
            end
            
            con = Block.readPort(pars,sys.Connection);
            sys = set_Connection(sys, con);
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = Block.solveMean(con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end

            [~,~,ext] = fileparts(sys.fileName);
            if strcmp(ext,'.mat')
                scatMat = load(sys.fileName);
                modelname = fields(scatMat);
                if numel(modelname) == 1
                    sysTemp = scatMat.(modelname{1});
                    if ~isa(sysTemp,'ss')
                        error('Invalid model: the loaded file is not in a state-space object')
                    end
                else
                    error('Invalid model: the .mat file loaded contains multiple variables.')
                end
            else
                error('Specify extension (.mat) of the scattering matrix.')
            end
            
            ATemp = sysTemp.A;
            BTemp = sysTemp.B;
            CTemp = sysTemp.C;
            DTemp = sysTemp.D;
            
            D = eye(numel(sys.waves));
            D = [D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)
            % replace acoustic part of D by external model
            D(1:2,1:2) = DTemp;
            B = BTemp;
            B = [B,zeros(size(B,1),size(D,2)-2)];
            C = CTemp;
            C = [C;zeros(size(D,1)-2,size(C,2))];
            A = ATemp;

            sys = sys.updatesss(sss(A,B,C,D,[],0));
            
            sys = twoport(sys);            
            sys.uptodate = true;
        end
    end
end