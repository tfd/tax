classdef blochBC < AcBlock & sss
    % blochBC observer class to impose Bloch-BC of mode order m on systems
    % with rotational degree of fredom N (=number of burners) 
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = blochBC(pars);
    % Input:   * pars.Name:  string of the name of the element
    %          * pars.N: degree of discrete roational symmetry (number of burners)
    %          * pars.m: azimuthal mode order m=0,1,...,N/2
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: blochBC object
    % ------------------------------------------------------------------
    % Authors:      Matthias Häringer, Guillaume Fournier (fournier@tfd.mw.tum.de)
    % Last Change:  13 January 2022
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        N,modeOrder
    end
    
    properties (SetAccess = protected)
        Area % long range effects! 'protected' is a marker so changeParam cannot change it without updating the entire system
        Mach
        rho
        c_sound
        kappa
        Mmol
        alpha
    end
    
    
    methods
        function sys = blochBC(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
%             sys@sss(zeros(2,2));
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            if iscell(pars.N)
                sys.N = eval(cell2mat(pars.N));
            else
                sys.N = pars.N;
            end
            if iscell(pars.modeOrder)
                sys.modeOrder = eval(cell2mat(pars.modeOrder));
            else
                sys.modeOrder = pars.modeOrder;
            end
            
            % save BC for reference in changeParam
            for field = {'A','Mach','rho','c','kappa','Mmol','alpha'}
                fc = char(field);
                pc = fc;
                if strcmp(fc,'A')
                    pc = 'Area';
                elseif strcmp(fc,'c')
                    pc = 'c_sound';
                end
                if iscell(pars.(fc))
                    sys.(pc) = eval(cell2mat(pars.(fc)));
                else
                    sys.(pc) = pars.(fc);
                end
            end
            
            con = Block.readPort(pars,sys.Connection);
            % sign convention for Mach numbers
            con{1}.Mach = -con{1}.Mach;
            sys = set_Connection(sys, con);
            
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = Block.solveMean(con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Set functions
        function sys = set.N(sys, N)
            if not(isequal(sys.N, N))
                sys.N = N;
                sys.uptodate = false;
            end
        end
        function sys = set.modeOrder(sys, modeOrder)
            if not(isequal(sys.modeOrder, modeOrder))
                sys.modeOrder = modeOrder;
                sys.uptodate = false;
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end
            
            Ntemp = sys.N;
            m = sys.modeOrder;
            
            D = eye(numel(sys.waves));
            sys.D = [D([2,1,3:end],:);eye(numel(sys.waves))];
            sys.D(1,2) = exp(-1i*2*m*pi/Ntemp);
            sys.D(2,1) = exp(1i*2*m*pi/Ntemp);
            
            sys = twoport(sys);
            
            sys.uptodate = true;
        end
    end
end