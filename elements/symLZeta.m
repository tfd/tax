classdef symLZeta < AcBlock & sss
    % SYMLZETA area jump class with effective and reduced length and loss
    % coefficient. As LZETA but symmetric with respect to BC
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = lZeta(pars);
    % Input:   * pars.Name:     string of name of the chokedExit
    %          * pars.Aratio:   ratio of areas A_downstream / A_upstream
    % optional * pars.zeta:     double loss coefficient
    %          * pars.lred:     double reduced length
    %          * pars.leff:     double effective length
    %          * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: lZeta object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  03 May 2017
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        zeta,lred,leff,
        % overwrite propagation velocities
        cOver % downstream (according to connections) here! for consistency with lred...
    end
    
    properties (SetAccess = protected)
        Aratio % long range effects! 'protected' is a marker so changeParam cannot change it without updating the entire system
    end

    properties(Dependent)
        cProp % downstream (according to connections) here! for consistency with lred...
    end

    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end
    
    methods
        function sys = symLZeta(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
        end

        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = solveMean(sys,con);
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
%             %% Remove Mach and A then solve mean.
%             if (Block.checkField(con{1},'A')&&Block.checkField(con{2},'A')) && (Block.checkField(con{1},'Mach')||Block.checkField(con{2},'Mach'))
%                 A(1) = con{1}.A;
%                 con{1} = rmfield(con{1},'A');
%                 A(2) = con{2}.A;
%                 con{2} = rmfield(con{2},'A');
%                 if Block.checkField(con{1},'Mach')
%                     Mach(1) = con{1}.Mach;
%                     con{1} = rmfield(con{1},'Mach');
%                     Mach(2) = -A(1)/A(2)*Mach(1);
%                 end
%                 if Block.checkField(con{2},'Mach')
%                     Mach(2) = con{2}.Mach;
%                     con{2} = rmfield(con{2},'Mach');
%                     Mach(1) = -A(2)/A(1)*Mach(2);
%                 end
%                 
%                 con = Block.solveMean(con);
%                 
%                 %% Then solve massbalance and append
%                 con{1}.A = A(1);
%                 con{2}.A = A(2);
%                 con{1}.Mach = Mach(1);
%                 con{2}.Mach = Mach(2);
%                 
%                 sys.Connection = con;
%             end
%             
%             if Block.checkPort(con,AcBlock.Port)
%                 sys = update(sys);
%             end
        end

        %% Get & Set functions
        function cProp = get.cProp(sys)
            c = sys.Connection{2}.c;
            Mach = sys.Connection{2}.dir*sys.Connection{2}.Mach;
            u = c*Mach;
            for wavec = sys.waves'
                wave = char(wavec);
                if strcmp(wave,'f')
                    cProp.(wave) = u+c;
                elseif strcmp(wave,'g')
                    cProp.(wave) = u-c;
                else
                    cProp.(wave) = u;
                end
            end
            % overwrite option
            Overwrite = fieldnames(sys.cOver);
            for i = 1:numel(Overwrite)
                if isfield(cProp,Overwrite{i}) && ~isempty(sys.cOver.(Overwrite{i}))
                    if sign(sys.cOver.(Overwrite{i}))==sign(Mach)
                        cProp.(Overwrite{i}) = sys.cOver.(Overwrite{i});
                    else
                        warning(['Upstream propagation of any other wave than g is not implemented yet. Omitting to overwrite sys.cProp.',Overwrite{i},'!']);
                    end
                end
            end
        end
        function sys = set.zeta(sys, zeta)
            if not(isequal(sys.zeta, zeta))
                sys.zeta = zeta;
                sys.uptodate = false;
            end
        end
        function sys = set.lred(sys, lred)
            if not(isequal(sys.lred, lred))
                sys.lred = lred;
                sys.uptodate = false;
            end
        end
        function sys = set.leff(sys, leff)
            if not(isequal(sys.leff, leff))
                sys.leff = leff;
                sys.uptodate = false;
            end
        end
        
        
        %% Generate system
        function [sys] = update(sys)
            % LZETA area jump with pressure loss coefficient zeta and reduced length
            % lred and effective length leff
            %
            % (c) Copyright 2010 tdTUM. All Rights Reserved.
            if sys.uptodate
                return
            end
            Machi = sys.Connection{1}.Mach;
            
            ai = sys.Connection{1}.A;
            c = sys.Connection{1}.c;
            % rho = Connection{1}.rho;
            Machj = sys.Connection{2}.Mach;
            
            aj = sys.Connection{2}.A;
            
            zeta = sys.zeta; %#ok<*PROP>
            lred = sys.lred;
            leff = sys.leff;
            
            % Possible automatic calculation/approximation of reduced length: (?)
            % l is the reduced length (according to Schuermans & Polifke "Modeling
            % Transfer Matrices of Premixed Flames and Comparison with Experimental
            % Results"
            % The equation for l is:
            % l = integral(A1/A(x) *dx) evaluated from point 1 to point 2
            % An approximate value for this is 3/4*l
            % lred = 0.75*Block.l;
            
            % Possible automatic calculation/approximation of pressure loss: (?)
            % zeta = (1 - (aj/ai))^2 ;
            % if (ai/aj) > 1
            %   zeta = 1.25*zeta*(0.2+ (ai/aj));
            % end
            
            % Matrix is obtained by symbolic solving for the scattering matrix, see
            % documentation and lZeta_symbolic.m
            s = tf('s');
            
            % Original taX formulation (equivalent to ta2):
            % D(1,:) = [ -(Machi*ai*c - Machj*aj*c + ai*c*kappa_pi - aj*c*kappa_pj + Machi*Machj*ai*c*kappa_pj - Machi*Machj*aj*c*kappa_pi + Machj*ai*c*kappa_pi*kappa_pj - Machi*aj*c*kappa_pi*kappa_pj + Machj*aj*kappa_pi*lred*s + aj*kappa_pi*kappa_pj*lred*s + Machi*Machj*ai*c*kappa_pj*zeta + Machj*ai*c*kappa_pi*kappa_pj*zeta)/(Machi*ai*c - Machj*aj*c + ai*c*kappa_mi - aj*c*kappa_pj + Machi*Machj*ai*c*kappa_pj - Machi*Machj*aj*c*kappa_mi + Machj*ai*c*kappa_mi*kappa_pj - Machi*aj*c*kappa_mi*kappa_pj + Machj*aj*kappa_mi*lred*s + aj*kappa_mi*kappa_pj*lred*s + Machi*Machj*ai*c*kappa_pj*zeta + Machj*ai*c*kappa_mi*kappa_pj*zeta),                                                                                                                                                                                                                                               -(aj*c*(kappa_mj - kappa_pj)*(Machj^2*zeta + Machj^2 - 1))/(Machi*ai*c - Machj*aj*c + ai*c*kappa_mi - aj*c*kappa_pj + Machi*Machj*ai*c*kappa_pj - Machi*Machj*aj*c*kappa_mi + Machj*ai*c*kappa_mi*kappa_pj - Machi*aj*c*kappa_mi*kappa_pj + Machj*aj*kappa_mi*lred*s + aj*kappa_mi*kappa_pj*lred*s + Machi*Machj*ai*c*kappa_pj*zeta + Machj*ai*c*kappa_mi*kappa_pj*zeta)];
            % D(2,:) = [                                                                                                                                                                                                                                                 (ai*(kappa_mi - kappa_pi)*(- c*Machi^2 + lred*s*Machi + c))/(Machi*ai*c - Machj*aj*c + ai*c*kappa_mi - aj*c*kappa_pj + Machi*Machj*ai*c*kappa_pj - Machi*Machj*aj*c*kappa_mi + Machj*ai*c*kappa_mi*kappa_pj - Machi*aj*c*kappa_mi*kappa_pj + Machj*aj*kappa_mi*lred*s + aj*kappa_mi*kappa_pj*lred*s + Machi*Machj*ai*c*kappa_pj*zeta + Machj*ai*c*kappa_mi*kappa_pj*zeta), -(Machi*ai*c - Machj*aj*c + ai*c*kappa_mi - aj*c*kappa_mj + Machi*Machj*ai*c*kappa_mj - Machi*Machj*aj*c*kappa_mi + Machj*ai*c*kappa_mi*kappa_mj - Machi*aj*c*kappa_mi*kappa_mj + Machj*aj*kappa_mi*lred*s + aj*kappa_mi*kappa_mj*lred*s + Machi*Machj*ai*c*kappa_mj*zeta + Machj*ai*c*kappa_mi*kappa_mj*zeta)/(Machi*ai*c - Machj*aj*c + ai*c*kappa_mi - aj*c*kappa_pj + Machi*Machj*ai*c*kappa_pj - Machi*Machj*aj*c*kappa_mi + Machj*ai*c*kappa_mi*kappa_pj - Machi*aj*c*kappa_mi*kappa_pj + Machj*aj*kappa_mi*lred*s + aj*kappa_mi*kappa_pj*lred*s + Machi*Machj*ai*c*kappa_pj*zeta + Machj*ai*c*kappa_mi*kappa_pj*zeta)];
            
            % Polifke simplification polif11a
            % T = [1         , s*leff/c - zeta*Machi;...
            %      -s*lred/c , ai/aj                ];
            % hardcoded solution:
            % D(1,:) = [                                                 1 - (2*Machj + (2*lred*s)/c + 2)/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1),                                        2/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1)];
            % D(2,:) = [ ((2*ai)/aj - 2*(Machj + (lred*s)/c)*(Machi*(zeta + ai^2/aj^2 - 1) + (leff*s)/c))/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1), 1 - (2*Machj + (2*ai)/aj + (2*lred*s)/c)/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1)];
            
            % Gentemann & Polifke 2003 Definition:
            % T= [1, (1-zeta-(ai/aj)^2)*Machi - s*leff/c; ...
            %     -s*lred/c - Machj,  ai/aj];
            % hardcoded solution:
%             tf_sys(1,:) = [                                                 1 - (2*Machj + (2*lred*s)/c + 2)/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1),                                        2/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1)];
%             tf_sys(2,:) = [ ((2*ai)/aj - 2*(Machj + (lred*s)/c)*(Machi*(zeta + ai^2/aj^2 - 1) + (leff*s)/c))/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1), 1 - (2*Machj + (2*ai)/aj + (2*lred*s)/c)/(Machj + Machi*(zeta + ai^2/aj^2 - 1) + ai/aj + (leff*s)/c + (lred*s)/c + 1)];
            
            % automatically generated code based on Gentemann & Polifke
            % 2003, equations (11) and (12), including sign correction for
            % Machi
            tf_sys = [-(-Machi-ai/aj+Machi*zeta-(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0)/(Machi+ai/aj-Machi*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0),...
                -(Machj*2.0-2.0)/(Machi+ai/aj-Machi*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0);...
                -((ai*-2.0)/aj+(Machi^2*ai*2.0)/aj-(Machi^2*ai*zeta*2.0)/aj+(Machi*lred*s*2.0)/c+1.0/c^2*leff*lred*s^2*2.0-(Machi*lred*s*zeta*2.0)/c+(Machi*ai*leff*s*2.0)/(aj*c))/((Machj+1.0)*(Machi+ai/aj-Machi*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0)),...
                ((Machj-1.0)*(-Machi+ai/aj+Machi*zeta-(leff*s)/c+(lred*s)/c+(Machi*ai)/aj-1.0))/((Machj+1.0)*(Machi+ai/aj-Machi*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0))];
            % same with abs(Machi) for pressure drop (wrong for Machj<0)
%             tf_sys = [(Machi+ai/aj+abs(Machi)*zeta+(leff*s)/c-(lred*s)/c-(Machi*ai)/aj-1.0)/(Machi+ai/aj+abs(Machi)*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0),...
%                 -(Machj*2.0-2.0)/(Machi+ai/aj+abs(Machi)*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0);...
%                 -((ai*-2.0)/aj+(Machi^2*ai*2.0)/aj+(Machi*lred*s*2.0)/c+1.0/c^2*leff*lred*s^2*2.0+(Machi*ai*abs(Machi)*zeta*2.0)/aj+(lred*s*abs(Machi)*zeta*2.0)/c+(Machi*ai*leff*s*2.0)/(aj*c))/((Machj+1.0)*(Machi+ai/aj+abs(Machi)*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0)),...
%                 -((Machj-1.0)*(Machi-ai/aj+abs(Machi)*zeta+(leff*s)/c-(lred*s)/c-(Machi*ai)/aj+1.0))/((Machj+1.0)*(Machi+ai/aj+abs(Machi)*zeta+(leff*s)/c+(lred*s)/c+(Machi*ai)/aj+1.0))];

            % based on Silva et al. 2016, including sign correction for
            % Machi
%             tf_sys(1,:) = 1.0*[ -1*(1-ai/aj+Machi*zeta)/(1+ai/aj-Machi*zeta),  2/(1+ai/aj-Machi*zeta) ];
%             tf_sys(2,:) = 1.0*[ 0.5*((1+ai/aj+Machi*zeta)-(1-ai/aj-Machi*zeta)*(1-ai/aj+Machi*zeta)/(1+ai/aj-Machi*zeta)),(1-ai/aj-Machi*zeta)/(1+ai/aj-Machi*zeta) ];
            
            % Transformation to scattering matrix
            % sys = transformTtoS(T);
            
            % Add convective waves
            if numel(sys.waves)<3
                Mat = [];
            end
            for i = 3:numel(sys.waves)
                cPropVal = sys.cProp.(sys.waves{i});
                if strncmp(sys.waves{i},'omega',5) ||... % swirl waves do not pass
                        cPropVal==0 % no convection --> nothing passes the area change
                    Mat(i-2,i-2) = 0; %#ok<AGROW>
                elseif cPropVal>0
                    Mat(i-2,i-2) = cPropVal/(lred*s+cPropVal); %#ok<AGROW>
                else
                    Mat(i-2,i-2) = cPropVal/(-lred*s+cPropVal); %#ok<AGROW>
                end
            end
            tf_sys = [blkdiag(tf_sys,Mat);eye(numel(sys.waves))];

            % Build system
            ss_sys = ss(tf_sys);
            
            sys.A = ss_sys.A;sys.B = ss_sys.B; sys.C = ss_sys.C; sys.D = ss_sys.D; sys.E = ss_sys.E;
            
            sys = twoport(sys);
            
            sys.uptodate = true;
        end
        
        %% mean solve algorithm (check for identity)
        function con = solveMean(sys,con)
            fields1=[];fields2=[];
            if not(isempty(con{1}))
                fields1 = fieldnames(con{1})';
            end
            if not(isempty(con{2}))
                fields2 = fieldnames(con{2})';
            end
            fields = unique([fields1,fields2]);
            
            %% Swap sign of downstream velocity
            % Downstream Block of connection has a velocity pointing
            % inwards (negative)
            if Block.checkField(con{2},'Mach')
                con{2}.Mach = -con{2}.Mach;
            end
            
            %% Propagate quantities
            for fieldc = fields
                field = char(fieldc);
                if strcmp(field,'A') % special treatment for area
                    %% Propagate area
                    if Block.checkField(con{1},'A')&&Block.checkField(con{2},'A')
                        if not(Block.isequalAbs(con{1}.A*sys.Aratio,con{2}.A))
                            error('Mean values are inconsistent.')
                        end
                    elseif Block.checkField(con{1},'A')
                        con{2}.A = con{1}.A*sys.Aratio;
                    elseif Block.checkField(con{2},'A')
                        con{1}.A = con{2}.A/sys.Aratio;
                    end
                elseif strcmp(field,'Mach') % special treatment for Mach number
                    %% Propagate Mach number
                    if Block.checkField(con{1},'Mach')&&Block.checkField(con{2},'Mach')
                        if not(Block.isequalAbs(con{1}.Mach/sys.Aratio,con{2}.Mach))
                            error('Mean values are inconsistent.')
                        end
                    elseif Block.checkField(con{1},'Mach')
                        con{2}.Mach = con{1}.Mach/sys.Aratio;
                    elseif Block.checkField(con{2},'Mach')
                        con{1}.Mach = con{2}.Mach*sys.Aratio;
                    end
                else
                    %% Check for consistency or match quantities
                    if Block.checkField(con{1},field)&&Block.checkField(con{2},field)
                        if (not(Block.isequalAbs(con{1}.(field),con{2}.(field)))) && (not(strcmp(field,'idx')||strcmp(field,'dir')))
                            error('Mean values are inconsistent.')
                        end
                    elseif Block.checkField(con{1},field)
                        con{2}.(field) = con{1}.(field);
                    elseif Block.checkField(con{2},field)
                        con{1}.(field) = con{2}.(field);
                    end
                end
            end
            
            %% (Re)swap sign of downstream velocity
            if Block.checkField(con{2},'Mach')
                con{2}.Mach = -con{2}.Mach;
            end
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            if iscell(pars.Aratio)
                sys.Aratio = eval(cell2mat(pars.Aratio));
            else
                sys.Aratio = pars.Aratio;
            end
            
            if isfield(pars,'zeta')
                if iscell(pars.zeta)
                    sys.zeta = eval(cell2mat(pars.zeta));
                else
                    sys.zeta = pars.zeta;
                end
            else
                sys.zeta = 0;
            end
            if isfield(pars,'lred')
                if iscell(pars.lred)
                    sys.lred = eval(cell2mat(pars.lred));
                else
                    sys.lred = pars.lred;
                end
            else
                sys.lred = 0;
            end
            if isfield(pars,'leff')
                if iscell(pars.leff)
                    sys.leff = eval(cell2mat(pars.leff));
                else
                    sys.leff = pars.leff;
                end
            else
                sys.leff = 0;
            end

            sys.cOver = struct(); % initialize with empty struct as long as there is no field for adjustment
            references.cOver = [];
        end
    end
end