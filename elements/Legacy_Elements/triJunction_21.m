classdef triJunction_21 < AcBlock & sss
    % TriJunction_21 class with characteristic length, volume and area.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = triJunction_21(pars);
    % Input:   * pars.Name: string of name of the triJunction
    % optional * pars.V: double loss coefficient
    %          * pars.lchar: double characteristic length
    %          * pars.mix: string if 'on' mix gases (differences in set_Connection)
    %          * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: triJunction_21 object
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de), Thomas Emmert (emmert@tfd.mw.tum.de)
    % Last Change:  05 May 2017
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        Volume,lchar,mix
    end
    
    methods
        function sys = triJunction_21(pars)
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,AcBlock.Port,AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(3,3));
            
            %% Create Block from Simulink getmodel()
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            if isfield(pars,'Volume')
                if iscell(pars.Volume)
                    sys.Volume = eval(cell2mat(pars.Volume));
                else
                    sys.Volume = pars.Volume;
                end
            else
                sys.Volume = 0;
            end
            if isfield(pars,'lchar')
                if iscell(pars.lchar)
                    sys.lchar = eval(cell2mat(pars.lchar));
                else
                    sys.lchar = pars.lchar;
                end
            else
                sys.lchar = 0;
            end
            if isfield(pars,'mix')
                if iscell(pars.mix)
                    sys.mix = strcmp('on',eval(cell2mat(pars.mix)));
                else
                    sys.mix = strcmp('on',pars.mix);
                end
            else
                sys.mix = false;
            end
            
            sys.Connection = Block.readPort(pars,sys.Connection);
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            %% analyse set of connections
            n = length(con);
            chk = zeros(1,n);
            chkIn = zeros(1,n);
            for i = 1:n
                chk(i) = Block.checkField(con{i},'Mach');
                if chk(i)
                    chkIn(i) = con{i}.Mach<0;
                end
            end
            % Mach vector
            Mach=zeros(1,n);
            vchk=find(chk);
            for i=vchk
                Mach(i)=con{i}.Mach;
            end
            %% mix or propagate
            if sys.mix % mix
                con = triJunction_12.solveMeanMix(con);
            else
                % Propagate everything but Mach number
                con = triJunction_12.solveMean(con);
                % solve mass balance or check consictency, if there is enough information
                if all(chk) % all Mach numbers known --> check consistency
                    if not(sum(Mach)==0)
                        error('Mean values are inconsistent.')
                    end
                elseif sum(chk) == n-1 % only one Mach number is missing --> solve mass balance
                    % mass balance (sign convention: Mach pointing out of the element)
                    Mach(chk==0)=-sum(Mach);

                    % append
                    for i=1:n
                        con{i}.Mach = Mach(i);
                    end
                end
            end
            %% write
            sys.Connection = con;
            
            if Block.checkPort(con,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Set functions
        function sys = set.Volume(sys, Volume)
            if not(isequal(sys.Volume, Volume))
                sys.Volume = Volume;
                sys.uptodate = false;
            end
        end
        function sys = set.lchar(sys, lchar)
            if not(isequal(sys.lchar, lchar))
                sys.lchar = lchar;
                sys.uptodate = false;
            end
        end
        function sys = set.mix(sys, mix)
            if not(isequal(sys.mix, mix))
                sys.mix = mix;
                sys.uptodate = false;
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            % TriJunction_12 class with characteristic length, volume and area.
            %
            % (c) Copyright 2016 tdTUM. All Rights Reserved.
            if sys.uptodate
                return
            end
            Ma1 = sys.Connection{1}.Mach;
            Ma2 = sys.Connection{2}.Mach;
            Ma3 = sys.Connection{3}.Mach;
            
            a = sys.Connection{1}.A;
            c = sys.Connection{1}.c;
            
            V = sys.Volume;
            lc = sys.lchar;
            
            % Matrix is obtained by symbolic solving for the scattering matrix, see
            % documentation and lZeta_symbolic.m
            s = tf('s');
                        
            % automatically generated code from buildTri12.mn:
            tf_sys = [...
                [(Ma1*3.0-Ma2*3.0-Ma3*3.0+Ma1*Ma2*3.0+Ma1*Ma3*3.0-Ma2*Ma3*3.0+(lc*s*6.0)/c+Ma1*Ma2*Ma3*3.0+1.0/c^2*lc^2*s^2*9.0-Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma2*lc*s*6.0)/c+(Ma3*lc*s*6.0)/c-(V*s*3.0)/(a*c)-(V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3)/a+(Ma1*V*s*2.0)/(a*c)-(Ma2*V*s*2.0)/(a*c)-(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)-(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a-3.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);...
                    ((Ma3+(lc*s)/c+1.0)*(Ma1^2*3.0+(Ma1*lc*s*3.0)/c+(V*1.0/c^2*lc*s^2)/a+(Ma1*V*s)/(a*c)-3.0)*-2.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);...
                    ((Ma2+(lc*s)/c+1.0)*(Ma1^2*3.0+(Ma1*lc*s*3.0)/c+(V*1.0/c^2*lc*s^2)/a+(Ma1*V*s)/(a*c)-3.0)*-2.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);],...
                [((Ma3+(lc*s)/c+1.0)*(Ma2^2*3.0+(Ma2*lc*s*3.0)/c+(V*1.0/c^2*lc*s^2)/a+(Ma2*V*s)/(a*c)-3.0)*-2.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);...
                    (Ma1*-3.0+Ma2*3.0-Ma3*3.0+Ma1*Ma2*3.0-Ma1*Ma3*3.0+Ma2*Ma3*3.0+(lc*s*6.0)/c+Ma1*Ma2*Ma3*3.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0-Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*6.0)/c+(Ma3*lc*s*6.0)/c-(V*s*3.0)/(a*c)-(V*1.0/c^2*lc*s^2*2.0)/a+(Ma1*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3)/a-(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)-(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)-(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a-3.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);...
                    ((Ma1+(lc*s)/c+1.0)*(Ma2^2*3.0+(Ma2*lc*s*3.0)/c+(V*1.0/c^2*lc*s^2)/a+(Ma2*V*s)/(a*c)-3.0)*-2.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);],...
                [((Ma2+(lc*s)/c+1.0)*(Ma3^2*3.0+(Ma3*lc*s*3.0)/c+(V*1.0/c^2*lc*s^2)/a+(Ma3*V*s)/(a*c)-3.0)*-2.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);...
                    ((Ma1+(lc*s)/c+1.0)*(Ma3^2*3.0+(Ma3*lc*s*3.0)/c+(V*1.0/c^2*lc*s^2)/a+(Ma3*V*s)/(a*c)-3.0)*-2.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);...
                    (Ma1*-3.0-Ma2*3.0+Ma3*3.0-Ma1*Ma2*3.0+Ma1*Ma3*3.0+Ma2*Ma3*3.0+(lc*s*6.0)/c+Ma1*Ma2*Ma3*3.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0-Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*6.0)/c+(Ma2*lc*s*6.0)/c-(V*s*3.0)/(a*c)-(V*1.0/c^2*lc*s^2*2.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3)/a-(Ma1*V*s*2.0)/(a*c)-(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)-(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a-3.0)/(Ma1*9.0+Ma2*9.0+Ma3*9.0+Ma1*Ma2*9.0+Ma1*Ma3*9.0+Ma2*Ma3*9.0+(lc*s*1.8e1)/c+Ma1*Ma2*Ma3*9.0+1.0/c^2*lc^2*s^2*9.0+Ma1*1.0/c^2*lc^2*s^2*3.0+Ma2*1.0/c^2*lc^2*s^2*3.0+Ma3*1.0/c^2*lc^2*s^2*3.0+(Ma1*lc*s*1.2e1)/c+(Ma2*lc*s*1.2e1)/c+(Ma3*lc*s*1.2e1)/c+(V*s*3.0)/(a*c)+(V*1.0/c^2*lc*s^2*6.0)/a+(Ma1*Ma2*lc*s*6.0)/c+(Ma1*Ma3*lc*s*6.0)/c+(Ma2*Ma3*lc*s*6.0)/c+(V*1.0/c^3*lc^2*s^3*3.0)/a+(Ma1*V*s*2.0)/(a*c)+(Ma2*V*s*2.0)/(a*c)+(Ma3*V*s*2.0)/(a*c)+(Ma1*Ma2*V*s)/(a*c)+(Ma1*Ma3*V*s)/(a*c)+(Ma2*Ma3*V*s)/(a*c)+(Ma1*V*1.0/c^2*lc*s^2*2.0)/a+(Ma2*V*1.0/c^2*lc*s^2*2.0)/a+(Ma3*V*1.0/c^2*lc*s^2*2.0)/a+9.0);]...
                ];
            % from buildTri12.mn, simple
%             tf_sys = [...
%                 [(-2.0/3.0)/(Ma1+1.0)+1.0/3.0;...
%                     (Ma1*(-2.0/3.0)+2.0/3.0)/(Ma2+1.0);...
%                     (Ma1*(-2.0/3.0)+2.0/3.0)/(Ma3+1.0)],...
%                 [(Ma2*(-2.0/3.0)+2.0/3.0)/(Ma1+1.0);...
%                     (-2.0/3.0)/(Ma2+1.0)+1.0/3.0;...
%                     (Ma2*(-2.0/3.0)+2.0/3.0)/(Ma3+1.0)],...
%                 [(Ma3*(-2.0/3.0)+2.0/3.0)/(Ma1+1.0);...
%                     (Ma3*(-2.0/3.0)+2.0/3.0)/(Ma2+1.0);...
%                     (-2.0/3.0)/(Ma3+1.0)+1.0/3.0]...
%                 ];
            % from buildTri12.mn, no Bernoulli
%             tf_sys = [...
%                 [2.0/(Ma1+Ma2+Ma3+3.0)-1.0;...
%                     2.0/(Ma1+Ma2+Ma3+3.0);...
%                     2.0/(Ma1+Ma2+Ma3+3.0)],...
%                 [2.0/(Ma1+Ma2+Ma3+3.0);...
%                     2.0/(Ma1+Ma2+Ma3+3.0)-1.0;...
%                     2.0/(Ma1+Ma2+Ma3+3.0)],...
%                 [2.0/(Ma1+Ma2+Ma3+3.0);...
%                     2.0/(Ma1+Ma2+Ma3+3.0);...
%                     2.0/(Ma1+Ma2+Ma3+3.0)-1.0]...
%                 ];
            % from buildTri12.mn, no Bernoulli, incompressible conti
%             tf_sys = [...
%                 [-1.0/3.0;2.0/3.0;2.0/3.0],...
%                 [2.0/3.0;-1.0/3.0;2.0/3.0],...
%                 [2.0/3.0;2.0/3.0;-1.0/3.0]...
%                 ];
            
            ss_sys = ss(tf_sys);
            
            sys.A = ss_sys.A;sys.B = ss_sys.B; sys.C = ss_sys.C; sys.D = ss_sys.D; sys.E = ss_sys.E;
            
            sys = multiport(sys);
            
            sys.uptodate = true;
        end
    end

    methods(Static)
        %% Overload solveMean
        % arbitrary number of connections, no propagation of Mach number
        function con = solveMean(con,fields)
            %% Get number and fields of connections
            n=length(con);
            if nargin<2 % if fields is given, these fields are explicitely propagated
                fields=[];
                for i=1:n
                    if not(isempty(con{i}))
                        fieldsn = fieldnames(con{i})';
                        fields = unique([fields,fieldsn]);
                    end
                end
                % Delete Mach, idx and dir from fields list
                fields(strcmp(fields,'Mach'))=[];
                fields(strcmp(fields,'idx'))=[];
                fields(strcmp(fields,'dir'))=[];
            end
            %% Propagate quantities
            for fieldc = fields
                field = char(fieldc);
                % generate vector from checkField
                chk=zeros(1,n);
                for i=1:n
                    chk(i)=Block.checkField(con{i},field);
                end
                vchk=find(chk);
                % check consistency
                if sum(chk)>1 % more than one entry
                    for i=vchk(2:end)
                        if not(Block.isequalAbs(con{vchk(1)}.(field),con{i}.(field)))
                        end
                    end
                end
                % match
                if sum(chk)>0 % at least one entry
                    for i=1:n
                        con{i}.(field) = con{vchk(1)}.(field);
                    end
                end
            end
        end
        %% Add solveMeanMix
        % arbitrary number of connections; mixing of properties assuming
        % equal areas, ideal gas and const heat capacity; automatic OutPort
        % recognition
        function con = solveMeanMix(con)
            %% initialize
            % constants
            Rmol = 8314.4598; % J/(kmol K)
            % propagate areas
            con = triJunction_12.solveMean(con,{'A'});
            % get number and fields of connections
            n = length(con);
            fields = [];
            for i=1:n
                if not(isempty(con{i}))
                    fieldsn = fieldnames(con{i})';
                    fields = unique([fields,fieldsn]);
                end
            end
            % delete idx and dir from fields list
            fields(strcmp(fields,'idx'))=[];
            fields(strcmp(fields,'dir'))=[];
            % find fully defined, Mach-defined and In-connections
            % chkFull = cellfun(@(x) Block.checkPort(x,fields), con);
            chkMach = cellfun(@(x) Block.checkField(x,'Mach'), con);
            chkIn = false(size(chkMach));
            chkIn(chkMach) = cellfun(@(x) x.Mach<0, con(chkMach));
            chkOut = false(size(chkMach));
            chkOut(chkMach) = cellfun(@(x) x.Mach>=0, con(chkMach)); % explicit
            %% safe run
            % always propagate explicit Out, save and update all chk
            con(chkOut) = triJunction_12.solveMean(con(chkOut));
            consave = con;
            chkFull = cellfun(@(x) Block.checkPort(x,fields), con);
            chkMach = cellfun(@(x) Block.checkField(x,'Mach'), con);
            chkIn(chkMach) = cellfun(@(x) x.Mach<0, con(chkMach));
            chkOut(chkMach) = cellfun(@(x) x.Mach>=0, con(chkMach));
            % further determination impossible if:
            if sum(~chkMach)>1 ||... % more than one Mach number is missing or
                    sum(~chkFull & chkIn)>1 ||... % more than one InPort is still undefined or
                    (any(~chkFull & chkIn) && all(~chkFull(chkOut))) % all OutPorts and min one InPort are still undefined
                return
            end
            % compute known mass and mole flow rates, heat capacities, temperatures and pressure
            ip = find(chkFull,1,'first');
            p = con{ip}.rho*con{ip}.c^2/con{ip}.kappa;
            mdot = nan(1,n);
            mdot(chkFull) = cellfun(@(x) x.rho*x.Mach*x.c*x.A, con(chkFull));
            ndot = nan(1,n);
            ndot(chkFull) = cellfun(@(x) x.rho*x.Mach*x.c*x.A/x.Mmol, con(chkFull));
            cp = nan(1,n);
            cp(chkFull) = cellfun(@(x) x.kappa/(x.kappa-1)*Rmol/x.Mmol, con(chkFull));
            T = nan(1,n);
            T(chkFull) = cellfun(@(x) x.Mmol*x.c^2/(x.kappa*Rmol), con(chkFull));
            if max(T(chkFull))-min(T(chkFull))>100
                warning('Mixing with high temperature difference contradicts assumption of constant heat capacities. Results may be inexact.');
            end
            alpha(chkFull) = cellfun(@(x) x.alpha, con(chkFull));
            % check if a complete solution is consistent
            if all(chkFull) && sum(mdot)~=0 || sum(ndot)~=0 || sum(mdot.*cp.*T)~=0
                error('Mean values are inconsistent.');
            elseif all(chkFull)
                return
            end
            %% assume undefined Ports are either all Out or In
            % compute missing mass and mole flow rate, heat capacity and temperature
            mdotmiss = -sum(mdot(chkFull));
            ndotmiss = -sum(ndot(chkFull));
            cpmiss = -sum(mdot(chkFull).*cp(chkFull))/mdotmiss;
            Tmiss = -sum(mdot(chkFull).*cp(chkFull).*T(chkFull))/(mdotmiss*cpmiss);
            alphamiss = -sum(mdot(chkFull).*alpha(chkFull))/mdotmiss;
            % compute connection properties from intermediate results
            iNotFull = find(~chkFull,1,'first'); % first not fully defined connection
            con{iNotFull}.Mmol = mdotmiss/ndotmiss;
            con{iNotFull}.kappa = cpmiss/(cpmiss-Rmol/con{iNotFull}.Mmol);
            con{iNotFull}.rho = p*con{iNotFull}.Mmol/(Rmol*Tmiss);
            con{iNotFull}.c = sqrt(con{iNotFull}.kappa*Rmol*Tmiss/con{iNotFull}.Mmol);
            con{iNotFull}.alpha = alphamiss;
            % propagate to all not fully defined connections
            con(~chkFull) = triJunction_12.solveMean(con(~chkFull));
            % solve continuity
            Machmiss = mdotmiss/(con{iNotFull}.rho*con{iNotFull}.c*con{iNotFull}.A); % sum of all Mach numbers of formerly undefined connections
            con{~chkMach}.Mach = Machmiss - sum(cellfun(@(x) x.Mach, con{~chkFull & chkMach}));
            %% check assumption
            % update chkIn
            chkIn = cellfun(@(x) x.Mach<0, con);
            chkOut = cellfun(@(x) x.Mach>=0, con);
            if ~all(chkIn==~chkFull) || ~all(chkOut==~chkFull)
                con = consave; % reset because assumption was not met
            end
        end
    end
end