classdef reference < AcBlock & sss
    % REFERENCE contains the heat release model for global heat release
    % fluctuations
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = reference(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.Ts: desired sampling time of the model
    %          * pars.filename:  name of the file containing the model
    %          * pars.modelname: name of the model to load
    %          * pars.active:    flame is active or inactive
    % Output:  * sys: reference object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  26 Apr 2015
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        filename,modelname,active,pars2,fMax
%         filename,modelname,active,fMax
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end

    methods
        function sys = reference(pars)
            % Call empty constructors with correct in and output dimension
            % and port number
            sys@AcBlock(AcBlock.Port,AcBlock.Port,[]);
            sys@sss(zeros(2*numel(pars.waves)+1,numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            con = sys.Connection;
            con{3}.FlameName = ['Q_',sys.Name];
            sys = set_Connection(sys, con);
        end

        %% Set functions
        function sys = set.active(sys, active)
            if not(isequal(sys.active, active))
                sys.active = active;
                sys.uptodate = false;
            end
        end
        function sys = set.filename(sys, filename)
            if not(isequal(sys.filename, filename))
                sys.filename = filename;
                sys.uptodate = false;
            end
        end
        function sys = set.modelname(sys, modelname)
            if not(isequal(sys.modelname, modelname))
                sys.modelname = modelname;
                sys.uptodate = false;
            end
        end
        function sys = set.fMax(sys, fMax)
            if not(isequal(sys.fMax, fMax))
                sys.fMax = fMax;
                sys.uptodate = false;
            end
        end
        function sys = set.pars2(sys, pars)
            if not(isequal(sys.pars2, pars))
                sys.pars2 = pars;
                sys.uptodate = false;
            end
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            conFl = con(3);
            con = con(1:2);
            con = Block.solveMean(con);
            con(3) = conFl;
            sys.Connection = con;
            if Block.checkPort(con(1:2),AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            % FTF_REFERENCE velocity sensitive FTF Flame model reference system
            % generation.
            if sys.uptodate
                return
            end
            sys = clear(sys);
            
            % Name of u'/ubar reference velocity
            RefName = ['u_',sys.Name];
            
            Den =  -1/(sys.Connection{1}.Mach*sys.Connection{1}.c); % minus because of sign convention at upstream Connection: Mach pointing outwards, i.e. it is typically negative
            
%             sys.D = [0 1;1 0;Den -Den];
            auxD = [Den -Den];
            sys.D = eye(numel(sys.waves));
            sys.D = [sys.D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)
            sys.D = [sys.D;auxD,zeros(1,numel(sys.waves)-2)];
            
            sys = twoport(sys);
            
            sys.y{2*numel(sys.waves)+1} = RefName;
            
            if sys.active
                %% Get names of signals
                [~,name,ext] = fileparts(sys.filename);
                if isempty(ext)
                    error('Specify extension (.m or .mat) of flame in reference Block.')
                else
                    if strcmp(ext,'.m')
                        [FTFsys,sys.pars2] = feval(name,sys.pars2,sys.state);
                    elseif strcmp(ext,'.mat')
                        FTFsys = load(sys.filename,sys.modelname); % model laden
                        FTFsys = FTFsys.(sys.modelname);
                    end
                end
%                 load(sys.filename); % model laden
                % Name of heat release Q'/Qbar
                FlameName = sys.Connection{3}.FlameName;
                
%                 FTFsys = eval(sys.modelname);
                FTFsys.u{1} = RefName;
                FTFsys.y = {FlameName};
                if (isa(FTFsys,'idpoly')||isa(FTFsys,'idtf'))
                    % Adapt and append noise input
                    FTFsys = sss(adaptTsAndDelays(FTFsys,sys.Ts,sys.fMax));
                    sys = connect(sys, FTFsys, [sys.u; FTFsys.u{1} ;FTFsys.u(FTFsys.InputGroup.Noise)], [sys.y(1:2*numel(sys.waves));FlameName;RefName]);
                else
                    % Adapt and do not append noise input
                    FTFsys = sss(adaptTsAndDelays(FTFsys,sys.Ts,sys.fMax));
                    sys = connect(sys, FTFsys, [sys.u; FTFsys.u], [sys.y(1:2*numel(sys.waves));FlameName;RefName]);
                end
                % re-sort OutputGroup for easier visual check of output order
                for wavec = sys.waves.'
                    wave = char(wavec);
                    auxSort = sortrows([sys.y(sys.OutputGroup.(wave)),num2cell(sys.OutputGroup.(wave).')]);
                    sys.OutputGroup.(wave) = cell2mat(auxSort(:,2)).';
                end
                
                % Rename flame reference u' input to avoid reconnecting in
                % tax
                sys.y{2*numel(sys.waves)+2} = [sys.y{2*numel(sys.waves)+2},'_y'];
                
                sys.OutputGroup.Flame = 2*numel(sys.waves)+[1,2];
            else
                sys.OutputGroup.Flame = 2*numel(sys.waves)+1;
            end
            
            sys.uptodate = true;
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            sys.Ts = pars.Ts;
            sys.fMax = pars.fMax;
            
            sys.filename = char(pars.filename);
            sys.modelname = char(pars.modelname);
            sys.active = strcmp(pars.active, 'on');

            if isfield(pars,'references')
                sys.pars2 = rmfield(pars,{'filename','Name','element','active','references'});
            else
                sys.pars2 = rmfield(pars,{'filename','Name','element','active'});
            end
            for field = fields(sys.pars2).'
                fc = char(field);
                references.pars2.(fc) = fc;
            end
        end
    end
end