classdef rankineHugoniot < AcBlock & sss
    % rankineHugoniot class describing a temperature jump across a flat
    % flame considering heat release fluctuations.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = rankineHugoniot(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.nRef: number of flame reference inputs/ heat release
    %          contributions
    %          * pars.Mach: downstream Mach number
    %          * pars.rho:  downstream density
    %          * pars.c:    downstream speed of sound
    % Output:  * sys: rankineHugoniot object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  19 Apr 2017
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
%     properties%(SetAccess = private)
%         % isentropic exponent
%         kappa
%     end
    
    methods
        function sys = rankineHugoniot(pars)
            % Determine number of flames
            if (iscell(pars.nRef))
                nFlames = eval(cell2mat(pars.nRef));
            else
                nFlames = pars.nRef;
            end
            FlamePorts = cell(nFlames,1);
            
            % Call constructor with correct number of ports and port type
            sys@AcBlock(AcBlock.Port,FlamePorts{:},AcBlock.Port);
            % Call constructor with correct in and output dimension
            sys@sss(zeros(2*numel(pars.waves),numel(pars.waves)+nFlames));
            
            sys.Name = pars.Name;
            sys.waves = pars.waves;
%             sys.kappa = 1.4;

            %% Create Block from Simulink getmodel()
            con = sys.Connection;
            idxAcOut = 2+nFlames;
            if iscell(pars.Mach)
                con{idxAcOut}.Mach = eval(cell2mat(pars.Mach));
                con{idxAcOut}.rho = eval(cell2mat(pars.rho));
                con{idxAcOut}.c = eval(cell2mat(pars.c));
            else
                con{idxAcOut}.Mach = pars.Mach;
                con{idxAcOut}.rho = pars.rho;
                con{idxAcOut}.c = pars.c;
            end
            sys.Connection = con;
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            %% Remove values past flame
            conac = con([1,end]);
            conacd =conac{2};
            confl = con(2:end-1);
            
            conac{2} = rmfield(conac{2},{'Mach','rho','c','alpha'});
            
            %% Solve for mean
            conac = Block.solveMean(conac);
            %% Patch in values past flame
            conac{2}.Mach = conacd.Mach;conac{2}.rho = conacd.rho;conac{2}.c = conacd.c;
            conac{2}.alpha = 0;
            
            con ={conac{1},confl{1:end},conac{2}}; 
            
            checkFlame = true;
            for i = 1: numel(confl)
                if not(isfield(sys.Connection{1+i},'FlameName'))
                    checkFlame = false;
                end
            end
            
            sys.Connection = con;
            if Block.checkPort(conac,AcBlock.Port)&&(checkFlame)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            if sys.uptodate
                return
            end
            
            M_u = -sys.Connection{1}.Mach;
            c_u = sys.Connection{1}.c;
            rho_u = sys.Connection{1}.rho;
            u_u = M_u*c_u;
            
            M_d = sys.Connection{end}.Mach;
            c_d = sys.Connection{end}.c;
            rho_d = sys.Connection{end}.rho;
            
            gamma = sys.Connection{1}.kappa;
            tempRatio = (c_d/c_u)^2;
            
            % prefactor = 1/( (rho_u*c_u)/(rho_d*c_d) + (T_d/T_u-1)*M_d + gamma*(T_d/T_u-1)*M_u + 1 );
            Denomminator = 1/( (rho_u*c_u)/(rho_d*c_d) + (tempRatio-1)*M_d + gamma*(tempRatio-1)*M_u + 1 );
            
            A(1,1) = -(rho_u*c_u)/(rho_d*c_d) + (tempRatio-1)*M_d - gamma*(tempRatio-1)*M_u + 1;
            A(1,2) = 2;
            A(2,1) = 2*( (rho_u*c_u)/(rho_d*c_d) - (tempRatio-1)^2*gamma*M_u*M_d ) ;
            A(2,2) = (rho_u*c_u)/(rho_d*c_d) + (tempRatio-1)*M_d - gamma*(tempRatio-1)*M_u - 1;
            
            % Heat release endogenous excitation
            
            A(1,3) = M_d + 1;
            % localMatrix(1,3) = localMatrix(1,3)*u_u/u_u*(tempRatio-1);
            
            A(2,3) = - M_d*( gamma*(tempRatio-1)*M_u +1 ) + ((rho_u*c_u)/(rho_d*c_d) + (tempRatio-1)*M_d) ;
            A(:,3) = A(:,3)*u_u*(tempRatio-1);
            
            A = Denomminator*A;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%             M_u = sys.Connection{1}.dir*sys.Connection{1}.Mach;
%             c_u = sys.Connection{1}.c;
%             rho_u = sys.Connection{1}.rho;
%             c_d = sys.Connection{end}.c;
%             rho_d = sys.Connection{end}.rho;
%             
%             k_u = sys.Connection{1}.kappa;
%             Term1 = M_u * (c_d^2/c_u^2 - 1);
%             Term2 = 1;
%             Term3 = rho_d*c_d / (rho_u*c_u);
%             
%             Denominator = 1 + Term1 + Term2*Term3 + k_u*Term1*Term3;
%             
%             D = [   -(1 - Term1 - Term2*Term3 + k_u*Term1*Term3) / Denominator,                                         2*Term3 / Denominator,                          (M_u + Term3) * c_u * Term1 / Denominator;...
%                     Term2 - k_u*Term1 + (Term2+k_u*Term1) * (1 - Term1 - Term2*Term3 + k_u*Term1*Term3) / Denominator,  1 - 2*Term3*(Term2+k_u*Term1) / Denominator,    c_u*Term1 - (M_u + Term3) * c_u * Term1 * (Term2+k_u*Term1) / Denominator];
% 
%             A = D;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            confl = sys.Connection(2:end-1);
            nFlames = size(confl,2);
            
            sys.D = [blkdiag(A(:,1:2),eye(numel(sys.waves)-2)),[A(:,3);zeros(numel(sys.waves)-2,1)]];
            for i = 3:numel(sys.waves)
                if strcmp(sys.waves{i},'alpha') ||... % phi waves are not transmitted
                        strncmp(sys.waves{i},'omega',5) % omega waves are not transmitted
                    sys.D(i,i) = 0;
                end
                if strcmp(sys.waves{i},'s') && ismember('alpha',sys.waves) % phi waves can cause temperature fluctuations
                    sys.D = [sys.D,zeros(numel(sys.waves),1)];
                    sys.D(i,end) = 1;
                end
            end
            sys.D = [sys.D(1:numel(sys.waves),:);eye(numel(sys.waves)),zeros(numel(sys.waves),size(sys.D,2)-numel(sys.waves));sys.D(numel(sys.waves)+1:end,:)]; % add inputs to outputs
            sys = twoport(sys,[1,2+nFlames]);

            FlameNames = cellfun(@(x) x.FlameName, confl, 'UniformOutput', false);
            Tcon = cellfun(@(x) isfield(x,'TFlame'),confl);
            Tcontrue = cellfun(@(x) isfield(x,'TFlame') && x.TFlame,confl);
            
            if any(Tcontrue)
                DQ = blkdiag(ones(1,nFlames),ones(1,sum(Tcontrue)));
            else
                DQ = ones(1,nFlames);
            end
            Qp = sss(DQ);
            Qp.u(1:nFlames) = FlameNames(:);
            Qp.u(Tcon) = cellfun(@(x) [x,'_Q'], Qp.u(Tcon), 'UniformOutput', false);
            Qp.u(nFlames+1:end) = cellfun(@(x) [x,'_T'], FlameNames(Tcontrue), 'UniformOutput', false);
            if any(Tcontrue)
                Qp.y = {sys.Name;[sys.Name,'_T']};
            else
                Qp.y = {sys.Name};
            end
            Qp.InputGroup.Flame = 1:nFlames+sum(Tcontrue);
            
            sys.u{numel(sys.waves)+1} = sys.Name;
            if ismember('alpha',sys.waves) && ismember('s',sys.waves)
                sys.u{numel(sys.waves)+2} = [sys.Name,'_T'];
            end
            sys = connect(sys,Qp,[sys.u(1:numel(sys.waves));Qp.u],sys.y);
            % re-sort OutputGroup for easier visual check of output order
            for wavec = sys.waves.'
                wave = char(wavec);
                auxSort = sortrows([sys.y(sys.OutputGroup.(wave)),num2cell(sys.OutputGroup.(wave).')]);
                sys.OutputGroup.(wave) = cell2mat(auxSort(:,2)).';
            end
            
            sys.uptodate = true;
        end
    end
end