classdef loudSpeaker_p < AcBlock & sss
    % LOUDSPEAKER class for calculating inhomogeneous solutions of the
    % network model.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = loudSpeaker_p(pars);
    % Input:   * pars.Name:  string of name of the chokedExit
    %          * pars.Amp:   amplification/gain of loudspeaker input
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: loudSpeaker_p object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  26 Nov 2018
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties(SetAccess = private)
        % amplification/gain of loudspeaker input
        Amp
    end

    properties (SetAccess = protected)
        Area % long range effects! 'protected' is a marker so changeParam cannot change it without updating the entire system
        Mach
        rho
        c_sound
        kappa
        Mmol
        alpha
    end

    methods
        function sys = loudSpeaker_p(pars)
            % Call empty constructors with correct in and output dimension
            % and port number
            sys@AcBlock(AcBlock.Port);
            sys@sss(zeros(1,2));
            
            sys.Amp=1;
            
            %% Create Block from Simulink getmodel()
            sys.Amp = eval(cell2mat(pars.Amp));
            sys.Name = pars.Name;
            sys.waves = pars.waves;

            % save BC for reference in changeParam
            for field = {'A','Mach','rho','c','kappa','Mmol','alpha'}
                fc = char(field);
                pc = fc;
                if strcmp(fc,'A')
                    pc = 'Area';
                elseif strcmp(fc,'c')
                    pc = 'c_sound';
                end
                if iscell(pars.(fc))
                    sys.(pc) = eval(cell2mat(pars.(fc)));
                else
                    sys.(pc) = pars.(fc);
                end
            end

            con = Block.readPort(pars,sys.Connection);
            % sign convention for Downstream BCs
            if strcmp(char(pars.loc),'Downstream')
                con{1}.Mach = -con{1}.Mach;
            end
            
            sys = set_Connection(sys, con);
        end
        
        function sys = set.Amp(sys,Amp)
            if not(isequal(sys.Amp, Amp))
                sys.Amp = Amp;
                sys.uptodate = false;
            end
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = con(1);
            if Block.checkPort(con(1),AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function sys = update(sys)
            if sys.uptodate
                return
            end
            outFlow = sys.Connection{1}.Mach<0 || (sys.Connection{1}.Mach==0 && sys.Connection{1}.dir<0); % is meanflow leaving the system here? No dir!
            nin = 1 + outFlow*(numel(sys.waves)-2);
            nout = numel(sys.waves);
            sys.D = [-1,zeros(1,nin-1),sys.Amp/sys.Connection{1}.rho/sys.Connection{1}.c;zeros(nout-1-nin,nin+1);eye(nin),zeros(nin,1)]; % f = -g + p'/(rho*c) or g = -f + p'/(rho*c)
            sys = oneport(sys);
            
            sys.InputName(nin+1) = {sys.Name};
            sys.InputGroup.loudSpeaker = nin+1;
            
            sys.uptodate = true;
        end
    end
end