classdef simpleDuct < AcBlock & ss
    % simpleDuct simple acoustic model of a duct.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = simpleDuct(pars);
    % Input:   * pars.Name: string of name of the chokedExit
    %          * pars.l:    length of duct
    %          * pars.fMax: maximum frequency to be resolved by duct
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: simpleDuct object
    %
    % sys = simpleDuct(Duct);
    % Input:   * Duct: Duct model
    % Output:  * simpleDuct: analytic time delay duct model
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de)
    % Last Change:  27 Mar 2015
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block, Duct
    
    properties
        l,fMax,cProp,StateGroup,n;
    end
    
    methods
        function sys = simpleDuct(pars)
            % Call empty constructors with correct in and output dimension
            % and port number
            sys@AcBlock(AcBlock.Port,AcBlock.Port);
            sys@ss(zeros(2*numel(pars.waves),numel(pars.waves)));

            sys.fMax = pars.fMax;
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            
            if iscell(pars.l)
                sys.l = eval(cell2mat(pars.l));
            else
                sys.l = pars.l;
            end
            
            % Copy Connection from Duct to simpleDuct
            if isa(pars, 'Duct')
                sys = set_Connection(sys, pars.Connection);
            end
            
            con = Block.readPort(pars,sys.Connection);
            sys = set_Connection(sys, con);
        end
        
        %% Set functions
        function sys = set.l(sys, l)
            if not(isequal(sys.l, l))
                sys.l = l;
                sys.uptodate = false;
            end
        end
        function sys = set.fMax(sys, fMax)
            if not(isequal(sys.fMax, fMax))
                sys.fMax = fMax;
                sys.uptodate = false;
            end
        end
        
        %% Get functions
        function n = get.n(sys)
            n = size(sys.D);
        end
        function cProp = get.cProp(sys)
            c = sys.Connection{1}.c;
            Mach = sys.Connection{1}.dir*sys.Connection{1}.Mach;
            u = c*Mach;
            for wavec = sys.waves'
                wave = char(wavec);
                if strcmp(wave,'f'); cProp.f = u+c; %#ok<ALIGN>
                elseif strcmp(wave,'g'); cProp.g = u-c;
                elseif strcmp(wave,'omega1'); cProp.omega1 = u; % adapt with Alp!
                elseif strcmp(wave,'omega2'); cProp.omega2 = u; % adapt with Alp!
%                 elseif strcmp(wave,'omegafree'); cProp.omegafree = u;
                else; cProp.(wave) = u; end % default
            end
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            % exception for position
            savePos = con{2}.pos;
            con{2}.pos = con{1}.pos;
            % solve
            sys.Connection = Block.solveMean(con);
            sys.Connection{2}.pos = savePos; % insert exception
            % update
            if Block.checkPort(sys.Connection,AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Declaration of Abstract functions
        function sys = update(sys)
            %% Check if system is uptodate
            if sys.uptodate
                return
            end
            
            cP = sys.cProp;
            
            D = eye(numel(sys.waves));
            sys.D = [D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)
            auxDelay = [sys.l/cP.g;sys.l/cP.f]; % g, then f, because of OutputDelay, instead of InputDelay
            for i = 3:numel(sys.waves)
                if cP.(sys.waves{i}) == 0
                    auxDelay = [auxDelay;0]; %#ok<AGROW>
                    sys.D(i,i) = 0; % no convective transfer without mean flow
                else
                    auxDelay = [auxDelay;sys.l/cP.(sys.waves{i})]; %#ok<AGROW>
                end
            end
            auxDelay = [auxDelay;zeros(numel(sys.waves),1)];
            sys.OutputDelay = abs(auxDelay); % direction is already given by In/Out distribution
            
            % Give names to the ports and create in- and output groups
            sys = twoport(sys);
            
            %% Populate plotting quantities
            sys.state.x = [0,sys.l];
            
            sys.uptodate = true;
        end
    end
end