classdef referenceGeneral < AcBlock & sss
    % REFERENCE contains the heat release model for global heat release
    % fluctuations
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = referenceGeneral(pars);
    % Input:   * pars.Name:         string of name of the chokedExit
    %          * pars.Ts:           desired sampling time of the model
    %          * pars.filename:     name of the file containing the model
    %          * pars.filename2:    name of the second file containing the model
    %          * pars.active:       flame is active or inactive
    % Output:  * sys:               referenceGeneral object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert, Felix Schily, Guillaume Fournier (fournier@tfd.mw.tum.de)
    % Last Change:  21 Sep 2021
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block
    
    properties
        filename,active,pars2,fMax,refType,dp_fuel,maketf,makefrd
%         filename,active,fMax,refType,dp_fuel,maketf,makefrd
    end
    properties (SetAccess = protected)
        filename2 % long range effects, because the connection to symRankineHugoniot is redefined, which influences Q-coefficient in symRankineHugoniot!
        % 'protected' is a marker so changeParam cannot change it without updating the entire system
    end
    properties (SetAccess = immutable) % can only be changed by constructor
        pars % memory how things were when the constructor was last used
    end

    methods
        function sys = referenceGeneral(pars)
            % Call empty constructors with correct in and output dimension
            % and port number
            sys@AcBlock(AcBlock.Port,AcBlock.Port,[]);
            sys@sss(zeros(2*numel(pars.waves)+1,numel(pars.waves)));
            
            %% Create Block from Simulink getmodel()
            % outsource the translation from parameters to properties,
            % except sys.pars itself (because pars is immutable)
            sys.pars = pars;
            [sys,references] = sys.pars2sys(sys, pars);
            sys.pars.references = references;
            
            con = sys.Connection;
            con{3}.FlameName = ['Q_',sys.Name];
            if strcmp(sys.refType,'phi''/phimean')
                con{3}.FTFphi = true;
            else
                con{3}.FTFphi = false;
            end
            if ~isempty(sys.filename2)
                con{3}.knownETF = true;
            else
                con{3}.knownETF = false;
            end
            sys = set_Connection(sys, con);
            
        end

        %% Set functions
        function sys = set.active(sys, active)
            if not(isequal(sys.active, active))
                sys.active = active;
                sys.uptodate = false;
            end
        end
        function sys = set.filename(sys, filename)
            if not(isequal(sys.filename, filename))
                sys.filename = filename;
                sys.uptodate = false;
            end
        end
        function sys = set.filename2(sys, filename)
            if not(isequal(sys.filename2, filename))
                sys.filename2 = filename;
                sys.uptodate = false;
            end
        end
        function sys = set.fMax(sys, fMax)
            if not(isequal(sys.fMax, fMax))
                sys.fMax = fMax;
                sys.uptodate = false;
            end
        end
        function sys = set.pars2(sys, pars)
            if not(isequal(sys.pars2, pars))
                sys.pars2 = pars;
                sys.uptodate = false;
            end
        end
        function sys = set.dp_fuel(sys,dp_fuel)
            if not(isequal(sys.dp_fuel, dp_fuel))
                sys.dp_fuel = dp_fuel;
                sys.uptodate = false;
            end
        end
        function sys = set.refType(sys,refType)
            if not(isequal(sys.refType, refType))
                sys.refType = refType;
                sys.uptodate = false;
            end
        end
        function sys = set.maketf(sys,maketf)
            if not(isequal(sys.maketf, maketf))
                sys.maketf = maketf;
                sys.uptodate = false;
            end
        end
        function sys = set.makefrd(sys,makefrd)
            if not(isequal(sys.makefrd, makefrd))
                sys.makefrd = makefrd;
                sys.uptodate = false;
            end
        end
        
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            conFl = con(3);
            con = con(1:2);
            con = Block.solveMean(con);
            con(3) = conFl;
            sys.Connection = con;
            if Block.checkPort(con(1:2),AcBlock.Port)
                sys = update(sys);
            end
        end
        
        %% Generate system
        function [sys] = update(sys)
            % FTF_REFERENCE FTF Flame model reference system generation for arbitrary reference quantity.
            if sys.uptodate
                return
            end
            sys = clear(sys);
            frddisc = 1e4;

            switch sys.refType
                case 'u''/umean'
                    % Name of u'/ubar reference velocity
                    RefName = ['u_',sys.Name];
                    fMax4Delay = max(sys.fMax.f,sys.fMax.g);

                    if sys.Connection{1}.Mach==0
                        auxD = zeros(1,numel(sys.waves));
                    else
                        Den =  -1/(sys.Connection{1}.Mach*sys.Connection{1}.c); % minus because of sign convention at upstream Connection: Mach pointing outwards, i.e. it is typically negative
                        auxD = [Den, -Den, zeros(1,numel(sys.waves)-2)];
                    end
                case 'p''/(rhomean*cmean^2)'
                    % Name of p'/(rho*c*c) reference pressure
                    RefName = ['p_',sys.Name];
                    fMax4Delay = max(sys.fMax.f,sys.fMax.g);

                    Den =  1/sys.Connection{1}.c; % normalized with rho*c^2 for low frequency limit 1
                    auxD = [Den, Den, zeros(1,numel(sys.waves)-2)];
                case 'utan''/utanmean (tangential)'
                    % Name of utan'/utanbar reference
                    RefName = ['utan_',sys.Name];

                    auxD = zeros(1,numel(sys.waves));
                    warning('Swirl wave reference is not implemented yet.');
                    auxD(strcmp(sys.waves,sys.refType)) = 1; % if there is no corresponding quantity, reference is not used.
                    if ~any(strncmp(sys.waves,'omega',5))
                        warning(['Reference element ' sys.Name ' is inactive, as it refers to a quantity not regarded in the simulation.']);
                        fMax4Delay = 1;
                    else
                        fMax4Delay = max(cellfun(@(x) sys.fMax.(x),sys.waves(strncmp(sys.waves,'omega',5))));
                    end
                case 'phi''/phimean' % new version: from convective alpha
                    % Name of phi'/phibar reference equivalence ratio
                    RefName = ['phi_',sys.Name];

                    auxD = zeros(1,numel(sys.waves));
                    auxD(strcmp(sys.waves,'alpha')) = 1/(1-sys.Connection{1}.alpha)/sys.Connection{1}.alpha; % rewrite alpha' as phi'/phibar.
                    if ~any(strcmp(sys.waves,'alpha'))
                        warning(['Reference element ' sys.Name ' is inactive, as it refers to a quantity not regarded in the simulation.']);
                        fMax4Delay = 1;
                    else
                        fMax4Delay = max(cellfun(@(x) sys.fMax.(x),sys.waves(strcmp(sys.waves,'alpha'))));
                    end
                case 's''/cp' % new version: from convective entropy
                    % Name of s'/cp reference
                    RefName = ['s_',sys.Name];

                    auxD = zeros(1,numel(sys.waves));
                    auxD(strcmp(sys.waves,'s')) = 1; % if there is no corresponding quantity, reference is not used.
                    if ~any(strcmp(sys.waves,'s'))
                        warning(['Reference element ' sys.Name ' is inactive, as it refers to a quantity not regarded in the simulation.']);
                        fMax4Delay = 1;
                    else
                        fMax4Delay = max(cellfun(@(x) sys.fMax.(x),sys.waves(strcmp(sys.waves,'s'))));
                    end
                otherwise % new version: all regular convective quantities
                    % Name of wave'/wavebar reference
                    RefName = [sys.refType,'_',sys.Name];

                    auxD = zeros(1,numel(sys.waves));
                    auxD(strcmp(sys.waves,sys.refType)) = 1; % if there is no corresponding quantity, reference is not used.
                    if ~any(strcmp(sys.waves,sys.refType))
                        warning(['Reference element ' sys.Name ' is inactive, as it refers to a quantity not regarded in the simulation.']);
                        fMax4Delay = 1;
                    else
                        fMax4Delay = max(cellfun(@(x) sys.fMax.(x),sys.waves(strcmp(sys.waves,sys.refType))));
                    end
            end
            sys.D = eye(numel(sys.waves));
            sys.D = [sys.D([2,1,3:end],:);eye(numel(sys.waves))]; % exchange f and g (--> scattering matrix)
            sys.D = [sys.D;auxD];
            
            sys = twoport(sys);
            
            sys.y{2*numel(sys.waves)+1} = RefName;
            
            if sys.active
                %% Get names of signals
                [~,name,ext] = fileparts(sys.filename);
                if isempty(ext)
                    error('Specify extension (.m or .mat) of flame in reference Block.')
                else
                    if strcmp(ext,'.m')
                        [FTFsys,sys.pars2] = feval(name,sys.pars2,sys.state);
                    elseif strcmp(ext,'.mat')
                        FTFsys = load(sys.filename); % load model
                        modelname = fields(FTFsys);
                        if numel(modelname) == 1
                            FTFsys = FTFsys.(modelname{1});
                        else
                            error('Invalid model: the .mat file loaded contains multiple variables. Please load only a single FTF.')
                        end
                    end
                end
                % Name of heat release Q'/Qbar
                QFlameName = [sys.Connection{3}.FlameName,'_Q'];
                
                FTFsys.u{1} = RefName;
                FTFsys.y = {QFlameName};
                if isempty(sys.filename2)
                    ext2 = '.mat';
                else
                    [~,~,ext2] = fileparts(sys.filename2); % already needed here for check
                end
                if sys.maketf && strcmp(ext,'.mat') && strcmp(ext2,'.mat')
                    tfsys = tf(connect(tf(ss(sys)), FTFsys, [sys.u; FTFsys.u], [sys.y(1:end-1);QFlameName;RefName]));
                elseif sys.makefrd && strcmp(ext,'.mat') && strcmp(ext2,'.mat')
                    frdsys = connect(ss(sys), frd(FTFsys,[0,logspace(0,log(2*pi/FTFsys.Ts),frddisc)]), [sys.u; FTFsys.u], [sys.y(1:end-1);QFlameName;RefName]);
                end
                if (isa(FTFsys,'idpoly')||isa(FTFsys,'idtf'))
                    % Adapt and append noise input
                    FTFsys = sss(adaptTsAndDelays(FTFsys,sys.Ts,fMax4Delay));
                    sys = connect(sys, FTFsys, [sys.u; FTFsys.u{1}; FTFsys.u(FTFsys.InputGroup.Noise)], [sys.y(1:end-1);QFlameName;RefName]);
                else
                    % Adapt and do not append noise input
                    FTFsys = sss(adaptTsAndDelays(FTFsys,sys.Ts,fMax4Delay));
                    sys = connect(sys, FTFsys, [sys.u; FTFsys.u], [sys.y(1:end-1);QFlameName;RefName]);
                end
                % re-sort OutputGroup for easier visual check of output order
                for wavec = sys.waves.'
                    wave = char(wavec);
                    auxSort = sortrows([sys.y(sys.OutputGroup.(wave)),num2cell(sys.OutputGroup.(wave).')]);
                    sys.OutputGroup.(wave) = cell2mat(auxSort(:,2)).';
                end
                
                % add transfer function for T, if given
                if strncmp(sys.refType,'phi',3) && ~isempty(sys.filename2)
                    [~,name2,ext2] = fileparts(sys.filename2);
                    if isempty(ext2)
                        error('Specify extension (.m or .mat) of flame in reference Block.')
                    else
                        if strcmp(ext2,'.m')
                            [TTFsys,sys.pars2] = feval(name2,sys.pars2,sys.state);
                        elseif strcmp(ext2,'.mat')
                            TTFsys = load(sys.filename2); % load model
                            modelname2 = fields(TTFsys);
                            if numel(modelname2) == 1
                                TTFsys = TTFsys.(modelname2{1});
                            else
                                error('Invalid model: the .mat file loaded contains multiple variables. Please load only a single ETF.')
                            end
                        end
                    end
                    % Name of heat release T'/Tbar
                    TFlameName = [sys.Connection{3}.FlameName,'_T'];

                    TTFsys.u{1} = [RefName,'_T']; % using additional name for disambiguation
                    sys.y{strcmp(sys.y,RefName)} = [RefName,'_T']; % using additional name for disambiguation
                    TTFsys.y = {TFlameName};
                    if sys.maketf && strcmp(ext,'.mat') && strcmp(ext2,'.mat')
                        tfsys.y{strcmp(tfsys.y,RefName)} = [RefName,'_T']; % using additional name for disambiguation
                        tfsys = tf(connect(tfsys, TTFsys, [sys.u; TTFsys.u(~ismember(TTFsys.u,sys.u))], [sys.y(1:end-1);TFlameName;[RefName,'_T']]));
                        % undo additional name for output and complete naming scheme for input
                        tfsys.y{strcmp(tfsys.y,[RefName,'_T'])} = RefName;
                        tfsys.u{strcmp(tfsys.u,RefName)} = [RefName,'_Q'];
                    elseif sys.makefrd && strcmp(ext,'.mat') && strcmp(ext2,'.mat')
                        frdsys.y{strcmp(frdsys.y,RefName)} = [RefName,'_T']; % using additional name for disambiguation
                        frdsys = connect(frdsys, frd(TTFsys,[0,logspace(0,log(2*pi/TTFsys.Ts),frddisc)]), [frdsys.u; TTFsys.u(~ismember(TTFsys.u,frdsys.u))], [frdsys.y(1:end-1);TFlameName;[RefName,'_T']]);
                        % undo additional name for output and complete naming scheme for input
                        frdsys.y{strcmp(frdsys.y,[RefName,'_T'])} = RefName;
                        frdsys.u{strcmp(frdsys.u,RefName)} = [RefName,'_Q'];
                    end
                    if (isa(TTFsys,'idpoly')||isa(TTFsys,'idtf'))
                        % Adapt and append noise input
                        TTFsys = sss(adaptTsAndDelays(TTFsys,sys.Ts,fMax4Delay));
                        sys = connect(sys, TTFsys, [sys.u; TTFsys.u{1} ;TTFsys.u(TTFsys.InputGroup.Noise)], [sys.y(1:end-1);TFlameName;[RefName,'_T']]);
                    else
                        % Adapt and do not append noise input
                        TTFsys = sss(adaptTsAndDelays(TTFsys,sys.Ts,fMax4Delay));
                        sys = connect(sys, TTFsys, [sys.u; TTFsys.u(~ismember(TTFsys.u,sys.u))], [sys.y(1:end-1);TFlameName;[RefName,'_T']]);
                    end
                    % re-sort OutputGroup for easier visual check of output order
                    for wavec = sys.waves.'
                        wave = char(wavec);
                        auxSort = sortrows([sys.y(sys.OutputGroup.(wave)),num2cell(sys.OutputGroup.(wave).')]);
                        sys.OutputGroup.(wave) = cell2mat(auxSort(:,2)).';
                    end
                    % undo additional name for output and complete naming scheme for input
                    sys.y{strcmp(sys.y,[RefName,'_T'])} = RefName;
                    sys.u{strcmp(sys.u,RefName)} = [RefName,'_Q'];
                    % OutputGroup for two transfer functions
                    sys.OutputGroup.Flame = 2*numel(sys.waves)+[1,2,3];
                else
                    % OutputGroup for only one transfer function
                    sys.OutputGroup.Flame = 2*numel(sys.waves)+[1,2];
                end
                
                % Rename flame reference input to avoid reconnecting in tax
                sys.y{end} = [sys.y{end},'_y'];
                if sys.maketf && strcmp(ext,'.mat') && strcmp(ext2,'.mat')
                    tfsys.y{end} = [tfsys.y{end},'_y'];
                elseif sys.makefrd && strcmp(ext,'.mat') && strcmp(ext2,'.mat')
                    frdsys.y{end} = [frdsys.y{end},'_y'];
                end
            else
                % OutputGroup without transfer function
                sys.OutputGroup.Flame = 2*numel(sys.waves)+1;
                if sys.maketf
                    sys = tf(ss(sys)); % make a ss first, as ss is defined for sss, then turn ss into tf
                elseif sys.makefrd
                    sys = frd(ss(sys),[0,logspace(0,log(2*pi*fMax4Delay*10),frddisc)]); % make a ss first, as ss is defined for sss, then turn ss into frd
                end
            end
            
            sys.uptodate = true;
            
            if sys.maketf % return tfsys
                sys = tfsys;
            elseif sys.makefrd
                sys = frdsys;
            end
        end
    end
    
    methods(Static)
        %% Convert pars to sys.(property) and back
        % This method is static to allow changeParam to set up a dummy
        % element as struct.
        function [sys,references] = pars2sys(sys, pars) % used in constructor or changeParam
            % references are later added to pars for all cases where the
            % property does not have the same name as the parameter
            sys.Name = pars.Name;
            sys.waves = pars.waves;
            sys.Ts = pars.Ts;
            sys.fMax = pars.fMax;
            if ~isfield(pars,'refType')
                sys.refType = 'u''/umean'; % default for general use
            elseif iscell(pars.refType)
                sys.refType = char(pars.refType);
            else
                sys.refType = pars.refType;
            end
            if isfield(pars,'dp_fuel')
                sys.dp_fuel = eval(cell2mat(pars.dp_fuel));
            end
            if ~isfield(pars,'maketf')
                sys.maketf = false; % default for general use
            end
            if ~isfield(pars,'makefrd')
                sys.makefrd = false; % default for general use
            end
            
            sys.filename = char(pars.filename);
            if isfield(pars,'filename2') && ~strcmp(char(pars.filename2),'[]')
                sys.filename2 = char(pars.filename2);
            else
                sys.filename2 = '';
            end
            sys.active = strcmp(pars.active, 'on');

            if isfield(pars,'references')
                sys.pars2 = rmfield(pars,{'filename','Name','element','active','references'});
            else
                sys.pars2 = rmfield(pars,{'filename','Name','element','active'});
            end
            for field = fields(sys.pars2).'
                fc = char(field);
                references.pars2.(fc) = fc;
            end
        end
    end
end