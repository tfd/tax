classdef Element < AcBlock & sss
    % Element generic tax element block class. This can be used to
    % load custom made Blocks, AcBlocks, sss,... into tax.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = Element(pars);
    % Input:   * pars.Name:      string of name of the chokedExit
    %          * pars.fileName:  filename of model
    %          * pars.modelName: name of the model
    % optional * pars.(...): see link to AcBlock.Port definitions below to
    %          specify mean values and port properties in constructor
    % Output:  * sys: Element object
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de)
    % Last Change:  27 Mar 2015
    % ------------------------------------------------------------------
    % See also: AcBlock.Port, AcBlock, Block, sss
    
    properties
        fileName,modelName,acPorts
    end
    methods
        function sys = Element(varargin)
            % Call empty constructors with correct in and output dimension
            % and port number
            if nargin==1 && isstruct(varargin{1})
                %% Create Block from Simulink getmodel()
                pars = varargin{1};
                nPorts = eval(cell2mat(pars.nIn)) + eval(cell2mat(pars.nOut));
                if strcmp(cell2mat(pars.acPorts),'on')
                    ports = repmat({AcBlock.Port},[1,nPorts]);
                else
                    ports = cell(nPorts,1);
                end
                con = ports;
                filename = char(pars.filename);
                modelname = char(pars.modelname);
                name = pars.Name;
                importSys = load(filename,modelname);
                importSys = importSys.(modelname);
            elseif nargin==3
                %% Manually create Block
                importSys = varargin{1};
                con = varargin{2};
                ports = cellfun(@(x) fieldnames(x)', con ,'UniformOutput', false);
                name = importSys.Name;
                filename = '';
                modelname = '';
            end
            sys@AcBlock(ports{:});
            sys@sss();
            if nargin==3 || ~strcmp(cell2mat(pars.acPorts),'on') % manual specification of the block requires full connection definition (no propagation through block)
                sys.Connection = con;
            end
            
            %% Load model
            if isa(importSys,'Element')
                %% Overwrite Block if it is an Element
                sys = importSys;
            else
                %% Update system Matrices if it is something else
                sys = sys.updatesss(importSys);
            end
            
            sys.Name = name;
            if nargin==1 && isstruct(varargin{1})
                sys.waves = pars.waves;
            elseif nargin==3
                sys.waves = varargin{3};
            end
            sys.fileName = filename;
            sys.modelName = modelname;
            sys.StateGroup.(sys.Name) = 1:sys.n;
            sys.Connection = sys.Connection;
            if nargin==3 || strcmp(cell2mat(pars.acPorts),'on')
                sys.acPorts = true;
            else
                sys.acPorts = false;
            end
        end
        %% Mean values on interfaces
        function sys = set_Connection(sys, con)
            sys.Connection = con;
            % always update, except, if acPorts, but incomplete port definition
            if ~(sys.acPorts && ~Block.checkPort(sys.Connection,AcBlock.Port))
                sys = update(sys);
            end
        end
        
        function sys =  update(sys)
            if sys.acPorts
                savesys = sys;
                sys = sys.clear;
                sys.D = zeros(numel(sys.Connection)*numel(sys.waves));
                sys = sys.multiport;
                nIn = sum(cellfun(@(x) ~isempty(x), sys.u));
                sys.D = zeros(numel(sys.Connection)*numel(sys.waves),nIn);
                sys.u = sys.u(1:nIn);
                % we assume that all inputs and outputs of savesys are found in the new sys
                idIn = cell2mat(cellfun(@(x) find(strcmp(savesys.u,x)), sys.u, 'UniformOutput', false));
                idOut = cell2mat(cellfun(@(x) find(strcmp(savesys.y,x)), sys.y, 'UniformOutput', false));
                if numel(idIn)~=numel(savesys.u) || numel(idOut)~=numel(savesys.y)
                    error('The inputs and/or outputs of the model do not match the inputs and outputs of a Block with acoustic ports.');
                end
                sys.A = savesys.A;
                sys.B = zeros(sys.n,nIn);
                sys.B(:,idIn) = savesys.B;
                sys.C = zeros(numel(sys.Connection)*numel(sys.waves),sys.n);
                sys.C(idOut,:) = savesys.C;
                sys.D(idOut,idIn) = savesys.D;
                sys.E = eye(savesys.n);
                % fill throughput outputs
                nOut = size(sys.D,1) - nIn; % regular outputs
                for i = 1:nIn
                    if sys.D(nOut+i,i) == 0 % replace zeros with a copy from the input
                        sys.D(nOut+i,i) = 1;
                    end
                end
            else % (legacy)
                for i = 1:length(sys.Connection)
                    if Block.isPort(sys.Connection(i),AcBlock.Port)
                        portName = ['port',num2str(i)];
                        if (sys.Connection{i}.dir==1) % upstream port
                            sys.y{sys.OutputGroup.(portName)} = [num2str(sys.Connection{i}.idx,'%02d'),'f'];
                            sys.u{sys.InputGroup.(portName)}  = [num2str(sys.Connection{i}.idx,'%02d'),'g'];
                            sys.OutputGroup.f = sys.OutputGroup.(portName);
                        else % downstream port
                            sys.u{sys.InputGroup.(portName)}  = [num2str(sys.Connection{i}.idx,'%02d'),'f'];
                            sys.y{sys.OutputGroup.(portName)} = [num2str(sys.Connection{i}.idx,'%02d'),'g'];
                            sys.OutputGroup.g = sys.OutputGroup.(portName);
                        end
                    end
                end
            end
            sys.uptodate = true;
        end
    end
end