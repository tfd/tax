function FullFixedFlame = taXFixed_MachOrder1(C,Ma_1,Ma_2,c_1,c_2,gamma_1,gamma_2,q,rho_1,rho_2)
%TAXFIXED_MACHORDER1
%    FULLFIXEDFLAME = TAXFIXED_MACHORDER1(C,MA_1,MA_2,C_1,C_2,GAMMA_1,GAMMA_2,Q,RHO_1,RHO_2)

%    This function was generated by the Symbolic Math Toolbox version 8.0.
%    27-Oct-2021 21:47:10

t2 = c_2.^2;
t3 = Ma_1.^2;
t4 = c_1.^2;
t5 = Ma_2.^2;
t6 = Ma_1.*q.*rho_1;
t7 = Ma_1.*rho_1.*t2;
t8 = Ma_2.*rho_2.*t2;
t9 = C.*Ma_1.*rho_1.*t2;
t12 = Ma_1.*gamma_2.*q.*rho_1;
t10 = t6+t7+t8+t9-t12;
t11 = 1.0./t10;
t13 = rho_1.*t3.*t4;
t14 = Ma_1.*Ma_2.*rho_2.*t2;
t15 = gamma_2.*rho_2.*t2.*t5;
t16 = t6+t7-t8+t9-t12;
t17 = 1.0./Ma_2;
t18 = 1.0./t10.^2;
t19 = Ma_1.*Ma_2.*rho_1.*t2;
t31 = rho_2.*t2.*t5;
t32 = gamma_2.*rho_1.*t3.*t4;
t20 = t13-t14+t15+t19-t31-t32;
t21 = q.*rho_1;
t22 = rho_1.*t2;
t23 = C.*rho_1.*t2;
t24 = C.*t2;
t38 = gamma_2.*q;
t25 = q+t24-t38;
t26 = Ma_1-Ma_2;
t27 = q.*rho_2;
t28 = rho_2.*t2;
t29 = C.*rho_2.*t2;
t34 = gamma_2.*q.*rho_2;
t30 = t27+t28+t29-t34;
t33 = 1.0./Ma_1;
t35 = t11.*t16;
t36 = gamma_1-1.0;
t37 = 1.0./t36;
t39 = rho_1.*t2.*t3;
t40 = q.*rho_1.*t3;
t41 = C.*rho_1.*t2.*t3;
t45 = gamma_2.*q.*rho_1.*t3;
t42 = t14+t39+t40+t41-t45;
t43 = gamma_2.*2.0;
t44 = t43-2.0;
t46 = 1.0./t42;
t47 = gamma_2-1.0;
t48 = C.*rho_2.*t2.*t5;
t49 = Ma_1.*rho_1.*t4;
t50 = Ma_2.*rho_2.*t4;
t51 = C.*Ma_1.*rho_1.*t4;
t52 = Ma_2.*gamma_1.*q.*rho_2;
t57 = Ma_2.*q.*rho_2;
t53 = t49+t50+t51+t52-t57;
t54 = C.*rho_1.*t3.*t4;
t55 = Ma_1.*Ma_2.*q.*rho_1;
t56 = Ma_1.*Ma_2.*q.*rho_2;
t58 = t6+t57;
t59 = t48-t54+t55-t56;
t60 = t11.*t59;
t61 = t47.*(t60-t18.*t20.*t58);
t62 = t11.*t47.*t58;
t63 = t61+t62;
t64 = 1.0./t42.^2;
t65 = Ma_1.*rho_1.*t3.*t4;
t66 = Ma_2.*rho_1.*t2.*t3;
t67 = Ma_1.*gamma_2.*rho_2.*t2.*t5;
t68 = t65+t66+t67-Ma_2.*rho_2.*t2.*t3-Ma_1.*rho_2.*t2.*t5-Ma_1.*gamma_2.*rho_1.*t3.*t4;
t69 = t18.*t20;
t70 = gamma_1.*rho_2.*t2.*t5;
t71 = t8+t49-Ma_2.*gamma_1.*rho_2.*t2-Ma_1.*gamma_2.*rho_1.*t4;
t72 = t7+t8;
t73 = t11.*t20;
t74 = t73-t18.*t20.*t72;
t75 = C.*t74;
t76 = C.*t11.*t72;
t77 = t75+t76;
FullFixedFlame = reshape([t35-t11.*(t13+t14+t15-rho_2.*t2.*t5-gamma_2.*rho_1.*t3.*t4-Ma_1.*Ma_2.*rho_1.*t2)+t16.*(Ma_1.*t11.*2.0-t18.*t20),t5.*t33.*(t11.*t26.*t30-t18.*t20.*t30).*2.0+t5.*t11.*t30.*t33.*2.0,-t44.*(t46.*(-t13+t31+t48+t56-C.*rho_1.*t3.*t4)-Ma_2.*q.*rho_2.*t64.*t68)-Ma_2.*q.*rho_2.*t44.*t46,C.*(t46.*(t13+t14+t15-t31-t32)-Ma_2.*rho_2.*t2.*t64.*t68).*-2.0-C.*Ma_2.*rho_2.*t2.*t46.*2.0,rho_1.*t2.*t3.*t17.*(t11.*t26-t18.*t20).*2.0+rho_1.*t2.*t3.*t11.*t17.*2.0,-t35-t11.*(t13+t14+t15-t19-t31-t32)+t16.*(t69+Ma_2.*t11.*2.0),t17.*t47.*(t11.*(-t13+t31+t55)+Ma_1.*q.*rho_1.*t18.*t20).*-2.0+Ma_1.*q.*rho_1.*t11.*t17.*t47.*2.0,C.*Ma_1.*rho_1.*t2.*t17.*(t69+Ma_2.*t11).*-2.0+C.*Ma_1.*rho_1.*t2.*t11.*t17.*2.0,t3.*t11.*t37.*(t21+t22+t23-rho_1.*t4-gamma_1.*q.*rho_1-gamma_2.*q.*rho_1-gamma_1.*rho_1.*t2+gamma_2.*rho_1.*t4+gamma_1.*gamma_2.*q.*rho_1-C.*gamma_1.*rho_1.*t2),t5.*t11.*t37.*(t27+t28+t29-t34-rho_2.*t4-gamma_1.*q.*rho_2-gamma_1.*rho_2.*t2+gamma_2.*rho_2.*t4+gamma_1.*gamma_2.*q.*rho_2-C.*gamma_1.*rho_2.*t2),t37.*t47.*(t11.*(t13-t31-t48+t54+t70-gamma_1.*rho_1.*t3.*t4+Ma_1.*Ma_2.*rho_1.*t4-Ma_1.*Ma_2.*rho_2.*t4-C.*gamma_1.*rho_1.*t3.*t4+C.*gamma_1.*rho_2.*t2.*t5)-t18.*t20.*t53)+t11.*t37.*t47.*t53,-C.*t37.*(t11.*(t13+t15-t31-t32+t70-gamma_1.*rho_1.*t3.*t4+gamma_1.*gamma_2.*rho_1.*t3.*t4-gamma_1.*gamma_2.*rho_2.*t2.*t5)-t18.*t20.*t71)-C.*t11.*t37.*t71,t3.*t11.*(t21+t22+t23-gamma_2.*q.*rho_1),t5.*t11.*t30,t47.*(t11.*(t13-t31-t48+t54)+Ma_2.*q.*rho_2.*t18.*t20)-Ma_2.*q.*rho_2.*t11.*t47,-C.*(t11.*(t13+t15-t31-t32)-Ma_2.*rho_2.*t2.*t18.*t20)-C.*Ma_2.*rho_2.*t2.*t11,-rho_1.*t3.*t11.*t25,-rho_2.*t5.*t11.*t25,t63,t77,-rho_1.*t3.*t11.*t25,-rho_2.*t5.*t11.*t25,t63,t77],[4,6]);
