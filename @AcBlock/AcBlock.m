classdef AcBlock < Block
    % AcBlock class is a template for acoustic blocks of a tax network
    % model. 
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  24 Apr 2018
    % ------------------------------------------------------------------
    % See also: Block
    
    properties (Constant)
        % Port definition for an acoustic plain wave port
        % * pars.rho:   {double} of density [kg/m^3]
        % * pars.Mach:  {double} of Mach Number [-]
        % * pars.c:     {double} of speed of sound [m/s]
        % * pars.A:     {double} cross-sectional Area [m^2]
        % * pars.kappa: {double} isentropic exponent [-]
        % * pars.Mmol:  {double} molar mass [kg/kmol] or [g/mol]
        % * pars.alpha: {double} fuel mass fraction = mdot_fuel/mdot [-]
        % * pars.idx:   {integer} global index of connection
        % * pars.dir:   {+-1} direction/orientation of the port (up- or downstream)
        % * pars.pos:   {1x3 double} three dimensional position of the port for plotting [m,m,m]
        Port = {'rho','Mach','c','A','kappa','Mmol','alpha','idx','dir','pos'};
    end
    
    properties
        state, waves
    end
    
    methods
        function sys = AcBlock(varargin)
            sys@Block(varargin{:});

            sys.state.f.idx = 1;
            sys.state.f.x = 0;
            sys.state.g.idx = 1;
            sys.state.g.x = 0;
            
            Vals = sys.Port(~strcmp(sys.Port,'dir') & ~strcmp(sys.Port,'idx')); % exclude dir and idx from sys.Port for loop
            for Valc = Vals
                Val = char(Valc);
                sys.state.f.(Val) = [];
                sys.state.g.(Val) = [];
            end
        end
        
        function state = get.state(sys)
            state = sys.state;
%             [idxAcPort] = find(cellfun(@(x) Block.checkPort({x},AcBlock.Port),sys.Connection));
            idx = find(cellfun(@(x) Block.checkPort({x},AcBlock.Port),sys.Connection));
%             [idxPortDown] = cellfun(@(x) x.dir>0,sys.Connection(idxAcPort));
%             
%             % Choose downstream port as state
%             idx = idxAcPort(idxPortDown==1);
%             if isempty(idx)
%                 % If no downstream ports exist, choose upstream port
%                 idx = idxAcPort(idxPortDown~=1);
%             end
            
            if not(isempty(idx))
                for wavec = sys.waves'
                    wave = char(wavec);
                    Vals = [sys.Port,{'x'}]; % add x to sys.Port for loop
                    Vals = Vals(~strcmp(Vals,'dir')); % exclude dir from sys.Port for loop
                    % create wave fields if not existent yet
                    if ~isfield(state,wave)
                        for Valc = Vals
                            Val = char(Valc);
                            state.(wave).(Val) = [];
                        end
                        state.(wave).idx = idx;
                        state.(wave).x = zeros(size(idx));
                    end
                    % loop
                    for Valc = Vals
                        Val = char(Valc);
                        % Get state from connection
%                         if ~isfield(state.(wave),Val) || isempty(state.(wave).(Val)) || ~isstruct(state.(wave).(Val))
%                             if isfield(state.(wave),Val) && ~isempty(state.(wave).(Val))
%                                 newVal = state.(wave).(Val);
%                                 state.(wave).(Val) = [];
%                             else
%                                 newVal = sys.Connection{idx(1)}.(Val);
%                                 if strcmp(state,'Mach')
%                                     % Direction is corrected for Mach number
%                                     newVal = sys.Connection{idx(1)}.dir*newVal;
%                                 end
%                             end
%                             % First valid port is defining the state
%                             state.(wave).(Val) = newVal;
%                         end
                        if ~isfield(state.(wave),Val) || isempty(state.(wave).(Val))
                            % First valid port is defining the state
                            state.(wave).(Val) = cell2mat(cellfun(@(x) x.(Val).', sys.Connection(idx), 'UniformOutput', false));
                            if strcmp(Val,'Mach')
                                % Direction is corrected for Mach number
                                state.(wave).(Val) = cell2mat(cellfun(@(x) x.dir, sys.Connection(idx), 'UniformOutput', false)).*state.(wave).(Val);
                            end
                        end
                    end
                end
                % old version: for legacy blocks (like GFlame)
                for Valc = sys.Port
                    Val = char(Valc);
                    % Get state from connection
                    if not(isfield(state,Val))||isempty(state.(Val)) % Collision at 'alpha', which exists as a field of the connection and as a wave. --> wave takes precedence
                        % First valid port is defining the state
                        state.(Val) = cell2mat(cellfun(@(x) x.(Val).', sys.Connection(idx), 'UniformOutput', false));
                        if strcmp(Val,'Mach')
                            % Direction is corrected for Mach number
                            state.(Val) = cell2mat(cellfun(@(x) x.dir, sys.Connection(idx), 'UniformOutput', false)).*state.(Val);
                        end
                    end
                end
            end
        end
    end
end