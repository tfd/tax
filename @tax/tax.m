classdef tax < sss & AcBlock
    % ThermoAcoustic Network (tax) class
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate
    % thermoacoustic network systems. It is developed by the
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
    % For updates and further information please visit our homepage
    %     http://www.tfd.mw.tum.de
    % or have a look at the wiki:
    %     https://tax.wiki.tum.de
    % ------------------------------------------------------------------
    % sys = tax(PathToModel,fMax,plot3D,'Param1',Value1,'Param2',Value2,...);
    % Input:        * PathToModel: string with the path to a tax simulink
    %               model
    %               * fMax: maximum frequency to be resolved
    %     optional  * plot3D: true or false(default) to avoid necessity to
    %               recompute geometry when it's not needed (saves time)
    %               * Param1: BlockName.BlockProperty (e.g. INLET.r)
    %               * Value1: value of block proporty
    %               * 'noConnect': returns unconnected tax object without
    %               evaluating evalSteadyState and changeParam (for
    %               parameter studies with variable meanflow)
    % Output:       * sys: thermoacoustic network (tax) model
    %
    % sys = tax(Blocks, Connections,fMax,plot3D);
    % Input:        * Blocks: Cell array of Block class elements that
    %               constitute the network system
    %               * Connections: Cell array describing the
    %               interconnections of the network model.
    %               * fMax: maximum frequency to be resolved
    %     optional  * plot3D: true or false(default) to avoid necessity to
    %               recompute geometry when it's not needed (saves time)
    % Output:       * sys: thermoacoustic network (tax) model
    % ------------------------------------------------------------------
    % Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  21 September 2021
    % ------------------------------------------------------------------
    % Copyright (C) 2015  Professur fuer Thermofluiddynamik
    % http://www.tfd.mw.tum.de
    %
    % This program is free software; you can redistribute it and/or
    % modify it under the terms of the GNU General Public License
    % as published by the Free Software Foundation; either version 2
    % of the License, or (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, write to the Free Software
    % Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
    
    properties
        modelPath
        fMax
        property, xScale, format, freqScaling
        Blocks
        blockOrder % for plotting only
        plot3D % remember whether 3D-plotting was foreseen
    end
    
    properties (Constant)
        propertys = {'p_u','p/rho/c_u','f_g','impedance_admittance','intensity'};
        xScales = {'X [m]','Connection [Index]'};
        formats = {'absPhase','realImag','logAbsPhase'};
        freqScalings = {'lin','log'};
    end
    
    properties(Dependent)
        Connections
    end
    
    methods
        function sys = tax(varargin)
            sys@sss();
            sys@AcBlock([]);
            % Defaults
            sys.property = sys.propertys{1};
            sys.xScale = sys.xScales{1};
            sys.format = sys.formats{1};
            sys.freqScaling = sys.freqScalings{1};
            % all warnings on
            warning('on');
            
            %% Retrieve system from Simulink model
            if ischar(varargin{1})
                [sys.modelPath, sys.Name] = fileparts(varargin{1});
                if isa(varargin{2},'double') && numel(varargin{2}) == 1
                    sys.fMax.f = varargin{2};
                    sys.fMax.g = varargin{2};
                elseif isstruct(varargin{2}) && ismember('f',fieldnames(varargin{2})) && ismember('g',fieldnames(varargin{2}))
                    % make sure f and g are the first fields!
                    fields = fieldnames(varargin{2});
                    fields = [{'f';'g'};unique(fields(~strcmp(fields,'f')&~strcmp(fields,'g')))];
                    for field = fields'
                        sys.fMax.(char(field)) = varargin{2}.(char(field));
                    end
                else
                    error('incorrect specification of fMax');
                end
                sys.waves = fieldnames(sys.fMax);

                if isempty(sys.modelPath)
                    % Just the name was given, path needs to be
                    % constructed from current working directory
                    sys.modelPath = fullfile(pwd,sys.Name);
                else
                    % Full path was specified
                    sys.modelPath = fullfile(sys.modelPath,sys.Name);
                end
                
                load_system(sys.modelPath); % This line seems to be irrelevant for the build of the system. It only changes the gcs result.
                sys = getModel(sys);
                % additional input to decide between speed and 3D plotting
                if ~any(cellfun(@(x) strcmpi('noConnect',x),varargin)) && numel(varargin)>2
                    sys.plot3D = varargin{3};
                    iargin = 4; % we don't have to care about noConnect, since iargin will only be used for connected models
                else
                    sys.plot3D = false; % default is false --> rather speed than 3D plotting
                    iargin = 3;
                end
            elseif iscell(varargin{1})
                %% Read in manually defined Blocks and Connections
                if isa(varargin{3},'double') && numel(varargin{3}) == 1
                    sys.fMax.f = varargin{3};
                    sys.fMax.g = varargin{3};
                elseif isstruct(varargin{3}) && ismember('f',fieldnames(varargin{3})) && ismember('g',fieldnames(varargin{3}))
                    % make sure f and g are the first fields!
                    fields = fieldnames(varargin{3});
                    fields = [{'f';'g'},unique(fields(~strcmp(fields,'f')&~strcmp(fields,'g')))];
                    for field = fields'
                        sys.fMax.(char(field)) = varargin{3}.(char(field));
                    end
                else
                    error('incorrect specification of fMax');
                end
                sys.waves = fieldnames(sys.fMax);
                sys.Blocks = varargin{1};
                sys.Connections = varargin{2};
                % additional input to decide between speed and 3D plotting
                if ~any(cellfun(@(x) strcmpi('noConnect',x),varargin)) && numel(varargin)>3
                    sys.plot3D = varargin{4};
                    iargin = 5;
                else
                    sys.plot3D = false; % default is false --> rather speed than 3D plotting
                    iargin = 4;
                end
            end
            
            if ~any(cellfun(@(x) strcmpi('noConnect',x),varargin))
                nAcPorts = zeros(1,numel(sys.Blocks));
                for i = 1:numel(nAcPorts)
                    nAcPorts(i) = sum(cellfun(@(x) Block.isPort({x},AcBlock.Port), sys.Blocks{i}.Connection)); % number of acoustic ports of each block
                end
                tic
                if ~sys.plot3D && sum(nAcPorts-2) == -2 % each oneport ends a branch, each multiport starts nAcPorts-2 branches <==> no loops
                    % if there are no loops, no ducts must be split for
                    % plotting reasons, i.e. we can omit makeGeo without
                    % changing the structure of the model.

                    % set positions of connections, x-direction only,
                    % overwrite known positions, since no 3D plotting can
                    % be done anyway
                    
                    % initialize all pos as empty, to avoid contradictions
                    for i = 1:numel(sys.Blocks)
                        for ii = 1:numel(sys.Blocks{i}.Connection)
                            sys.Blocks{i}.Connection{ii}.pos = [];
                        end
                    end
                    % set one to zero arbitrarily
                    sys.Blocks{1}.Connection{1}.pos = [0,0,0];
                else
                    % otherwise makeGeo is mandatory.
                    sys = makeGeo(sys); % this can change the number of connections and blocks for 3d plotting purposes! --> splitDuct
                end
                toc
                sys = evalSteadyState(sys);
                toc
                sys = changeParam(sys,varargin{iargin:end}); % runs sys.update
                toc
            end
        end
        
        function sys = set.fMax(sys, fMax)
            if not(isequal(sys.fMax, fMax))
                sys.fMax = fMax;
                sys.uptodate = false;
            end
        end
        
        function sys = set.Connections(sys, Connections)
            % Assign indices from Connections to Blocks Connection.
            BlocksList = cellfun(@(x) x.Name, sys.Blocks, 'UniformOutput', false);
            for blockNameC = BlocksList
                blockName = char(blockNameC);
                idxBlk = strcmp(blockName,BlocksList);
                
                % Upstream Connections
                indexConnectionUp = find(strcmp(Connections(:,3),blockName));
                for id = indexConnectionUp'
                    sys.Blocks{idxBlk}.Connection{cell2mat(Connections(id,4))}.idx = id;
                    sys.Blocks{idxBlk}.Connection{cell2mat(Connections(id,4))}.dir = -1;
                end
                % Downstream Connections
                indexConnectionDown = find(strcmp(Connections(:,1), blockName));
                for id = indexConnectionDown'
                    sys.Blocks{idxBlk}.Connection{cell2mat(Connections(id,2))}.idx = id;
                    sys.Blocks{idxBlk}.Connection{cell2mat(Connections(id,2))}.dir = 1;
                end
            end
            sys.uptodate = false;
        end
        
        function Connections = get.Connections(sys)
            % Retrieve Connections from Block Connection indices
            Connections = {};
            nBlocks = size(sys.Blocks,2);
            for i = 1:nBlocks
                nCons = size(sys.Blocks{i}.Connection,2);
                for ii = 1:nCons
                    if sys.Blocks{i}.Connection{ii}.dir==1
                        Connections{sys.Blocks{i}.Connection{ii}.idx,1} = sys.Blocks{i}.Name; %#ok<AGROW>
                        Connections{sys.Blocks{i}.Connection{ii}.idx,2} = ii; %#ok<AGROW>
                    elseif sys.Blocks{i}.Connection{ii}.dir==-1
                        Connections{sys.Blocks{i}.Connection{ii}.idx,3} = sys.Blocks{i}.Name; %#ok<AGROW>
                        Connections{sys.Blocks{i}.Connection{ii}.idx,4} = ii; %#ok<AGROW>
                    else
                        % no entry as the connection is an open end
                    end
                end
            end
        end
        
        function sys = set.blockOrder(sys,blockOrder)
            if isempty(blockOrder)
                sys.blockOrder = sys.get_blockOrder;
            elseif numel(unique(blockOrder)) ~= numel(sys.Blocks) || any(unique(blockOrder) ~= 1:numel(sys.Blocks)) %#ok<MCSUP>
                error('Bad blockOrder definition.');
            elseif any(blockOrder~=sys.blockOrder)
                sys.blockOrder = blockOrder;
                sys.uptodate = false;
            end
        end

        function blockOrder = get.blockOrder(sys)
            if ~isempty(sys.blockOrder)
                blockOrder = sys.blockOrder;
            else
                blockOrder = sys.get_blockOrder;
            end
        end
    end
    
    methods(Static)
        plotParameterStudy(J,combinations,Labels,iX,iY)
        [c,ceq] = distanceConstraint(activeVec,active,A_active,b_active,index,dist,indexEq,distEq)
        criterion = distanceMaximizer(activeVec,active,A_active,b_active,relevant,exponent)
        value = evalParam(expr)
        logicval = compareProp(prop1,prop2)
    end
    
end