function plotScatteringMatrix(varargin)
% Plot scattering matrix
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% plotScatteringMatrix(sys1,sys2,...sysN,freq);
% Input:        * sys1..N: tax objects
%               * freq: Frequency vector
% ------------------------------------------------------------------
% Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  08 Oct 2017
% ------------------------------------------------------------------
% See also tax/scatter

if isnumeric(varargin{end})
    freq = varargin{end};
    varargin(end) = [];
else
    scalarFMax = max(cellfun(@(x) varargin{1}.fMax.(x),varargin{1}.waves));
    freq = 2*pi*linspace(1,scalarFMax,100);
end

% varargin = cellfun(@(x) x.scatter('dropIO') ,varargin,'UniformOutput',false);
varargin = cellfun(@(x) x.scatter('dropIOandEmptyScatter') ,varargin,'UniformOutput',false);

varargin{end+1} = freq;

index = find(cellfun(@(x) numel(x.y)>0 && numel(x.u)>0, varargin(1:end-1))); % all non-empty systems
if ~isempty(index)
    figure()
    bode(varargin{[index,end]})
end