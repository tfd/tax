function [c,ceq] = distanceConstraint(activeVec,active,A_active,b_active,index,dist,indexEq,distEq)
    % distanceConstraint sets up a non-linear constraint function for
    % geometry optimization in makeGeo
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = tax(PathToModel);
    % Input:        * posVec: vector with all position data of the problem
    %               * index: matrix with indices of the constrained
    %               connectios: [con1,con2]
    %               * distance: vector with the maximum distances for the
    %               connections specified in index
    %               * indexEq: like index, but for exact distance
    %               specifications
    %               * distanceEq: like distance, but for indexEq
    % Output:       * c: function value to be smaller or equal than zero
    %               * ceq: function value to be exactly zero
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  17 Apr 2018
    % ------------------------------------------------------------------
    
    posVec(active) = activeVec;
    posVec(~active) = A_active*activeVec + b_active;
    allPos = reshape(posVec,numel(posVec)/3,3);
    x = allPos(:,1);
    y = allPos(:,2);
    z = allPos(:,3);
    if isempty(index)
        c = [];
    else
        c = sqrt((x(index(:,1))-x(index(:,2))).^2 + (y(index(:,1))-y(index(:,2))).^2 + (z(index(:,1))-z(index(:,2))).^2) - dist;
    end
    if isempty(indexEq)
        ceq = [];
    else
        ceq = sqrt((x(indexEq(:,1))-x(indexEq(:,2))).^2 + (y(indexEq(:,1))-y(indexEq(:,2))).^2 + (z(indexEq(:,1))-z(indexEq(:,2))).^2) - distEq;
    end
end