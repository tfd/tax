function [blockOrder] = get_blockOrder(sys)
% get_blockOrder function to predefine the 1d block order of the entire
% model (for plotting purposes)
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% Input:        * sys: tax object
% Output:       * blockOrder: integer vector with indices of blocks in
%               geometric order 
% ------------------------------------------------------------------
% Authors:      Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  24 Apr 2018
% ------------------------------------------------------------------

% Recognize acoustic connections
AcConIdx = false(1,size(sys.Connections,1));
for i = 1:size(sys.Connections,1)
    AcConIdx(i) = ~isempty(sys.Connections{i,1}) && ~isempty(sys.Connections{i,3}) && Block.isPort(sys.Blocks{sys.getBlock(sys.Connections{i,1})}.Connection(sys.Connections{i,2}),AcBlock.Port);
end
Connections = sys.Connections(AcConIdx,:);
% Mark special blocks
cStartBlocks = Connections(~ismember(Connections(:,1),Connections(:,3)),1).'; % all upstream ends
cMergeBlocks = unique(Connections(arrayfun(@(i) ismember(Connections(i,3),Connections([1:i-1,i+1:end],3)), 1:size(Connections,1)),3)).'; % all blocks with more than one acoustic upstream connection
cForkBlocks = unique(Connections(arrayfun(@(i) ismember(Connections(i,1),Connections([1:i-1,i+1:end],1)), 1:size(Connections,1)),1)).'; % all blocks with more than one acoustic downstream connection
cEndBlocks = Connections(~ismember(Connections(:,3),Connections(:,1)),3).'; % all downstream ends
if isempty(cStartBlocks)
    startBlocks = find(sys.getBlock(Connections{1,1})); % initialize with first block of Connections, if empty
else
    startBlocks = cellfun(@(x) find(sys.getBlock(x)), cStartBlocks);
end
if isempty(cMergeBlocks)
    mergeBlocks = [];
else
    mergeBlocks = cellfun(@(x) find(sys.getBlock(x)), cMergeBlocks);
end
if isempty(cForkBlocks)
    forkBlocks = [];
else
    forkBlocks = cellfun(@(x) find(sys.getBlock(x)), cForkBlocks);
end
if isempty(cEndBlocks)
    endBlocks = [];
else
    endBlocks = cellfun(@(x) find(sys.getBlock(x)), cEndBlocks);
end
% find all regular paths, every path starts with a startBlock, mergeBlock
% or forkBlock and ends with a endBlock, forkBlock or mergeBlock, so there
% is overlap, that can be used to link the paths together
for i = 1:numel(startBlocks) % start with startBlock
    % initialize
    nextBlock = -1;
    % first step
    paths{i} = startBlocks(i); %#ok<AGROW>
    while ~ismember(nextBlock,unique([startBlocks,mergeBlocks,forkBlocks,endBlocks]))
        % initialize
        thisBlock = paths{i}(end);
        nextBlock = find(sys.getBlock(Connections{find(strcmp(Connections(:,1),sys.Blocks{thisBlock}.Name),1,'first'),3}));
        % step
        paths{i} = [paths{i},nextBlock];
    end
    if ismember(nextBlock,startBlocks)
        paths{i} = paths{i}(2:end); %#ok<AGROW>
    end
end
for i = 1:numel(mergeBlocks) % start with mergeBlock
    % initialize
    nextBlock = -1;
    % first step
    paths{end+1} = mergeBlocks(i); %#ok<AGROW>
    while ~ismember(nextBlock,unique([startBlocks,mergeBlocks,forkBlocks,endBlocks]))
        % initialize
        thisBlock = paths{end}(end);
        nextBlock = find(sys.getBlock(Connections{find(strcmp(Connections(:,1),sys.Blocks{thisBlock}.Name),1,'first'),3}));
        % step
        paths{end} = [paths{end},nextBlock];
    end
end
for i = 1:numel(forkBlocks) % start with forkBlock
    dnForkBlocks = cellfun(@(x) find(sys.getBlock(x)), Connections(strcmp(Connections(:,1),sys.Blocks{forkBlocks(i)}.Name),3));
    for ii = 1:numel(dnForkBlocks)
        % initialize
        paths{end+1} = forkBlocks(i); %#ok<AGROW>
        nextBlock = dnForkBlocks(ii);
        % first step after fork
        paths{end} = [paths{end},nextBlock];
        while ~ismember(nextBlock,unique([startBlocks,mergeBlocks,forkBlocks,endBlocks]))
            % initialize
            thisBlock = paths{end}(end);
            nextBlock = find(sys.getBlock(Connections{find(strcmp(Connections(:,1),sys.Blocks{thisBlock}.Name),1,'first'),3}));
            % step
            paths{end} = [paths{end},nextBlock];
        end
    end
end
% block sorting is kind of arbitrary, since optimization would quickly
% exceed RAM limitations: start at starting blocks or forks, walk till
% merges or ending blocks
newPaths = paths(1);
paths = reshape(paths(2:end),1,numel(paths)-1); % reshape, to make sure, it's a row vector
while ~isempty(paths)
    for i = 1:numel(newPaths)
        nothingHappened = true;
        if ismember(newPaths{i}(1),unique([forkBlocks,mergeBlocks])) % check first element of current path
            newIdx = find(cellfun(@(x) x(end)==newPaths{i}(1) ,paths),1,'first');
            newPaths = [newPaths(1:i-1),paths(newIdx),newPaths(i:end)];
            if ~isempty(newIdx)
                paths = [paths(1:newIdx-1),paths(newIdx+1:end)];
                nothingHappened = false;
            end
        end
        if nothingHappened && ismember(newPaths{i}(end),unique([forkBlocks,mergeBlocks])) % check last element of current path
            newIdx = find(cellfun(@(x) x(1)==newPaths{i}(end) ,paths),1,'first');
            newPaths = [newPaths(1:i),paths(newIdx),newPaths(i+1:end)];
            if ~isempty(newIdx)
                paths = [paths(1:newIdx-1),paths(newIdx+1:end)];
            end
        end
        if ~ismember(newPaths{i}(1),unique([forkBlocks,mergeBlocks])) && ~ismember(newPaths{i}(end),unique([forkBlocks,mergeBlocks])) % none of the above
            error('Model is not fully connected acoustically.');
        end
    end
end
% translate to vector
for i = 1:numel(newPaths)
    if ismember(newPaths{i}(1),forkBlocks) && ~ismember(newPaths{i}(1),mergeBlocks)
        newPaths{i} = newPaths{i}(2:end);
    end
    if ~ismember(newPaths{i}(end),forkBlocks) && ismember(newPaths{i}(end),mergeBlocks)
        newPaths{i} = newPaths{i}(1:end-1);
    end
end
blockOrder = cell2mat(newPaths);