function [sys] = makeGeo(sys)
% makeGeo function to resolve the 3d-geometry of the entire model (for
% plotting purposes) using the information specified by the user
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% Input:        * sys: tax object
% Output:       * sys: tax object with entirely resolved positions 
% ------------------------------------------------------------------
% Authors:      Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  29 Apr 2018
% ------------------------------------------------------------------

BlkList = cellfun(@(x) x.Name, sys.Blocks, 'UniformOutput', false);
Blocks = sys.Blocks;
Connections = sys.Connections;
atLeastOnePosition = false;

%% Recognize acoustic connections
AcConIdx = false(1,size(Connections,1));
for i = 1:size(Connections,1)
    AcConIdx(i) = Block.isPort(Blocks{sys.getBlock(Connections{i,1})}.Connection(Connections{i,2}),AcBlock.Port);
end
AcConIdx = find(AcConIdx);
Connections = Connections(AcConIdx,:); %#ok<FNDSB>

%% Iterate over all connections and check, if there is at least one given position, otherwise set one
for i = 1:size(Connections,1)
    % Determine index of Block
    idxu = find(strcmp(Connections{i,1},BlkList));
    idxd = find(strcmp(Connections{i,3},BlkList));
    % Determine index of port
    portu = Connections{i,2};
    portd = Connections{i,4};
    % Check
    if (~isempty(Blocks{idxu}.Connection{portu}.pos) && numel(Blocks{idxu}.Connection{portu}.pos)==3) ||...
            (~isempty(Blocks{idxd}.Connection{portd}.pos) && numel(Blocks{idxd}.Connection{portd}.pos)==3)
        atLeastOnePosition = true;
        break;
    end
end
% Initialize first connection, incase there is no other position given
if ~atLeastOnePosition
    idxu = find(strcmp(Connections{1,1},BlkList));
    portu = Connections{1,2};
    Blocks{idxu}.Connection{portu}.pos = [0,0,0]; %#ok<FNDSB>
end

%% Iterate over all connections and propagate positions
finished = false;
maxIter = 100; ii = 0;
doneCon = false(size(Connections,1),1);
while ~finished
    finished = true;
    oldDoneCon = doneCon;
    doneCon = false(size(Connections,1),1);

    for i = 1:size(Connections,1)
        % Determine index of Block
        idxu = find(strcmp(Connections{i,1},BlkList));
        idxd = find(strcmp(Connections{i,3},BlkList));
        % Determine index of port
        portu = Connections{i,2};
        portd = Connections{i,4};
        % Retrieve connection properties of desired Block
        conu = Blocks{idxu}.Connection;
        cond = Blocks{idxd}.Connection;
        % Select connection properties of desired Port
        con{1} = conu{portu};
        con{2} = cond{portd};
        % Propagate positions
        if ~isempty(con{1}.pos) && numel(con{1}.pos)==3 && isempty(con{2}.pos) % propagate downstream
            con{2}.pos = con{1}.pos;
        elseif ~isempty(con{2}.pos) && numel(con{2}.pos)==3 && isempty(con{1}.pos) % propagate upstream
            con{1}.pos = con{2}.pos;
        elseif ~isempty(con{1}.pos) && ~isempty(con{2}.pos) % contradictory definition
            if any(con{1}.pos~=con{2}.pos)
                error('Contradictory position definition.')
            end
        elseif ~isempty(con{1}.pos) || ~isempty(con{2}.pos) % incomplete definition
            error('Incomplete position definition.')
        end
        % Save the solved values
        Blocks{idxu}.Connection{portu}.pos = con{1}.pos;
        Blocks{idxd}.Connection{portd}.pos = con{2}.pos;
        % Propagate through compact elements
        conEmpty = cellfun(@(x) isprop(x,'pos') && isempty(x.pos), Blocks{idxu}.Connection);
        if ~isprop(Blocks{idxu},'l') && any(conEmpty)
            finished = false;
            for ic = find(conEmpty)
                Blocks{idxu}.Connection{ic}.pos = con{1}.pos;
            end
        end
        conEmpty = cellfun(@(x) isprop(x,'pos') && isempty(x.pos), Blocks{idxd}.Connection);
        if ~isprop(Blocks{idxd},'l') && any(conEmpty)
            finished = false;
            for ic = find(conEmpty)
                Blocks{idxd}.Connection{ic}.pos = con{2}.pos;
            end
        end
        % Save which connections are done
        if ~isempty(con{1}.pos)
            doneCon(i) = true;
        end
    end

    % Check for abort
    if all(doneCon == oldDoneCon)
        finished = true;
    end
    % Limit number of iterations
    if ii<maxIter
        ii = ii+1;
    else
        error(['Geometry calculations did not finish after ',num2str(maxIter),' Iterations.'])
    end
end

%% Iterate over all ducts (or better: all elements that have the property l) and check or compute lengths
splitDuctIndex = [];
for i = 1:numel(Blocks)
    if isprop(Blocks{i},'l')
        if isempty(Blocks{i}.l) && ~isempty(Blocks{i}.Connection{1}.pos) && ~isempty(Blocks{i}.Connection{2}.pos) % both positions given, unknown length
            Blocks{i}.l = sqrt(sum((Blocks{i}.Connection{1}.pos - Blocks{i}.Connection{2}.pos).^2)); % set length
        elseif ~isempty(Blocks{i}.l) && ~isempty(Blocks{i}.Connection{1}.pos) && ~isempty(Blocks{i}.Connection{2}.pos) % both positions and length given
            if Blocks{i}.l < sqrt(sum((Blocks{i}.Connection{1}.pos - Blocks{i}.Connection{2}.pos).^2))
                error('Contradictory position and length definition.')
            elseif Blocks{i}.l > sqrt(sum((Blocks{i}.Connection{1}.pos - Blocks{i}.Connection{2}.pos).^2))
                splitDuctIndex = [splitDuctIndex,i]; %#ok<AGROW>
            end
        elseif isempty(Blocks{i}.l)
            error('Incomplete position and length definition.')
        end
    end
end

%% Identify geometric loops
% map all possible paths, start with first block index (1 by definition)
paths = recursiveLoopFinder(sys,1);
% find all paths that contain loops and check triangle relation
for i = 1:numel(paths)
    if numel(unique(paths{i}(1:2:end))) < numel(sort(paths{i}(1:2:end))) % check for loop
        % find all lengths of non-compact elements in the loop
        iFirst = 2*find(paths{i}(1:2:end)==paths{i}(end),1,'first')-1; % first relevant block
        Con = Blocks{paths{i}(iFirst)}.Connection;
        Name = Blocks{paths{i}(iFirst)}.Name;
        if Con{Connections{paths{i}(iFirst+1),2*find(strcmp(Connections(paths{i}(iFirst+1),[1,3]),Name))}}.dir ==...
                Con{Connections{paths{i}(end-1),2*find(strcmp(Connections(paths{i}(end-1),[1,3]),Name))}}.dir % the loop is attached only on one side of the block
            iFirst = iFirst+2; % don't consider this block for triangle relation
        end
        L = zeros(1,numel(sys.Blocks));
        for ii = paths{i}(iFirst:2:end) % only consider the part of the path that actually makes the loop
            if isprop(Blocks{ii},'l')
                L(ii) = Blocks{ii}.l;
            else
                L(ii) = 0;
            end
        end
        Lmax = max(L);
        iLmax = find(L==Lmax);
        if sum(L)-Lmax<=Lmax
            splitDuctIndex = [splitDuctIndex,iLmax]; %#ok<AGROW>
        end
    end
end

%% Split ducts, if necessary
splitDuctIndex = unique(splitDuctIndex);
if ~isempty(splitDuctIndex)
    for i = splitDuctIndex
        newBlock = Blocks{i};
        baseName = Blocks{i}.Name;
        baseLength = Blocks{i}.l;
        % rename and resize
        Blocks{i}.Name = [baseName,'_part1'];
        newBlock.Name = [baseName,'_part2'];
        Blocks{i}.l = baseLength/2;
        newBlock.l = baseLength/2;
        % make new Connection (we assume there are only two connections per
        % block, new blocks with more than two connections and a length are not
        % foreseen)
        conIdx = size(sys.Connections,1) + 1;
        Blocks{i}.Connection{2}.idx = conIdx;
        newBlock.Connection{1}.idx = conIdx;
        Blocks{i}.Connection{2}.pos = [];
        newBlock.Connection{1}.pos = [];
        % append newBlock
        Blocks{end+1} = newBlock; %#ok<AGROW>
        sys.Blocks = Blocks;
        sys.Connections(conIdx,:) = {Blocks{i}.Name, 2, newBlock.Name, 1};
        % names in Connections are replaced automatically
    end
    % update BlkList, AcConIdx and Connections. Update of positions is not
    % required, since the position of the new connection is unknown by definition
    Connections = sys.Connections;
    AcConIdx = false(1,size(Connections,1));
    for i = 1:size(Connections,1)
        AcConIdx(i) = Block.isPort(Blocks{sys.getBlock(Connections{i,1})}.Connection(Connections{i,2}),AcBlock.Port);
    end
    AcConIdx = find(AcConIdx);
    Connections = Connections(AcConIdx,:); %#ok<FNDSB>
    BlkList = cellfun(@(x) x.Name, sys.Blocks, 'UniformOutput', false);
end

%% Make constraints for optimization
% make list of positions
allPos = nan(size(Connections,1),3);
for i = 1:size(Connections,1)
    if ~isempty(Blocks{sys.getBlock(Connections{i,1})}.Connection{Connections{i,2}}.pos)
        allPos(i,:) = Blocks{sys.getBlock(Connections{i,1})}.Connection{Connections{i,2}}.pos;
    end
end
posVec = reshape(allPos,numel(allPos),1);
% linear equation system for fixed positions
Aeq = eye(3*size(Connections,1));
Aeq = Aeq(~isnan(posVec),:);
beq = posVec(~isnan(posVec));
% non-linear constraint function for finite fixed distances (non-compact
% elements) and finite maximum distances (splitduct), additional terms to
% Aeq and beq for zero-distance constraints
dist = [];
index = [];
distEq = [];
indexEq = [];
for i = 1:numel(Blocks)
    % only acoustic connections or upstream parts of non-acoustic connections
    con = Blocks{i}.Connection(cellfun(@(x) Blocks{i}.isPort({x},AcBlock.Port), Blocks{i}.Connection));
    if ~isprop(Blocks{i},'l') % compact: distance is zero
        beq = [beq;zeros(3*numel(con)-3,1)]; %#ok<AGROW>
        for ii = 2:numel(con)
            Aeq = [Aeq;zeros(3,size(Aeq,2))]; %#ok<AGROW>
            i1 = find(...
                strcmp(sys.Connections{con{1}.idx,1},Connections(:,1)) &...
                strcmp(sys.Connections{con{1}.idx,3},Connections(:,3)) &...
                sys.Connections{con{1}.idx,2}==cell2mat(Connections(:,2)) &...
                sys.Connections{con{1}.idx,4}==cell2mat(Connections(:,4))...
                ); % translate from sys.Connections to Connections
            i2 = find(...
                strcmp(sys.Connections{con{ii}.idx,1},Connections(:,1)) &...
                strcmp(sys.Connections{con{ii}.idx,3},Connections(:,3)) &...
                sys.Connections{con{ii}.idx,2}==cell2mat(Connections(:,2)) &...
                sys.Connections{con{ii}.idx,4}==cell2mat(Connections(:,4))...
                ); % translate from sys.Connections to Connections
            Aeq(end-2,[i1,i2]) = [1,-1];
            Aeq(end-1,size(Connections,1)+[i1,i2]) = [1,-1];
            Aeq(end,2*size(Connections,1)+[i1,i2]) = [1,-1];
        end
    else % non-compact: two groups with zero distance among themselves, but with finite distance between each other
        fDist = Blocks{i}.l;
        ii1 = cellfun(@(x) x.dir==-1, con); % all upstream acoustic connections
        ii2 = find(~ii1); % all others (downstream or non-acoustic)
        ii1 = find(ii1);
        distEq = [distEq;fDist]; %#ok<AGROW>
        i1 = find(...
            strcmp(sys.Connections{con{ii1(1)}.idx,1},Connections(:,1)) &...
            strcmp(sys.Connections{con{ii1(1)}.idx,3},Connections(:,3)) &...
            sys.Connections{con{ii1(1)}.idx,2}==cell2mat(Connections(:,2)) &...
            sys.Connections{con{ii1(1)}.idx,4}==cell2mat(Connections(:,4))...
            ); % translate from sys.Connections to Connections
        i2 = find(...
            strcmp(sys.Connections{con{ii2(1)}.idx,1},Connections(:,1)) &...
            strcmp(sys.Connections{con{ii2(1)}.idx,3},Connections(:,3)) &...
            sys.Connections{con{ii2(1)}.idx,2}==cell2mat(Connections(:,2)) &...
            sys.Connections{con{ii2(1)}.idx,4}==cell2mat(Connections(:,4))...
            ); % translate from sys.Connections to Connections
        indexEq = [indexEq;i1,i2]; %#ok<AGROW>
        beq = [beq;zeros(3*numel(con)-6,1)]; %#ok<AGROW>
        for ii = ii1(2:end)
            Aeq = [Aeq;zeros(3,size(Aeq,2))]; %#ok<AGROW>
            i1 = find(...
                strcmp(sys.Connections{con{ii1(1)}.idx,1},Connections(:,1)) &...
                strcmp(sys.Connections{con{ii1(1)}.idx,3},Connections(:,3)) &...
                sys.Connections{con{ii1(1)}.idx,2}==cell2mat(Connections(:,2)) &...
                sys.Connections{con{ii1(1)}.idx,4}==cell2mat(Connections(:,4))...
                ); % translate from sys.Connections to Connections
            i2 = find(...
                strcmp(sys.Connections{con{ii}.idx,1},Connections(:,1)) &...
                strcmp(sys.Connections{con{ii}.idx,3},Connections(:,3)) &...
                sys.Connections{con{ii}.idx,2}==cell2mat(Connections(:,2)) &...
                sys.Connections{con{ii}.idx,4}==cell2mat(Connections(:,4))...
                ); % translate from sys.Connections to Connections
            Aeq(end-2,[i1,i2]) = [1,-1];
            Aeq(end-1,size(Connections,1)+[i1,i2]) = [1,-1];
            Aeq(end,2*size(Connections,1)+[i1,i2]) = [1,-1];
        end
        for ii = ii2(2:end)
            Aeq = [Aeq;zeros(3,size(Aeq,2))]; %#ok<AGROW>
            i1 = find(...
                strcmp(sys.Connections{con{ii2(1)}.idx,1},Connections(:,1)) &...
                strcmp(sys.Connections{con{ii2(1)}.idx,3},Connections(:,3)) &...
                sys.Connections{con{ii2(1)}.idx,2}==cell2mat(Connections(:,2)) &...
                sys.Connections{con{ii2(1)}.idx,4}==cell2mat(Connections(:,4))...
                ); % translate from sys.Connections to Connections
            i2 = find(...
                strcmp(sys.Connections{con{ii}.idx,1},Connections(:,1)) &...
                strcmp(sys.Connections{con{ii}.idx,3},Connections(:,3)) &...
                sys.Connections{con{ii}.idx,2}==cell2mat(Connections(:,2)) &...
                sys.Connections{con{ii}.idx,4}==cell2mat(Connections(:,4))...
                ); % translate from sys.Connections to Connections
            Aeq(end-2,[i1,i2]) = [1,-1];
            Aeq(end-1,size(Connections,1)+[i1,i2]) = [1,-1];
            Aeq(end,2*size(Connections,1)+[i1,i2]) = [1,-1];
        end
    end
end
% define active and relevant quantities
relevant = unique([index;indexEq]);
[~,R,E] = qr(Aeq,0); % QR decomposition
tol = 1e-10; % tolerance
if ~isvector(R)
    R = abs(diag(R));
else
    R = R(1);   
end
r = find(R>=tol*R(1),1,'last'); %rank estimation
inactive = sort(E(1:r));
active = ~ismember(1:numel(E),inactive);
invAeqin = inv(Aeq(:,~active));
A_active = -invAeqin*Aeq(:,active);
b_active = invAeqin*beq; %#ok<MINV>
% project index on active quantities
nonlcon = @(activeVec) tax.distanceConstraint(activeVec,active,A_active,b_active,index,dist,indexEq,distEq); % finally use distance function

%% Build initial solution for geometry (without regard for constraints from element lengths)
finished = false;
maxIter = 100; ii = 0;
while ~finished
    finished = true;

    % Iterate over all connections
    for i = 1:size(Connections,1)
        % Determine index of Block
        idxu = find(strcmp(Connections{i,1},BlkList));
        idxd = find(strcmp(Connections{i,3},BlkList));
        % Determine index of port
        portu = Connections{i,2};
        portd = Connections{i,4};
        % Retrieve connection properties of desired Block
        conu = Blocks{idxu}.Connection;
        cond = Blocks{idxd}.Connection;
        % Select connection properties of desired Port
        con{1} = conu{portu};
        con{2} = cond{portd};
        % Propagate positions
        if ~isempty(con{1}.pos) % propagate downstream, possibly overwrite con{2}.pos
            con{2}.pos = con{1}.pos;
        elseif ~isempty(con{2}.pos) % propagate upstream
            con{1}.pos = con{2}.pos;
        end
        % Save the solved values
        Blocks{idxu}.Connection{portu}.pos = con{1}.pos;
        Blocks{idxd}.Connection{portd}.pos = con{2}.pos;
        % Check if all other connection positions of both blocks are defined, and initialize if not
        if ~isempty(con{1}.pos)
            for ic = 1:numel(Blocks{idxu}.Connection)
                if isfield(Blocks{idxu}.Connection{ic},'pos') && isempty(Blocks{idxu}.Connection{ic}.pos)
                    finished = false;
                    if isprop(Blocks{idxu},'l')
                        Blocks{idxu}.Connection{ic}.pos = Blocks{idxu}.Connection{portu}.pos - [Blocks{idxu}.l * sign(Blocks{idxu}.Connection{portu}.dir - Blocks{idxu}.Connection{ic}.dir),0,0];
                    else
                        Blocks{idxu}.Connection{ic}.pos = Blocks{idxu}.Connection{portu}.pos;
                    end
                end
            end
            for ic = 1:numel(Blocks{idxd}.Connection)
                if isfield(Blocks{idxd}.Connection{ic},'pos') && isempty(Blocks{idxd}.Connection{ic}.pos)
                    finished = false;
                    if isprop(Blocks{idxd},'l')
                        Blocks{idxd}.Connection{ic}.pos = Blocks{idxd}.Connection{portd}.pos - [Blocks{idxd}.l * sign(Blocks{idxd}.Connection{portd}.dir - Blocks{idxd}.Connection{ic}.dir),0,0];
                    else
                        Blocks{idxd}.Connection{ic}.pos = Blocks{idxd}.Connection{portd}.pos;
                    end
                end
            end
        else
            finished = false;
        end
    end

    % Limit number of iterations
    if ii<maxIter
        ii = ii+1;
    else
        error(['Geometry calculations did not finish after ',num2str(maxIter),' Iterations.'])
    end
end
% make new list of positions
allPos = nan(size(Connections,1),3);
for i = 1:size(Connections,1)
    if ~isempty(Blocks{sys.getBlock(Connections{i,1})}.Connection{Connections{i,2}}.pos)
        allPos(i,:) = Blocks{sys.getBlock(Connections{i,1})}.Connection{Connections{i,2}}.pos;
    end
end
posVec0 = reshape(allPos,numel(allPos),1);

%% Solve and optimize solution for geometry
% initialize
activeVec0 = posVec0(active);
exponent = 1;
exitflag = 0;
ii = 0;
while exitflag~=1 && ii<15 && ~isempty(activeVec0)
    if exitflag<1
        exponent = exponent*.8;
        [activeVec,~,exitflag] = fmincon(@(activeVec) tax.distanceMaximizer(activeVec,active,A_active,b_active,relevant,exponent),activeVec0,[],[],[],[],[],[],nonlcon);
    elseif exitflag>1
        exponent = exponent*1.3;
        [activeVec,~,exitflag] = fmincon(@(activeVec) tax.distanceMaximizer(activeVec,active,A_active,b_active,relevant,exponent),activeVec,[],[],[],[],[],[],nonlcon);
        if exitflag<1
            [activeVec,~,exitflag] = fmincon(@(activeVec) tax.distanceMaximizer(activeVec,active,A_active,b_active,relevant,exponent),activeVec0,[],[],[],[],[],[],nonlcon);
        end
    end
    ii = ii+1;
end
if isempty(activeVec0)
    activeVec = activeVec0;
end
posVec(active) = activeVec;
posVec(~active) = A_active*activeVec + b_active;
allPos = reshape(posVec,numel(posVec)/3,3);
for i = 1:size(Connections,1)
    Blocks{sys.getBlock(Connections{i,1})}.Connection{Connections{i,2}}.pos = allPos(i,:);
    Blocks{sys.getBlock(Connections{i,3})}.Connection{Connections{i,4}}.pos = allPos(i,:);
end

%% Save
sys.Blocks = Blocks;