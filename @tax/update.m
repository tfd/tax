function sys = update(sys)
% update function to update a tax object.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate
% thermoacoustic network systems. It is developed by the
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% sys = update(sys);
% Input:        * sys: thermoacoustic network (tax) model object
% Output:       * sys: thermoacoustic network (tax) model object
%
% ------------------------------------------------------------------
% Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  08 Oct 2017
% ------------------------------------------------------------------


if not(all(cellfun(@(x) x.uptodate,sys.Blocks)))||not(sys.uptodate)
    for i = 1:length(sys.Blocks)
        % Adapt fMax for all Blocks
        if isprop(sys.Blocks{i},'fMax')
            sys.Blocks{i}.fMax = sys.fMax;
        end
        % Adapt waves for all Blocks
        sys.Blocks{i}.waves = sys.waves;
        % Update Blocks
        sys.Blocks{i} = update(sys.Blocks{i});
    end
    sys.uptodate = false;
end

if sys.uptodate
    return
end
x0 = sys.x0;
sys = clear(sys);

% Define a geometric block order
blockOrder = sys.blockOrder;

%% Adapt Blocks and collect IOs
Blk = sys.Blocks(blockOrder);
Input = {};
Output = {};
% regular (geometric) IOs
for i = 1:length(Blk)
    % Adapt model type and sampling times
    Blk{i} = adaptTsAndDelays(Blk{i},sys.Ts);
    % Collect all inputs and outputs
    fields = fieldnames(Blk{i}.InputGroup);
    regularIn = fields(cellfun(@(x) ismember(x,sys.waves), fields));
    for InputGroupc = regularIn'
        InputGroup = char(InputGroupc);
        Input = [Input; Blk{i}.u(Blk{i}.InputGroup.(InputGroup))]; %#ok<AGROW>
    end
    fields = fieldnames(Blk{i}.OutputGroup);
    regularOut = fields(cellfun(@(x) ismember(x,sys.waves), fields));
    for OutputGroupc = regularOut'
        OutputGroup = char(OutputGroupc);
        Output = [Output; Blk{i}.y(Blk{i}.OutputGroup.(OutputGroup))]; %#ok<AGROW>
    end
end
% other IOs only if there are no doubles
for i = 1:length(Blk)
    % Collect all inputs and outputs
    fields = fieldnames(Blk{i}.InputGroup);
    otherIn = fields(cellfun(@(x) ~ismember(x,sys.waves), fields));
    for InputGroupc = otherIn'
        InputGroup = char(InputGroupc);
        newInput = Blk{i}.u(Blk{i}.InputGroup.(InputGroup));
        Input = [Input; newInput(~ismember(newInput,Input))]; %#ok<AGROW>
    end
    fields = fieldnames(Blk{i}.OutputGroup);
    otherOut = fields(cellfun(@(x) ~ismember(x,sys.waves), fields));
    for OutputGroupc = otherOut'
        OutputGroup = char(OutputGroupc);
        newOutput = Blk{i}.y(Blk{i}.OutputGroup.(OutputGroup));
        Output = [Output; newOutput(~ismember(newOutput,Output))]; %#ok<AGROW>
    end
end
% Input = sort(unique(Input));
% Output = sort(unique(Output));
if numel(unique(Input))<numel(Input)
    error('Input names are not unique.');
end
if numel(unique(Output))<numel(Output)
    error('Output names are not unique.');
end

%% Connect the acoustic system
Blk =[{sys},Blk];
sys = connect(Blk{:},Input,Output);
% sys.Blocks = Blk(2:end);

% Append postfix to ensure uniqueness of inputs and outputs for further
% connect() commands
sys.y  = cellfun(@(x) [x '_y' ],sys.y,'UniformOutput',false);

%% Generate state vectors for plotting acoustics
BlkList = cellfun(@(x) x.Name, sys.Blocks, 'UniformOutput', false);

Vals = [sys.Port,{'x'}]; % add x to sys.Port for inner loops
% Vals = Vals(~strcmp(Vals,'dir') & ~strcmp(Vals,'pos')); % exclude dir and pos from sys.Port for inner loops
Vals = Vals(~strcmp(Vals,'dir')); % exclude dir from sys.Port for inner loops
for wavec = sys.waves'
    wave = char(wavec);
    len = length(sys.OutputGroup.(wave));
    for Valc = Vals
        Val = char(Valc);
        if strcmp(Val,'pos')
            sys.state.(wave).(Val) = nan(3, len);
        else
            sys.state.(wave).(Val) = nan(1, len);
        end
    end
    idxX = 0;

    % run through blocks in right order (for x)
    for i = 1:numel(blockOrder)
        if isfield(sys.Blocks{blockOrder(i)}.OutputGroup,wave) % nothing happens for deactivated wave propagation
            nY = numel(sys.Blocks{blockOrder(i)}.OutputGroup.(wave));
            for Valc = Vals
                Val = char(Valc);
                if strcmp(Val,'x') && i>1
                    sys.state.(wave).x(:, idxX + (1:nY)) = sys.Blocks{blockOrder(i)}.state.(wave).x + sys.state.(wave).x(idxX);
                else
                    sys.state.(wave).(Val)(:, idxX + (1:nY)) = sys.Blocks{blockOrder(i)}.state.(wave).(Val);
                end
            end
            idxX = idxX + nY;
        end
    end

%     % Initialize first value using upstream Block and downstream port
%     firstAcCon = 1;
%     while (isempty(sys.Connections{firstAcCon,2}))
%         firstAcCon = firstAcCon+1;
%     end
%     idxPort = cell2mat(sys.Connections(firstAcCon,2));
%     idxBlk = strcmp(char(sys.Connections(firstAcCon,1)),BlkList);
%     if (Block.checkPort(sys.Blocks{idxBlk}.Connection(idxPort),AcBlock.Port) && isfield(sys.Blocks{idxBlk}.OutputGroup,wave))
%         len = length(sys.Blocks{idxBlk}.OutputGroup.(wave));
%         for Valc = Vals
%             Val = char(Valc);
%             sys.state.(wave).(Val)(idxX:idxX+len-1) = sys.Blocks{idxBlk}.state.(wave).(Val);
%         end
%         idxX = idxX+len;
%     end
% 
%     for i = firstAcCon:(size(sys.Connections,1))
%         if not(isempty(sys.Connections{i,3}))
%             % Downstream Block
%             idxPort = cell2mat(sys.Connections(i,4));
%             idxBlk = strcmp(char(sys.Connections(i,3)),BlkList);
%             % Ensure acoustic connection and downstream port
%             if (Block.checkPort(sys.Blocks{idxBlk}.Connection(idxPort),AcBlock.Port) && isfield(sys.Blocks{idxBlk}.OutputGroup,wave))
%                 forward =...
%                     strcmp(wave,'f') ||...
%                     (sys.Blocks{idxBlk}.Connection{idxPort}.dir*sys.Blocks{idxBlk}.Connection{idxPort}.Mach > 0 && ~strcmp(wave,'g'));
%                 % make len (exception for ducts, otherwise 1
%                 if isa(sys.Blocks{idxBlk},'Duct')
%                     len = length(sys.Blocks{idxBlk}.OutputGroup.(wave));
%                 else
%                     len = 1;
%                 end
%                 % make aux_dX for non-forward waves, depending on upstream block
%                 if isprop(sys.Blocks{strcmp(char(sys.Connections(i,1)),BlkList)},'dX')
%                     aux_dX = sys.Blocks{strcmp(char(sys.Connections(i,1)),BlkList)}.dX.(wave);
%                 else % previous block is compact
%                     aux_dX = eps;
%                 end
%                 if idxX==1
%                     sys.state.(wave).x(idxX:idxX+len-1)     = sys.Blocks{idxBlk}.state.(wave).x;
%                     sys.state.(wave).idx(idxX:idxX+len-1)	= sys.Blocks{idxBlk}.state.(wave).idx + 1;
%                 elseif ~forward
%                     sys.state.(wave).x(idxX:idxX+len-1)     = sys.state.(wave).x(idxX-1)   + sys.Blocks{idxBlk}.state.(wave).x  + aux_dX;
%                     sys.state.(wave).idx(idxX:idxX+len-1)   = floor(sys.state.(wave).idx(idxX-1)) + sys.Blocks{idxBlk}.state.(wave).idx;
%                 else
%                     sys.state.(wave).x(idxX:idxX+len-1)     = sys.state.(wave).x(idxX-1)   + sys.Blocks{idxBlk}.state.(wave).x;
%                     sys.state.(wave).idx(idxX:idxX+len-1)   = sys.state.(wave).idx(idxX-1) + sys.Blocks{idxBlk}.state.(wave).idx;
%                 end
%                 for Valc = Vals(~strcmp(Vals,'x') & ~strcmp(Vals,'idx'))
%                     Val = char(Valc);
%                     sys.state.(wave).(Val)(idxX:idxX+len-1)	= sys.Blocks{idxBlk}.state.(wave).(Val);
%                 end
%                 idxX = idxX+len;
%             end
%         end
%     end
end

if size(x0,1)==sys.n
    sys.x0 = x0;
elseif ~isempty(x0) && ~all(x0==0)
    warning('Updating changed the order of the system, initial vector is  reinitialized to zero');
end
sys.uptodate = true;
end