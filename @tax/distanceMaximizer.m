function criterion = distanceMaximizer(activeVec,active,A_active,b_active,relevant,exponent)
    % distanceConstraint sets up a non-linear constraint function for
    % geometry optimization in makeGeo
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % Input:        * posVec: vector with all position data of the problem
    % Output:       * criterion: function value to be minimized
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  27 Apr 2018
    % ------------------------------------------------------------------
    
    posVec(active) = activeVec;
    posVec(~active) = A_active*activeVec + b_active;
    allPos = reshape(posVec,numel(posVec)/3,3);
    x = allPos(relevant,1);
    y = allPos(relevant,2);
    z = allPos(relevant,3);
    Mat = (x-x.').^2 + (y-y.').^2 + (z-z.').^2; % includes a factor 2, but I don't care
    % 4th root, so small distance dominate and yield a higher criterion,
    % which will be avoided by fmincon
    criterion = -sum(sum(Mat.^(.5*exponent)));
end