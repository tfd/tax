function [J,combinations,Labels,models] = serialParameterStudy(sys,fun,funInputs,varargin)
% parameterStudy function is iterating over a set of parameters in order to
% compute a function value of 
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% [J,combinations,Labels,models] = parameterStudy(sys,fun,varargin);
% Input:        * sys:       tax object
%               * fun:       function to be evaluated
%               * funInputs: cell array with additional inputs for the
%                            function to be evaluated. 1st column has the
%                            values, 2nd column gives a string, that
%                            retrieves the functions value when evaluated
%               * varargin:  linear cell containing the labels (odd
%                            indices) and values (even) of parameters to
%                            be permuted
% Output:       * J:             function value evaluated on the tax models
%               * combinations:  parameter combinations used to compute
%               * Labels:        Identifiers of the permuted parameters
%               * models:        resulting tax models
% ------------------------------------------------------------------
% Authors:      Stefan Jaensch, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  30 Oct 2018
% ------------------------------------------------------------------

% define input
funInputString = 'funInputs{1,1}';
for i = 2:size(funInputs,1)
    funInputString = [funInputString,',funInputs{',num2str(i),',1}']; %#ok<AGROW>
end
myFunInputs = funInputs; % init

% convert non-cell parameter values to cell (necessary for allcomb)
for i = 1:2:length(varargin)
    if ~iscell(varargin{i+1})
        varargin{i+1} = num2cell(varargin{i+1});
    end
end

combinations = allcomb(varargin{2:2:end});
[nCombs, ~] = size(combinations);

Labels = varargin(1:2:end);

J = cell(nCombs,1);
models = J;

tic
tmp = [Labels; combinations(1,:)];
firstmodel = changeParam(sys,tmp{:}); % old and save version
models{1} = update(firstmodel);
J{1} = eval(['fun(models{1},',funInputString,');']);
t = toc;
disp(['Number of combinations: ' num2str(nCombs)])
disp(['Start time: ' datestr(now,'HH:MM:SS')])
disp(['Estimated computation time: ' datestr(datenum(0,0,0,0,0,t*nCombs),'DD:HH:MM:SS') ' (' num2str(t*nCombs) 's)'])

iClosest = 0:nCombs-1;
lSeed = 1;
for i = numel(varargin):-2:2
    lSeed = lSeed*numel(varargin{i});
    iClosest(lSeed+1:lSeed:end) = 1:lSeed:nCombs-lSeed;
end
nOut = nargout;
% warning off
for i = 2:nCombs
    tmp = [Labels; combinations(i,:)];
    models{i} = changeParam(sys,tmp{:}); % old and save version
    try
        models{i}.Name = [sys.Name '_' fSetName(combinations(i,:))];
    catch
        models{i}.Name = [sys.Name '_' num2str(i)]; % just count for non-numeric parameters
    end
    for ii = 1:size(funInputs,1)
        if ~isempty(funInputs{ii,2})
            myFunInputs{ii,1} = eval(funInputs{ii,2});
        end
    end
    if ~nOut>3 && ~ismember(iClosest(i),iClosest(i+1:end))
        models{iClosest(i)} = []; % save some RAM
    end
    [J{i},models{i}] = Loop(fun,models{i},combinations(i,:),funInputString,myFunInputs);
end
% warning on
toc

end

function Name = fSetName(combinations)
Name = strjoin(cellfun(@num2str,combinations,'UniformOutput',false),'_');
end

function [J,model] = Loop(fun,model,combination,funInputString,funInputs) %#ok<INUSD,INUSL>
try
    model = update(model);
    J = eval(['fun(model,',funInputString,');']);
catch err
    J = [];
    warning(['Combination ' fSetName(combination) ' failed'] )
    disp(err.message)
end
end