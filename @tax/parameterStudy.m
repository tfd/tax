function [J,combinations,Labels,models] = parameterStudy(sys,fun,varargin)
% parameterStudy function is iterating over a set of parameters in order to
% compute a function value of 
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% [J,combinations,Labels,models] = parameterStudy(sys,fun,varargin);
% Input:        * sys:       tax object
%               * fun:       function to be evaluated
%               * varargin:  linear cell containing the labels (odd
%               indices) and values (even) of parameters to be permuted
% Output:       * J:             function value evaluated on the tax models
%               * combinations:  parameter combinations used to compute
%               * Labels:        Identifiers of the permuted parameters
%               * models:        resulting tax models
% ------------------------------------------------------------------
% Authors:      Stefan Jaensch, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  26 Sep 2018
% ------------------------------------------------------------------

% convert non-cell parameter values to cell (necessary for allcomb)
for i = 1:2:length(varargin)
    if ~iscell(varargin{i+1})
        varargin{i+1} = num2cell(varargin{i+1});
    end
end

combinations = allcomb(varargin{2:2:end});
[nCombs, ~] = size(combinations);

Labels = varargin(1:2:end);

J = cell(nCombs,1);
models = J;
% for i = 1:nCombs    
%     tmp = [Labels; combinations(i,:)];
%     try
%         models{i} = changeParam(models{i-1},tmp{:}); % use old system for speedup (put the arguments requiring long updates first in the function call!)
%     catch
%         models{i} = changeParam(sys,tmp{:}); % old and save version
%     end
%     try
%         models{i}.Name = [sys.Name '_' fSetName(combinations(i,:))];
%     catch
%         models{i}.Name = [sys.Name '_' num2str(i)]; % just count for non-numeric parameters
%     end
% end

tic
tmp = [Labels; combinations(1,:)];
firstmodel = changeParam(sys,tmp{:}); % old and save version
models{1} = update(firstmodel);
J{1} = fun(models{1});
t = toc;
if t*nCombs>30
    cluster = parcluster('local'); % build the 'local' cluster object
    nw = cluster.NumWorkers; % get the number of workers
else
    nw = 1;
end
disp(['Number of combinations: ' num2str(nCombs)])
disp(['Start time: ' datestr(now,'HH:MM:SS')])
disp(['Estimated computation time: ' datestr(datenum(0,0,0,0,0,t*nCombs/nw),'DD:HH:MM:SS') ' (' num2str(t*nCombs/nw) 's)'])

fmstats = whos('firstmodel');
maxMem = 1.7e10; % bytes
nWorkers = 4;
memWorker = 700e6; % bytes
lSeed = numel(varargin{end});
iSeed = 1:lSeed:nCombs;
nSeed = numel(iSeed);
i = 1;
while nWorkers*(memWorker + nSeed*fmstats.bytes) > .5*maxMem && i<numel(varargin)/2
    lSeed = lSeed*numel(varargin{end-2*i});
    iSeed = 1:lSeed:nCombs;
    nSeed = numel(iSeed);
    i = i+1;
end
sJ = cell(nSeed,1);
seeds = sJ;
nOut = nargout;

seeds{1} = models{1};
sJ{1} = J{1};
if t*nCombs>30
    parfor i = 2:nSeed
        tmp = [Labels; combinations(iSeed(i),:)]; %#ok<PFBNS>
        seeds{i} = changeParam(sys,tmp{:}); % old and save version
        try
            seeds{i}.Name = [sys.Name '_' fSetName(combinations(iSeed(i),:))];
        catch
            seeds{i}.Name = [sys.Name '_' num2str(iSeed(i))]; % just count for non-numeric parameters
        end
        [sJ{i},seeds{i}] = Loop(fun,seeds{i},combinations(iSeed(i),:));
    end
    J(iSeed) = sJ;
    models(iSeed) = seeds;
    parfor i = 2:nCombs
        if ~any(i==iSeed)
            tmp = [Labels; combinations(i,:)];
            try
                model = changeParam(seeds{find(iSeed<i,1,'last')},tmp{:}); %#ok<PFBNS> % use seed system for speedup (put the arguments requiring long updates first in the function call!)
            catch
                model = changeParam(sys,tmp{:}); % old and save version
            end
            try
                model.Name = [sys.Name '_' fSetName(combinations(i,:))];
            catch
                model.Name = [sys.Name '_' num2str(i)]; % just count for non-numeric parameters
            end
            if nOut>3
                [J{i},models{i}] = Loop(fun,model,combinations(i,:));
            else
                [J{i},~] = Loop(fun,model,combinations(i,:)); % save some RAM
            end
        end
    end
else
    for i = 2:nCombs
        tmp = [Labels; combinations(i,:)];
        models{i} = changeParam(sys,tmp{:}); % old and save version
        try
            models{i}.Name = [sys.Name '_' fSetName(combinations(i,:))];
        catch
            models{i}.Name = [sys.Name '_' num2str(i)]; % just count for non-numeric parameters
        end
        [J{i},models{i}] = Loop(fun,models{i},combinations(i,:));
    end
end
toc

end

function Name = fSetName(combinations)
Name = strjoin(cellfun(@num2str,combinations,'UniformOutput',false),'_');
end

function [J,model] = Loop(fun,model,combination)
try
    model = update(model);
    J = fun(model);
catch err
    J = [];
    warning(['Combination ' fSetName(combination) ' failed'] )
    disp(err.message)
end
end