function sys = scatter(varargin)
% scatter function removes parts of the system, that are not part of the
% scattering matrix specified by the user.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% sys = tax(PathToModel);
% Input:        * sys: tax object
%               * chars of scatteringDummy Block names to be used for
%               scattering matrix computation, with optional strings to
%               specify whether block sits at 'Upstream' or 'Downstream'
%               end of the desired part of the system
%               * 'dropIO': option to drop in- and outputs that are not
%               part of the scattering matrix
%               * 'SortConnections':  Sort and reindex the Connections
% Output:       * sys: tax object of scattering matrix
% ------------------------------------------------------------------
% Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  03 May 2018
% ------------------------------------------------------------------

sys = varargin{1};
scatBlockName = {};
loc = {};
keepList = {};
dropIO = 0;
dropIOandEmptyScatter = 0;
SortConnections = 0;
truncateRef = 0;

ii = 0;
iii = 0;
for i = 2:nargin
    if ischar(varargin{i})
        if strcmp(varargin{i},'SortConnections')
            SortConnections = 1;
        elseif strcmp(varargin{i},'dropIO')
            dropIO = 1;
        elseif strcmp(varargin{i},'dropIOandEmptyScatter')
            dropIOandEmptyScatter = 1;
        elseif strcmp(varargin{i},'keepQT')
            for j = find(cellfun(@(x) strncmp(x,'Q_',2) && (strcmp(x(end-3:end),'_Q_y') || strcmp(x(end-3:end),'_T_y')),sys.y))'
                iii = iii+1;
                keepList{iii} = sys.y{j}; %#ok<AGROW>
            end
            for j = find(cellfun(@(x) strncmp(x,'Q_',2) && (strcmp(x(end-1:end),'_Q') || strcmp(x(end-1:end),'_T')),sys.u))'
                iii = iii+1;
                keepList{iii} = sys.u{j}; %#ok<AGROW>
            end
        elseif strcmp(varargin{i},'keep')
            iii = iii+1;
            keepList{iii} = varargin{i+1}; %#ok<AGROW>
        elseif strcmp(varargin{i},'truncateRef')
            truncateRef = 1;
        elseif not(strcmp(varargin{i},'Upstream') || strcmp(varargin{i},'Downstream') || (ischar(varargin{i-1}) && strcmp(varargin{i-1},'keep')))
            ii = ii+1;
            scatBlockName{ii} = varargin{i}; %#ok<AGROW>
            if i+1<=nargin
                if strcmp(varargin{i+1},'Upstream')||strcmp(varargin{i+1},'Downstream')
                    loc{ii} = varargin(i+1); %#ok<AGROW>
                end
            end
        end
    elseif iscell(varargin{i})
        scatCell = varargin{i};
        scatBlockName = scatCell(:,1);
        if size(scatCell,2)>0
            loc = scatCell(:,2);
        end
    end
end

if isempty(scatBlockName)
    scatBlockName = cellfun(@(x) x.Name,sys.Blocks(sys.getBlock('class','scatteringDummy')),'UniformOutput',false);
end

Connections = sys.Connections;

% retrun if there is no scatterBlock
if isempty(scatBlockName)
    warning('No ScatterBlocks found. No inputs and outputs defined.');
    sys.y = {};
    sys.u = {};
    sys.C = zeros(0,size(sys.A,2));
    sys.D = zeros(0,0);
    sys.B = zeros(size(sys.A,1),0);
    return
end
scatterBlocks = sys.Blocks(sys.getBlock(scatBlockName));
% exclude inactive scatteringDummies
j = 1;
for i = 1:numel(scatterBlocks)
    if ~scatterBlocks{j}.active
        scatterBlocks(j) = []; % remove inactive blocks from list
    else
        j = j+1;
    end
end

if not(isempty(loc)) % Override location/orientation of scattering blocks
    for ii = 1:length(loc)
        scatterBlocks{ii}.loc = loc{ii};
    end
end

if isempty(scatterBlocks)
    msgbox('There is no scattering block in the network.','Scattering block missing','warning');
end

head=[];
for i = 1: length(scatterBlocks)
    if strcmp(scatterBlocks{i}.loc,'Upstream')
        idx = scatterBlocks{i}.Connection{1}.idx;
        Connections{end+1,1} = Connections{idx,1}; Connections{idx,1} = ''; %#ok<AGROW>
        Connections{end,2} = Connections{idx,2}; Connections{idx,2} = '';
        head = idx+1;
    else
        idx = scatterBlocks{i}.Connection{2}.idx;
        Connections{end+1,3} = Connections{idx,3}; Connections{idx,3} = ''; %#ok<AGROW>
        Connections{end,4} = Connections{idx,4}; Connections{idx,4} = '';
        head = idx-1;
    end
end

% Sort Connections and truncate the superfluous ends
Connections = sys.sortConnections(Connections,head);

% find all reference elements
refBlockName = cellfun(@(x) x.Name,[sys.Blocks(sys.getBlock('class','referenceGeneral')),sys.Blocks(sys.getBlock('class','reference'))],'UniformOutput',false);
if truncateRef
    % truncate reference connections
    for i = 1:numel(refBlockName)
        outIdx = find(strcmp(Connections(:,1),refBlockName{i}) & cellfun(@(x) ~isempty(x) && x==3, Connections(:,2)));
        if ~isempty(outIdx)
            Connections = Connections([1:outIdx-1,outIdx+1:end],:);
        end
    end
else
    % only truncate reference connections, if the flame is not in the connection list anyway
    auxConnections = Connections;
    for i = 1:numel(refBlockName)
        outIdx = find(strcmp(auxConnections(:,1),refBlockName{i}) & cellfun(@(x) ~isempty(x) && x==3, auxConnections(:,2)));
        if ~isempty(outIdx)
            auxConnections = auxConnections([1:outIdx-1,outIdx+1:end],:);
        end
    end
    for i = 1:numel(refBlockName)
        outIdx = find(strcmp(Connections(:,1),refBlockName{i}) & cellfun(@(x) ~isempty(x) && x==3, Connections(:,2)));
        if ~isempty(outIdx) && ~ismember(Connections{outIdx,3},unique([auxConnections(:,1);auxConnections(:,3)]))
            Connections = Connections([1:outIdx-1,outIdx+1:end],:);
        end
    end
end

% Remove Blocks
Blocklist = unique([Connections(:,1);Connections(:,3)]);
sys.Blocks = sys.Blocks(sys.getBlock(Blocklist));

% Set new indices of connections
if SortConnections
    sys.Connections = sys.sortConnections(Connections);
end

% Update system
sys.uptodate= false; sys = sys.update;

if dropIOandEmptyScatter
    inIdx = [];
    outIdx = [];
    for i = 1:numel(keepList)
        inIdx = [inIdx,find(strcmp(keepList{i},sys.u),1,'first')]; %#ok<AGROW>
        outIdx = [outIdx,find(strcmp(keepList{i},sys.y),1,'first')]; %#ok<AGROW>
    end
    sys = sys.truncate(...
        unique([sys.OutputGroup.Scatter,reshape(outIdx,1,numel(outIdx))]),...
        unique([sys.InputGroup.Scatter,reshape(inIdx,1,numel(inIdx))])...
        ); % regular dropIO first
    sTest = 1.5*abs(sys.maxGrowthRate);
    if isnan(sTest)
        sTest = 1.5*abs(max(real(sys.eig)));
    end
    if isempty(sys.A)
        checkMat = sys.D;
    else
        checkMat = sys.C*((sTest*eye(size(sys.A))-sys.A)\sys.B) + sys.D; % check for zero rows and columns to determine obsolete inputs and outputs
    end
    idxIn = any(checkMat,1); % keep inputs that have an impact
    idxOut = any(checkMat,2); % keep outputs that show a reaction to any input
    % renew keepList indices
    inIdx = [];
    outIdx = [];
    for i = 1:numel(keepList)
        inIdx = [inIdx,find(strcmp(keepList{i},sys.u),1,'first')]; %#ok<AGROW>
        outIdx = [outIdx,find(strcmp(keepList{i},sys.y),1,'first')]; %#ok<AGROW>
    end
    sys = sys.truncate(...
        unique([reshape(find(idxOut),1,numel(find(idxOut))),reshape(outIdx,1,numel(outIdx))]),...
        unique([reshape(find(idxIn),1,numel(find(idxIn))),reshape(inIdx,1,numel(inIdx))])...
        ); % still keep stuff from keepList
elseif dropIO
    inIdx = [];
    outIdx = [];
    for i = 1:numel(keepList)
        inIdx = [inIdx,find(strcmp(keepList{i},sys.u),1,'first')]; %#ok<AGROW>
        outIdx = [outIdx,find(strcmp(keepList{i},sys.y),1,'first')]; %#ok<AGROW>
    end
    sys = sys.truncate(...
        unique([sys.OutputGroup.Scatter,reshape(outIdx,1,numel(outIdx))]),...
        unique([sys.InputGroup.Scatter,reshape(inIdx,1,numel(inIdx))])...
        );
end