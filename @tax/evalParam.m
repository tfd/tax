function value = evalParam(expr)
% evalParam function to interpret results from get_param from simulink
% model.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% value = evalParam(expr);
% Input:        * expr: result of a call of get_param(...)
% Output:       * value: interpreted value
%
% ------------------------------------------------------------------
% Authors:      Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  19 Sep 2018
% ------------------------------------------------------------------

try
    value = eval(expr); % numbers or formula
catch
    if strcmp(expr,'on')
        value = true;
    elseif strcmp(expr,'off')
        value = false;
    else
        value = expr;
    end
end

