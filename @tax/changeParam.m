function [sys] = changeParam(sys,varargin)
    % changeParam function to change parameters in a tax model.
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = getModel(sys,'Param1',Value1,'Param2',Value2,...);
    % Input:        * sys: thermoacoustic network (tax) model
    %               * Param1: BlockName.BlockProperty (e.g. INLET.r)
    %               * Value1: value of block property 
    % Output:       * sys: thermoacoustic network (tax) model
    %
    % ------------------------------------------------------------------
    % Authors:      Stefan Jaensch, Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  30 Sep 2021
    % ------------------------------------------------------------------

    % try to do things quickly, assuming there are no long range properties
    % involved (this is the old changeParam, except longRangeChanged,
    % newLRDummy and try-catch)
    longRangeChanged = false; % initialize decision variable
    newLRDummy = cell(1,numel(sys.Blocks)); % cell array of block dummies with projected long-range changes
    sys_save = sys; % copy of the system to enable check whether changeParam did what it was supposed to do.

    % allow collective input of name value pairs to be changed as cell array
    if numel(varargin)==1 && iscell(varargin{1})
        varargin = varargin{1};
    end

    for i = 1:2:length(varargin)
        C = strsplit(varargin{i},'.');
        try
            eval(['sys.Blocks{getBlock(sys,C{1})}.' strjoin(C(2:end),'.') '=varargin{i+1};']) % we cannot avoid the ugly eval command.
                % Problems arise from properties that have a dot in them, like in case of Duct.activeProp.alpha
        catch err
            % error message evaluation (2 conditions, because it has changed with
            % latest Matlab Version)
            errLRProp = ...
                strncmp(err.message,'You cannot set the read-only property',37) ...
                || strncmp(err.message,'Unable to set the',17);
            if errLRProp && all(tax.compareProp(varargin{i+1},eval(['sys.Blocks{getBlock(sys,C{1})}.' strjoin(C(2:end),'.')])))
                % if there is no actual change, just continue. If the value
                % already differs from the GUI that will be logged later.
                continue
            elseif errLRProp
                longRangeChanged = true;
                % Don't abort (break), because this gives a finished result for
                % changedSimpleProps later.
                iBlock = getBlock(sys,C{1});
                if isempty(newLRDummy{iBlock})
                    % create dummy with uptodate long-range parameters (--> sys.Blocks{iBlock}.pars)
                    newLRDummy{iBlock} = sys.Blocks{iBlock}.pars2sys(struct(), sys.Blocks{iBlock}.pars);
                end
                % update dummy to log projected changes of long-range parameters
                eval(['newLRDummy{iBlock}.' strjoin(C(2:end),'.') '=varargin{i+1};'])
            else
                eval(['sys.Blocks{getBlock(sys,C{1})}.' strjoin(C(2:end),'.') '=varargin{i+1};']) % repeat the error, so the user can see the full error message
            end
        end
    end

    if ~longRangeChanged
        sys = update(sys);
    else

        %%%%%%%%%% -- change long-range properties -- %%%%%%%%%%
        % some nomenclature:
        %   customProps -- properties that are not standard properties of the tax class
        %   changed... -- whatever comes behind this prefix differs from what can be found in the GUI
        %
        %   element, gui -- refers to the simulink diagram
        %   block -- refers to the matlab model
        %   props -- refers to the matlab model properties
        %   pars -- refers to the parameters given to the constructor of the block
        %
        % Caution! changeParam relies on properties having the same variable name
        % in the GUI of simulink (mask) as in the element code. When defining new
        % elements this convention should be obeyed to.
        %
        % How long-range properties are changed:
        % 1. Find all customProps that already differ from the GUI
        % (changedOldCustomProps) and all that will after the run of changeParam
        % (changedNewCustomProps).
        % 2. Check which ones are long-range properties --> changedOldLRProps and
        % changedNewLRProps.
        % 3a. If no long-range parameters are changed (changedNewLRProps is empty),
        % run old-fashioned changeParam, i.e. do the changes locally.
        % 3b. If there are changes on long-range parameters to be made
        % (changedNewLRProps is not empty), save all values from the GUI
        % (oldMaskPars), enter all long-range changes in the GUI (using
        % changedOldLRProps and changedNewLRProps), retrieve a new model from the
        % GUI using getmodel, reset the GUI, and set up a new model using the
        % result from getmodel. Then run old-fashioned changeParam to reapply all
        % simple changes that have ever been done to the model using
        % changedOldCustomProps and changedNewCustomProps.

        % some settings and initializations
        taxProps = properties(sys); % list of general properties every tax model has (i.e. these are not customProps)
        load_system(sys.modelPath); % set correct system for gcs (to avoid clicking errors)
        elements = find_system(gcs); % compile list of elements in the system regarded
        nName = numel(sys.Name)+2; % position in the string where block names start
%         oldMaskPars = {}; % initialize list of mask parameters as shown in the GUI, that will be overwritten
        changedSimpleProps = {}; % initialize cell array of simple changes compared to GUI
        changedLRProps = {}; % initialize cell array of long-range changes compared to GUI

        %% (1) Compile changedSimpleProps and changedLRProps
        for i = 1:numel(sys.Blocks)
            % make a dummy struct for the block regarded with gui pars
            guiDummy{i} = makeGuiBlock(sys.Blocks{i},elements,nName); %#ok<AGROW>
            % make a dummy struct for the block regarded with updated sys.Blocks{i}.pars (if it doesn't already exist)
            if isempty(newLRDummy{i})
                newLRDummy{i} = sys.Blocks{i}.pars2sys(struct(), sys.Blocks{i}.pars);
            end
            % find blockSimpleProps and blockLRProps of the block and element regarded
            blockProps = properties(sys.Blocks{i});
            mc = metaclass(sys.Blocks{i});
            propNames = {mc.PropertyList.Name};
            propSetAccess = {mc.PropertyList.SetAccess};
            propDependent = [mc.PropertyList.Dependent].';
            blockSimpleProps{i} = blockProps(~propDependent(ismember(blockProps,propNames)) & cellfun(@(x) strcmp(propSetAccess(strcmp(propNames,x)),'public'), blockProps) & ~ismember(blockProps,taxProps)); %#ok<AGROW>
            blockLRProps{i} = blockProps(~propDependent(ismember(blockProps,propNames)) & cellfun(@(x) strcmp(propSetAccess(strcmp(propNames,x)),'protected'), blockProps) & ~ismember(blockProps,taxProps)); %#ok<AGROW>
            % go through blockSimpleProps and check whether the respective property
            % differs from the GUI. This works for all simple properties, since all
            % simple properties must be directly specified in the GUI. They can't
            % be derived from somewhere else, because that would make them
            % long-range! We accept [] as a value (in particular for LR).
            for ii = 1:numel(blockSimpleProps{i})
                if ~all(tax.compareProp(sys.Blocks{i}.(blockSimpleProps{i}{ii}), guiDummy{i}.(blockSimpleProps{i}{ii}))) % simple property differs from GUI
                    changedSimpleProps = [changedSimpleProps, [sys.Blocks{i}.Name, '.', blockSimpleProps{i}{ii}], sys.Blocks{i}.(blockSimpleProps{i}{ii})]; %#ok<AGROW>
                end
                % all projected simple changes have been done already,
                % therefore changedSimpleProps does not distinguish between
                % changes from this run of changeParam or earlier runs.
            end
            % go through blockLRProps and check which properties do and
            % will differ from the GUI using newLRDummy
            for ii = 1:numel(blockLRProps{i})
                if ~all(tax.compareProp(newLRDummy{i}.(blockLRProps{i}{ii}), guiDummy{i}.(blockLRProps{i}{ii}))) % long-range property differs from GUI
                    if isfield(sys.Blocks{i}.pars, blockLRProps{i}{ii})
                        changedLRProps = [changedLRProps, [sys.Blocks{i}.Name '.' blockLRProps{i}{ii}], newLRDummy{i}.(blockLRProps{i}{ii})]; %#ok<AGROW>, log changes
                    elseif isfield(sys.Blocks{i}.pars.references, blockLRProps{i}{ii})
                        if isstruct(sys.Blocks{i}.pars.references.(blockLRProps{i}{ii}))
                            for field = fields(sys.Blocks{i}.pars.references.(blockLRProps{i}{ii}))'
                                fc = char(field);
                                if isstruct(sys.Blocks{i}.(blockLRProps{i}{ii}){i}.(blockLRProps{i}{ii}).(fc))
                                    % Caution, this code can only deal with single-level struct
                                    % properties. block.prop1.prop2 should work whereas
                                    % block.prop1.prop2.prop3 causes trouble.
                                    error('changeParam cannot handle multi-level structs.');
                                else
                                    changedLRProps = [changedLRProps, [sys.Blocks{i}.Name '.' sys.Blocks{i}.pars.references.(blockLRProps{i}{ii}).(fc)], newLRDummy{i}.(blockLRProps{i}{ii}).(fc)]; %#ok<AGROW>
                                end
                            end
                        else
                            changedLRProps = [changedLRProps, [sys.Blocks{i}.Name '.' sys.Blocks{i}.pars.references.(blockLRProps{i}{ii})], newLRDummy{i}.(blockLRProps{i}{ii})]; %#ok<AGROW>
                        end
                    else
                        error('something went wrong with pars.references')
                    end
                end
            end
        end

        %% (2) Execute changes. Long-range first, then redo simple changes
        % There are two variables similar to varargin:
        % changedSimpleProps and changedLRProps. Therefore the loop is also
        % similar...
        for i = 1:2:length(changedLRProps)
            C = strsplit(changedLRProps{i},'.');
            if numel(C)~=2
                error('something went wrong with changedLRProps');
            end
            blockName = C{1};
            parName = C{2};
            parValue = changedLRProps{i+1};
            % instead of changing the properties directly, change the
            % corresponding mask parameters
            if ismember(get_param(elements{cellfun(@(x) strcmp(x(nName:end),blockName), elements)}, parName),{'on','off'})
                % logical values are translated to on and off
                if parValue
                    set_param(elements{cellfun(@(x) strcmp(x(nName:end),blockName), elements)}, parName, 'on');
                else
                    set_param(elements{cellfun(@(x) strcmp(x(nName:end),blockName), elements)}, parName, 'off');
                end
            elseif isempty(parValue)
                set_param(elements{cellfun(@(x) strcmp(x(nName:end),blockName), elements)}, parName, '[]');
            else
                % otherwise the parameter will be a string with the value.
                % num2str of a string is the same string --> safe
                set_param(elements{cellfun(@(x) strcmp(x(nName:end),blockName), elements)}, parName, num2str(parValue,20)); % use 20 significant digits, in tax.compareProp default is 12
            end
        end

        % Now make a new system:
        warning('on');
        nBlocks = numel(sys.Blocks); % remember to check whether the sys was truncated by scatter
        sys.Blocks = {}; % delete blocks
        sys = getModel(sys); % new blocks with adapted mask parameters
        close_system(gcs,0); % close system without saving (to retain the old mask parameters
        load_system(sys.modelPath); % reopen so initial state is reinstalled
        % prepare for makeGeo (or its omission)
        nAcPorts = zeros(1,numel(sys.Blocks));
        for i = 1:numel(nAcPorts)
            nAcPorts(i) = sum(cellfun(@(x) Block.isPort({x},AcBlock.Port), sys.Blocks{i}.Connection)); % number of acoustic ports of each block
        end
        if ~sys.plot3D && sum(nAcPorts-2) == -2 % see tax.m for detailed comments
            % initialize all pos as empty, to avoid contradictions
            for i = 1:numel(sys.Blocks)
                for ii = 1:numel(sys.Blocks{i}.Connection)
                    sys.Blocks{i}.Connection{ii}.pos = [];
                end
            end
            % set one to zero arbitrarily
            sys.Blocks{1}.Connection{1}.pos = [0,0,0];
        else
            sys = makeGeo(sys); % this can change the number of connections and blocks for 3d plotting purposes! --> splitDuct
        end
        sys = evalSteadyState(sys); % propagate mask parameters, after reset of mask parameters, in case there are input errors
        sys = sys.changeParam(changedSimpleProps); % repeat previous simple changes (i.e. without long-range loop)
        if nBlocks~=numel(sys.Blocks)
            error('Changing long-range properties of systems that have been truncated using scatter.m is not implemented yet.');
        end
        
        sys = update(sys);

        %% (3) Check if all changes were executed, and if nothing else has changed
        % exception: when symRankineHugoniot is run with const cp, Mratio and kratio are 1, and Hu is NaN
        flameNames = cellfun(@(x) x.Name, sys.Blocks(cellfun(@(x) isa(x,'symRankineHugoniot') || isa(x,'symAreaChangeRankineHugoniot'), sys.Blocks)), 'UniformOutput', false);
%         if any(ismember(cellfun(@(x) [x '.Version'], flameNames, 'UniformOutput', false), varargin(1:2:end)))
%             affectedFlames = flameNames(ismember(varargin(1:2:end), cellfun(@(x) [x '.Version'], flameNames, 'UniformOutput', false)));
%             for flame = reshape(affectedFlames, 1, numel(affectedFlames))
            for flame = reshape(flameNames, 1, numel(flameNames))
                fc = char(flame);
                auxBlock = sys.Blocks{sys.getBlock(fc)}; % abbreviate the blcok in question
%                 iVersion = find(ismember(varargin(1:2:end), [fc '.Version']))*2;
%                 if strncmp(varargin{iVersion},'constant cp',11)
                if strncmp(auxBlock.Version,'constant cp',11)
                    % make implicit changes explicit, if they aren't already
%                     if ismember([fc '.Mratio'], varargin(1:2:end)) % obsolete!
%                         iMratio = find(ismember(varargin(1:2:end), [fc '.Mratio']))*2;
%                         varargin{iMratio} = 1; % change!
% %                         warning(['Other than requested ' fc '.Mratio was changed to 1, since ' fc '.Version is ' varargin{iVersion}]); % warn
%                         warning(['Other than requested ' fc '.Mratio was changed to 1, since ' fc '.Version is ' auxBlock.Version]); % warn
%                     else
%                         varargin = [varargin, [fc '.Mratio'], 1]; %#ok<AGROW>
%                     end
                    if ismember([fc '.kratio'], varargin(1:2:end))
                        ikratio = find(ismember(varargin(1:2:end), [fc '.kratio']))*2;
                        varargin{ikratio} = 1; % change!
%                         warning(['Other than requested ' fc '.kratio was changed to 1, since ' fc '.Version is ' varargin{iVersion}]); % warn
                        warning(['Other than requested ' fc '.kratio was changed to 1, since ' fc '.Version is ' auxBlock.Version]); % warn
                    else
                        varargin = [varargin, [fc '.kratio'], 1]; %#ok<AGROW>
                    end
                    if ismember([fc '.Hu'], varargin(1:2:end))
                        iHu = find(ismember(varargin(1:2:end), [fc '.Hu']))*2;
                        varargin{iHu} = NaN; % change!
%                         warning(['Other than requested ' fc '.Hu was changed to NaN, since ' fc '.Version is ' varargin{iVersion}]); % warn
                        warning(['Other than requested ' fc '.Hu was changed to NaN, since ' fc '.Version is ' auxBlock.Version]); % warn
                    else
                        varargin = [varargin, [fc '.Hu'], NaN]; %#ok<AGROW>
                    end
                    % if the version was just changed to constant cp, the
                    % fuel might be affected
                    if ismember([fc '.Version'], varargin(1:2:end))
                        warning(['By implicit request ' fc '.fuel was changed to other']); % warn
                        varargin = [varargin, [fc '.fuel'], auxBlock.fuel]; %#ok<AGROW>
                    end
                elseif isprop(auxBlock,'fuel') && ~strcmp(auxBlock.fuel,'other')
                    % changes can occur from change in Version (when fuel becomes relevant)
                    if ismember([fc '.Version'], varargin(1:2:end))
                        warning(['By implicit request ' fc '.fuel was changed to ' num2str(auxBlock.fuel)]); % warn
                        varargin = [varargin, [fc '.fuel'], auxBlock.fuel]; %#ok<AGROW>
                    end
                    % changes can occur from change in fuel itself
                    if ismember([fc '.fuel'], varargin(1:2:end))
                        warning(['By implicit request ' fc '.Hu was changed to ' num2str(auxBlock.Hu) ' J/kg, since ' fc '.fuel is ' auxBlock.fuel]); % warn
                        varargin = [varargin, [fc '.Hu'], auxBlock.Hu]; %#ok<AGROW>
                        warning(['By implicit request ' fc '.a_CaHb was changed to ' num2str(auxBlock.a_CaHb) ', since ' fc '.fuel is ' auxBlock.fuel]); % warn
                        varargin = [varargin, [fc '.a_CaHb'], auxBlock.a_CaHb]; %#ok<AGROW>
                        warning(['By implicit request ' fc '.b_CaHb was changed to ' num2str(auxBlock.b_CaHb) ', since ' fc '.fuel is ' auxBlock.fuel]); % warn
                        varargin = [varargin, [fc '.b_CaHb'], auxBlock.b_CaHb]; %#ok<AGROW>
                    end
                    % allow that Hu, a_CaHb and b_CaHb remain constant if fuel is known
                    if ismember([fc '.Hu'], varargin(1:2:end))
                        iHu = find(ismember(varargin(1:2:end), [fc '.Hu']))*2;
                        if varargin{iHu} ~= auxBlock.Hu
                            varargin{iHu} = auxBlock.Hu; % change!
                            warning(['Other than requested ' fc '.Hu was changed to ' num2str(auxBlock.Hu) ' J/kg, since ' fc '.fuel is ' auxBlock.fuel]); % warn
                        end
                    end
                    if ismember([fc '.a_CaHb'], varargin(1:2:end))
                        ia = find(ismember(varargin(1:2:end), [fc '.a_CaHb']))*2;
                        if varargin{ia} ~= auxBlock.a_CaHb
                            varargin{ia} = auxBlock.a_CaHb; % change!
                            warning(['Other than requested ' fc '.a_CaHb was changed to ' num2str(auxBlock.a_CaHb) ', since ' fc '.fuel is ' auxBlock.fuel]); % warn
                        end
                    end
                    if ismember([fc '.b_CaHb'], varargin(1:2:end))
                        ib = find(ismember(varargin(1:2:end), [fc '.b_CaHb']))*2;
                        if varargin{ib} ~= auxBlock.b_CaHb
                            varargin{ib} = auxBlock.b_CaHb; % change!
                            warning(['Other than requested ' fc '.b_CaHb was changed to ' num2str(auxBlock.b_CaHb) ', since ' fc '.fuel is ' auxBlock.fuel]); % warn
                        end
                    end
                end
                % No warning for Aratio reset! If a user is dumb enough to
                % try that, they shall suffer the error.
            end
%         end
        % check
        for i = 1:numel(sys.Blocks)
            blockAllProps = [blockSimpleProps{i}; blockLRProps{i}];
            for ii = 1:numel(blockAllProps)
                cprop1 = [sys.Blocks{i}.Name '.' blockAllProps{ii}];
                if ismember(cprop1, varargin(1:2:end)) ...
                        && ~all(tax.compareProp(sys.Blocks{i}.(blockAllProps{ii}), varargin{find(ismember(varargin(1:2:end), cprop1),1,'first')*2}))
                    error([cprop1 ' has not changed, although it was requested.'])
                elseif isstruct(sys.Blocks{i}.(blockAllProps{ii})) ...
                        && any(cellfun(@(x) ismember([cprop1 '.' char(x)], varargin(1:2:end)), fields(sys.Blocks{i}.(blockAllProps{ii}))))
                    for field = fields(sys.Blocks{i}.(blockAllProps{ii}))'
                        fc = char(field);
                        if isstruct(sys.Blocks{i}.(blockAllProps{ii}).(fc))
                            error('changeParam cannot handle multi-level structs.');
                        end
                        cprop2 = [sys.Blocks{i}.Name '.' blockAllProps{ii} '.' fc];
                        if ismember(cprop2, varargin(1:2:end)) ...
                                && ~all(tax.compareProp(sys.Blocks{i}.(blockAllProps{ii}).(fc), varargin{find(ismember(varargin(1:2:end), cprop2),1,'first')*2}))
                            error([cprop2 ' has not changed, although it was requested.'])
                        elseif ~ismember(cprop2, varargin(1:2:end)) ...
                                && ~all(tax.compareProp(sys.Blocks{i}.(blockAllProps{ii}).(fc), sys_save.Blocks{i}.(blockAllProps{ii}).(fc)))
                            error([cprop2 ' has changed, although it was not requested.'])
                        end
                    end
                elseif (~ismember(cprop1, varargin(1:2:end)) ...
                            || (isstruct(sys.Blocks{i}.(blockAllProps{ii})) ...
                                && ~any(cellfun(@(x) ismember([cprop1 '.' char(x)], varargin(1:2:end)), fields(sys.Blocks{i}.(blockAllProps{ii})))))) ...
                        && ~all(tax.compareProp(sys.Blocks{i}.(blockAllProps{ii}), sys_save.Blocks{i}.(blockAllProps{ii})))
                    error([cprop1 ' has changed, although it was not requested.'])
                end
            end
        end
    end
end

function guiBlock = makeGuiBlock(block,elements,nName)
    % parameters as last used (e.g. by long-range changeParam)
    blockOldPars = block.pars;
    % parameters as specified by GUI
    elementParNames = fields(blockOldPars);
    guiPars = blockOldPars; % initialize
    for ii = 1:numel(elementParNames)
        try
            guiPars.(elementParNames{ii}) = {get_param(elements{cellfun(@(x) strcmp(x(nName:end),block.Name), elements)}, elementParNames{ii})};
        catch err % if the parameter does not occur in the mask, don't bother, take the one from the block (as initialized),...
            if ~strncmp(err.message,'SubSystem block (mask) does not have a parameter named',54) % ... otherwise...
                % ... repeat the error!
                guiPars.(elementParNames{ii}) = {get_param(elements{cellfun(@(x) strcmp(x(nName:end),block.Name), elements)}, elementParNames{ii})};
            end
        end
    end
    % generate a dummy twin of the block, but with old parameters
    guiBlock = block.pars2sys(struct(), guiPars); % looks strange when displayed in command line but works.
end
