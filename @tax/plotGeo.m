function plotGeo(sys,varargin)
    % plotGeo plots the geometry defined and optimized in makeGeo
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % Input:        * sys: tax object
    % Output:       -- none --
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  24 Apr 2018
    % ------------------------------------------------------------------
    
    if nargin>2
        Label = varargin{1};
        textPos = varargin{2};
        mode = 'noLabels';
    elseif nargin>1
        mode = varargin{1};
    else
        mode = 'ductLabels';
    end
    if ~exist('textPos','var')
        textPos = cell(1,numel(sys.Blocks));
    end
    % warn if makeGeo was not run
    if ~sys.plot3D
        warning(['The system ',sys.Name,' is not initialized for 3D plotting. This may cause incorrect outputs. Call tax.m with the input argument plot3D set to true to prevent this.']) 
    end
    
    Blocks = sys.Blocks;
    allPos = nan(1,3);
    for i = 1:numel(Blocks)
        con = Blocks{i}.Connection(cellfun(@(x) Blocks{i}.isPort({x},AcBlock.Port), Blocks{i}.Connection));
        pos = cell2mat(cellfun(@(x) x.pos.', con, 'UniformOutput', false)).';
        allPos = [allPos;pos;nan(1,3)]; %#ok<AGROW>
        if (isprop(Blocks{i},'l') && ~strcmp(mode,'noLabels')) || strcmp(mode,'allLabels')
            Label{i} = Blocks{i}.Name; 
        else
            Label{i} = ''; 
        end
        if isempty(textPos{i})
            textPos{i} = mean(pos,1);
        end
    end
    fig1 = figure();
    plot3(allPos(:,1),allPos(:,2),allPos(:,3),'.-k')
    grid on
    axis equal
    xlabel('x_1 [m]')
    ylabel('x_2 [m]')
    zlabel('x_3 [m]')
    set(fig1,'Name',['Geometry of ',sys.Name],'Color',[1 1 1]);
    for i = 1:numel(Blocks)
        text(textPos{i}(1),textPos{i}(2),textPos{i}(3),Label{i});
    end
end