function [abscissa,ordinate] = plot3dMode(sys,outVector,varargin)
    % plotGeo plots the geometry defined and optimized in makeGeo
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % Input:        * sys: tax object
    % Output:       * abscissa: struct containing plotdata
    %               * ordinate: struct containing plotdata
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  25 Apr 2018
    % ------------------------------------------------------------------
    
    % switch operation mode depending on input
    for i = 1:numel(varargin)
        if iscell(varargin{i})
            aux = varargin{i};
            if ischar(aux{i})
                Label = varargin{i};
            else
                textPos = varargin{i};
            end
        elseif ischar(varargin{i})
            if numel(varargin{i})>5 && strcmp(varargin{i}(end-5:end),'Labels')
                textmode = varargin{i};
            elseif strcmp(varargin{i},'complex') || strcmp(varargin{i},'abs') || strcmp(varargin{i},'phase')
                formatmode = varargin{i};
            else
                dispmode = varargin{i};
            end
        elseif isnumeric(varargin{i})
            scale = varargin{i};
        else
            error('Bad input definition.');
        end
    end

    % fill in missing parameters
    if ~exist('textmode','var') && ~exist('Label','var')
        textmode = 'ductLabels';
    elseif ~exist('textmode','var')
        textmode = 'noLabels';
    end
    if ~exist('textPos','var')
        textPos = cell(1,numel(sys.Blocks));
    end
    if~exist('formatmode','var')
        formatmode = 'complex';
    end
    if~exist('dispmode','var')
        dispmode = 'full';
    end

    % start like plotGeo
    Blocks = sys.Blocks;
    allPos = nan(1,3);
    for i = 1:numel(Blocks)
        con = Blocks{i}.Connection(cellfun(@(x) Blocks{i}.isPort({x},AcBlock.Port), Blocks{i}.Connection));
        pos = cell2mat(cellfun(@(x) x.pos.', con, 'UniformOutput', false)).';
        allPos = [allPos;pos;nan(1,3)]; %#ok<AGROW>
        if (isprop(Blocks{i},'l') && ~strcmp(textmode,'noLabels')) || strcmp(textmode,'allLabels')
            Label{i} = Blocks{i}.Name; 
        else
            Label{i} = ''; 
        end
        if isempty(textPos{i})
            textPos{i} = mean(pos,1);
        end
        % log geometry for scale
        if size(pos,1)>1 && (~exist('maxdiff','var') || maxdiff<sqrt(sum((pos(2,:) - pos(1,:)).^2)))
            maxdiff = sqrt(sum((pos(2,:) - pos(1,:)).^2));
        end
    end
    %% normal vectors
    % define normal vectors for ducts
    center = mean(allPos(~isnan(allPos(:,1)),:),1);
    longBlocks = cellfun(@(x) isprop(x,'l') && x.l>0, Blocks);
    if ~exist('scale','var')
        scale = maxdiff/5;
    end
    side = zeros(numel(Blocks),3);
    normal = zeros(numel(Blocks),3);
    for i = find(longBlocks)
        con = Blocks{i}.Connection(cellfun(@(x) Blocks{i}.isPort({x},AcBlock.Port), Blocks{i}.Connection));
        pos = cell2mat(cellfun(@(x) x.pos.', con, 'UniformOutput', false)).';
        start = pos(1,:) - center;
        diff = pos(2,:) - pos(1,:);
        side(i,:) = scale*cross(start,diff)/sqrt(sum(start.^2))/sqrt(sum(diff.^2));
        normal(i,:) = cross(side(i,:)/scale,diff);
        ntest = sqrt(sum(normal(i,:).^2))/sqrt(sum(diff.^2));
        if sqrt(sum(start.^2))==0 || ntest>.1
            normal(i,:) = scale*normal(i,:)/sqrt(sum(normal(i,:).^2));
        else
            start = pos(1,:) - center - [0,0,10];
            side(i,:) = scale*cross(start,diff)/sqrt(sum(start.^2))/sqrt(sum(diff.^2));
            normal(i,:) = cross(side(i,:)/scale,diff);
            ntest = sqrt(sum(normal(i,:).^2))/sqrt(sum(diff.^2));
            if sqrt(sum(start.^2))==0 || ntest>.1
                normal(i,:) = scale*normal(i,:)/sqrt(sum(normal(i,:).^2));
            else
                start = pos(1,:) - center - [10,0,0];
                side(i,:) = scale*cross(start,diff)/sqrt(sum(start.^2))/sqrt(sum(diff.^2));
                normal(i,:) = cross(side(i,:)/scale,diff);
                ntest = sqrt(sum(normal(i,:).^2))/sqrt(sum(diff.^2));
                normal(i,:) = scale*normal(i,:)/sqrt(sum(normal(i,:).^2));
                if ntest<.1
                    warning('Geometry not understood...');
                end
            end
        end
    end
    % define normal vectors for compact elements
    for i = find(~longBlocks)
        
    end
    %% make vectors
    for i = 1:numel(Blocks)
        % waves
        for wavec = sys.waves.'
            wave = char(wavec);
            if isfield(Blocks{i}.OutputGroup,wave)
                n.(wave)(i) = size(Blocks{i}.state.(wave).pos,2);
                ordinate.(wave){i} = Blocks{i}.state.(wave).pos;
                abscissa.(wave){i} = outVector(cell2mat(cellfun(@(x) find(strcmp(sys.y,[x,'_y'])), sys.Blocks{i}.y(sys.Blocks{i}.OutputGroup.(wave)), 'UniformOutput', false)));
                scaledAbscissa.(wave){i} = abscissa.(wave){i}/max(abs(outVector(sys.OutputGroup.(wave))));
                switch formatmode
                    case 'complex'
                        vectorAbscissa.(wave){i} = ordinate.(wave){i} + normal(i,:).'*reshape(real(scaledAbscissa.(wave){i}),1,n.(wave)(i)) + side(i,:).'*reshape(imag(scaledAbscissa.(wave){i}),1,n.(wave)(i));
                        absAbscissa.(wave){i} = reshape(abs(scaledAbscissa.(wave){i}),1,n.(wave)(i));
                    case 'abs'
                        vectorAbscissa.(wave){i} = ordinate.(wave){i} + normal(i,:).'*reshape(abs(scaledAbscissa.(wave){i}),1,n.(wave)(i));
                        absAbscissa.(wave){i} = reshape(abs(scaledAbscissa.(wave){i}),1,n.(wave)(i));
                    case 'phase'
                        vectorAbscissa.(wave){i} = ordinate.(wave){i} + normal(i,:).'*reshape(angle(scaledAbscissa.(wave){i})/pi,1,n.(wave)(i));
                        absAbscissa.(wave){i} = reshape(angle(scaledAbscissa.(wave){i})/(2*pi),1,n.(wave)(i));
                end
            else
                n.(wave)(i) = 1;
                ordinate.(wave){i} = nan(3,1);
                abscissa.(wave){i} = nan(1,1);
                scaledAbscissa.(wave){i} = nan(1,1);
                vectorAbscissa.(wave){i} = nan(3,1);
                absAbscissa.(wave){i} = nan(1,1);
            end
        end
        if ordinate.f{i}~=ordinate.g{i}
            error('Different ordinates for f and g!');
        end
        % p and u
        n.p(i) = n.f(i);
        ordinate.p{i} = ordinate.f{i};
        abscissa.p{i} = Blocks{i}.state.f.rho.'.*Blocks{i}.state.f.c.'.*(abscissa.f{i} + abscissa.g{i});
        scaledAbscissa.p{i} = abscissa.p{i}/max(sys.state.f.rho.'.*sys.state.f.c.'.*abs(outVector(sys.OutputGroup.f) + outVector(sys.OutputGroup.g)));
        n.u(i) = n.f(i);
        ordinate.u{i} = ordinate.f{i};
        abscissa.u{i} = abscissa.f{i} - abscissa.g{i};
        scaledAbscissa.u{i} = abscissa.u{i}/max(abs(outVector(sys.OutputGroup.f) - outVector(sys.OutputGroup.g)));
        for fieldc = {'p','u'}
            field = char(fieldc);
            switch formatmode
                case 'complex'
                    vectorAbscissa.(field){i} = ordinate.(field){i} + normal(i,:).'*reshape(real(scaledAbscissa.(field){i}),1,n.(field)(i)) + side(i,:).'*reshape(imag(scaledAbscissa.(field){i}),1,n.(field)(i));
                    absAbscissa.(field){i} = reshape(abs(scaledAbscissa.(field){i}),1,n.(field)(i));
                case 'abs'
                    vectorAbscissa.(field){i} = ordinate.(field){i} + normal(i,:).'*reshape(abs(scaledAbscissa.(field){i}),1,n.(field)(i));
                    absAbscissa.(field){i} = reshape(abs(scaledAbscissa.(field){i}),1,n.(field)(i));
                case 'phase'
                    vectorAbscissa.(field){i} = ordinate.(field){i} + normal(i,:).'*reshape(angle(scaledAbscissa.(field){i})/pi,1,n.(field)(i));
                    absAbscissa.(field){i} = reshape(angle(scaledAbscissa.(field){i})/(2*pi),1,n.(field)(i));
            end
        end
    end
    %% plot
    for fieldc = [{'p','u'},sys.waves(3:end).']
        field = char(fieldc);
        % new figure for each field
        fig1 = figure();
        plot3(allPos(:,1),allPos(:,2),allPos(:,3),'.-k')
        axis equal
        hold on
        grid on
        xlabel('x_1 [m]')
        ylabel('x_2 [m]')
        zlabel('x_3 [m]')
        set(fig1,'Name',['Geometry and ',field,'-fluctuation of ',sys.Name],'Color',[1 1 1]);
        % plot normal vectors and display block names
        for i = 1:numel(Blocks)
            text(textPos{i}(1),textPos{i}(2),textPos{i}(3),Label{i});
            con = Blocks{i}.Connection(cellfun(@(x) Blocks{i}.isPort({x},AcBlock.Port), Blocks{i}.Connection));
            pos = cell2mat(cellfun(@(x) x.pos.', con, 'UniformOutput', false)).';
            plot3(pos(1,1) + [0,side(i,1)],pos(1,2) + [0,side(i,2)],pos(1,3) + [0,side(i,3)],'.-k');
            plot3(pos(1,1) + [0,normal(i,1)],pos(1,2) + [0,normal(i,2)],pos(1,3) + [0,normal(i,3)],'.-k');
            plot3(pos(1,1) - [0,side(i,1)],pos(1,2) - [0,side(i,2)],pos(1,3) - [0,side(i,3)],'.:k');
            plot3(pos(1,1) - [0,normal(i,1)],pos(1,2) - [0,normal(i,2)],pos(1,3) - [0,normal(i,3)],'.:k');
        end
        % plot actual data
        switch dispmode
            case 'full'
                for i = 1:numel(Blocks)
                    patch([ordinate.(field){i}(1,1:end-1);vectorAbscissa.(field){i}(1,1:end-1);vectorAbscissa.(field){i}(1,2:end);ordinate.(field){i}(1,2:end)],...
                        [ordinate.(field){i}(2,1:end-1);vectorAbscissa.(field){i}(2,1:end-1);vectorAbscissa.(field){i}(2,2:end);ordinate.(field){i}(2,2:end)],...
                        [ordinate.(field){i}(3,1:end-1);vectorAbscissa.(field){i}(3,1:end-1);vectorAbscissa.(field){i}(3,2:end);ordinate.(field){i}(3,2:end)],...
                        [zeros(1,n.(field)(i)-1);absAbscissa.(field){i}(1:end-1);absAbscissa.(field){i}(2:end);zeros(1,n.(field)(i)-1)],'LineStyle','none');
                    plot3(vectorAbscissa.(field){i}(1,:),...
                        vectorAbscissa.(field){i}(2,:),...
                        vectorAbscissa.(field){i}(3,:),'.b');
                end
            case 'coloredVectors'
                for i = 1:numel(Blocks)
                    patch([ordinate.(field){i}(1,:);vectorAbscissa.(field){i}(1,:)],...
                        [ordinate.(field){i}(2,:);vectorAbscissa.(field){i}(2,:)],...
                        [ordinate.(field){i}(3,:);vectorAbscissa.(field){i}(3,:)],...
                        [zeros(1,n.(field)(i));absAbscissa.(field){i}],'FaceColor','none','EdgeColor','interp');
                    plot3(vectorAbscissa.(field){i}(1,:),...
                        vectorAbscissa.(field){i}(2,:),...
                        vectorAbscissa.(field){i}(3,:),'.b');
                end
            case 'colored'
                for i = 1:numel(Blocks)
                    patch([ordinate.(field){i}(1,1),vectorAbscissa.(field){i}(1,:),ordinate.(field){i}(1,end)].',...
                        [ordinate.(field){i}(2,1),vectorAbscissa.(field){i}(2,:),ordinate.(field){i}(2,end)].',...
                        [ordinate.(field){i}(3,1),vectorAbscissa.(field){i}(3,:),ordinate.(field){i}(3,end)].',...
                        [0,absAbscissa.(field){i},0].','FaceColor','none','EdgeColor','interp');
                end
            case 'vectors'
                for i = 1:numel(Blocks)
                    plot3([ordinate.(field){i}(1,:);vectorAbscissa.(field){i}(1,:)],...
                        [ordinate.(field){i}(2,:);vectorAbscissa.(field){i}(2,:)],...
                        [ordinate.(field){i}(3,:);vectorAbscissa.(field){i}(3,:)],'.-b');
                end
            case 'simple'
                for i = 1:numel(Blocks)
                    plot3(vectorAbscissa.(field){i}(1,:),...
                        vectorAbscissa.(field){i}(2,:),...
                        vectorAbscissa.(field){i}(3,:),'.-b');
                end
        end
        hold off;
    end
end