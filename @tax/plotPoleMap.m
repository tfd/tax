function plotPoleMap(varargin)
% Plot interactive pole map. Matlab data tips plots the eigenmode.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate
% thermoacoustic network systems. It is developed by the
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% plotPoleMap(sys1,sys2,...)
% Input:        * sys: thermoacoustic network (tax) model object
%
% ------------------------------------------------------------------
% Authors:  S. Jaensch, F. Schily, G. Fournier (fournier@tfd.mw.tum.de)
% Last Change:  11 May 2022
% ------------------------------------------------------------------

figure('name','Eigenvalues limited by MinGrowthMaxfreq in [Hz]')
hold all;
iNonSys = [];
for  i = 1:length(varargin)
    if islogical(varargin{i})
        threeD = varargin{i};
        iNonSys = [iNonSys,i]; %#ok<AGROW>
    elseif ischar(varargin{i})
        normPosVar = varargin{i};
        iNonSys = [iNonSys,i]; %#ok<AGROW>
    else
        sys = varargin{i};

        [result.V, D, result.W] = eig(sys);
        D = diag(D);
        result.D = D;
        plot(real(result.D), imag(result.D)/(2*pi),'x','Linewidth',2,'MarkerSize',10,'DisplayName',sys.Name)
    end
end
if ~exist('threeD','var')
    threeD = false;
end
if ~exist('normPosVar','var')
    normPosVar = '';
end
syss = varargin(~ismember(1:nargin,iNonSys));

% warn if makeGeo was not run
if threeD
    for i = 1:numel(syss)
        if ~syss{i}.plot3D
            warning(['The system ',syss{i}.Name,' is not initialized for 3D plotting. This may cause incorrect outputs. Call tax.m with the input argument plot3D set to true to prevent this.']) 
        end
    end
end

line([0 0],[-1,1] * sys.fMax.f,'Color','black','LineStyle','--','Linewidth',1.3);
line([-1,1] * sys.fMax.f,[0 0],'Color','black','LineStyle','-','Linewidth',1);
hold off;
ylim([0 sys.fMax.f]);
xlim([-sys.fMax.f max(60,1.1*max(real(result.D)))]);
xlabel('Growth rate [1/s]')
ylabel('Frequency [Hz]')
box on;
grid on;

MatlabVer = version('-release');
if strcmp(MatlabVer,'2018b') || str2double(MatlabVer(1:4)) >= 2019
    ax = gca;
    ax.Interactions = [zoomInteraction panInteraction];
end
    dcm_obj = datacursormode(gcf);
    dcm_obj.UpdateFcn = @(x,y) Callback_plotEigenMode(x,y,syss,threeD,normPosVar);

end

function output_txt = Callback_plotEigenMode(obj,event_obj,syss,threeD,normPosVar) %#ok<INUSL>
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

% reproduce eigenvalues of chosen system (incase there are eigenvalues of multiple systems in the plot)
isys = find(cellfun(@(x) strcmp(event_obj.Target.DisplayName,x.Name),syss));
sys = syss{isys}; %#ok<FNDSB> % use the system the chosen EV belongs to
[V, D, ~] = eig(sys);
D = diag(D); % EVs

pos = get(event_obj,'Position');
% Create cursor display
output_txt = {['Growth rate: ',num2str(pos(1),4) ' 1/s'],...
    ['Frequency: ',num2str(pos(2),4) ' Hz'],...
    ['Model name: ' event_obj.Target.DisplayName]};

% Determine eigenvector
i = find(full(D)==pos(1)+1i*pos(2)*(2*pi));
if isempty(i)
    % eig seems to produce different results depending on the output
    % desired: all(unique(sys.eig)==unique(D)) is not true. You get pretty
    % close though. Therefore we introduce a tolerance
    i = find(abs(full(D) - (pos(1)+1i*pos(2)*(2*pi))) == min(abs(full(D) - (pos(1)+1i*pos(2)*(2*pi)))));
    warning(['Eigenvalues do not perfectly match: deviation is: ',num2str(min(abs(full(D) - (pos(1)+1i*pos(2)*(2*pi)))))])
end

% Normalize
outVector = sys.C*V(:,i); % for normalization
if ~isempty(normPosVar)
    sep = find(normPosVar=='_',1,'first');
    normVar = normPosVar(sep+1:end);
    normPos = num2str(str2double(normPosVar(1:sep-1)),['%.',num2str(round(.5+log10(size(sys.Connections,1)))),'i']);
    switch normVar
        case 'u'
            yf = [normPos,'f_y'];
            yg = [normPos,'g_y'];
            norm = outVector(strcmp(sys.y,yf))-outVector(strcmp(sys.y,yg));
        case 'p'
            yf = [normPos,'f_y'];
            yg = [normPos,'g_y'];
            ipos = str2double(normPosVar(1:sep-1));
            con = sys.Blocks{sys.getBlock(sys.Connections{ipos,1})}.Connection{sys.Connections{ipos,2}};
            norm = con.rho*con.c*(outVector(strcmp(sys.y,yf))+outVector(strcmp(sys.y,yg)));
        case 's'
            norm = outVector(strcmp(sys.y,[normPos,'s_y']));
        case 'alpha'
            norm = outVector(strcmp(sys.y,[normPos,'alpha_y']));
        case 'f'
            norm = outVector(strcmp(sys.y,[normPos,'f_y']));
        case 'g'
            norm = outVector(strcmp(sys.y,[normPos,'g_y']));
    end
    V = V./norm;
end

% Convert format
C_f = sys.c(sys.OutputGroup.f,:);
C_g = sys.c(sys.OutputGroup.g,:);
f = C_f*V(:,i);
g = C_g*V(:,i);
outVector = sys.C*V(:,i); % for 3d plot
[AcVec, name, unit] = sys.calcProperty(f, g, sys.property);

if threeD
    % just plot
    sys.plot3dMode(outVector);
else
    % Check if plots already exists
    figTag = 'tax_EigenModePlot';
    fig = findobj('Tag',figTag);
    DisplayName = strjoin(output_txt,', ');
    if isempty(fig)
        fig = figure;
        fig.Tag = figTag;
        DisplayNames = {};
    else
        DisplayNames = get(fig.Children(1).Children,'DisplayName');
        if length(fig.Children(1).Children)==1
            DisplayNames = {DisplayNames};
        end
    end

    figure(fig);
    % Add plot if necessary, assume same discretization for f and g, which is
    % enforced by duct
    if ~any(cellfun(@(x) strcmp(DisplayName,x),DisplayNames))
        plotEigenMode(fig,sys.state.f.x,AcVec,name,unit,sys.format,DisplayName)
    end
end

end

function plotEigenMode(fig,x,AcVec,name,unit,format,DisplayName)
% plot eigenmode
XLabel = 'x [m]';
set(fig.Children,'NextPlot','add')
plotOpts = {'DisplayName',DisplayName};
switch format
    case 'realImag'
        k = 1;
        for i = 1:2
            subplot(2,2,k)
            plot(x,real(AcVec{i}),plotOpts{:},'Linewidth',2);
            ylabel(['Re(',name{i},') ', unit{i}]);
            xlabel(XLabel);
            
            subplot(2,2,k+1)
            plot(x, imag(AcVec{i}),plotOpts{:},'Linewidth',2);
            ylabel(['Im(',name{i},') ', unit{i}])
            xlabel(XLabel);
            
            k = k+2;
        end
        
    case 'absPhase'
        k = 1;
        for i = 1:2
            subplot(2,2,k)
            plot(x,abs(AcVec{i}),plotOpts{:},'Linewidth',2);
            ylabel(['|',name{i},'| ',unit{i}]);
            xlabel(XLabel);
            
            subplot(2,2,k+2)
            plot(x, angle(AcVec{i}),plotOpts{:},'Linewidth',2);
            ylabel(['\angle',name{i},' [rad]'])
            xlabel(XLabel);
            
            k = k+1;
        end
end
set(fig.Children,'NextPlot','replace')
set(fig.Children,'Box','on')
set(fig.Children,'XGrid','on')
set(fig.Children,'YGrid','on')
set(fig.Children,'XLim',x([1,end]))
end
