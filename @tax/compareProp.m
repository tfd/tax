function logicval = compareProp(prop1,prop2,acc)
% compareProp function to compare two property values
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% value = evalParam(expr);
% Input:        * prop1: a property value of a tax object
%               * prop2: a property value of a tax object
%               * acc: significant digits of the comparison in case the
%                   properties are numeric
% Output:       * logicval: true if the property values are the same
%
% ------------------------------------------------------------------
% Authors:      Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  01 Oct 2021
% ------------------------------------------------------------------

if nargin<3
    acc = 12;
end

% format input
if iscell(prop1) && iscell(prop2) && all(size(prop1)==size(prop2))
    sz = size(prop1);
elseif iscell(prop1) && ~iscell(prop2)
    sz = size(prop1);
    prop2 = repmat({prop2},sz); % expand prop2
elseif iscell(prop2) && ~iscell(prop1)
    sz = size(prop2);
    prop1 = repmat({prop1},sz); % expand prop1
elseif ~iscell(prop1) && ~iscell(prop2)
    sz = [1,1];
    prop1 = {prop1};
    prop2 = {prop2};    
else
    logicval = false;
    return
end
n = prod(sz);
logicval = false(sz);

% run through cells
for i = 1:n
    if isempty(prop1{i}) && isempty(prop2{i}) % empty fields are equal, no matter the class
        logicval(i) = true;
    elseif strcmp(class(prop1{i}),class(prop2{i})) % only compare something, if class is the same
        if isa(prop1{i},'char')
            logicval(i) = strcmp(prop1{i},prop2{i});
        elseif isa(prop1{i},'struct') ...
                && all(ismember(fields(prop1{i}),fields(prop2{i}))) ...
                && all(ismember(fields(prop2{i}),fields(prop1{i}))) % both direction so we know they have really the same fields
            logicval(i) = true; % initialize true
            for field = fields(prop1{i})'
                fc = char(field);
                % recursive exploration of structs
                if ~tax.compareProp(prop1{i}.(fc),prop2{i}.(fc))
                    logicval(i) = false;
                    break % if one field is false, it all goes to hell
                end
            end
        elseif ~isa(prop1{i},'struct') && all(size(prop1{i})==size(prop2{i}))
%             logicval(i) = all(reshape(prop1{i}==prop2{i},numel(prop1{i}),1));
            logicval(i) = all(reshape(arrayfun(@(index) strcmp(num2str(prop1{i}(index),acc),num2str(prop2{i}(index),acc)), 1:numel(prop1{i})),numel(prop1{i}),1));
        end
    end
end
