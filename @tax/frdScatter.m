function frdsys = frdScatter(varargin)
    % frdScatter function removes parts of the system, that are not part of the
    % scattering matrix specified by the user. The result is expressed as a
    % frequency domain function (frd).
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % sys = tax(PathToModel);
    % Input:        * sys: tax object
    %               * inputs for scatter.m
    % Output:       * sys: tax object of scattering matrix
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  11 Oct 2017
    % ------------------------------------------------------------------

    sys = varargin{1};
    % run regular scatter (no dropIO and no dropIOandEmptyScatter)
    scs = sys.scatter(varargin{2:end});

    % extract a tf system for each block
    tfBlocks = cell(size(scs.Blocks));
    s = tf('s'); % basic tf
    for i = 1:numel(tfBlocks)
        outIdx = [];
        if isa(scs.Blocks{i},'Duct')
            % regular Duct
            waves = scs.Blocks{i}.waves; % adjust waves vector: only active waves
            idx = cellfun(@(x) scs.Blocks{i}.activeProp.(x), waves);
            waves = waves(idx);
            tau = zeros(numel(waves),1);
            for ii = 1:numel(waves)
                wave = waves{ii};
                tau(ii) = abs(scs.Blocks{i}.l/scs.Blocks{i}.cProp.(wave));
            end
            tau = diag(tau);
            con = diag(ones(numel(waves),1));
            tau = tau(:,[2,1,3:end]);
            con = con(:,[2,1,3:end]);
            tfBlocks{i} = double(con~=0).*exp(-tau*s);
            % sensor outputs:
            allTauSens = zeros(numel(waves),numel(scs.Blocks{i}.sensorPositions));
            for iSens = 1:numel(scs.Blocks{i}.sensorPositions)
                for ii = 1:numel(waves)
                    wave = waves{ii};
                    if scs.Blocks{i}.cProp.(wave)>=0
                        allTauSens(ii,iSens) = scs.Blocks{i}.sensorPositions(iSens)/scs.Blocks{i}.cProp.(wave);
                    else
                        allTauSens(ii,iSens) = (scs.Blocks{i}.sensorPositions(iSens)-scs.Blocks{i}.l)/scs.Blocks{i}.cProp.(wave);
                    end
                    % extend outIdx
                    outIdx = [outIdx;...
                        find(strcmp(scs.Blocks{i}.y,[wave,'_',scs.Blocks{i}.Name,'@',num2str(scs.Blocks{i}.sensorPositions(iSens))]))]; %#ok<AGROW>
                end
                tauSens = diag(allTauSens(:,iSens));
                con = diag(ones(numel(waves),1));
                tfBlocks{i} = [tfBlocks{i};double(con~=0).*exp(-tauSens*s)];
            end
            % add regular outputs without discretization to outIdx
            outIdx = [find(cellfun(@(x)...
                any(    strcmp(x(1)     ,{'0','1','2','3','4','5','6','7','8','9'})) &&...
                ~any(   strcmp(x(end)   ,{'0','1','2','3','4','5','6','7','8','9'})),...
                scs.Blocks{i}.y));...
                outIdx]; %#ok<AGROW>
            % names
            tfBlocks{i}.InputName = scs.Blocks{i}.u;
            tfBlocks{i}.OutputName = scs.Blocks{i}.y(outIdx);
        elseif isa(scs.Blocks{i},'referenceGeneral')
            scs.Blocks{i}.makefrd = true;
            tfBlocks{i} = scs.Blocks{i}.update;
        elseif isempty(scs.Blocks{i}.A) % compact elements without other delay
            tfBlocks{i} = tf(scs.Blocks{i}.D);
            % names
            tfBlocks{i}.InputName = scs.Blocks{i}.u;
            tfBlocks{i}.OutputName = scs.Blocks{i}.y;
        else % other elements like reference with FTF, areaChange or junction with volume
            tfBlocks{i} = scs.Blocks{i}.C*((s*eye(size(scs.Blocks{i}.A))-scs.Blocks{i}.A)\scs.Blocks{i}.B) + scs.Blocks{i}.D;
            % names
            tfBlocks{i}.InputName = scs.Blocks{i}.u;
            tfBlocks{i}.OutputName = scs.Blocks{i}.y;
        end
    end

    % find obsolete inputs and outputs
    allInputNames = cellfun(@(x) x.InputName, tfBlocks, 'UniformOutput', false);
    allInputNames = sort(cat(1,allInputNames{:}));
    allOutputNames = cellfun(@(x) x.OutputName, tfBlocks, 'UniformOutput', false);
    allOutputNames = sort(cat(1,allOutputNames{:}));
    inIdx = cellfun(@(x) any(strcmp(x,scs.u)), allInputNames); % inputs from scs
    outIdx = cellfun(@(x) any(strcmp([x,'_y'],scs.y)), allOutputNames); % outputs from scs
    % find sampling frequencies and adapt to finest sampling frequency series
    freq = 0;
    for i = 1:numel(tfBlocks)
        if isprop(tfBlocks{i},'Frequency')
            freq = unique([tfBlocks{i}.Frequency;0]);
        end
    end
    for i = 1:numel(tfBlocks)
        if isprop(tfBlocks{i},'Frequency')
            tfBlocks{i} = interp(tfBlocks{i},freq);
        else
            tfBlocks{i} = frd(tfBlocks{i},freq);
        end
        tfBlocks{i}.Ts = 0;
    end
    warning off % of course we drop inputs and outputs here...
    frdsys = connect(tfBlocks{:},allInputNames(inIdx),allOutputNames(outIdx));
    warning on
end