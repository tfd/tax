function newPath = recursiveLoopFinder(sys,oldPath)
    % recursiveLoopFinder finds geometrical loops in the simulink model
    % specified by the user
    % ------------------------------------------------------------------
    % This file is part of tax, a code designed to investigate thermoacoustic
    % network systems. It is developed by:
    % Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
    % For updates and further information please visit www.tfd.mw.tum.de
    % ------------------------------------------------------------------
    % Input:        * sys: tax object
    %               * oldPath: row vector if integers, scheme: block
    %               number, connection number, block number, ...
    % Output:       * newPath: cell array of possible continuations of
    %               oldPath
    % ------------------------------------------------------------------
    % Authors:      Felix Schily (schily@tfd.mw.tum.de)
    % Last Change:  28 Apr 2018
    % ------------------------------------------------------------------

    % find current block: the last one on the path
    currentBlock = oldPath(end);
    if numel(oldPath)>1
        oldIdx = oldPath(end-1);
    else
        oldIdx = 0;
    end
    % find all acoustic connections to other blocks
    AcCons = sys.Blocks{currentBlock}.Connection(cellfun(@(x) Block.isPort({x},AcBlock.Port), sys.Blocks{currentBlock}.Connection));
    idxCons = cell2mat(cellfun(@(x) x.idx, AcCons, 'UniformOutput', false));
    idxCons = idxCons(idxCons~=oldIdx); % don't take the same way back!
    % initialize loop
    nc = numel(idxCons);
    newPath = cell(nc,1);
    neighborBlocks = zeros(nc,1);
    % loop over forks: add connection indices, find and add neighboring blocks
    for i = 1:nc
        % find neighboring block
        neighborNames = sys.Connections(idxCons(i),[1,3]);
        neighborBlocks(i) = find(sys.getBlock(neighborNames(~strcmp(neighborNames,sys.Blocks{currentBlock}.Name))));
        % add connection index and neighboring block number to path
        newPath{i} = [oldPath,idxCons(i),neighborBlocks(i)];
    end
    % stop if an end of the model is reached ( nc == 0 ) or if a loop was
    % found ( ismember(neighborBlocks(i),oldPath(1:2:end)) ), otherwise
    % call recursiveLoopFinder again
    if nc == 0
        newPath = {oldPath};
    else
        savePath = newPath; % so there is no mess with indices, when recursiveLoopFinder returns longer cell arrays
        newPath = {};
        for i = 1:nc
            if ~ismember(neighborBlocks(i),oldPath(1:2:end))
                % append the next level of forks
                newPath = [newPath;recursiveLoopFinder(sys,savePath{i})]; %#ok<AGROW>
            else
                newPath = [newPath;savePath(i)]; %#ok<AGROW>
            end
        end
    end
end