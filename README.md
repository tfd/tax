# taX - ThermoAcoustic Network Code

taX is a MATLAB toolbox for solving low-order thermo-acoustic network models of arbitrary topology.

Build your model with the convenient graphical user interface of Simulink and simply run the command:
```matlab
sys = tax('yourModelFile.slx',fMax)
```
Post-processing routines can be used to determine eigenmodes, mode shapes, frequency responses or scattering matrices and assess the stability of the system.

taX comes with a variety of frequently-used acoustic elements, but the library can easily be customized or extended.

## Requirements and Installation

*Programmed with:* MATLAB R2015a, R2017b and R2019a

*Tested on:* Microsoft Windows 10 and Ubuntu 18.04 LTS

*Requirements:* 
- MATLAB with Simulink. Minimum: MATLAB R2015a. Recommended: R2019a or newer.
- [sss](https://github.com/MORLab/sss) toolbox

In order to properly install taX and sss, it is recommended to clone the project from the git repositories using the command:

``git clone --recursive https://gitlab.lrz.de/tfd/tax.git /your/local/directory/taX``

## Documentation
Check out the guided tutorials on our [YouTube channel](https://www.youtube.com/channel/UCjMwaj044Fg973BJJXMAieg/featured)

## Copyright
This toolbox is developed by the [Thermo-Fluid Dynamics Group](https://www.epc.ed.tum.de/en/tfd/home/). If you intend to use it, please contact [Prof. Wolfgang Polifke](mailto:polifke@tfd.mw.tum.de).

## Acknowledgements
The developing team is thankful to all the research assistants and students at [TFD](https://www.epc.ed.tum.de/en/tfd/home/) that have contributed at creating and developing taX since 2014.

The MATLAB toolbox [sss](https://github.com/MORLab/sss) has been developed by [MORLab](https://www.epc.ed.tum.de/en/rt/research/model-order-reduction/), the model reduction lab at the [Chair of Automatic Control](https://www.epc.ed.tum.de/en/rt/home/), in collaboration with [TFD](https://www.epc.ed.tum.de/en/tfd/home/).
