%% Tutorial 4: Scattering Matrices %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Extract a scattering matrix from a model     %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Using the scatter element, you can extract from your model a scattering matrix
% of the region of your choice

% Case1: scatter in the middle of the network. Two scatter elements are needed.
% See in the model.

fMax = 500;                             % Maximum resolution frequency (Hz)
sys = tax('scatter1.slx',fMax);      % Read the Simulink file and create model

% Extract and plot the scattering matrix
sys.plotScatteringMatrix

% Case2: scatter from one boundary to somewhere in the network. Only one scatter
% element. See in the model

fMax = 500;                             % Maximum resolution frequency (Hz)
sys = tax('scatter2.slx',fMax);      % Read the Simulink file and create model

% Extract and plot the scattering matrix
sys.plotScatteringMatrix

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Impose a scattering matrix     %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% If you want to model a complex system where the elements of the library cannot
% capture all the physics or if you have experimental data you want to match,
% you can build and impose a scattering matrix in your system.

% Limitations: the current implementation works only for acoustics. Mean values
% have to be constant between the input and output of your scattering element.

% Here is an example with a swirler computed with LNSE
resultFolder = 'LNSEdata';

% Read the data
data = dlmread([resultFolder '/' 'abs_fd.csv'], ',', 5, 1);
freq = data(1:11, 1);
freq(1) = 1e-3;
gain_fufd = data(1:11, 2);
gain_gdfd = data(12:22, 2);

data = dlmread([resultFolder '/' 'arg_fd.csv'], ',', 5, 1);
phase_fufd = data(1:11, 2);
phase_gdfd = data(12:22, 2);

data = dlmread([resultFolder '/' 'abs_gu.csv'], ',', 5, 1);
gain_fugu = data(1:11, 2);
gain_gdgu = data(12:22, 2);

data = dlmread([resultFolder '/' 'arg_gu.csv'], ',', 5, 1);
phase_fugu = data(1:11, 2);
phase_gdgu = data(12:22, 2);


% Create a rational fit of the discrete data and plot for comparison

Swirler_fufd_rat = rationalfit(freq, gain_fufd.*exp(1i*phase_fufd/180*pi),'Npoles',5);
Swirler_fufd_rat = rationalFitToSys(Swirler_fufd_rat);
h = frd(gain_fufd.*exp(1i*phase_fufd/180*pi),2*pi*freq);


% bode options for ploting
P = bodeoptions;
P.FreqUnits = 'Hz';
P.FreqScale = 'linear';
P.MagUnits = 'abs';
P.xLim = [0 freq(end)];

P.PhaseWrapping = 'off';
P.PhaseUnits = 'rad';
P.PhaseVisible = 'on';
P.PhaseMatching = 'on';
P.PhaseMatchingFreq = 0;
P.PhaseMatchingValue = 0;
%
% plot to check the fit
bode(Swirler_fufd_rat, h, 'rx', P);
title(['rational fit T_{ud}'])
legend('rational fit', 'original data')

% Same of other transmission and reflection coefficient
Swirler_fugu_rat = rationalfit(freq, gain_fugu.*exp(1i*phase_fugu/180*pi),'Npoles',5);
Swirler_fugu_rat=rationalFitToSys(Swirler_fugu_rat);
h = frd(gain_fugu.*exp(1i*phase_fugu/180*pi),2*pi*freq);
figure
bode(Swirler_fugu_rat, h, 'rx', P);
title(['rational fit R_{uu}'])
legend('rational fit', 'original data')

Swirler_gdfd_rat = rationalfit(freq, gain_gdfd.*exp(1i*phase_gdfd/180*pi),'Npoles',5);
Swirler_gdfd_rat=rationalFitToSys(Swirler_gdfd_rat);
h = frd(gain_gdfd.*exp(1i*phase_gdfd/180*pi),2*pi*freq);
figure
bode(Swirler_gdfd_rat, h, 'rx', P);
title(['rational fit R_{dd}'])
legend('rational fit', 'original data')

Swirler_gdgu_rat = rationalfit(freq, gain_gdgu.*exp(1i*phase_gdgu/180*pi),'Npoles',5);
Swirler_gdgu_rat=rationalFitToSys(Swirler_gdgu_rat);
h = frd(gain_gdgu.*exp(1i*phase_gdgu/180*pi),2*pi*freq);
figure
bode(Swirler_gdgu_rat, h, 'rx', P);
title(['rational fit T_{du}'])
legend('rational fit', 'original data')

% Convert to ss
[a,b,c,d] = tf2ss(Swirler_fufd_rat.Numerator{1}, Swirler_fufd_rat.Denominator{1});
Swirler_fufd_ss = ss(a,b,c,d);
Swirler_fufd_ss.InputName = {'f_u'};
Swirler_fufd_ss.OutputName = {'f_d'};

[a,b,c,d] = tf2ss(Swirler_fugu_rat.Numerator{1}, Swirler_fugu_rat.Denominator{1});
Swirler_fugu_ss = ss(a,b,c,d);
Swirler_fugu_ss.InputName = {'f_u'};
Swirler_fugu_ss.OutputName = {'g_u'};

[a,b,c,d] = tf2ss(Swirler_gdfd_rat.Numerator{1}, Swirler_gdfd_rat.Denominator{1});
Swirler_gdfd_ss = ss(a,b,c,d);
Swirler_gdfd_ss.InputName = {'g_d'};
Swirler_gdfd_ss.OutputName = {'f_d'};

[a,b,c,d] = tf2ss(Swirler_gdgu_rat.Numerator{1}, Swirler_gdgu_rat.Denominator{1});
Swirler_gdgu_ss = ss(a,b,c,d);
Swirler_gdgu_ss.InputName = {'g_d'};
Swirler_gdgu_ss.OutputName = {'g_u'};


% Append all state-space matrices to get one state-space system that
% covers the whole swirler
    
a_con = blkdiag(Swirler_fufd_ss.A, Swirler_fugu_ss.A, Swirler_gdfd_ss.A, Swirler_gdgu_ss.A);
b_con = [Swirler_fufd_ss.B zeros(size(b)); Swirler_fugu_ss.B zeros(size(b)); ...
        zeros(size(b)) Swirler_gdfd_ss.B; zeros(size(b)) Swirler_gdgu_ss.B];
c_con = [Swirler_fufd_ss.C zeros(size(c)) Swirler_gdfd_ss.C zeros(size(c)); ...
        zeros(size(c)) Swirler_fugu_ss.C zeros(size(c)) Swirler_gdgu_ss.C];
d_con = zeros(2);

Swirler_ss = ss(a_con, b_con, c_con, d_con);
Swirler_ss.InputName = {'f_u'; 'g_d'};
Swirler_ss.OutputName = {'f_d'; 'g_u'};

% Check the overall scattering matrix
figure
bode(Swirler_ss, P)
title('rational fit full scattering matrix ')

% Export the matrix
save('swirler.mat','Swirler_ss');

% Build the taX model
sys3 = tax('scatter3.slx',1000);
