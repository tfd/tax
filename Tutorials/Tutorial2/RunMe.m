%% Tutorial 2: loudspeaker forcing a duct %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Creating the model     %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fMax = 500;                             % Maximum resolution frequency (Hz)
sys = tax('loudspeaker.slx',fMax);      % Read the Simulink file and create model


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Postprocessing and Visualization  %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Interactive pole map
sys.plotPoleMap

% Frequency response
sys.plotLoudspeakerResponse

% Extract and plot the scattering matrix
sys.plotScatteringMatrix

% Plot the scattering matrix manually with some plotting options
omg = 2*pi*linspace(1,500,501);     % array omega (rad/s)
sc = sys.scatter('dropIO');

p = bodeoptions;
p.MagVisible    = 'off';    % hide the magnitude and keep only the phase
p.PhaseUnits    = 'rad';
p.PhaseWrapping = 'off';
p.FreqUnits     = 'Hz';
p.FreqScale     = 'linear';

bode(sc,omg,p);
