%% Tutorial 1: a simple duct %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Creating the model     %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fMax = 500;                       % Maximum resolution frequency (Hz)
sys = tax('simpleDuct.slx',fMax);      % Read the Simulink file and create model


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Postprocessing and Visualization  %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot the geometry in 3D
sys.plotGeo

% Interactive pole map
sys.plotPoleMap

% Eigenmodes (plot all the modeshapes of the eigenmodes within the frequency
% window of interest)
sys.plotEigenModes