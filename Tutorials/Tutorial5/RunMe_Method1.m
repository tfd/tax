%% Tutorial 5: Parameter study %%

% 2 ducts connected with an area change. We change the position of the area
% change but the total length is kept constant

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Method1: build a single model and update its parameters     %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fMax = 500;                                       % Maximum resolution frequency (Hz)

% Read the Simulink file and create model
% Comment/Uncomment to choose which model to run
sys = tax('parameterStudyAreaChangeReflectiveBoundary.slx',fMax);  % What do you notice unphysical with this model?
%sys = tax('parameterStudyAreaChangeOpenOpen.slx',fMax);

% Define the length vectors
lUpstream   = 0:0.025:1;
lDownstream = 1-lUpstream;    % total length constant of 1m

% Preallocate for speed. We track only 2 eigenmodes
EV = cell(1,numel(lUpstream));

% Parameter study
for iLength = 1:numel(lUpstream)
    
    % Update the model with the new parameters
    sys = changeParam(sys,'duct_upstream.l',lUpstream(iLength),'duct_downstream.l',lDownstream(iLength)); 
    
    % Compute the eigenvalues
    sysEigenvalues = eig(sys);
    
    % Exclude the spurious eigenmodes outside of the window of interest
    % real part = sigma, the growth rate, imaginary part = omega the frequency
    excludedPoints = cat(1,find(real(sysEigenvalues) > 2*pi*fMax),find(real(sysEigenvalues) < -2*pi*fMax),...
        find(imag(sysEigenvalues) <= 2*pi),find(imag(sysEigenvalues) > 2*pi*fMax));
    
    % Filter the spurious modes
    sysEigenvalues(excludedPoints) = [];
    
    EV{iLength} = sysEigenvalues.';
end

%%

% The next step is for plotting purpose only. You could avoid this
% post-processing depending on what you are aiming for.

% Preallocate for speed. We track 2 eigenmodes
Mode1 = zeros(1,numel(lUpstream));
Mode2 = zeros(1,numel(lUpstream));

for iLength = 1:numel(lUpstream)
    eigenValues = EV{iLength};
    if numel(eigenValues) ~= 2
        error('Incorrect number of modes tracked')
    end
    if iLength == 1
        [~,ix] = min(imag(eigenValues));
        Mode1(1,iLength) = eigenValues(ix);
        eigenValues(ix) = [];
        Mode2(1,iLength) = eigenValues;
    else
        [~,ix] = min(imag(eigenValues)-imag(Mode1(1,iLength-1)));
        Mode1(1,iLength) = eigenValues(ix);
        eigenValues(ix) = [];
        Mode2(1,iLength) = eigenValues;
        
    end
end

%%
% Growth rate as a function of area jump position
growthRate = figure;
hold on
for iLength = 1:numel(lUpstream)
    plot(lUpstream,real(Mode1),'kx-','MarkerSize',8,'Linewidth',1.1);
    plot(lUpstream,real(Mode2),'ro--','MarkerSize',8,'Linewidth',1.1);
end
xlabel('$x/L$ Position of the area change','interpreter','latex')
ylabel('$\sigma$ Growth rate','interpreter','latex')
legend('Fundamental','First harmonic')

%%
% Cycle increment. Another way of looking at growth rate. It is defined as the
% relative increment in amplitude per period of oscillation
% lambda = (exp(sigma(t+T))-exp(sigma*t))/exp(sigma*t) = exp(sigma T)-1 =
% exp(2pi/imag*real)-1
cycleIncrement = figure;
hold on
for iLength = 1:numel(lUpstream)
    plot(lUpstream,exp(2*pi./imag(Mode1).*real(Mode1))-1,'kx-','MarkerSize',8,'Linewidth',1.1);
    plot(lUpstream,exp(2*pi./imag(Mode2).*real(Mode2))-1,'ro--','MarkerSize',8,'Linewidth',1.1);
end
xlabel('$x/L$ Position of the area change','interpreter','latex')
ylabel('Cycle increment','interpreter','latex')
legend('Fundamental','First harmonic')
%%
% Frequency as a function of area change position
frequency = figure;
hold on
for iLength = 1:numel(lUpstream)
    plot(lUpstream,imag(Mode1)/imag(Mode1(1,1)),'kx-','MarkerSize',8,'Linewidth',1.1);
    plot(lUpstream,imag(Mode2)/imag(Mode1(1,1)),'ro--','MarkerSize',8,'Linewidth',1.1);
end
xlabel('$x/L$ Position of the area change','interpreter','latex')
ylabel('$f/f_0$ Dimensionless frequency','interpreter','latex')
legend('Fundamental','First harmonic')