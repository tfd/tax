%% Tutorial 5: Parameter study %%

% 2 ducts connected with an area change. We change the position of the area
% change but the total length is kept constant

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Method2: create new taX objects and save them all     %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fMax = 500;                                  % Maximum resolution frequency (Hz)

% Read the Simulink file and create model
% Comment/Uncomment to choose which model to run
sys = tax('parameterStudyAreaChangeReflectiveBoundary.slx',fMax);
%sys = tax('parameterStudyAreaChangeOpenOpen.slx',fMax);

% Define the length vectors
lUpstream   = 0:0.25:1;
lDownstream = 1-lUpstream;    % total length constant of 1m


%% The function parameterStudy allows you to create new taX models and store them all
% It's very useful to get a quick insight using functions like plotPoleMap

% Warning. This function creates all parameter combinations possible.
% If you change only one parameter, no problem.
% When you change multiple parameters, it creates all possible combinations.
% Here, 2x5 parameters, i.e. 25 combinations.
% For our problem, this is function is not suited and we would prefer Method1.
% However, it is perfect in the context of Monte Carlo simulations

[~,~,~,models] = parameterStudy(sys,@maxGrowthRate,'duct_upstream.l',lUpstream,'duct_downstream.l',lDownstream);

%% Plot the pole map of all the models at the same time
plotPoleMap(models{:})
