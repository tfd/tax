%% Tutorial 6: BRS-like burner %%

% Example of a complex network with feeding line, transfer functions due to velocity
% fluctuations and equivalence ratio fluctuations, entropy transfer function.


fMax = struct('f',500,'g',500,'alpha',10,'s',100); % solve until 500Hz for acoustics, 10 for swirl and 100 for entropy
sys = tax('BRS.slx',fMax);

% Plot the stability map
sys.plotPoleMap