%% Tutorial 3: Flame Transfer Function %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Case1: Discrete FFR     %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% You could have a discrete FFR (Flame Frequency Response) if you measured it
% experimentally or from LES with monofrequency excitation

Table = readtable('Flame.csv');  % load and read the table

f     = Table{:,1};   % discrete frequency, gain and phase (in rad) vector
gain  = Table{:,2};
phase = Table{:,3};


% Create a rational fit of the data
FTF = rationalfit(f,gain.*exp(1i*phase),'Npoles',5);
FTF = rationalFitToSys(FTF);
% Output is a continuous FTF model. Completele equivalence betwen TF, SS, etc.
% Conversion is straight forward
FTF = ss(FTF);


% Plot bode and compare to original data points
h = frd(gain.*exp(1i*phase),2*pi*f);


P = bodeoptions;
P.FreqUnits = 'Hz';
P.FreqScale = 'linear';
P.MagUnits = 'abs';
P.xLim = [0 500];

P.PhaseWrapping = 'off';
P.PhaseUnits = 'rad';
P.PhaseVisible = 'on';
P.PhaseMatching = 'on';
P.PhaseMatchingFreq = 0;
P.PhaseMatchingValue = 0;

bode(FTF,h,'rx',P)

save('Rationalfit.mat','FTF')
% This variable can be used in a taX model
sys1 = tax('model1.slx',500);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%    Case2: Discrete or continuous UIR    %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Usually from CFD/SI simulations. 
% LES with broadband forcing and system identification procedure
% See publications of Polifke's group

load('FTF_Arx.mat')

% plot Impulse Response
impulse(modelArx);

% Completely equivalent to TF or SS
FTF = ss(modelArx);
bode(FTF,P);

% can be used in a taX model
sys2 = tax('model2.slx',500);