function sys_ = rationalFitToSys(F_fit,OutputName,InputName)
%rationalFitToSys - converts a 
%           
% Syntax:   sys = rationalFitToSys(F_fit)
%
% Inputs:
%    F_fit - output of matlab function rationalfit
%
% Outputs:
%    sys - fitted model as tf object
% 
% Test function: The function can be validated by the following code
% 
%    sys = rss(4,3,2);
%    [H,wout] = freqresp(sys);
%    h = rationalfit(wout/2/pi,H);
%    sysFited = rationalFitToSys(h,sys.OutputName,sys.InputName);
%    bode(sys,sysFited)
% 
% Other m-files required: 
% Subfunctions: none
%
% See also: 
sys = {};
for iOut = 1:size(F_fit,1)
    for iIn = 1:size(F_fit,2)
        sys{iOut,iIn} = rationalFitToSys_SISO(F_fit(iOut,iIn));
    end
end
sys_ = [sys{1,:}];
for i = 2:size(sys,1)
    sys_ = [sys_;[sys{i,:}]];
end
if nargin == 3
   sys_.OutputName = OutputName;
   sys_.InputName = InputName;
end
end
function sys = rationalFitToSys_SISO(F_fit)
    syms s t;
    
    F_rat = F_fit.D;
    k = 1;
    while k <=length(F_fit.A)
        if isreal(F_fit.A(k))
            F_rat = F_rat + F_fit.C(k)/(s-F_fit.A(k));
            k = k+1;
        else
            ai = imag(F_fit.A(k));
            ar = real(F_fit.A(k));
            ci = imag(F_fit.C(k));
            cr = real(F_fit.C(k));
            F_rat = F_rat + -(2*ai*ci + 2*ar*cr - 2*cr*s)/(ai^2 + ar^2 - 2*ar*s + s^2);
            k = k+2;
        end
    end
    
    s = tf('s');
    sys = eval(char((vpa(F_rat))));
end