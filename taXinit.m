function taXinit()
% Adds the folders of tax functions to the path and checks for the
% existance of the sparse state space (sss) class toolbox and Sitools
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% taXinit();
% ------------------------------------------------------------------
% Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  26 Nov 2018
% ------------------------------------------------------------------
% See also: sss,tax

taXPath = fileparts(mfilename('fullpath'));
toolPath = fileparts(taXPath);

pathCell = regexp(path, pathsep, 'split');
onPath = ...
    any(strcmp(taXPath, pathCell)) && ...
    any(strcmp(fullfile(taXPath,'sss','src'), pathCell)) && ...
    any(strcmp(fullfile(taXPath,'TFDtools'), pathCell))  && ...
    any(strcmp(fullfile(taXPath,'Callbacks'), pathCell)) && ...
    any(strcmp(fullfile(taXPath,'elements'), pathCell))  && ...
    any(strcmp(fullfile(taXPath,'elements','Legacy_Elements'), pathCell));
% add the taX directory with required subdirectories to Matlab path
if not(onPath) || not(exist('sss','class'))
    addpath(taXPath);
    addpath(genpath(fullfile(taXPath,'sss','src')));
    addpath(genpath(fullfile(toolPath,'TFDtools')));
    addpath(fullfile(taXPath,'Callbacks'));
    addpath(fullfile(taXPath,'elements'));
    addpath(genpath(fullfile(taXPath,'elements','Legacy_Elements')));
end

if not(exist('sss','class'))
    error('Please install sss class and add it to the matlab path.')
end