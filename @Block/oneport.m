function sys = oneport(varargin)
% ONEPORT function to adjust names of inputs and outputs of 1D plane wave
% boundary conditions and set up input and output groups.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% sys = oneport(sys);
% Input:        * sys: AcBlock boundary model
% Output:       * sys: AcBlock boundary model with IO names and groups
% ------------------------------------------------------------------
% Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  24 Apr 2018
% ------------------------------------------------------------------
% See also: tax/twoport, tax/multiport

sys = varargin{1};
idxAcousticCon = 1;
if nargin==2
    idxAcousticCon= varargin{2};
end

%% Acoustic oneport element naming scheme
fields = {};
if isprop(sys,'activeProp')
    for wavec = sys.waves'
        wave = char(wavec);
        if isfield(sys.activeProp,wave) && sys.activeProp.(wave)
            fields = [fields;{wave}]; %#ok<AGROW>
        end
    end
else
    fields = sys.waves;
end

OutPort = sys.Connection{idxAcousticCon}.dir*sys.Connection{idxAcousticCon}.Mach>=0; % convection only in one direction (except g)
% only for ylength (copied from any BC, except chokedExit)
    outFlow = sys.Connection{1}.Mach<0 || (sys.Connection{1}.Mach==0 && sys.Connection{1}.dir<0); % is meanflow leaving the system here? No dir!
    nin = 1 + outFlow*(numel(sys.waves)-2);
    nout = numel(sys.waves);
ylength = nout - nin;
if(sys.Connection{1}.dir<0)
    %% Flow is directed into the element
    if (sys.Connection{idxAcousticCon(1)}.idx==0)
        % Upstream unconnected end (Scattering sys)
        sys.u{1} = ['f',sys.Name];
        sys.y{1} = ['g',sys.Name];
        sys.y{1+ylength} = ['f',sys.Name,'_uy'];
        for i = 3:numel(fields)
            if OutPort
                sys.u{i-1} = [fields{i},sys.Name];
                sys.y{i-1+ylength} = [fields{i},sys.Name,'_uy'];
                sys.InputGroup.(fields{i}) = i-1;
                sys.OutputGroup.(fields{i}) = i-1+ylength;
            else
                sys.y{i-1} = [fields{i},sys.Name];
                sys.OutputGroup.(fields{i}) = i-1;
            end
        end
    else
        sys.u{1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'f'];
        sys.y{1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'g'];
        sys.y{1+ylength} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'f_uy'];
        for i = 3:numel(fields)
            if OutPort
                sys.u{i-1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i}];
                sys.y{i-1+ylength} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i},'_uy'];
                sys.InputGroup.(fields{i}) = i-1;
                sys.OutputGroup.(fields{i}) = i-1+ylength;
            else
                sys.y{i-1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i}];
                sys.OutputGroup.(fields{i}) = i-1;
            end
        end
    end
    sys.InputGroup.f = 1;
    sys.OutputGroup.g = 1;
    sys.OutputGroup.f = 1+ylength;
else
    %% Flow is directed out of the element
    if (sys.Connection{idxAcousticCon(1)}.idx==0)
        % Downstream unconnected end (Scattering sys)
        sys.u{1} = ['g',sys.Name];
        sys.y{1} = ['f',sys.Name];
        sys.y{1+ylength} = ['g',sys.Name,'_uy'];
        for i = 3:numel(fields)
            if OutPort
                sys.y{i-1} = [fields{i},sys.Name];
                sys.OutputGroup.(fields{i}) = i-1;
            else
                sys.u{i-1} = [fields{i},sys.Name];
                sys.y{i-1+ylength} = [fields{i},sys.Name,'_uy'];
                sys.InputGroup.(fields{i}) = i-1;
                sys.OutputGroup.(fields{i}) = i-1+ylength;
            end
        end
    else
        sys.u{1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'g'];
        sys.y{1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'f'];
        sys.y{1+ylength} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'g_uy'];
        for i = 3:numel(fields)
            if OutPort
                sys.y{i-1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i}];
                sys.OutputGroup.(fields{i}) = i-1;
            else
                sys.u{i-1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i}];
                sys.y{i-1+ylength} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i},'_uy'];
                sys.InputGroup.(fields{i}) = i-1;
                sys.OutputGroup.(fields{i}) = i-1+ylength;
            end
        end
    end
    sys.InputGroup.g = 1;
    sys.OutputGroup.f = 1;
    sys.OutputGroup.g = 1+ylength;
end

sys.StateGroup.(sys.Name) = 1:sys.n;