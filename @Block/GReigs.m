function stable = GReigs(sys,iniEV)
% maxGReigs function determines the eigenvalue with the maximum growth rate
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% stable = stability(sys);
% Input:        * sys: tax object
% Output:       * stable: maximum growth rate. 
%                         Continuous time: maximum real part;
%                         Discrete time: maximum absolute value.
% ------------------------------------------------------------------
% Authors:      Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  25 Sep 2018
% ------------------------------------------------------------------

warning off
try
    EV = sys.eigs(1,'lr');
    stable = EV;
    if isnan(EV)
        error('dummy');
    end
catch
    try
        lm = abs(sys.eigs(1,'lm'));
        fMax = sys.fMax.f;
        EV0 = sys.eigs(1,lm + 1e-6*fMax*2*pi*1i);
        EV1 = sys.eigs(1,lm + fMax*2*pi*1i);
        while abs(EV1-EV0)>eps*abs(lm)*1e3
            EV0 = sys.eigs(1,lm + imag(EV0)*1i);
            EV1 = sys.eigs(1,lm + min(fMax*2*pi,imag(EV1))*1i);
        end
        if isnan(EV0) || isnan(EV1)
            error('dummy');
        else
            stable = EV0;
        end
    catch
        EV = sys.eig;
        stable = EV(imag(EV)>=0 & real(EV)==max(real(EV)));
    end
end
warning on

