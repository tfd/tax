function sys = multiport(varargin)
% MULTIPORT function to adjust names of inputs and outputs of 1D plane wave
% blocks with multiple ports and set up input and output groups.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% sys = multiport(sys);
% Input:        * sys: AcBlock multiport model
% Output:       * sys: AcBlock multiport model with IO names and groups
% ------------------------------------------------------------------
% Authors:      Thomas Emmert (emmert@tfd.mw.tum.de), Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  24 Apr 2018
% ------------------------------------------------------------------
% See also: tax/oneport, tax/twoport

sys = varargin{1};
idxAcousticCon = 1:length(sys.Connection);
dirAcousticCon = zeros(1,length(sys.Connection));
if nargin==2
    idxAcousticCon= varargin{2};
end

fields = {};
if isprop(sys,'activeProp')
    for wavec = sys.waves'
        wave = char(wavec);
        if sys.activeProp.(wave)
            fields = [fields;{wave}]; %#ok<AGROW>
        end
    end
else
    fields = reshape(sys.waves,numel(sys.waves),1);
end

%% Acoustic multiport element naming scheme
% initialize
ucount = length(idxAcousticCon)+1;
ycount = length(idxAcousticCon)+1;
for wavec = fields'
    wave = char(wavec);
    sys.InputGroup.(wave) = [];
    sys.OutputGroup.(wave) = [];
end
OutPort = cellfun(@(x) x.Mach>=0, sys.Connection(idxAcousticCon)); % convection only in one direction (except g)
ylength = numel(idxAcousticCon) + (numel(fields)-2)*sum(OutPort);
% run
for i = 1:length(idxAcousticCon)
    dirAcousticCon(i) = sys.Connection{idxAcousticCon(i)}.dir;
%     OutPort = sys.Connection{idxAcousticCon(i)}.Mach>=0; % convection only in one direction (except g)
    if sys.Connection{idxAcousticCon(i)}.idx==0
        if dirAcousticCon(i)==-1
            % Upstream unconnected end (Scattering sys)
            sys.u{i} = ['f',sys.Name];
            sys.InputGroup.f = [sys.InputGroup.f,i];
            sys.y{i} = ['g',sys.Name];
            sys.OutputGroup.g = [sys.OutputGroup.g,i];
            sys.y{i+ylength} = ['f',sys.Name,'_uy'];
            sys.OutputGroup.f = [sys.OutputGroup.f,i+ylength];
        else
            % Downstream unconnected end (Scattering sys)
            sys.u{i} = ['g',sys.Name];
            sys.InputGroup.g = [sys.InputGroup.g,i];
            sys.y{i} = ['f',sys.Name];
            sys.OutputGroup.f = [sys.OutputGroup.f,i];
            sys.y{i+ylength} = ['g',sys.Name,'_uy'];
            sys.OutputGroup.g = [sys.OutputGroup.g,i+ylength];
        end
        for ii = 3:numel(fields)
            if OutPort(i)
                sys.y{ycount} = [fields{ii},sys.Name];
                sys.OutputGroup.(fields{ii}) = [sys.OutputGroup.(fields{ii}),ycount];
                ycount = ycount+1;
            else
                sys.u{ucount} = [fields{ii},sys.Name];
                sys.InputGroup.(fields{ii}) = [sys.InputGroup.(fields{ii}),ucount];
                sys.y{ucount+ylength} = [fields{ii},sys.Name,'_uy'];
                sys.OutputGroup.(fields{ii}) = [sys.OutputGroup.(fields{ii}),ucount+ylength];
                ucount = ucount+1;
            end
        end
    else % connected ends...
        if dirAcousticCon(i)==-1 % upstream
            sys.u{i} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),'f'];
            sys.InputGroup.f = [sys.InputGroup.f,i];
            sys.y{i} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),'g'];
            sys.OutputGroup.g = [sys.OutputGroup.g,i];
            sys.y{i+ylength} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),'f_uy'];
            sys.OutputGroup.f = [sys.OutputGroup.f,i+ylength];
        else
            sys.u{i} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),'g'];
            sys.InputGroup.g = [sys.InputGroup.g,i];
            sys.y{i} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),'f'];
            sys.OutputGroup.f = [sys.OutputGroup.f,i];
            sys.y{i+ylength} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),'g_uy'];
            sys.OutputGroup.g = [sys.OutputGroup.g,i+ylength];
        end
        for ii = 3:numel(fields)
            if OutPort(i)
                sys.y{ycount} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),fields{ii}];
                sys.OutputGroup.(fields{ii}) = [sys.OutputGroup.(fields{ii}),ycount];
                ycount = ycount+1;
            else
                sys.u{ucount} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),fields{ii}];
                sys.InputGroup.(fields{ii}) = [sys.InputGroup.(fields{ii}),ucount];
                sys.y{ucount+ylength} = [num2str(sys.Connection{idxAcousticCon(i)}.idx,'%02d'),fields{ii},'_uy'];
                sys.OutputGroup.(fields{ii}) = [sys.OutputGroup.(fields{ii}),ucount+ylength];
                ucount = ucount+1;
            end
        end
    end
end

% sys.InputGroup.f = find(dirAcousticCon == -1);
% sys.OutputGroup.f = [find(dirAcousticCon == 1),sys.InputGroup.f+ylength];
% sys.InputGroup.g = find(dirAcousticCon == 1);
% sys.OutputGroup.g = [find(dirAcousticCon == -1),sys.InputGroup.g+ylength];
sys.StateGroup.(sys.Name) = 1:sys.n;