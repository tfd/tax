function stable = maxGReig(sys)
% maxGReig function determines the eigenvalue with the maximum growth rate
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% stable = stability(sys);
% Input:        * sys: tax object
% Output:       * stable: maximum growth rate. 
%                         Continuous time: maximum real part;
%                         Discrete time: maximum absolute value.
% ------------------------------------------------------------------
% Authors:      Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  25 Sep 2018
% ------------------------------------------------------------------

EV = sys.eig;
stable = EV(imag(EV)>=0 & real(EV)==max(real(EV)));

