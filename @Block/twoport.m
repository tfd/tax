function sys = twoport(varargin)
% TWOPORT function to adjust names of inputs and outputs of 1D plane wave
% blocks with two ports and set up input and output groups.
% ------------------------------------------------------------------
% This file is part of tax, a code designed to investigate thermoacoustic
% network systems. It is developed by:
% Professur fuer Thermofluiddynamik, Technische Universitaet Muenchen.
% For updates and further information please visit www.tfd.mw.tum.de
% ------------------------------------------------------------------
% sys = twoport(sys);
% Input:        * sys: AcBlock twoport model
% Output:       * sys: AcBlock twoport model with IO names and groups
% ------------------------------------------------------------------
% Authors:      Thomas Emmert, Felix Schily (schily@tfd.mw.tum.de)
% Last Change:  24 Apr 2018
% ------------------------------------------------------------------
% See also: tax/oneport

sys = varargin{1};
idxAcousticCon = [1,2];
if nargin==2
    idxAcousticCon= varargin{2};
end

%% Acoustic twoport element naming scheme
fields = {};
if isprop(sys,'activeProp')
    for wavec = sys.waves'
        wave = char(wavec);
        if isfield(sys.activeProp,wave) && sys.activeProp.(wave)
            fields = [fields;{wave}]; %#ok<AGROW>
        end
    end
else
    fields = sys.waves;
end

OutPort = sys.Connection{idxAcousticCon(1)}.dir*sys.Connection{idxAcousticCon(1)}.Mach>=0; % convection only in one direction (except g)
if (sys.Connection{idxAcousticCon(1)}.idx==0)
    % Upstream unconnected end (Scattering sys)
    sys.u{1} = ['f',sys.Name];
    sys.y{1} = ['g',sys.Name];
    sys.y{1+numel(fields)} = ['f',sys.Name,'_uy'];
    for i = 3:numel(fields)
        if OutPort
            sys.u{i} = [fields{i},sys.Name];
            sys.y{i+numel(fields)} = [fields{i},sys.Name,'_uy'];
        else
            sys.y{i} = [fields{i},sys.Name];
        end
    end
else
    sys.u{1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'f'];
    sys.y{1} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'g'];
    sys.y{1+numel(fields)} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),'f_uy'];
    for i = 3:numel(fields)
        if OutPort
            sys.u{i} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i}];
            sys.y{i+numel(fields)} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i},'_uy'];
        else
            sys.y{i} = [num2str(sys.Connection{idxAcousticCon(1)}.idx,'%02d'),fields{i}];
        end
    end
end

if (sys.Connection{idxAcousticCon(2)}.idx==0)
    % Downstream unconnected end (Scattering sys)
    sys.u{2} = ['g',sys.Name];
    sys.y{2} = ['f',sys.Name];
    sys.y{2+numel(fields)} = ['g',sys.Name,'_uy'];
    for i = 3:numel(fields)
        if OutPort
            sys.y{i} = [fields{i},sys.Name];
        else
            sys.u{i} = [fields{i},sys.Name];
            sys.y{i+numel(fields)} = [fields{i},sys.Name,'_uy'];
        end
    end
else
    sys.u{2} = [num2str(sys.Connection{idxAcousticCon(2)}.idx,'%02d'),'g'];
    sys.y{2} = [num2str(sys.Connection{idxAcousticCon(2)}.idx,'%02d'),'f'];
    sys.y{2+numel(fields)} = [num2str(sys.Connection{idxAcousticCon(2)}.idx,'%02d'),'g_uy'];
    for i = 3:numel(fields)
        if OutPort
            sys.y{i} = [num2str(sys.Connection{idxAcousticCon(2)}.idx,'%02d'),fields{i}];
        else
            sys.u{i} = [num2str(sys.Connection{idxAcousticCon(2)}.idx,'%02d'),fields{i}];
            sys.y{i+numel(fields)} = [num2str(sys.Connection{idxAcousticCon(2)}.idx,'%02d'),fields{i},'_uy'];
        end
    end
end

sys.InputGroup.f = 1;
sys.OutputGroup.f = [1+numel(fields),2];
sys.InputGroup.g = 2;
sys.OutputGroup.g = [1,2+numel(fields)];
for i = 3:numel(fields)
    sys.InputGroup.(fields{i}) = i;
    if OutPort
        sys.OutputGroup.(fields{i}) = [i+numel(fields),i];
    else
        sys.OutputGroup.(fields{i}) = [i,i+numel(fields)];
    end
end
sys.StateGroup.(sys.Name) = 1:sys.n;
