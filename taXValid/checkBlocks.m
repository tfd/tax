% checkBlocks
% init: get list
clear
load savePath.mat % outsource big files (such that they are exempt from sync)
% libName = 'taXlibrary_Matlab2017b';
libName = 'taXlibrary';
while ~isempty(gcs)
    if strcmp(gcs,libName)
        close_system(gcs,0)
    else
        close_system
    end
end
currentPath = pwd;
% cd('/home/felix/Dokumente/tax');taXinit;
cd('../');taXinit;
% taXlibrary_Matlab2017b;
taXlibrary;
allBlocks = find_system;
cd(currentPath);
% check all
for i = 2:numel(allBlocks)
% for i = [22,23]
    % load
    pathparts = strsplit(allBlocks{i},filesep);
    name = pathparts{2};
    load([savePath,'SR',name,'.mat']); % BC, pars, Ts, y, sys
    % loops
    if ~isempty(y{1})
        for ii = 1:numel(BC)
%             for iii = 3
            for iii = 1:numel(pars)
                if strcmp(name,'symFlame') || strcmp(name,'symTempJump') || strcmp(name,'BurnerFlame') % exception to explore error vs Mach order
                    for iv = 1:numel(y{ii,iii,1})
                        if isempty(y{ii,iii,1}{iv}) % only check the first element
                            ok{i}{ii,iii,iv} = 'empty y'; %#ok<SAGROW>
                        elseif isa(sys{ii,iii,1},'sss') && isempty(sys{ii,iii,1}.x0) && ~ischar(y{ii,iii,1}{1})
                            % check steady state value
                            auxOk = eqn(name,BC(ii),newpars{ii,iii,1},Ts{ii,iii,1},sys{ii,iii,1},y{ii,iii,1}{iv}(:,end),(1:numel(y{ii,iii,1})==iv)');
                            if numel(auxOk)>1 % this means the errors are given instead of a result
                                errors(1,:) = auxOk;
                                for v = 2:numel(MaFactor)
                                    errors(v,:) = eqn(name,BC(ii),newpars{ii,iii,v},Ts{ii,iii,v},sys{ii,iii,v},y{ii,iii,v}{iv}(:,end),(1:numel(y{ii,iii,v})==iv)'); 
                                end
                                if strncmp(sys{ii,iii,1}.Version,'constant cp, 1st',16) ...
                                        || strncmp(sys{ii,iii,1}.Version,'realistic fluid, 1st',20) ...
                                        || strcmp(sys{ii,iii,1}.Version,'constant cp, classic Rankine-Hugoniot, f and g only')
                                    normalizedErrors = errors./repmat(MaFactor.',1,3);
                                    MachOrder = 1;
                                elseif strncmp(sys{ii,iii,1}.Version,'realistic fluid, 3rd',20)
                                    normalizedErrors = errors./repmat(MaFactor.^3.',1,3);
                                    MachOrder = 3;
                                end
                                ok{i}{ii,iii,iv} = ...
                                    normalizedErrors(1,1)>=max(normalizedErrors(:,1))-100*eps ... % some safety margin with eps
                                    && normalizedErrors(1,2)>=max(normalizedErrors(:,2))-100*eps ...
                                    && normalizedErrors(1,3)>=max(normalizedErrors(:,3))-100*eps; %#ok<SAGROW>
                                if ~ok{i}{ii,iii,iv}
                                    % find true order
                                    for v = 1:3
                                        trueMachOrder(v) = fminsearch(@(trueOrder) sum(abs(errors(:,v)./MaFactor.^trueOrder.'/(errors(1,v)+100*eps)-1)), MachOrder); %#ok<SAGROW>
                                    end
                                    if any(trueMachOrder<MachOrder-.1)
                                        % try again, omit the first data
                                        % point, in case there is
                                        % accidentally a good match in the
                                        % first data point.
                                        errors2 = errors(2:end,:);
                                        MaFactor2 = MaFactor(2:end)/MaFactor(2);
                                        for v = 1:3
                                            trueMachOrder(v) = fminsearch(@(trueOrder) sum(abs(errors2(:,v)./MaFactor2.^trueOrder.'/(errors2(1,v)+100*eps)-1)), MachOrder);
                                        end
                                    end
                                    if any(trueMachOrder<MachOrder-.1)
                                        disp([num2str(trueMachOrder(1),'%.4f'),'    ',num2str(trueMachOrder(2),'%.4f'),'    ',num2str(trueMachOrder(3),'%.4f'),' :   ',sys{ii,iii,1}.Version,', Mach(1) = ',num2str(-sys{ii,iii,1}.Connection{1}.Mach),', T_ratio = ',num2str(sys{ii,iii,1}.Tratio),', input: ',sys{ii,iii,1}.u{iv}]);
                                    else
                                        ok{i}{ii,iii,iv} = true; %#ok<SAGROW> % allow a tolerance of .1 in MachOrder
                                    end
                                end
                            else
                                ok{i}{ii,iii,iv} = auxOk; %#ok<SAGROW>
                                if ~auxOk
                                    % either there is a not ok in the
                                    % accurate representation, or the
                                    % steady state values of an approximate
                                    % version do not fit.
                                    disp(['not ok:   ',sys{ii,iii,1}.Version,', Mach(1) = ',num2str(-sys{ii,iii,1}.Connection{1}.Mach),', T_ratio = ',num2str(sys{ii,iii,1}.Tratio),', input: ',sys{ii,iii,1}.u{iv}]);
                                end
                            end
                        else
                            % check transient behavior or BC error
                            u = zeros(numel(y{ii,iii}),size(y{ii,iii}{iv},2));
                            u(iv,2:end) = ones;
                            ok{i}{ii,iii,iv} = eqn(name,BC(ii),newpars{ii,iii},Ts{ii,iii},sys{ii,iii},y{ii,iii}{iv},u); %#ok<SAGROW>
                            if ischar(y{ii,iii}{1}) && strcmp(y{ii,iii}{1},'BCerror')
                                break;
                            end
                        end
                    end
                else % regular
                    for iv = 1:numel(y{ii,iii})
                        if isempty(y{ii,iii}{iv})
                            ok{i}{ii,iii,iv} = 'empty y';
                        elseif isa(sys{ii,iii},'sss') && isempty(sys{ii,iii}.x0) && ~ischar(y{ii,iii}{1})
                            % check steady state value
                            ok{i}{ii,iii,iv} = eqn(name,BC(ii),newpars{ii,iii},Ts{ii,iii},sys{ii,iii},y{ii,iii}{iv}(:,end,:),(1:numel(y{ii,iii})==iv)');
                        else
                            % check transient behavior or BC error
                            u = zeros(numel(y{ii,iii}),size(y{ii,iii}{iv},2));
                            u(iv,2:end) = ones;
                            ok{i}{ii,iii,iv} = eqn(name,BC(ii),newpars{ii,iii},Ts{ii,iii},sys{ii,iii},y{ii,iii}{iv},u);
                            if ischar(y{ii,iii}{1}) && strcmp(y{ii,iii}{1},'BCerror')
                                break;
                            end
                        end
                    end
                end
            end
        end
    else
        ok{i}=[];
    end
    % clear RAM
    clear Ts y BC pars sys
end
% close library
while ~isempty(gcs)
    if strcmp(gcs,libName)
        close_system(gcs,0)
    else
        close_system
    end
end
% add markers in allBlocks
for i = 1:size(ok,2)
    if isempty(ok{i})
        allBlocks{i,2} = 'empty';
    else
        allBlocks{i,2} = true;
    end
    for ii = 1:size(ok{i},1)
        for iii = 1:size(ok{i},2)
            for iv = 1:size(ok{i},3)
                if ~isempty(ok{i}{ii,iii,iv}) && strcmp(ok{i}{ii,iii,iv},'empty y')
                    allBlocks{i,2} = 'no y';
                elseif ~isempty(ok{i}{ii,iii,iv}) && any(ok{i}{ii,iii,iv}~=1)
                    allBlocks{i,2} = false;
                    break
                end
            end
            if ~allBlocks{i,2}
                break
            end
        end
        if ~allBlocks{i,2}
            break
        end
    end
end
% save result
save('okFiles.mat','ok','allBlocks');
