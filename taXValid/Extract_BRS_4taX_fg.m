clear;
clc;
close all;


modelName = 'BRS_v44_export_LNSE_665020';

MPHfolder = '/media/mmeindl/SSD1/BRS/export/';
sssfolder = '/media/mmeindl/SSD1/BRS/';


loadModel = [MPHfolder modelName '.mph'];%'/media/mmeindl/SSD1/BRS/export/BRS_v44_export_479520.mph';
%saveModel = '/media/mmeindl/SSD1/BRS/BRS_v44_nmesh_EV';


inputs = {'fin'; 'gout'; 'dQin'};
outputs = {'comp1.bnd17'; 'comp1.bnd18'; 'comp1.var7';};
solver = {'sol2'};


[sys, Null] = comsol2sss(loadModel, inputs, outputs, solver);
fprintf('States Combustor: %i\n\n', length(sys.A));

sys.u = {'fin'; 'gout'; 'dQin'};
sys.y = {'gin'; 'fout'; 'uref'};

sys.StateGroup.Comsol = 1:sys.n;

% check if all in- and outputs are present:
sum(sys.B,1)
sum(sys.C,2)

sysComsol = sys;

FTFstr = 'cold';
save([modelName '_' FTFstr '_sss.mat' ], 'sys', 'Null');

%% create and connect the FTF

FTFstr = 'hot';
% FTF

load('FTF_ss.mat');
FTFss.u = sysComsol.y(3);
FTFss.y = sysComsol.u(3);

FTFss.StateGroup.FTF = 1:FTFss.n;

% create the scaling factor to come up for the reduced heat release in
% Comsol

% % mean heat release from OpenFOAM LES
% QmeanOF = 33262;
% 
% % mean heat release from Comsol model
% QmeanCOMSOL = 28667.13;
% 
% FTF_factor = sss(QmeanOF/QmeanCOMSOL);
% FTF_factor.u = FTFss.y;
% FTF_factor.y = sysComsol.u(3);
% FTFfull = connect(FTFss, FTF_factor, sysComsol.y(3), sysComsol.u(3));
sys = connect(sysComsol, FTFss, {'fin'; 'gout'}, {'gin'; 'fout'});

save([modelName '_' FTFstr '_sss.mat' ], 'sys', 'Null');
%% build the taX element

FTFstr = 'cold';

SSSname = 'sys';
%modelpath = strcat('/media/mmeindl/SSD1/BRS/', 'BRS4taX_', FTFstr,'_',  num2str(length(sys.StateGroup.Comsol)), '.mat');
modelpath = [sssfolder modelName '_' FTFstr '_sss.mat'];
importSys = load(modelpath, SSSname);
importSys = importSys.(SSSname);

% dir = 1  => upstream port

% Inlet Swirler
con{1}.rho = []; % kg/m^3
con{1}.Mach = []; %m/s
con{1}.c = []; % m/s
con{1}.A = [];% m^2
con{1}.idx = {};
con{1}.dir = 1;

% Outlet Chamber
con{2}.rho = 0.18411; % kg/m^3
con{2}.Mach = 0.0104; %m/s
con{2}.c = 849.54; % m/s
con{2}.A = (90/1000)^2;% m^2
con{2}.idx = {};
con{2}.dir = -1;


importSys.Name = SSSname;
sys = Element(importSys,con);

% port 1-12 are plenum, port 13-24  are chamber
sys.InputGroup.port1 = 1;
sys.InputGroup.port2 = 2;

sys.OutputGroup.port1 = 1;
sys.OutputGroup.port2 = 2;



%save([pwd,'/',SSSname,'_',num2str(length(sys.StateGroup.Comsol)),'_',FTFstr,'_Element'],'sys');
save([sssfolder '/' modelName '_' FTFstr '_Element.mat'],'sys');
fprintf('Element saved as %s\n', [modelpath '/' modelName '_' FTFstr '_Element.mat']);
%% build the taX element

FTFstr = 'hot';

SSSname = 'sys';
%modelpath = strcat('/media/mmeindl/SSD1/BRS/', 'BRS4taX_', FTFstr,'_',  num2str(length(sys.StateGroup.Comsol)), '.mat');
modelpath = [sssfolder modelName '_' FTFstr '_sss.mat'];
importSys = load(modelpath, SSSname);
importSys = importSys.(SSSname);

% dir = 1  => upstream port

% Inlet Swirler
%     con{1}.rho = 1.1602; % kg/m^3
%     con{1}.Mach = -0.03196; %m/s
%     con{1}.c = 349.94; % m/s
%     con{1}.A = pi/4*((40/1000)^2-(16/1000)^2);% m^2
%     con{1}.idx = {};
%     con{1}.dir = 1;

con{1}.rho = []; % kg/m^3
con{1}.Mach = []; %m/s
con{1}.c = []; % m/s
con{1}.A = [];% m^2
con{1}.idx = {};
con{1}.dir = 1;

% Outlet Chamber
con{2}.rho = 0.18411; % kg/m^3
con{2}.Mach = 0.0104; % m/s
con{2}.c = 849.54; % m/s
con{2}.A = (90/1000)^2;% m^2
con{2}.idx = {};
con{2}.dir = -1;


importSys.Name = SSSname;
sys = Element(importSys,con);

% port 1-12 are plenum, port 13-24  are chamber
sys.InputGroup.port1 = 1;
sys.InputGroup.port2 = 2;

sys.OutputGroup.port1 = 1;
sys.OutputGroup.port2 = 2;



save([sssfolder '/' modelName '_' FTFstr '_Element.mat'],'sys');
fprintf('Element saved as %s\n', [modelpath '/' modelName '_' FTFstr '_Element.mat']);