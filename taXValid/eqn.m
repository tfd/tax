% checker for block behavior
function ok = eqn(name,BC,pars,Ts,sys,yOut,uIn)
    % list of elements that may correctly run into BCerror
    BCerrorAllowed = {...
        'Block';...
        'BurnerFlame';...
        'Flame';...
        'subsonicNozzle';...
        'symFlame';...
        'symTempJump';...
        'triJunction_12';...
        'triJunction_21';...
        };
    if ~ischar(yOut)
        % init
        back = true; % Euler backwards integration?
        bt = (back == true); % short
        % make some variables
%         for wavec = sys.waves'
%             wave = char(wavec);
%             % add empty groups
%             if ~isfield(sys.InputGroup,wave)     sys.InputGroup.(wave) = [];  end %#ok<SEPEX>
%             if ~isfield(sys.OutputGroup,wave)    sys.OutputGroup.(wave) = []; end %#ok<SEPEX>
%             % sort waves, negative indices for uIn
%             aux.(wave) = sortrows([[sys.u(sys.InputGroup.(wave));sys.y(sys.OutputGroup.(wave))],mat2cell([-sys.InputGroup.(wave)';sys.OutputGroup.(wave)'],ones(1,numel(sys.InputGroup.(wave))+numel(sys.OutputGroup.(wave))))],1);
%             aux.(wave)(:,1) = cellfun(@(x) x([1:2,4:end]),aux.(wave)(:,1),'UniformOutput',false);
%         end
%         Index = [cell2mat(aux.f(ismember(aux.f(:,1),aux.g(:,1)),2)),cell2mat(aux.g(ismember(aux.g(:,1),aux.f(:,1)),2))];
%         for i = 3:numel(sys.waves)
%             wave = sys.waves{i};
%             if ~isempty(aux.(wave))
%                 Index(1:size(aux.(wave),1),i) = cell2mat(aux.(wave)(:,2));
%             else
%                 Index(1,i) = 0;
%             end
%         end
%         if ~isempty(Index)
%             f = zeros(size(Index,1),size(yOut,2));
%             g = zeros(size(Index,1),size(yOut,2));
%             f(Index(:,1)<0,:) = uIn(-Index(Index(:,1)<0,1),:);
%             f(Index(:,1)>0,:) = yOut(Index(Index(:,1)>0,1),:);
%             g(Index(:,2)<0,:) = uIn(-Index(Index(:,2)<0,2),:);
%             g(Index(:,2)>0,:) = yOut(Index(Index(:,2)>0,2),:);
%             u = f - g;
%             prc = f + g; % p/(rho c)
%             for i = 3:numel(sys.waves)
%                 convective.(sys.waves{i})(Index(:,i)<0,:) = double(uIn(-Index(Index(:,i)<0,i),:)); % cast double!
%                 convective.(sys.waves{i})(Index(:,i)>0,:) = double(yOut(Index(Index(:,i)>0,i),:));
%             end
%         end
        f = yOut(sys.OutputGroup.f,:);
        g = yOut(sys.OutputGroup.g,:);
        u = f - g;
        prc = f + g; % p/(rho c)
        for i = 3:numel(sys.waves)
            convective.(sys.waves{i}) = double(yOut(sys.OutputGroup.(sys.waves{i}),:)); % cast double!
        end
%         Ma = [sys.Connection{1}.Mach*sys.Connection{1}.dir; sys.state.f.Mach'*sys.state.dir];
%         c = [sys.Connection{1}.c; sys.state.f.c'];
%         rho = [sys.Connection{1}.rho; sys.state.f.rho'];
%         A = [sys.Connection{1}.A; sys.state.f.A'];
%         kappa = [sys.Connection{1}.kappa; sys.state.f.kappa'];
%         Mmol = [sys.Connection{1}.Mmol; sys.state.f.Mmol'];
%         alpha = [sys.Connection{1}.alpha; sys.state.f.alpha'];
        Rmol = 8.3144598*1e3; % J/(kmol*K)
        Ma = sys.state.f.Mach.';
        c = sys.state.f.c.';
        rho = sys.state.f.rho.';
        A = sys.state.f.A.';
        kappa = sys.state.f.kappa.';
        Mmol = sys.state.f.Mmol.';
        alpha = sys.state.f.alpha.';
        acc = 1e4*eps*max(abs([reshape(sys.A,1,numel(sys.A)),reshape(sys.B,1,numel(sys.B)),reshape(sys.C,1,numel(sys.C)),reshape(sys.D,1,numel(sys.D)),reshape(sys.E,1,numel(sys.E))]));
        BCbase = struct('Mach',BC.Mach(1),'rho',BC.rho,'c',BC.c,'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        if strcmp(name,'subsonicNozzle')
            auxBC = repmat(BCbase,1,2);
        end
        if strcmp(name,'symAreaChange') % symmetric lZeta
            auxBC = repmat(BCbase,1,2);
            auxBC(~ismember(1:2,BC.port(1))).A = BC.A*sys.Aratio^(1-2*(BC.port(1)-1)); % geometry
            auxBC(~ismember(1:2,BC.port(1))).Mach = BC.Mach(1)/sys.Aratio^(1-2*(BC.port(1)-1)); % steady continuity
        end
        if strcmp(name,'symFlame') || strcmp(name,'symTempJump') || strcmp(name,'BurnerFlame') % symmetric rankineHugoniot
            auxBC = repmat(BCbase,1,2);
            MR = sys.Mratio;
            if strcmp(name,'BurnerFlame')
                auxBC(~ismember(1:2,BC.port(1))).A = BC.A*sys.Aratio^(1-2*(BC.port(1)-1)); % geometry
            end
            auxBC(~ismember(1:2,BC.port(1))).rho = BC.rho*(MR/sys.Tratio)^(1-2*(BC.port(1)-1)); % ideal gas law, isobaric state change (not used)
            auxBC(~ismember(1:2,BC.port(1))).c = BC.c*(sys.kratio*sys.Tratio/MR)^(.5-(BC.port(1)-1)); % definition of c, ideal gas law
            auxBC(~ismember(1:2,BC.port(1))).Mach = BC.Mach(1)*(sys.Tratio/(sys.kratio*MR))^(.5-(BC.port(1)-1)); % steady continuity, assuming isobaric state change (not used)
            auxBC(~ismember(1:2,BC.port(1))).kappa = BC.kappa*sys.kratio^(1-2*(BC.port(1)-1)); % definition
            auxBC(~ismember(1:2,BC.port(1))).Mmol = BC.Mmol*MR^(1-2*(BC.port(1)-1)); % definition
            auxBC(~ismember(1:2,BC.port(1))).alpha = 0; % definition
        end
        if strcmp(name,'triJunction_12') || strcmp(name,'triJunction_21') % trijunctions
            auxBC = repmat(BCbase,1,3);
            auxBC(BC.port(1)).Mach = BC.Mach(1);
            auxBC(BC.port(2)).Mach = BC.Mach(2);
            auxBC(~ismember(1:3,BC.port)).Mach =  - BC.Mach(1)*sys.Connection{BC.port(1)}.dir/sys.Connection{~ismember(1:3,BC.port)}.dir...
                                                  - BC.Mach(2)*sys.Connection{BC.port(2)}.dir/sys.Connection{~ismember(1:3,BC.port)}.dir; % steady continuity
%             Ma = cellfun(@(x) x.Mach*x.dir, sys.Connection);
%             c = [c; sys.state.f.c];
%             rho = [rho; sys.state.f.rho];
%             A = [A; sys.state.f.A];
%             kappa = [kappa; sys.state.f.kappa];
%             Mmol = [Mmol; sys.state.f.Mmol];
%             alpha = [alpha; sys.state.f.alpha];
        end
        if isfield(pars,'dp_fuel')
            dp = sys.dp_fuel;
            phi = 1./c(1)*(-u/Ma(1)-prc*(kappa(1)-1)/(2*((1+(dp*kappa(1)/(rho(1)*c(1).^2))^((kappa(1)-1)/(kappa(1))))-1)));
        end
        if numel(sys.Connection) == 1 || strcmp(name,'meanflow') % BC
            if strcmp(char(pars.Mach),'[]') || strcmp(char(pars.rho),'[]') || strcmp(char(pars.c_sound),'[]') || strcmp(char(pars.Area),'[]')
                auxBC = BC;
            else
                auxBC = struct(...
                    'Mach',str2double(char(pars.Mach)),...
                    'rho',str2double(char(pars.rho)),...
                    'c',str2double(char(pars.c_sound)),...
                    'A',str2double(char(pars.Area)),...
                    'kappa',str2double(char(pars.kappa)),...
                    'Mmol',str2double(char(pars.Mmol)),...
                    'alpha',str2double(char(pars.alpha)),...
                    'pos',eval(char(pars.pos)));
            end
        end
    elseif strcmp(yOut,'BCerror') && ~any(strcmp(name,BCerrorAllowed))
        ok = false;
        return
    else
        acc = 1e4*eps;
    end

    % decide which element and check criterion
    if strcmp(name,'Block') && ischar(yOut) && strcmp(yOut,'BCerror')
        ok = sys.Connection{1}.Mach*sys.Connection{1}.dir < 0; % this is a specific test case!

    elseif strcmp(name,'Block')
        sys2 = load(sys.fileName);
        if ~isa(sys2.sys,'Element')
            sys2.sys = sys.updatesss(sys2.sys);
        end
        simmodel = lsimmod(sys2.sys,uIn(cell2mat(cellfun(@(x) find(strcmp(x,sys2.sys.u)), sys.u, 'UniformOutput', false)),:).',Ts,'backwardEuler');
        ok = all(all(acc > abs(yOut(cell2mat(cellfun(@(x) find(strcmp(x,sys2.sys.y)), sys.y, 'UniformOutput', false)),:)-simmodel))); % behaves like simmodel, connections don't need to be implemented

    elseif strcmp(name,'Duct') && isempty(sys.x0) && BC.Mach(1)~=0 % length: zero, convection included
        ok = all(f == f(1,:)) && all(g == g(1,:)) && all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(3:end))) &&...
            all(cellfun(@(x) checkBC(x,BC,{'pos'}), sys.Connection)); % connections equal

    elseif strcmp(name,'Duct') && isempty(sys.x0) && BC.Mach(1)==0 % length: zero, zero convection
        ok = all(f == f(1,:)) && all(g == g(1,:)) &&...
            all(cellfun(@(x) checkBC(x,BC,{'pos'}), sys.Connection)); % connections equal

    elseif strcmp(name,'Duct') && BC.Mach(1)>0 % regular duct, forward
        ok = all(all(acc > abs(-1/BC.c/(1+BC.Mach(1))*dfdt(f) - dfdx(f,'f')))) &&...
            all(all(acc > abs(1/BC.c/(1-BC.Mach(1))*dgdt(g) - dgdx(g,'g')))) &&...
            all(cellfun(@(xwave) full(all(all(acc > abs(-1/sys.cProp.(xwave)*dfdt(convective.(xwave)) - dfdx(convective.(xwave),xwave))))),sys.waves(3:end))) &&...
            all(cellfun(@(x) checkBC(x,BC,{'pos'}), sys.Connection)); % connections equal

    elseif strcmp(name,'Duct') && BC.Mach(1)<0 % regular duct, backward
        ok = all(all(acc > abs(-1/BC.c/(1+BC.Mach(1))*dfdt(f) - dfdx(f,'f')))) &&...
            all(all(acc > abs(1/BC.c/(1-BC.Mach(1))*dgdt(g) - dgdx(g,'g')))) &&...
            all(cellfun(@(xwave) full(all(all(acc > abs(-1/sys.cProp.(xwave)*dgdt(convective.(xwave)) - dgdx(convective.(xwave),xwave))))),sys.waves(3:end))) &&...
            all(cellfun(@(x) checkBC(x,BC,{'pos'}), sys.Connection)); % connections equal

    elseif strcmp(name,'Duct') && BC.Mach(1)==0 % regular duct, zero convection
        ok = all(all(acc > abs(-1/BC.c/(1+BC.Mach(1))*dfdt(f) - dfdx(f,'f')))) &&...
            all(all(acc > abs(1/BC.c/(1-BC.Mach(1))*dgdt(g) - dgdx(g,'g')))) &&...
            all(cellfun(@(x) checkBC(x,BC,{'pos'}), sys.Connection)); % connections equal

    elseif strcmp(name,'Excitation')
        if strcmp(char(pars.excitation),'inactive')
            ok = all(f == f(1,:)) && all(g == g(1,:)) &&... % no influence
                all(cellfun(@(x) checkBC(x,BC), sys.Connection)); % connections equal
        elseif strcmp(char(pars.excitation),'phi') && sys.Connection{1}.Mach == 0
            ok = all(u == 0) &&...
                all(cellfun(@(x) checkBC(x,BC), sys.Connection)); % connections equal
        else
            ok = (all(acc > abs(u(1,:) - sys.Amp*uIn(strcmp(sys.u,'Test')))) ||...
                all(acc > abs(prc(1,:)*rho(1)*c(1) - sys.Amp*uIn(strcmp(sys.u,'Test')))) ||...
                all(acc > abs(phi(1,:) - sys.Amp*uIn(strcmp(sys.u,'Test'))))) &&...
                all(cellfun(@(x) checkBC(x,BC), sys.Connection)); % connections equal
        end

    elseif strcmp(name,'Flame')
        upBC = struct('Mach',BC.Mach(1),'rho',BC.rho,'c',BC.c,'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        dnBC = struct('Mach',str2double(char(pars.Mach)),'rho',str2double(char(pars.rho)),'c',str2double(char(pars.c)),'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',0,'pos',BC.pos);
        confl = sys.Connection(2:end-1);
        Tcontrue = cellfun(@(x) isfield(x,'TFlame') && x.TFlame,confl);
        Q = uIn(numel(sys.waves)+1:end,:);
        s = Q(Tcontrue);
        Q = Q(~Tcontrue,:);
%       ok = all(acc > abs(prc(1,:)*rho(1)*c(1)-prc(2,:)*rho(2)*c(2) - rho(1)*c(1)^2*Ma(1)^2 * (c(2)^2/c(1)^2-1) * (u(1,:)/(Ma(1)*c(1)) +       sum(Q,1)))) &&... % Rankine Hugoniot I w/o Q' --> Diss Kopitz
        ok = all(acc > abs(prc(1,:)*rho(1)*c(1)-prc(2,:)*rho(2)*c(2) - rho(1)*c(1)^2*Ma(1)   * (c(2)^2/c(1)^2-1) * (u(1,:)/(	  c(1)) + Ma(1)*sum(Q,1)))) &&... % alternative form of Rankine Hugoniot I w/o Q' --> Diss Kopitz
            all(acc > abs(u(1,:)-u(2,:) + c(1)*Ma(1) * (c(2)^2/c(1)^2-1) * (sum(Q,1)-kappa(1)*prc(1,:)/c(1)))) &&... % Rankine Hugoniot II w/o Q' --> Diss Kopitz
            all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(~strcmp(sys.waves,'f')&~strcmp(sys.waves,'g')&~strcmp(sys.waves,'s')&~strcmp(sys.waves,'alpha')&~strncmp(sys.waves,'omega',5)))) &&... % convective for compact elements: no influence except for: s @ TFlame, alpha, omega --> 0 
            all(sum(s,1) + convective.s(1,:) == convective.s(2,:)) &&... % s @ TFlame
            all(cellfun(@(xwave) all(0 == convective.(xwave)(2,:)),sys.waves(strcmp(sys.waves,'alpha')&strncmp(sys.waves,'omega',5)))) &&... % alpha, omega --> 0, 
            checkBC(sys.Connection{1},upBC) &&... % upstream BC
            checkBC(sys.Connection{2+str2double(char(pars.nRef))},dnBC); % downstream BC

    elseif strcmp(name,'areaChange') && isempty(sys.x0) % length: zero
        upBC = struct('Mach',BC.Mach(1),'rho',BC.rho,'c',BC.c,'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        dnBC = struct('Mach',BC.Mach(1)*BC.A/sys.Connection{2}.A,'rho',BC.rho,'c',BC.c,'A',sys.Connection{2}.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        ok = all(acc > abs(A(2)*(prc(2,:)*Ma(2)+u(2,:)) - A(1)*(prc(1,:)*Ma(1)+u(1,:)))) &&... % continuity
            all(acc > abs((prc(2,:)+Ma(2)*u(2,:)) - (prc(1,:)+Ma(1)*u(1,:)) + sys.zeta*Ma(1)*u(1,:))) &&... % Bernoulli
            (...
                (BC.Mach(1)~=0 && all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(~strcmp(sys.waves,'f')&~strcmp(sys.waves,'g')&~strncmp(sys.waves,'omega',5))))) ||...
                (BC.Mach(1)==0 && all(cellfun(@(xwave) all(0 == convective.(xwave)(1+(BC.Mach(1)>=0),:)),sys.waves(strncmp(sys.waves,'omega',5)))))... 
                ) &&... % convective for compact elements: no influence except for: omega --> 0 
            all(cellfun(@(xwave) all(0 == convective.(xwave)(1+(BC.Mach(1)>=0),:)),sys.waves(strncmp(sys.waves,'omega',5)))) &&... % omega --> 0, 
            checkBC(sys.Connection{1},upBC) &&... % upstream BC
            checkBC(sys.Connection{2},dnBC); % downstream BC

    elseif strcmp(name,'areaChange')
        upBC = struct('Mach',BC.Mach(1),'rho',BC.rho,'c',BC.c,'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        dnBC = struct('Mach',BC.Mach(1)*BC.A/sys.Connection{2}.A,'rho',BC.rho,'c',BC.c,'A',sys.Connection{2}.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        ok = all(acc > abs(A(2)*(prc(2,(1:end-1)+bt)*Ma(2)+u(2,(1:end-1)+bt)) - A(1)*(prc(1,(1:end-1)+bt)*Ma(1)+u(1,(1:end-1)+bt)) + A(2)*sys.lred/c(1)*ddt(prc(1,:)))) &&... % continuity
            all(acc > abs((prc(2,(1:end-1)+bt)+Ma(2)*u(2,(1:end-1)+bt)) - (prc(1,(1:end-1)+bt)+Ma(1)*u(1,(1:end-1)+bt)) + sys.leff/c(1)*ddt(u(1,:)) + sys.zeta*Ma(1)*u(1,(1:end-1)+bt))) &&... % Bernoulli
            (...
                (BC.Mach(1)~=0 && all(cellfun(@(xwave) full(all(acc > abs(A(2)*rho(2)*c(2)*Ma(2)*convective.(xwave)(2,(1:end-1)+bt) - A(1)*rho(1)*c(1)*Ma(1)*convective.(xwave)(1,(1:end-1)+bt) + A(2)*rho(1+(BC.Mach(1)>=0))*sys.lred*ddt(convective.(xwave)(1+(BC.Mach(1)>=0),:))))),sys.waves(~strcmp(sys.waves,'f')&~strcmp(sys.waves,'g')&~strncmp(sys.waves,'omega',5))))) ||...
                (BC.Mach(1)==0 && all(cellfun(@(xwave) all(0 == convective.(xwave)(1+(BC.Mach(1)>=0),:)),sys.waves(strncmp(sys.waves,'omega',5)))))... 
                ) &&... % convective for compact elements: no influence except for: omega --> 0 
            all(cellfun(@(xwave) full(all(0 == convective.(xwave)(2,:))),sys.waves(strncmp(sys.waves,'omega',5)))) &&... % omega --> 0
            checkBC(sys.Connection{1},upBC) &&... % upstream BC
            checkBC(sys.Connection{2},dnBC); % downstream BC

    elseif strcmp(name,'chokedExit')
        ok = all(acc > abs(u - Ma(1)*(kappa(1)-1)/2*prc - Ma(1)*c(1)*convective.s/2)) &&...
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'closedEnd')
        ok = all(acc > abs(u + prc*Ma(1))) &&... % mdot' = 0
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'generalRef') || strcmp(name,'refGeneric') || strcmp(name,'ref_p') || strcmp(name,'ref_phi') || strcmp(name,'refInterpolate') || strcmp(name,'refG') || strcmp(name,'refnTau')
        if strcmp(name,'refInterpolate') || strcmp(name,'refG') || strcmp(name,'refnTau')
            [model,~] = feval(sys.filename(1:end-2),pars,sys.state);
        else
            model = load(sys.filename);
            model = model.model;
        end
        if find(any(uIn~=0,2)) == numel(sys.waves)+1 % direct flame input
            IN = uIn(numel(sys.waves)+1,:);
            IN2 = zeros(1,size(uIn,2));
            INview = zeros(1,size(uIn,2));
        elseif find(any(uIn~=0,2)) == numel(sys.waves)+3 % direct Tflame input
            IN2 = uIn(numel(sys.waves)+3,:);
            IN = zeros(1,size(uIn,2));
            INview = zeros(1,size(uIn,2));
        elseif find(any(uIn~=0,2)) == numel(sys.waves)+2 || find(any(uIn~=0,2)) == numel(sys.waves)+4 % noise input
            IN = 0; % only for isfinite
            INview = zeros(1,size(uIn,2));
        else
            if strcmp(name,'refGeneric') || strcmp(name,'refInterpolate') || strcmp(name,'refG') || strcmp(name,'refnTau') || (isfield(pars,'refType') && strcmp(char(pars.refType),'u''/umean'))
                IN = u(1,:)/(Ma(1)*c(1));
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'p''/(rhomean*cmean^2)')
                IN = prc(1,:)/c(1);
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'phi''/phimean (emulated)')
                IN = phi(1,:);
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'phi''/phimean') && isfield(convective,'alpha')
                IN = 1/(1-BC.alpha)/BC.alpha*convective.alpha(1,:);
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'phi''/phimean')
                IN = 0;
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'s''/cp') && isfield(convective,'s')
                IN = convective.s(1,:);
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'s''/cp')
                IN = 0;
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'utan''/utanmean (tangential)') && isfield(convective,'utan')
                IN = convective.utan(1,:);
            elseif isfield(pars,'refType') && strcmp(char(pars.refType),'utan''/utanmean (tangential)')
                IN = 0;
            end
            INview = IN;
            if IN == 0
                INview = zeros(1,size(uIn,2));
            end
        end
        if any(~isfinite(IN)) % infinite signals (e.g. Ma = 0)
            ok = all(all(f == f(1,:)) & all(g == g(1,:))) &&... % no direct influence
                all(cellfun(@(xwave) all(all(convective.(xwave) == convective.(xwave)(1,:))),sys.waves(3:end))) &&... % convective for compact elements: no influence
                all(cellfun(@(x) checkBC(x,BC), sys.Connection(1:2))); % acoustic connections equal
        elseif strcmp(char(pars.active),'off') || find(any(uIn~=0,2)) == numel(sys.waves)+2 || (numel(IN) == 1 && IN == 0) % inactive flame
            ok = all(all(f == f(1,:)) & all(g == g(1,:))) &&... % no direct influence
                all(cellfun(@(xwave) all(all(convective.(xwave) == convective.(xwave)(1,:))),sys.waves(3:end))) &&... % convective for compact elements: no influence
                all(acc > abs(INview-yOut(end,:))) &&... % referenceValueOut
                all(cellfun(@(x) checkBC(x,BC), sys.Connection(1:2))); % acoustic connections equal
        else % regular
            if isa(model,'idtf') || isa(model,'idpoly')
                mTs = model.Ts;
            else
                mTs = Ts;
            end
            Tflame = false;
            if isa(model,'sss') && ~isempty(model.x0)
                simmodel = lsim(model,interp1(-Ts:Ts:Ts*(size(uIn,2)-1),[0,IN],(0:mTs:Ts*(size(uIn,2)-1))'),mTs,[],struct('method','backwardEuler','Ts_sample',mTs))';
            elseif isa(model,'sss')
                simmodel = model.D*IN;
            else
                simmodel = lsim(model,interp1(-Ts:Ts:Ts*(size(uIn,2)-1),[0,IN],(0:mTs:Ts*(size(uIn,2)-1))'),(0:mTs:Ts*(size(uIn,2)-1))')';
                if isprop(sys,'filename2') && ~isempty(sys.filename2) && strcmp(sys.y{numel(sys.waves)+2},'Q_Test_T')
                    Tflame = true;
                    model2 = load(sys.filename2);
                    model2 = model2.model;
                    mTs2 = model2.Ts;
                    if ~exist('IN2','var')
                        IN2 = IN;
                    end
                    simmodel2 = lsim(model2,interp1(-Ts:Ts:Ts*(size(uIn,2)-1),[0,IN2],(0:mTs2:Ts*(size(uIn,2)-1))'),(0:mTs2:Ts*(size(uIn,2)-1))')';
                end
            end
            ok = all(all(f == f(1,:)) & all(g == g(1,:))) &&... % no direct influence
                all(cellfun(@(xwave) all(all(convective.(xwave) == convective.(xwave)(1,:))),sys.waves(3:end))) &&... % convective for compact elements: no influence
                (all(5e-2*max(abs(simmodel)) >= abs(interp1(0:Ts:Ts*(size(uIn,2)-1),yOut(2*numel(sys.waves)+1,:),mTs/2:mTs:Ts*(size(uIn,2)-1)-mTs/2)-simmodel(2:end))) ||... % QOut, accuracy 5 percent, because of large timesteps in testmodels
                    2e-2*max(abs(simmodel)) >= sum(abs(interp1(0:Ts:Ts*(size(uIn,2)-1),yOut(2*numel(sys.waves)+1,:),mTs/2:mTs:Ts*(size(uIn,2)-1)-mTs/2)-simmodel(2:end)),2)/(numel(simmodel)-1)) &&... % QOut, integrated accuracy 2 percent, because of large timesteps in testmodels
                (~Tflame ||...
                    all(5e-2*max(abs(simmodel2)) >= abs(interp1(0:Ts:Ts*(size(uIn,2)-1),yOut(2*numel(sys.waves)+2,:),mTs2/2:mTs2:Ts*(size(uIn,2)-1)-mTs2/2)-simmodel2(2:end))) ||... % s'/cp_Out, accuracy 5 percent, because of large timesteps in testmodels
                    2e-2*max(abs(simmodel2)) >= sum(abs(interp1(0:Ts:Ts*(size(uIn,2)-1),yOut(2*numel(sys.waves)+2,:),mTs2/2:mTs2:Ts*(size(uIn,2)-1)-mTs2/2)-simmodel2(2:end)),2)/(numel(simmodel2)-1)) &&... % s'/cp_Out, integrated accuracy 2 percent, because of large timesteps in testmodels
                all(acc > abs(INview-yOut(end,:))) &&... % referenceValueOut
                all(cellfun(@(x) checkBC(x,BC), sys.Connection(1:2))); % acoustic connections equal
        end

    elseif strcmp(name,'loudSpeaker')
        ok = all(u == sys.Amp*uIn(sys.InputGroup.loudSpeaker)) &&...
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'loudSpeaker_p')
        ok = all(acc > abs(prc*rho(1)*c(1) - sys.Amp*uIn(sys.InputGroup.loudSpeaker))) &&...
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'loudSpeaker_phi')
        ok = ...
            ((~sys.phiConvective && ((Ma(1) == 0 && all(f == g)) || all(acc > abs(phi - sys.Amp*uIn(sys.InputGroup.loudSpeaker))))) ||... % Mach == 0: r = 1, no influence of uIn(sys.InputGroup.loudSpeaker,:)
                (sys.phiConvective && all(f == g) && all(acc > abs((1-auxBC.alpha)*convective.alpha - auxBC.alpha*sys.Amp*uIn(sys.InputGroup.loudSpeaker)))) ||... % convective
                (sys.phiConvective && all(f == g) && auxBC.alpha == 0)) &&... % mean alpha = 0 --> no effect
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'meanflow')
        ok = all(f == f(1,:)) && all(g == g(1,:)) &&...
            all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(3:end))) &&... % convective for compact elements: no influence
            all(cellfun(@(x) checkBC(x,auxBC), sys.Connection)); % 

    elseif strcmp(name,'observer')
        ok = all(f == f(1,:)) && all(g == g(1,:)) &&...
            all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(3:end))) &&... % convective for compact elements: no influence
            ((strcmp(char(pars.property),'p_u') && all(all(yOut(sys.OutputGroup.Observer(1:2),:) == [prc(1,:)*rho(1)*c(1);u(1,:)]))) ||...
                (strcmp(char(pars.property),'f_g') && all(all(yOut(sys.OutputGroup.Observer(1:2),:) == [f(1,:);g(1,:)])))) &&... % output check
            all(all(cellfun(@(xwave) convective.(xwave)(1,:),sys.waves(3:end)) == yOut(sys.OutputGroup.Observer(3:end),:),2)) &&... % convective output check
            all(cellfun(@(x) checkBC(x,BC), sys.Connection)); % connections equal

    elseif strcmp(name,'openEnd')
        ok = all(acc > abs(prc + Ma(1)*u)) &&... % lin. Bernoulli
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'reflEnd') || strcmp(name,'nonReflEnd')
        ok = ((strcmp(char(pars.loc),'Upstream') && all(f == str2double(char(pars.r))*g+uIn(sys.InputGroup.loudSpeaker,:))) || (strcmp(char(pars.loc),'Downstream') && all(g == str2double(char(pars.r))*f+uIn(sys.InputGroup.loudSpeaker,:)))) &&...
            checkBC(sys.Connection{BC.port(1)},auxBC); % only one connection, check for transcription errors

    elseif strcmp(name,'scatter')
        ok = all(f == f(1,:)) && all(g == g(1,:)) &&... % no direct influence
            all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(3:end))) &&... % convective for compact elements: no influence
            all(cellfun(@(x) checkBC(x,BC), sys.Connection)); % connections equal

    elseif strcmp(name,'simpleDuct') % simple (ss) duct
        tauf = sys.l/(c(1)*(1+Ma(1)));
        nf = ceil(tauf/Ts);
        taug = sys.l/(c(1)*(1-Ma(1)));
        ng = ceil(taug/Ts);
        for wavec = sys.waves(3:end)'
            wave = char(wavec);
            tau = abs(sys.l/sys.cProp.(wave));
            nc.(wave) = min(size(yOut,2),ceil(tau/Ts));
        end
        ok = all(acc > abs(f(2,[1:nf,nf+2:end])-[zeros(1,nf),f(1,2:end-nf)])) && all(acc > abs(g(1,[1:ng,ng+2:end])-[zeros(1,ng),g(2,2:end-ng)])) &&... % check all except at jump (interpolation inaccuracies are tolerated)
            all(cellfun(@(xwave) all(acc > abs(convective.(xwave)(1+(BC.Mach(1)>=0),[1:nc.(xwave),nc.(xwave)+2:end])-[zeros(1,nc.(xwave)),convective.(xwave)(2-(BC.Mach(1)>=0),2:end-nc.(xwave))])), sys.waves(3:end))) &&... % check all except at jump (interpolation inaccuracies are tolerated)
            all(cellfun(@(x) checkBC(x,BC,{'pos'}), sys.Connection)); % connections equal

    elseif strcmp(name,'subsonicNozzle')
        if ischar(yOut) && strcmp(yOut,'BCerror')
            ok = []; % allow BCerror
        else
            nt = size(yOut,2); % number of time-steps regarded
            % some values...
            rho_p = repmat(rho./c,1,nt).*prc - repmat(rho,1,nt).*convective.s; % density fluctuation
            cpT = c.^2./(kappa-1); % c1^2/(kappa1-1) = cp*T1
            cp = kappa./(kappa-1).*Rmol./Mmol;
            h_p = repmat(cpT,1,nt).*convective.s + repmat(c,1,nt).*prc; % Gibbs
            % isentropic state change: same total temperature, pressure, and density (one more than necessary, but whatever)
            T = cpT./cp;
            p = rho.*c.^2./kappa;
            Ttratio = 1 + (kappa-1)/2.*Ma.^2;
            Tt = T.*Ttratio;
            pt = p.*Ttratio.^(kappa./(kappa-1));
            rhot = rho.*Ttratio.^(1./(kappa-1));
            meanIsentropic = acc > max(abs([1-Tt(1)/Tt(2),1-pt(1)/pt(2),1-rhot(1)/rhot(2)]));
            % mean conservation equations
            h = cpT; % df: h0 = cp*T0
            meanMass = acc > abs(sum(rho.*c.*Ma.*A.*[-1;1]))/abs(rho(1)*c(1)*Ma(1)*A(1));
            meanEnergy = acc > abs(sum(rho.*c.*Ma.*A.*(h + (c.*Ma).^2/2).*[-1;1]))/abs(rho(1)*c(1)^3*A(1)); % normalize with inlet kinetic energy (except Mach and 0.5)

            % unsteady:
            mass = acc > abs(sum(...
                + repmat(c.*Ma,1,nt)    .*rho_p     .*repmat([-1;1].*A,1,nt)...
                + repmat(rho,1,nt)      .*u         .*repmat([-1;1].*A,1,nt)...
                ,1))/abs(rho(1)*c(1)*A(1));
            energy = acc > abs(sum(...
                + repmat(c.*Ma.*h + .5*(c.*Ma).^3,1,nt)     .*rho_p     .*repmat([-1;1].*A,1,nt) ...
                + repmat(rho.*h + 1.5*rho.*(c.*Ma).^2,1,nt) .*u         .*repmat([-1;1].*A,1,nt) ...
                + repmat(rho.*c.*Ma,1,nt)                   .*h_p       .*repmat([-1;1].*A,1,nt) ...
                ,1))/abs(rho(1)*A(1)*c(1)^3);

            ok = meanIsentropic && meanMass && meanEnergy &&... % mean (no momentum: a nozzle can produce thrust, and we are too lazy to analyse axial forces from wall pressure distributions. Basically the outcome should be compressible Bernoulli)
                all(mass) && all(energy) &&... % unsteady without momentum
                all(cellfun(@(xwave) all(0 == convective.(xwave)(2-(BC.Mach(1)<0),:)),sys.waves(strncmp(sys.waves,'omega',5)))) &&... % omega --> 0. Incorrect, but that's the way it's implemented for now.
                checkBC(sys.Connection{1},auxBC(1),{'rho','c','A','Mach'}) &&... % upstream BC: except rho, c, A and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                checkBC(sys.Connection{2},auxBC(2),{'rho','c','A','Mach'}); % downstream BC: except rho, c, A and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
        end

    elseif strcmp(name,'symAreaChange') && isempty(sys.x0) % length: zero
        ok = all(acc > abs(A(2)*(prc(2,:)*Ma(2)+u(2,:)) - A(1)*(prc(1,:)*Ma(1)+u(1,:)))) &&... % continuity
            all(acc > abs((prc(2,:)+Ma(2)*u(2,:)) - (prc(1,:)+Ma(1)*u(1,:)) + sys.zeta*Ma(1)*u(1,:))) &&... % Bernoulli
            (...
                (BC.Mach(1)~=0 && all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(~strcmp(sys.waves,'f')&~strcmp(sys.waves,'g')&~strncmp(sys.waves,'omega',5))))) ||...
                (BC.Mach(1)==0 && all(cellfun(@(xwave) all(0 == convective.(xwave)(1+(BC.Mach(1)>=0),:)),sys.waves(strncmp(sys.waves,'omega',5)))))... 
                ) &&... % convective for compact elements: no influence except for: omega --> 0 
            all(cellfun(@(xwave) all(0 == convective.(xwave)(1+(BC.Mach(1)>=0),:)),sys.waves(strncmp(sys.waves,'omega',5)))) &&... % omega --> 0, 
            checkBC(sys.Connection{1},auxBC(1)) &&... % upstream BC
            checkBC(sys.Connection{2},auxBC(2)); % downstream BC

    elseif strcmp(name,'symAreaChange')
        ok = all(acc > abs(A(2)*(prc(2,(1:end-1)+bt)*Ma(2)+u(2,(1:end-1)+bt)) - A(1)*(prc(1,(1:end-1)+bt)*Ma(1)+u(1,(1:end-1)+bt)) + A(2)*sys.lred/c(1)*ddt(prc(1,:)))) &&... % continuity
            all(acc > abs((prc(2,(1:end-1)+bt)+Ma(2)*u(2,(1:end-1)+bt)) - (prc(1,(1:end-1)+bt)+Ma(1)*u(1,(1:end-1)+bt)) + sys.leff/c(1)*ddt(u(1,:)) + sys.zeta*Ma(1)*u(1,(1:end-1)+bt))) &&... % Bernoulli
            (...
                (BC.Mach(1)~=0 && all(cellfun(@(xwave) full(all(acc > abs(A(2)*rho(2)*c(2)*Ma(2)*convective.(xwave)(2,(1:end-1)+bt) - A(1)*rho(1)*c(1)*Ma(1)*convective.(xwave)(1,(1:end-1)+bt) + A(2)*rho(1+(BC.Mach(1)>=0))*sys.lred*ddt(convective.(xwave)(1+(BC.Mach(1)>=0),:))))),sys.waves(~strcmp(sys.waves,'f')&~strcmp(sys.waves,'g')&~strncmp(sys.waves,'omega',5))))) ||...
                (BC.Mach(1)==0 && all(cellfun(@(xwave) all(0 == convective.(xwave)(1+(BC.Mach(1)>=0),:)),sys.waves(strncmp(sys.waves,'omega',5)))))... 
                ) &&... % convective for compact elements: no influence except for: omega --> 0 
            all(cellfun(@(xwave) full(all(0 == convective.(xwave)(2,:))),sys.waves(strncmp(sys.waves,'omega',5)))) &&... % omega --> 0
            checkBC(sys.Connection{1},auxBC(1)) &&... % upstream BC
            checkBC(sys.Connection{2},auxBC(2)); % downstream BC

    elseif strcmp(name,'symFlame') || strcmp(name,'symTempJump') || strcmp(name,'BurnerFlame')
        if ischar(yOut) && strcmp(yOut,'BCerror')
            ok = []; % allow BCerror
        else
            % first gather some general data
            nt = size(yOut,2); % number of time-steps regarded
            % heat release fluctuations (ETF-inut does not occur)
            if strcmp(name,'symTempJump')
                Q = 0;
                nRef = 0;
            else
                Q = sum(uIn(sys.InputGroup.FlameQ,:),1);
                nRef = str2double(char(pars.nRef));
            end
            % some values...
            mf = sys.meanflow;
            C_1 = mf.C_1;
            C_2 = mf.C_2;
            if Ma(1)>=0
                h = [mf.h_1;mf.h_2];
            else
                h = [mf.h_2;mf.h_1];
            end
            q_net = mf.q_net;
            q = mf.q;
            Qmean = rho(1)*A(1)*c(1)*abs(Ma(1))*q;
            Qloss = Qmean - rho(1)*A(1)*c(1)*abs(Ma(1))*q_net;
            phi_plus = convective.alpha/(sys.alpha*(1-sys.alpha));
            if Ma(1)>=0
                A_dn = A(2);
                Mmol_plus = phi_plus.*repmat([C_1;C_2],1,nt); % non-dimensional fluctuation fo the molar mass due to alpha
            else
                A_dn = A(1);
                Mmol_plus = phi_plus.*repmat([C_2;C_1],1,nt); % non-dimensional fluctuation fo the molar mass due to alpha
            end
            rho_p = repmat(rho./c,1,nt).*prc - repmat(rho,1,nt).*convective.s + repmat(rho,1,nt).*Mmol_plus; % density fluctuation
            cpT = c.^2./(kappa-1); % c1^2/(kappa1-1) = cp*T1
            h_p = repmat(cpT,1,nt).*convective.s + repmat(c,1,nt).*prc; % Gibbs

            % now go into cases
            if strcmp(sys.Version,'realistic fluid, full order in Mach, moving flame') ...
                    || strcmp(sys.Version,'realistic fluid, full order in Mach, fixed heat source')
                % steady
                meanMass = acc > abs(sum(rho.*A.*c.*Ma.*[-1;1]))/abs(rho(1)*A(1)*c(1)*max(abs(Ma(1)),.02));
                meanMomentum = acc > abs(sum((rho.*A.*(c.*Ma).^2 + rho.*A_dn.*c.^2./kappa).*[-1;1]))/abs(rho(1)*A(1)*(c(1)*max(abs(Ma(1)),.02))^2);
                meanEnergy = acc > abs(sum(rho.*A.*c.*Ma.*(h + (c.*Ma).^2/2).*[-1;1]) - Qmean + Qloss)/abs(Qmean);
                % get Vdot_s
                Vdot_s = sum(yOut(strncmp(sys.y,[sys.Name,'_flameMotion'],numel(sys.Name)+12),:),1);
                % unsteady
                mass = acc > abs(sum(...
                    + repmat(A.*c.*Ma,1,nt)    .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho,1,nt)      .*u         .*repmat([-1;1],1,nt) ...
                    - (rho                     .*Vdot_s)   .*repmat([-1;1],1,nt) ...
                    ,1))/abs(rho(1)*A(1)*c(1)*Ma(1));
                momentum = acc > abs(sum(...
                    + repmat(A.*(c.*Ma).^2,1,nt)   .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(2*A.*rho.*c.*Ma,1,nt) .*u         .*repmat([-1;1],1,nt) ...
                    + repmat(A_dn*rho.*c,1,nt)     .*prc       .*repmat([-1;1],1,nt) ... % use A(2) because of jet condition
                    - ((rho.*c.*Ma)                .*Vdot_s)   .*repmat([-1;1],1,nt) ... % irrelevant in case of symFlame or symTempJump, because of meanMass
                    ,1))/abs(rho(1)*A(1)*(c(1)*Ma(1))^2);
                energy = acc > abs(sum(...
                    + repmat(A.*c.*Ma.*h + .5*A.*(c.*Ma).^3,1,nt)             .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho.*h + 1.5*A.*rho.*(c.*Ma).^2,1,nt)         .*u         .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho.*c.*Ma,1,nt)                              .*h_p       .*repmat([-1;1],1,nt) ...
                    - ((rho.*h + .5*rho.*(c.*Ma).^2 - rho.*c.^2./kappa)       .*Vdot_s)   .*repmat([-1;1],1,nt) ...
                    ,1) - Q*Qmean)/abs(Qmean);
                ok = meanMass && meanMomentum && meanEnergy &&... % mean
                    all(mass) && all(momentum) && all(energy) &&... % unsteady
                    all(cellfun(@(xwave) all(0 == convective.(xwave)(2-(BC.Mach(1)<0),:)),sys.waves(strcmp(sys.waves,'alpha')&strncmp(sys.waves,'omega',5)))) &&... % alpha, omega --> 0, 
                    checkBC(sys.Connection{1},auxBC(1),{'rho','Mach'}) &&... % upstream BC: except rho and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                    checkBC(sys.Connection{2+nRef},auxBC(2),{'rho','Mach'}); % downstream BC: except rho and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                ok;
            elseif strncmp(sys.Version,'realistic fluid',15)
                % steady
                meanMass = acc > abs(sum(rho.*A.*c.*Ma.*[-1;1]))/abs(rho(1)*A(1)*c(1)*max(abs(Ma(1)),.02));
                meanMomentum = acc > abs(sum((rho.*A.*(c.*Ma).^2 + rho.*A_dn.*c.^2./kappa).*[-1;1]))/abs(rho(1)*A(1)*(c(1)*max(abs(Ma(1)),.02))^2);
                meanEnergy = acc > abs(sum(rho.*A.*c.*Ma.*(h + (c.*Ma).^2/2).*[-1;1]) - Qmean + Qloss)/abs(Qmean);
                % get Vdot_s
                Vdot_s = sum(yOut(strncmp(sys.y,[sys.Name,'_flameMotion'],numel(sys.Name)+12),:),1);
                % unsteady, save errors, normalization without Mach number
                massError = abs(sum(...
                    + repmat(A.*c.*Ma,1,nt)    .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho,1,nt)      .*u         .*repmat([-1;1],1,nt) ...
                    - (rho                     .*Vdot_s)   .*repmat([-1;1],1,nt) ...
                    ,1))/abs(rho(1)*A(1)*c(1));
                momentumError = abs(sum(...
                    + repmat(A.*(c.*Ma).^2,1,nt)   .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(2*A.*rho.*c.*Ma,1,nt) .*u         .*repmat([-1;1],1,nt) ...
                    + repmat(A_dn*rho.*c,1,nt)     .*prc       .*repmat([-1;1],1,nt) ... % use A(2) because of jet condition
                    - ((rho.*c.*Ma)                .*Vdot_s)   .*repmat([-1;1],1,nt) ... % irrelevant in case of symFlame or symTempJump, because of meanMass
                    ,1))/abs(rho(1)*A(1)*c(1)^2);
                energyError = abs(sum(...
                    + repmat(A.*c.*Ma.*h + .5*A.*(c.*Ma).^3,1,nt)             .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho.*h + 1.5*A.*rho.*(c.*Ma).^2,1,nt)         .*u         .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho.*c.*Ma,1,nt)                              .*h_p       .*repmat([-1;1],1,nt) ...
                    - ((rho.*h + .5*rho.*(c.*Ma).^2 - rho.*c.^2./kappa)       .*Vdot_s)   .*repmat([-1;1],1,nt) ...
                    ,1) - Q*Qmean)/abs(rho(1)*A(1)*c(1)^3);
                ok = meanMass && meanMomentum && meanEnergy &&... % mean
                    all(cellfun(@(xwave) all(0 == convective.(xwave)(2-(BC.Mach(1)<0),:)),sys.waves(strcmp(sys.waves,'alpha')&strncmp(sys.waves,'omega',5)))) &&... % alpha, omega --> 0, 
                    checkBC(sys.Connection{1},auxBC(1),{'rho','Mach'}) &&... % upstream BC: except rho and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                    checkBC(sys.Connection{2+nRef},auxBC(2),{'rho','Mach'}); % downstream BC: except rho and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                if ok % if the rest is ok, replace ok with the error list
                    ok = [massError,momentumError,energyError];
                end
            elseif strncmp(sys.Version,'constant cp, 1st order in Mach',30) ... % we assume 1st order! Other cases need to be implemented separately
                    || strcmp(sys.Version,'constant cp, classic Rankine-Hugoniot, f and g only')
                Qmean = rho(1)*A(1)*c(1)*Ma(1) * (cpT(1)*(sys.Tratio-1) + .5.*(c(2)*Ma(2))^2 - .5.*(c(1)*Ma(1))^2); % acount for kinetic energy
                h = cpT; % df: h0 = cp*T0
                % steady
                meanMass = acc > abs(sum(rho.*A.*c.*Ma.*[-1;1]))/abs(rho(1)*A(1)*c(1)*max(abs(Ma(1)),.02));
                meanMomentum = acc > abs(sum((rho.*A.*(c.*Ma).^2 + rho.*A_dn.*c.^2./kappa).*[-1;1]))/abs(rho(1)*A(1)*(c(1)*max(abs(Ma(1)),.02))^2);
                meanEnergy = acc > abs(sum(rho.*A.*c.*Ma.*(h + (c.*Ma).^2/2).*[-1;1]) - Qmean + Qloss)/abs(Qmean);
                % get Vdot_s
                Vdot_s = sum(yOut(strncmp(sys.y,[sys.Name,'_flameMotion'],numel(sys.Name)+12),:),1);
                % unsteady, save errors, normalization without Mach number
                massError = abs(sum(...
                    + repmat(A.*c.*Ma,1,nt)    .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho,1,nt)      .*u         .*repmat([-1;1],1,nt) ...
                    - (rho                     .*Vdot_s)   .*repmat([-1;1],1,nt) ...
                    ,1))/abs(rho(1)*A(1)*c(1));
                momentumError = abs(sum(...
                    + repmat(A.*(c.*Ma).^2,1,nt)   .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(2*A.*rho.*c.*Ma,1,nt) .*u         .*repmat([-1;1],1,nt) ...
                    + repmat(A_dn*rho.*c,1,nt)     .*prc       .*repmat([-1;1],1,nt) ... % use A(2) because of jet condition
                    - ((rho.*c.*Ma)                .*Vdot_s)   .*repmat([-1;1],1,nt) ... % irrelevant in case of symFlame or symTempJump, because of meanMass
                    ,1))/abs(rho(1)*A(1)*c(1)^2);
                energyError = abs(sum(...
                    + repmat(A.*c.*Ma.*h + .5*A.*(c.*Ma).^3,1,nt)             .*rho_p     .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho.*h + 1.5*A.*rho.*(c.*Ma).^2,1,nt)         .*u         .*repmat([-1;1],1,nt) ...
                    + repmat(A.*rho.*c.*Ma,1,nt)                              .*h_p       .*repmat([-1;1],1,nt) ...
                    - ((rho.*h + .5*rho.*(c.*Ma).^2 - rho.*c.^2./kappa)       .*Vdot_s)   .*repmat([-1;1],1,nt) ...
                    ,1) - Q*Qmean)/abs(rho(1)*A(1)*c(1)^3);
                ok = meanMass && meanMomentum && meanEnergy &&... % mean
                    all(cellfun(@(xwave) all(0 == convective.(xwave)(2-(BC.Mach(1)<0),:)),sys.waves(strcmp(sys.waves,'alpha')&strncmp(sys.waves,'omega',5)))) &&... % alpha, omega --> 0, 
                    checkBC(sys.Connection{1},auxBC(1),{'rho','Mach'}) &&... % upstream BC: except rho and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                    checkBC(sys.Connection{2+nRef},auxBC(2),{'rho','Mach'}); % downstream BC: except rho and Mach, they were checked in steady state conditions and they are too complicated to derive in auxBC
                if ok % if the rest is ok, replace ok with the error list
                    ok = [massError,momentumError,energyError];
                end
            else
                ok = false;
            end
        end

    elseif strcmp(name,'tempJump')
        upBC = struct('Mach',BC.Mach(1),'rho',BC.rho,'c',BC.c,'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
        dnBC = struct('Mach',str2double(char(pars.Mach)),'rho',str2double(char(pars.rho)),'c',str2double(char(pars.c)),'A',BC.A,'kappa',BC.kappa,'Mmol',BC.Mmol,'alpha',BC.alpha,'pos',BC.pos);
%       ok = all(acc > abs(prc(1,:)*rho(1)*c(1)-prc(2,:)*rho(2)*c(2) - rho(1)*c(1)^2*Ma(1)^2 * (c(2)^2/c(1)^2-1) * (u(1,:)/(Ma(1)*c(1)) ))) &&... % Rankine Hugoniot I w/o Q' --> Diss Kopitz
        ok = all(acc > abs(prc(1,:)*rho(1)*c(1)-prc(2,:)*rho(2)*c(2) - rho(1)*c(1)^2*Ma(1)   * (c(2)^2/c(1)^2-1) * (u(1,:)/(	  c(1)) ))) &&... % alternative form of Rankine Hugoniot I w/o Q' --> Diss Kopitz
            all(acc > abs(u(1,:)-u(2,:) + c(1)*Ma(1) * (c(2)^2/c(1)^2-1) * (-kappa(1)*prc(1,:)/c(1)))) &&... % Rankine Hugoniot II w/o Q' --> Diss Kopitz
            all(cellfun(@(xwave) all(convective.(xwave) == convective.(xwave)(1,:)),sys.waves(3:end))) &&... % convective for compact elements: no influence
            checkBC(sys.Connection{1},upBC) &&... % upstream BC
            checkBC(sys.Connection{2},dnBC); % downstream BC

    elseif (strcmp(name,'triJunction_12') || strcmp(name,'triJunction_21')) && ischar(yOut) && strcmp(yOut,'BCerror') % BCerror
        con = sys.Connection;
        mdot = nan(1,numel(con));
        dfPort = cellfun(@(x) Block.checkPort({x},AcBlock.Port), con);
        dfMach = cellfun(@(x) ~isempty(x.Mach), con);
        dfMachIn = cellfun(@(x) ~isempty(x.Mach) && x.Mach<0, con);
        auxcon = con;
        auxcon(~dfPort & ~dfMachIn) = auxcon(dfPort & ~dfMachIn);
        for i = 1:numel(mdot)
            if dfMach(i)
                auxcon{i}.Mach = con{i}.Mach;
            else
                auxcon{i}.Mach = [];
            end
        end
        dfPort = cellfun(@(x) Block.checkPort({x},AcBlock.Port), con);
        for i = 1:numel(mdot)
            if dfPort(i)
                mdot(i) = auxcon{i}.Mach*auxcon{i}.c*auxcon{i}.rho*auxcon{i}.A;
            end
        end
        mdotmiss = -sum(mdot(~isnan(mdot)));
        ok = sum(cellfun(@(x) isempty(x.Mach), con))>1 ||... % not enough Mach numbers given
            (~sys.mix && cellfun(@(x) ~checkBC(con{find(dfPort,1,'first')},x,{'Mach'}), con(dfPort))) ||... % inconsistent properties
            (sys.mix && (...
                cellfun(@(x) ~checkBC(con{find(dfPort & ~dfMachIn,1,'first')},x,{'Mach'}), con(dfPort & ~dfMachIn)) ||... % inconsistent outports
                (mdotmiss>=0 && any(~dfPort & dfMachIn)) ||... % unknown InPort but Outflow required
                (mdotmiss<0 && any(~dfPort & dfMachIn)) ||... % more than one unknown InPort
                sum(~dfPort & dfMachIn)>1 ... % more than one unknown InPort
                )); % incomplete definition

    elseif (strcmp(name,'triJunction_12') || strcmp(name,'triJunction_21')) && isempty(sys.x0) % length: zero
        OutPorts = cellfun(@(x) x.Mach>=0, sys.Connection);
        iOutPort = find(OutPorts,1,'first');
        con = sys.Connection;
        ok = all(acc > abs(...
                + con{1}.rho*con{1}.dir*u(1) - con{1}.rho*con{1}.Mach*con{1}.c*convective.s(1) + con{1}.rho*con{1}.Mach*prc(1)...
                + con{2}.rho*con{2}.dir*u(2) - con{2}.rho*con{2}.Mach*con{2}.c*convective.s(2) + con{2}.rho*con{2}.Mach*prc(2)...
                + con{3}.rho*con{3}.dir*u(3) - con{3}.rho*con{3}.Mach*con{3}.c*convective.s(3) + con{3}.rho*con{3}.Mach*prc(3)...
                )) &&... % unsteady continuity
            all(acc*max([con{1}.c,con{2}.c,con{3}.c].^2) > abs(...
                + con{1}.rho*con{1}.c^2*con{1}.dir*u(1)/(con{1}.kappa-1) + con{1}.rho*con{1}.c^2*con{1}.Mach*prc(1)*con{1}.kappa/(con{1}.kappa-1)...
                + con{2}.rho*con{2}.c^2*con{2}.dir*u(2)/(con{2}.kappa-1) + con{2}.rho*con{2}.c^2*con{2}.Mach*prc(2)*con{2}.kappa/(con{2}.kappa-1)...
                + con{3}.rho*con{3}.c^2*con{3}.dir*u(3)/(con{3}.kappa-1) + con{3}.rho*con{3}.c^2*con{3}.Mach*prc(3)*con{3}.kappa/(con{3}.kappa-1)...
                )) &&... % unsteady energy
            all(cellfun(@(xwave)...
                full(all(acc > abs(...
                    + con{1}.rho*con{1}.c*con{1}.Mach*convective.(xwave)(1)...
                    + con{2}.rho*con{2}.c*con{2}.Mach*convective.(xwave)(2)...
                    + con{3}.rho*con{3}.c*con{3}.Mach*convective.(xwave)(3)...
                ))), sys.waves(~strcmp(sys.waves,'f') & ~strcmp(sys.waves,'g') & ~strcmp(sys.waves,'s') & ~strncmp(sys.waves,'omega',5)))) &&... % unsteady convective transport
            all(acc > abs(con{2}.rho*con{2}.c*prc(2) - con{1}.rho*con{1}.c*prc(1))) &&... % prc = const
            all(acc > abs(con{3}.rho*con{3}.c*prc(3) - con{1}.rho*con{1}.c*prc(1))) &&... % prc = const
            ((~sys.mix &&... % const properties
                    checkBC(sys.Connection{1},auxBC(1)) &&... % 1st BC
                    checkBC(sys.Connection{2},auxBC(2)) &&... % 2nd BC
                    checkBC(sys.Connection{3},auxBC(3))) ||... % 3rd BC
                (sys.mix &&... % mixing
                    acc > abs(con{1}.rho*con{1}.Mach*con{1}.c*con{1}.A + con{2}.rho*con{2}.Mach*con{2}.c*con{2}.A + con{3}.rho*con{3}.Mach*con{3}.c*con{3}.A) &&... % continuity
                    acc*con{1}.c^2 > abs(con{1}.rho*con{1}.Mach*con{1}.c^3/(con{1}.kappa-1) + con{2}.rho*con{2}.Mach*con{2}.c^3/(con{2}.kappa-1) + con{3}.rho*con{3}.Mach*con{3}.c^3/(con{3}.kappa-1)) &&... % energy
                    acc > abs(con{1}.rho*con{1}.Mach*con{1}.c*con{1}.alpha + con{2}.rho*con{2}.Mach*con{2}.c*con{2}.alpha + con{3}.rho*con{3}.Mach*con{3}.c*con{3}.alpha) &&... % concentration
                    all(cellfun(@(x) checkBC(x,sys.Connection{iOutPort},{'Mach'}), sys.Connection(OutPorts))))); % out connections equal (2nd law)

    elseif strcmp(name,'triJunction_12') || strcmp(name,'triJunction_21')
        OutPorts = cellfun(@(x) x.Mach>=0, sys.Connection);
        iOutPort = find(OutPorts,1,'first');
        con = sys.Connection;
        ok = all(acc > abs(...
                + con{1}.rho*con{1}.dir*u(1,(1:end-1)+bt) - con{1}.rho*con{1}.Mach*con{1}.c*convective.s(1,(1:end-1)+bt) + con{1}.rho*con{1}.Mach*prc(1,(1:end-1)+bt)...
                + con{2}.rho*con{2}.dir*u(2,(1:end-1)+bt) - con{2}.rho*con{2}.Mach*con{2}.c*convective.s(2,(1:end-1)+bt) + con{2}.rho*con{2}.Mach*prc(2,(1:end-1)+bt)...
                + con{3}.rho*con{3}.dir*u(3,(1:end-1)+bt) - con{3}.rho*con{3}.Mach*con{3}.c*convective.s(3,(1:end-1)+bt) + con{3}.rho*con{3}.Mach*prc(3,(1:end-1)+bt)...
                + sys.Volume/A(1)*(...
                    + con{iOutPort}.rho/con{iOutPort}.c*ddt(prc(iOutPort,:))...
                    - con{iOutPort}.rho*ddt(convective.s(iOutPort,:))...
                ))) &&... % unsteady continuity
            all(acc*max([con{1}.c,con{2}.c,con{3}.c].^2) > abs(...
                + con{1}.rho*con{1}.c^2*con{1}.dir*u(1,(1:end-1)+bt)/(con{1}.kappa-1) + con{1}.rho*con{1}.c^2*con{1}.Mach*prc(1,(1:end-1)+bt)*con{1}.kappa/(con{1}.kappa-1)...
                + con{2}.rho*con{2}.c^2*con{2}.dir*u(2,(1:end-1)+bt)/(con{2}.kappa-1) + con{2}.rho*con{2}.c^2*con{2}.Mach*prc(2,(1:end-1)+bt)*con{2}.kappa/(con{2}.kappa-1)...
                + con{3}.rho*con{3}.c^2*con{3}.dir*u(3,(1:end-1)+bt)/(con{3}.kappa-1) + con{3}.rho*con{3}.c^2*con{3}.Mach*prc(3,(1:end-1)+bt)*con{3}.kappa/(con{3}.kappa-1)...
                + sys.Volume/A(1)*(...
                    + con{iOutPort}.rho*con{iOutPort}.c*ddt(prc(iOutPort,:))/(con{iOutPort}.kappa-1)...
                ))) &&... % unsteady energy
            all(cellfun(@(xwave)...
                full(all(acc > abs(...
                    + con{1}.rho*con{1}.c*con{1}.Mach*convective.(xwave)(1,(1:end-1)+bt)...
                    + con{2}.rho*con{2}.c*con{2}.Mach*convective.(xwave)(2,(1:end-1)+bt)...
                    + con{3}.rho*con{3}.c*con{3}.Mach*convective.(xwave)(3,(1:end-1)+bt)...
                    + sys.Volume/A(1)*(...
                        + con{iOutPort}.rho*ddt(convective.(xwave)(iOutPort,:))...
                )))), sys.waves(~strcmp(sys.waves,'f') & ~strcmp(sys.waves,'g') & ~strcmp(sys.waves,'s') & ~strncmp(sys.waves,'omega',5)))) &&... % unsteady convective transport
            all(acc > abs(con{2}.rho*con{2}.c*prc(2,(1:end-1)+bt) - con{1}.rho*con{1}.c*prc(1,(1:end-1)+bt))) &&... % prc = const
            all(acc > abs(con{3}.rho*con{3}.c*prc(3,(1:end-1)+bt) - con{1}.rho*con{1}.c*prc(1,(1:end-1)+bt))) &&... % prc = const
            ((~sys.mix &&... % const properties
                    checkBC(sys.Connection{1},auxBC(1)) &&... % 1st BC
                    checkBC(sys.Connection{2},auxBC(2)) &&... % 2nd BC
                    checkBC(sys.Connection{3},auxBC(3))) ||... % 3rd BC
                (sys.mix &&... % mixing
                    acc > abs(con{1}.rho*con{1}.Mach*con{1}.c*con{1}.A + con{2}.rho*con{2}.Mach*con{2}.c*con{2}.A + con{3}.rho*con{3}.Mach*con{3}.c*con{3}.A) &&... % continuity
                    acc*con{1}.c^2 > abs(con{1}.rho*con{1}.Mach*con{1}.c^3/(con{1}.kappa-1) + con{2}.rho*con{2}.Mach*con{2}.c^3/(con{2}.kappa-1) + con{3}.rho*con{3}.Mach*con{3}.c^3/(con{3}.kappa-1)) &&... % energy
                    acc > abs(con{1}.rho*con{1}.Mach*con{1}.c*con{1}.alpha + con{2}.rho*con{2}.Mach*con{2}.c*con{2}.alpha + con{3}.rho*con{3}.Mach*con{3}.c*con{3}.alpha) &&... % concentration
                    all(cellfun(@(x) checkBC(x,sys.Connection{iOutPort},{'Mach'}), sys.Connection(OutPorts))))); % out connections equal (2nd law)

    else
        ok = false;
    end
    
    % subfunctions
    function ok = checkBC(Connection,BC,exceptions) % check, whether Connection agrees with BC
        overwrite = struct('Mach',false,'rho',false,'c',false,'A',false,'kappa',false,'Mmol',false,'alpha',false,'pos',false);
        if nargin>2
            for fieldc = exceptions
                field = char(fieldc);
                overwrite.(field) = true;
            end
        end
        ok =...
            (overwrite.Mach  ||     acc > abs(Connection.Mach  - Connection.dir*BC.Mach(1))) &&...
            (overwrite.rho   ||     acc > abs(Connection.rho   - BC.rho)) &&...
            (overwrite.c     ||     acc > abs(Connection.c     - BC.c)) &&...
            (overwrite.A     ||     acc > abs(Connection.A     - BC.A)) &&...
            (overwrite.kappa ||     acc > abs(Connection.kappa - BC.kappa)) &&...
            (overwrite.Mmol  ||     acc > abs(Connection.Mmol  - BC.Mmol)) &&...
            (overwrite.pos   || all(acc > abs(Connection.pos   - BC.pos))) &&...
            (overwrite.alpha ||     acc > abs(Connection.alpha - BC.alpha));
    end

    function dvar = ddt(var) % time derivative
        dvar = (var(:,2:end)-var(:,1:end-1))/Ts; % only consider output values!
    end

    function df = dfdt(f) % time derivative
        df = (f(2:end,2:end)-f(2:end,1:end-1))/Ts; % only consider output values!
    end

    function dg = dgdt(g) % time derivative
        dg = (g(1:end-1,2:end)-g(1:end-1,1:end-1))/Ts; % only consider output values!
    end

    function df = dfdx(f,wave) % spatial derivative
        switch min(str2double(char(pars.order)),size(f,1)-1) % case inf is not taken into account
            case 1
                df = (...
                    f(2:end,(1:end-1)+bt) - f(1:end-1,(1:end-1)+bt)... % upwind
                    )/sys.dX.(wave);
            case 2
                df = ([...
                    ( f(3,(1:end-1)+bt) - f(1,(1:end-1)+bt) )/2;... % central for first element
                    ( 3*f(3:end,(1:end-1)+bt) - 4*f(2:end-1,(1:end-1)+bt) + f(1:end-2,(1:end-1)+bt) )/2;... % 2nd order upwind
                    ])/sys.dX.(wave);
            case 3
                df = ([...
                    ( -f(4,(1:end-1)+bt) +6*f(3,(1:end-1)+bt) -3*f(2,(1:end-1)+bt) -2*f(1,(1:end-1)+bt) )/6;... % 3rd order downwind for first element
                    ( 2*f(4:end,(1:end-1)+bt) +3*f(3:end-1,(1:end-1)+bt) -6*f(2:end-2,(1:end-1)+bt) +f(1:end-3,(1:end-1)+bt) )/6;... % 3rd order upwind
                    ( 11*f(end,(1:end-1)+bt) -18*f(end-1,(1:end-1)+bt) +9*f(end-2,(1:end-1)+bt) -2*f(end-3,(1:end-1)+bt) )/6;... % 3rd order upwind (very) for last element
                    ])/sys.dX.(wave);
        end
    end

    function dg = dgdx(g,wave) % spatial derivative
        bt = (back == true);
        switch min(str2double(char(pars.order)),size(g,1)-1) % case inf is not taken into account
            case 1
                dg = (...
                    g(2:end,(1:end-1)+bt) - g(1:end-1,(1:end-1)+bt)... % upwind
                    )/sys.dX.(wave);
            case 2
                dg = ([...
                    ( -g(3:end,(1:end-1)+bt) + 4*g(2:end-1,(1:end-1)+bt) - 3*g(1:end-2,(1:end-1)+bt) )/2;... % 2nd order upwind
                    ( g(end,(1:end-1)+bt) - g(end-2,(1:end-1)+bt) )/2;... % central for last element
                    ])/sys.dX.(wave);
            case 3
                dg = ([...
                    ( 2*g(4,(1:end-1)+bt) -9*g(3,(1:end-1)+bt) +18*g(2,(1:end-1)+bt) -11*g(1,(1:end-1)+bt) )/6;... % 3rd order downwind (very) for last element
                    ( -g(4:end,(1:end-1)+bt) +6*g(3:end-1,(1:end-1)+bt) -3*g(2:end-2,(1:end-1)+bt) -2*g(1:end-3,(1:end-1)+bt) )/6;... % 3rd order upwind
                    ( 2*g(end,(1:end-1)+bt) +3*g(end-1,(1:end-1)+bt) -6*g(end-2,(1:end-1)+bt) +g(end-3,(1:end-1)+bt) )/6;... % 3rd order downwind for second last element
                    ])/sys.dX.(wave);
        end
    end
end