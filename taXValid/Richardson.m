% Richardson Extrapolation
function [fextra,invA] = Richardson(f,dx,invA)
    if nargin<3 || isempty(invA)
        N = numel(dx);
        A = repmat(reshape(dx,N,1),1,N).^repmat(0:N-1,N,1);
        invA = inv(A);
    end
    fg = invA*f;
    fextra = fg(1,:);
end
    