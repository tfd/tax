% make a step response of the given element and save all outputs.
function varargout = saveStep(name,BC,pars,Nmax,steady,NTs_Steady)
    %% input
    if nargin < 6 || isempty(NTs_Steady)
        NTs_Steady = 10; % NTs for stateless elements
    end
    if nargin < 5 || isempty(steady)
        steady = 1e-3; % abortion criterion
    end
    if nargin < 4 || isempty(Nmax)
        Nmax = 1e7; % longest allowable timeseries
    end
    if nargin < 3 || isempty(pars) || (isa(pars,'double') && pars == -1)
        if exist('pars','var') && pars == -1 % return parameternames and default pars struct
            auxpars = true;
            clear pars
        else
            auxpars = false;
        end
        % find all (inner) parameters of the block and initialize the element
        nAllParams = get_param(char(name),'MaskNames');
        pars.Name = 'Test';
        pars.Ts = 0;
        pars.fMax = struct('f',2000,'g',2000,'s',200,'alpha',200);
        pars.waves = fieldnames(pars.fMax);
        for i = 1:numel(nAllParams)
            pars.(nAllParams{i}) = get_param(name,nAllParams{i});
        end
        if auxpars % return parameternames and default pars struct
            varargout = {fieldnames(pars),pars};
            return
        end
    end
    pars.waves = fieldnames(pars.fMax); % patch...
    if nargin < 2 || isempty(BC)
        BC = struct('port',[1,2],'Mach',[1e-3,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',29,'alpha',0.06);
    end
    % process name
    pathparts = strsplit(name{1},filesep);
    libName = pathparts{1};
    blockName = pathparts{2};

    %% actual run...
    if (strcmp(blockName,'Flame') || strcmp(blockName,'tempJump')) && BC.port(1)==1 % fulfill continuity with downstream Mach number
        pars.Mach = {num2str(BC.Mach(1)*BC.rho*BC.c/(str2double(char(pars.rho))*str2double(char(pars.c))))};
    end
    varargout{5} = pars;
    if isfield(pars,'element') && strncmp(fliplr(char(pars.element)),fliplr('rankineHugoniot'),14) && ~strcmp(blockName,'symTempJump') % adjust number of ports on block
        set_param(libName,'Lock','off')
        switchNRef(name{1},str2double(char(pars.nRef)));
    end
    if isfield(pars,'loc') && ~strcmp(blockName,'scatter') % adjust upsteam or downstream ports on block
        set_param(libName,'Lock','off')
        switchBCUpDownStream(name{1},char(pars.loc));
    end
    % build element with pars
    try
        sys = feval(char(pars.element),pars);
    catch
        disp([char(name),' does not run.']);
        varargout = cell(1,5);
        return
    end
    % find all ports, find their types, initialize with .dir and .idx
    for i = 1:numel(sys.Connection)
        acPorts(i) = Block.isPort(sys.Connection(i),AcBlock.Port); %#ok<AGROW>
    end
    findAcPorts = find(acPorts);
    varargout{4} = sum(double(acPorts));
    ports = get_param(name,'portconnectivity');
    upPorts = cellfun(@(x) ~isempty(x), {ports{1}.SrcBlock});
    if strncmp(fliplr(char(pars.element)),fliplr('rankineHugoniot'),14) && ~strcmp(blockName,'symTempJump') % adjust number of ports on block
        switchNRef(name{1},1);
        set_param(libName,'Lock','on')
    end
    if isfield(pars,'loc') && ~strcmp(blockName,'scatter') % adjust upsteam or downstream ports on block
        switchBCUpDownStream(name{1},'upstream');
        set_param(libName,'Lock','on')
    end
    for i = 1:numel(acPorts)
        sys.Connection{i}.idx = i;
        sys.Connection{i}.dir = 1;
        sys.Connection{i}.dir(upPorts(i)) = -1;
    end
    % set values for acoustic port (unless set already: set to BC)
    if ~Block.checkPort(sys.Connection(findAcPorts(BC.port(1))),AcBlock.Port)
        sys.Connection{findAcPorts(BC.port(1))}.Mach = BC.Mach(1)*sys.Connection{findAcPorts(BC.port(1))}.dir;
        sys.Connection{findAcPorts(BC.port(1))}.c = BC.c;
        sys.Connection{findAcPorts(BC.port(1))}.rho = BC.rho;
        sys.Connection{findAcPorts(BC.port(1))}.A = BC.A;
        sys.Connection{findAcPorts(BC.port(1))}.kappa = BC.kappa;
        sys.Connection{findAcPorts(BC.port(1))}.Mmol = BC.Mmol;
        sys.Connection{findAcPorts(BC.port(1))}.alpha = BC.alpha;
        sys.Connection{findAcPorts(BC.port(1))}.pos = BC.pos;
    end
    % element specific: Block
    if strcmp(blockName,'Block')
        % copy connection
        sys.Connection = Block.solveMean(sys.Connection);
    end
    % element specific: rankineHugoniot
    if strncmp(fliplr(char(pars.element)),fliplr('ankineHugoniot'),13) % omit the R, in case strncmp ever becomes case-sensitive
        % establish flame ports
        flamePorts = find(~acPorts);
        for i = flamePorts
            sys.Connection{i}.FlameName = ['Flame',num2str(i)];
            if numel(flamePorts)==2 && i==flamePorts(2) % two flames --> second flame is mixRef
                sys.Connection{i}.FlameName = [sys.Connection{i}.FlameName,'_phi'];
                sys.Connection{i}.FTFphi = true;
                sys.Connection{i}.knownETF = false;
            end
            if numel(flamePorts)>2 && i==flamePorts(3) % more than two flames --> third flame is mixRef with ETF
                sys.Connection{i}.FlameName = [sys.Connection{i}.FlameName,'_phi'];
                sys.Connection{i}.FTFphi = true;
                sys.Connection{i}.knownETF = true;
            end
            % always only one phi-flame, which makes sense phyically, and
            % is now required in taX
        end
    end
    % element specific: refG
    if strcmp(blockName,'refG')
        % no negative or zero Mach numbers
        if BC.Mach(1)<=0
            varargout(1:3) = cell(1,3);
            return
        end
    end
    % element specific: triJunction
    if strcmp(char(pars.element),'junction')
        % specify addtl Mach number
        sys.Connection{findAcPorts(BC.port(2))}.Mach = BC.Mach(2)*sys.Connection{findAcPorts(BC.port(2))}.dir;
    end
    % element specific: Duct and simpleDuct
    if isprop(sys,'l')
        % specify addtl position
        sys.Connection{findAcPorts(BC.port(2))}.pos = sys.Connection{findAcPorts(BC.port(1))}.pos + [0,0,1]*sys.l;
    end
    % solveMean --> set other acoustic port(s)
    try
        sys = set_Connection(sys,sys.Connection);
        sys = update(sys);
        varargout{3} = sys;
    catch err
        if strcmp(err.message,'Mean values are inconsistent.') ||...
                strcmp(err.message,'Mean values are inconsistent: Flow direction and lower heating value contradict temperature ratio.') ||...
                strcmp(err.message,'Mean values are incomplete.') ||...
                strcmp(err.message,'The inputs and/or outputs of the model do not match the inputs and outputs of a Block with acoustic ports.')
            varargout(1:3) = cell(1,3);
            varargout{2}{1} = 'BCerror';
            varargout{3} = sys;
        else
            disp([char(name),': set_Connection does not run.']);
            varargout(1:3) = cell(1,3);
        end
        return
    end
    % run step response
    % time step: 100 samples for one oscillation in fMax
    Ts = 1e-2/pars.fMax.f;
    for wavec = pars.waves'
        wave = char(wavec);
        Ts = max(Ts,1e-2/pars.fMax.(wave));
    end
    if isa(sys,'sss') && isempty(sys.x0)
        NTs = NTs_Steady; % no. of time steps
    else
        NTs = 1e3; % no. of time steps, init
    end
    varargout{1} = Ts;
    for i = 1:numel(sys.u)
        if isa(sys,'sss')
            y{i} = lsimmod(sys(sys.y,sys.u(i)),[0;ones(NTs,1)],Ts,'backwardEuler'); %#ok<AGROW>
        else
            y{i} = lsim(sys(sys.y,sys.u(i)),[0;ones(NTs,1)],linspace(0,NTs*Ts,NTs+1))'; %#ok<AGROW>
        end
        % iterate until steady state is reached or too long time series
        while NTs>NTs_Steady &&... % don't run elements without a state
                NTs<Nmax &&... % too long
                ~all(...
                (max(y{i}(:,end-1e2:end),[],2)-min(y{i}(:,end-1e2:end),[],2))./(max(y{i},[],2)-min(y{i},[],2))<steady |... % steady
                (max(y{i},[],2)-min(y{i},[],2))<10*eps... % or all-time zero
                )
            NTs = 2*NTs;
            if isa(sys,'sss')
                y{i} = lsimmod(sys(sys.y,sys.u(i)),[0;ones(NTs,1)],Ts,'backwardEuler');
            else
                y{i} = lsim(sys(sys.y,sys.u(i)),[0;ones(NTs,1)],linspace(0,NTs*Ts,NTs+1))';
            end
        end
    end
    varargout{2} = y;
end