if strcmp(pNames{ii},'order') %#ok<*IJCL>
    parlist.(name).(pNames{ii}) = {'1','2','3'};%,'inf'};
elseif strcmp(pNames{ii},'minres')
    parlist.(name).(pNames{ii}) = {'10','40'};
elseif strcmp(pNames{ii},'l')
    parlist.(name).(pNames{ii}) = {'0','.5'};
elseif strcmp(pNames{ii},'excitation')
    parlist.(name).(pNames{ii}) = {'u','p','phi','inactive'};
elseif strcmp(pNames{ii},'AmpU')
    parlist.(name).(pNames{ii}) = {'0','.5','1','1.5'};
elseif strcmp(pNames{ii},'AmpP')
    parlist.(name).(pNames{ii}) = {'0','.5','1','1.5'};
elseif strcmp(pNames{ii},'AmpPhi')
    parlist.(name).(pNames{ii}) = {'0','.5','1','1.5'};
elseif strcmp(pNames{ii},'dp_fuel')
    parlist.(name).(pNames{ii}) = {'1','5e2','5e6'};
elseif strcmp(pNames{ii},'nRef')
    parlist.(name).(pNames{ii}) = {'1','2','5'};
elseif strcmp(pNames{ii},'Mach')
%     parlist.(name).(pNames{ii}) = {'-.2','0','.2','[]'};
    if strcmp(name,'chokedExit') % exception for chokedExit: only positive (non-zero) Mach numbers
        parlist.(name).(pNames{ii}) = {'.2'};
    else
        parlist.(name).(pNames{ii}) = {'-.2','0','.2'};
    end
elseif strcmp(pNames{ii},'rho')
%     parlist.(name).(pNames{ii}) = {'.1','2','[]'};
    parlist.(name).(pNames{ii}) = {'.1','2'};
elseif strcmp(pNames{ii},'c') || strcmp(pNames{ii},'c_sound')
%     parlist.(name).(pNames{ii}) = {'1e2','1e3','[]'};
    parlist.(name).(pNames{ii}) = {'1e2','1e3'};
elseif strcmp(pNames{ii},'zeta')
    parlist.(name).(pNames{ii}) = {'0','1'};
elseif strcmp(pNames{ii},'lred')
    parlist.(name).(pNames{ii}) = {'0','1'};
elseif strcmp(pNames{ii},'leff')
    parlist.(name).(pNames{ii}) = {'0','1'};
elseif strcmp(pNames{ii},'A') || strcmp(pNames{ii},'Area')
%     parlist.(name).(pNames{ii}) = {'.005','.01','.02','[]'};
    parlist.(name).(pNames{ii}) = {'.005','.01','.02'};
elseif strcmp(pNames{ii},'loc')
    if strcmp(name,'chokedExit') % exception for chokedExit: only Downstream
        parlist.(name).(pNames{ii}) = {'Downstream'};
    else
        parlist.(name).(pNames{ii}) = {'Upstream','Downstream'};
    end
elseif strcmp(pNames{ii},'refType')
%     parlist.(name).(pNames{ii}) = {'u''/umean','p''/(rhomean*cmean^2)','phi''/phimean (emulated)','utan''/utanmean (tangential)','phi''/phimean','s''/cp'};
    parlist.(name).(pNames{ii}) = {'u''/umean','p''/(rhomean*cmean^2)','utan''/utanmean (tangential)','phi''/phimean','s''/cp'}; % phi emulated is obsolete!
elseif strcmp(pNames{ii},'active')
    parlist.(name).(pNames{ii}) = {'on','off'};
elseif strcmp(pNames{ii},'Amp')
    parlist.(name).(pNames{ii}) = {'0','.5','1','1.5'};
elseif strcmp(pNames{ii},'r')
    parlist.(name).(pNames{ii}) = {'-1','0','.5','1','1.5'};
elseif strcmp(pNames{ii},'property')
    parlist.(name).(pNames{ii}) = {'p_u','f_g'};
elseif strcmp(pNames{ii},'Flammentyp')
    parlist.(name).(pNames{ii}) = {'V-Flame','Conical Flame'};%,'M-Flame'
elseif strcmp(pNames{ii},'VModell')
    parlist.(name).(pNames{ii}) = {'convective','uniform'};
elseif strcmp(pNames{ii},'R')
    parlist.(name).(pNames{ii}) = {'.01','.1'};
elseif strcmp(pNames{ii},'H')
    parlist.(name).(pNames{ii}) = {'.07','.2'};
elseif strcmp(pNames{ii},'gamma') % Verhältnis r(V) zu r(K)
    parlist.(name).(pNames{ii}) = {'.2','.8'};
elseif strcmp(pNames{ii},'SchemeSpace')
    parlist.(name).(pNames{ii}) = {'upwind','central'};
elseif strcmp(pNames{ii},'nY')
    parlist.(name).(pNames{ii}) = {'10','20'};
elseif strcmp(pNames{ii},'n')
    parlist.(name).(pNames{ii}) = {'-.5','0','1'};
elseif strcmp(pNames{ii},'tau')
    parlist.(name).(pNames{ii}) = {'0','1e-2'};
elseif strcmp(pNames{ii},'damping_model_popup')
    parlist.(name).(pNames{ii}) = {'no damping','Kirchhoff (wide tubes)','Peters','Dokumanci (best but slow)','Howe (turbulent)'};
elseif strcmp(pNames{ii},'nu')
    parlist.(name).(pNames{ii}) = {'1e-6','1e-3'};
elseif strcmp(pNames{ii},'Pr')
    parlist.(name).(pNames{ii}) = {'.7','2'};
elseif strcmp(pNames{ii},'Volume')
    parlist.(name).(pNames{ii}) = {'0','.003'};
elseif strcmp(pNames{ii},'lchar')
    parlist.(name).(pNames{ii}) = {'0','.3'};
elseif strcmp(pNames{ii},'pos')
    parlist.(name).(pNames{ii}) = {'[0,0,0]'};
elseif strcmp(pNames{ii},'phiConvective')
    parlist.(name).(pNames{ii}) = {'on','off'};
elseif strcmp(pNames{ii},'Version')
    parlist.(name).(pNames{ii}) = {'constant cp, classic Rankine-Hugoniot, f and g only','constant cp, 1st order in Mach, fixed heat source','constant cp, 1st order in Mach, moving flame','realistic fluid, 1st order in Mach, moving flame','realistic fluid, 3rd order in Mach, moving flame','realistic fluid, full order in Mach, moving flame','realistic fluid, full order in Mach, fixed heat source'};
elseif strcmp(pNames{ii},'Tratio')
    parlist.(name).(pNames{ii}) = {'5','.2'};
elseif strcmp(pNames{ii},'fuel')
    parlist.(name).(pNames{ii}) = {'CH4','other'}; % don't check H2, because that would require different BC (messy)
end



