% QACrit: quality assurance criterions
\section{Quality Assurance Criteria}
\subsection{Why?}
The goal of this document is to establish fixed quality assurance criteria for taX. Alongside a code will be developed that is capable of testing single elements with respect to these criteria (see \ref{Test}). With a clear expectation and standard testing procedure, new releases, bugfixes and patches will bring in less new bugs and quality can be kept constant.

\subsection{Criteria}
A good criterion should be easy to check (analytically!). The following criteria should be met by any change or new element of code in taX:
\begin{itemize}
\item numerical solution of the wave equation (see \ref{Theory})
\item negative mean velocity must yield correct results
\item correct functionality with respect to element parameters
\item agreement with all sign conventions (see \ref{Sign})
\item ...
\end{itemize}

\subsection{Theory}
\label{Theory}
Any element has as many unknown variables as it has acoustical ports. Observer-type elements may have additional outputs that are not directly visible in mass conservation and Bernoulli's equation, but those are treated later.

Let us first introduce the convention, that mass, energy or momentum added to the system is of negative sign (indicating same direction as the outward pointing normal vector on the surface of the control volume). This already stands in agreement with the sign convention in taX for the Mach numbers.

The continuity equation in this form is:

\begin{equation}
0 = V\pd{\rho}{t} + \sum_j^N \rho_j u_j A_j
\label{conti}
\end{equation}

With the perturbation approach ($\phi = \bar{\phi} + \phi'$) for $\rho$ and $u$ we find:

\begin{equation}
0 = V\pd{\rho'}{t} + \sum_j^N A_j (\bar{\rho} u'_j + \rho'_1 \bar{u}_j)
\label{conti2}
\end{equation}

Here $\pd{\bar{\rho}}{t} = 0$, $\bar{\rho} = \bar{\rho}_j$ and linearization $\phi' \psi' \approx 0$ are applied and the steady state continuity equation $\sum_j^N \bar{\rho} \bar{u}_j A_j = 0$ is subtracted. By applying Laplace Transform and $\rho' = \frac{p'}{c^2}$ (with the sonic velocity $c$) we reach:

\begin{equation}
0 = \frac{V s p'}{\bar{\rho}c^2} + \sum_j^N A_j \bigg(u'_j + p'_j \frac{\bar{u}_j}{\bar{\rho}c^2}\bigg)
\label{conti3}
\end{equation}

with the Laplace variable $s$. With $Ma = \frac{\bar{u}}{c}$ and the assumption of $p' = \frac{\sum_j^N p'_j}{N}$ we can rewrite the equation to:

\begin{equation}
0 = s \frac{V}{N c} \sum_j^N\frac{p'_j}{\bar{\rho}c} + \sum_j^N A_j \bigg(u'_j + Ma_j \frac{p'_j}{\bar{\rho}c}\bigg)
\label{conti4}
\end{equation} 

A similar strategy is applied to Bernoulli's equation.

\begin{equation}
0 = \pd{}{x_i}\bigg(\pd{\varphi}{t}+\frac{u^2}{2}+\frac{\kappa}{\kappa-1}\frac{p}{\rho}\bigg)
\label{Bern}
\end{equation}

Here, $\varphi$ is the unsteady velocity ''potential'' and $\kappa$ is the ratio of specific heat capacities of the medium (ideal gas behavior is implied). It is integrated along a streamline, from one port to another. You can find $N-1$ linearly independent equations by this method. Therefore, we integrate from port $1$ to all the other ports $2...j...N$. The equation from port $k$ to port $l$ is then the the difference between the ones from $1$ to $l$ and from $1$ to $k$.

\begin{align}
0 &= \int_1^j\pd{}{x_i}\bigg(\pd{\varphi}{t}+\frac{u^2}{2}+\frac{\kappa}{\kappa-1}\frac{p}{\rho}\bigg) dx_i\nonumber\\
&= \pd{\varphi_j-\varphi_1}{t}+\frac{u_j^2-u_1^2}{2}+\frac{\kappa}{\kappa-1}\bigg(\frac{p_j}{\rho_j}-\frac{p_1}{\rho_1}\bigg)
\label{Bern2}
\end{align}

The velocity potential is defined as:

\begin{equation}
\nabla\varphi = \vec{u}
\label{potential}
\end{equation}

We can express the derivative approximately by a characteristic length $l_{char}$. It is implied that the velocities are normal to the corresponding port.

\begin{equation}
u_j = \frac{\varphi_j - \varphi_C}{l_{char}}
\label{potential2}
\end{equation}

By solving a linear equation system it is possible to express the differences in the velocity potential by the velocities:

\begin{equation}
\varphi_j - \varphi_1 = l_{char} (u_j - u_1)
\label{potentialDiff}
\end{equation}

Insertion results into:

\begin{equation}
0 = l_{char}\pd{u_j - u_1}{t}+\frac{u_j^2-u_1^2}{2}+\frac{\kappa}{\kappa-1}\bigg(\frac{p_j}{\rho_j}-\frac{p_1}{\rho_1}\bigg)
\label{Bern3}
\end{equation}

Again, we apply perturbations for $u$, $p$ and $\rho$, linearize $\phi' \psi' \approx 0$ and subtract the steady state solution:

\begin{equation}
0 = l_{char}\pd{u'_j - u'_1}{t} + \bar{u}_j u'_j - \bar{u}_1 u'_1
+\frac{\kappa}{\kappa-1}\bigg(
\frac{\bar{p}_j+p'_j}{\bar{\rho}_j+\rho'_j}
-\frac{\bar{p}_j}{\bar{\rho}_j}
-\frac{\bar{p}_1+p'_1}{\bar{\rho}_1+\rho'_1}
+\frac{\bar{p}_1}{\bar{\rho}_1}
\bigg)
\label{Bern4}
\end{equation}

With a Taylor expansion

\begin{equation}
\frac{1}{\bar{\rho}+\rho'} \approx \frac{1}{\bar{\rho}} - \frac{1}{\bar{\rho}^2}\rho'
\label{Taylor}
\end{equation}

we can complete the linearization:

\begin{equation}
0 = l_{char}\pd{u'_j - u'_1}{t} + \bar{u}_j u'_j - \bar{u}_1 u'_1
+\frac{\kappa}{\kappa-1}\bigg(
\frac{p'_j}{\bar{\rho}_j}
-\frac{\bar{p}_j}{\bar{\rho}_j^2}\rho'_j
-\frac{p'_1}{\bar{\rho}_1}
+\frac{\bar{p}_1}{\bar{\rho}_1^2}\rho'_1
\bigg)
\label{Bern5}
\end{equation}

Insert now $\rho'c^2 = p'$, $\bar{p} = \bar{p}_1 = \bar{p}_j$, $\bar{\rho} = \bar{\rho}_1 = \bar{\rho}_j$ and the definition of sonic velocity in ideal gas $c^2 = \kappa R T = \kappa \frac{p}{\rho}$, :

\begin{align}
0 &= l_{char}\pd{u'_j - u'_1}{t} + \bar{u}_j u'_j - \bar{u}_1 u'_1
+p'_j\frac{\kappa}{\kappa-1}\bigg(
\frac{1}{\bar{\rho}_j}
-\frac{\bar{p}_j}{\bar{\rho}_j^2 c^2}\bigg)
-p'_1\frac{\kappa}{\kappa-1}\bigg(
\frac{1}{\bar{\rho}_1}
-\frac{\bar{p}_1}{\bar{\rho}_1^2 c^2}\bigg)\nonumber\\
&= l_{char}\pd{u'_j - u'_1}{t} + \bar{u}_j u'_j - \bar{u}_1 u'_1
+\frac{p'_j}{\bar{\rho}}\frac{\kappa}{\kappa-1}\bigg(
1-\frac{\bar{p}}{\bar{\rho} c^2}\bigg)
-\frac{p'_1}{\bar{\rho}}\frac{\kappa}{\kappa-1}\bigg(
1-\frac{\bar{p}}{\bar{\rho} c^2}\bigg)\nonumber\\
&= l_{char}\pd{u'_j - u'_1}{t} + \bar{u}_j u'_j - \bar{u}_1 u'_1
+\frac{p'_j}{\bar{\rho}}\frac{\kappa}{\kappa-1}\bigg(
1-\frac{1}{\kappa}\bigg)
-\frac{p'_1}{\bar{\rho}}\frac{\kappa}{\kappa-1}\bigg(
1-\frac{1}{\kappa}\bigg)\nonumber\\
&= l_{char}\pd{u'_j - u'_1}{t} + \bar{u}_j u'_j - \bar{u}_1 u'_1
+\frac{p'_j-p'_1}{\bar{\rho}}
\label{Bern6}
\end{align}

As with the continuity equation the following steps are Laplace Transform and inserting $Ma = \frac{\bar{u}}{c}$:

\begin{equation}
0 = \frac{l_{char} s(u_j - u'_1)}{c} + Ma_j u'_j - Ma_1 u'_1 + \frac{p'_j-p'_1}{\bar{\rho c}}
\label{Bern7}
\end{equation}

\subsection{Sign Conventions}
\label{Sign}

\subsection{Documentation of the Testing Code}
\label{Test}