% runSaveStep
%% init
clear
savePath = '/home/felix/Data/tax_BigFiles/taXValid/';
save('savePath.mat','savePath') % outsource big files (such that they are exempt from sync)
% libName = 'taXlibrary_Matlab2017b';
libName = 'taXlibrary';
while ~isempty(gcs)
    if strcmp(gcs,libName)
        close_system(gcs,0)
    else
        close_system
    end
end
currentPath = pwd;
% cd('/home/felix/Dokumente/tax');taXinit;
cd('../');taXinit;
% taXlibrary_Matlab2017b;
taXlibrary;
allBlocks = find_system;
cd(currentPath);
% make a complete list of parameters
for i = 2:numel(allBlocks)
    pathparts = strsplit(allBlocks{i},filesep);
    name = pathparts{2};
    [pNames,pars] = saveStep(allBlocks(i),[],-1);
    for ii = 1:numel(pNames)
        parlist.(name).(pNames{ii}) = pars.(pNames{ii});
        ControlParVar;
    end
end
clear pars
%% run step responses
% make BC permutations
auxBC = [...
    struct('port',[1,2],'Mach',[.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),... % Mmol for consistency with alpha and methane combustion
    struct('port',[1,2],'Mach',[0,.1],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[1,2],'Mach',[-.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[2,1],'Mach',[-.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[1,2],'Mach',[0,-.1],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[1,3],'Mach',[.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[3,1],'Mach',[.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[2,3],'Mach',[.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    struct('port',[3,2],'Mach',[.1,0],'c',340,'rho',1.2,'A',.01,'kappa',1.4,'Mmol',27.944536962215356,'alpha',0.04,'pos',[0,0,0]),...
    ];
for i = 2:numel(allBlocks)
% for i = [22,23]
    pathparts = strsplit(allBlocks{i},filesep);
    name = pathparts{2};
    allPars = parlist.(name);
    % make pars permutations
    fNames = fieldnames(allPars);
    for ii = 1:numel(fNames)
        if strcmp(fNames{ii},'waves')
            pars.waves = allPars.waves;
        elseif isa(allPars.(fNames{ii}),'cell')
            pars.(fNames{ii}) = allPars.(fNames{ii})(1);
        else
            pars.(fNames{ii}) = allPars.(fNames{ii});
        end
    end
    for ii = 5:numel(fNames)
        m = numel(pars);
        n = numel(allPars.(fNames{ii}));
        pars = repmat(pars,1,n);
        for iii = 1:n*m
            pars(iii).(fNames{ii}) = allPars.(fNames{ii})(ceil(iii/m));
        end
    end
    % run
    for ii = 1:numel(auxBC)
        for iii = 1:numel(pars)
            if strcmp(name,'symFlame') || strcmp(name,'symTempJump') || strcmp(name,'BurnerFlame')
                % make multiple runs to assess error vs Mach order
                MaFactor = 1./[1,2,4,8,16,32,64,128,256,1024];
                for iv = 1:numel(MaFactor)
                    dummyBC = auxBC;
                    dummyBC(ii).Mach = auxBC(ii).Mach*MaFactor(iv);
                    [Ts{ii,iii,iv},y{ii,iii,iv},sys{ii,iii,iv},nAcousticPorts,newpars{ii,iii,iv}] = saveStep(allBlocks(i),dummyBC(ii),pars(iii),2e5); %#ok<SAGROW>
                    if isfield(pars(iii),'Version') && strncmp(pars(iii).Version,'realistic fluid, full order in Mach',35)
                        break % only full Mach for exact cases
                    end
                end
            else
                % regular single run
                [Ts{ii,iii},y{ii,iii},sys{ii,iii},nAcousticPorts,newpars{ii,iii}] = saveStep(allBlocks(i),auxBC(ii),pars(iii),2e5); 
            end
            if isempty(nAcousticPorts)
                break;
            end
        end
        if isempty(nAcousticPorts) || (nAcousticPorts==1 && ii==1) || (nAcousticPorts==2 && ii==4) || (ismember(allBlocks(i),{[libName,'/Flame'],[libName,'/areaChange'],[libName,'/tempJump']}) && ii==1)
            break;
        end
    end
    % save and clear RAM
    BC = auxBC(1:ii); %#ok<NASGU>
    if exist('MaFactor','var')
        save([savePath,'SR',name,'.mat'],'Ts','y','pars','newpars','BC','sys','MaFactor');
    else
        save([savePath,'SR',name,'.mat'],'Ts','y','pars','newpars','BC','sys');
    end
    clear Ts y BC pars sys
end
% close library
while ~isempty(gcs)
    if strcmp(gcs,libName)
        close_system(gcs,0)
    else
        close_system
    end
end
